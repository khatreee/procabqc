﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{
	[Serializable]
	public class MeasuredPartInstance
	{

		private double MeasuredWidth = 0.0;
		private double MeasuredHeight = 0.0;
		private double MeasuredThickness = 0.0;
		private double MeasuredAngle = 90.0;
		private double MeasuredWidthError = 0.0;
		private double MeasuredHeightError = 0.0;
		private double MeasuredThicknessError = 0.0;
		private double MeasuredAngleError = 0.0;
		private bool WidthSpecificationStatus = false;
		private bool HeightSpecificationStatus = false;
		private bool ThicknessSpecificationStatus = false;
		private bool AngleSpecificationStatus = true;
		private string TimeDateStamp = "";
		private int InstanceNumber = 0;
		private bool ManualMeasurement = false;
		private bool MeasuredRotated = false;



		public double Width
		{

			get { return MeasuredWidth; }

		}

		public double Height
		{

			get { return MeasuredHeight; }

		}

		public double Thickness
		{

			get { return MeasuredThickness; }

		}

		public double Angle
		{
			get { return MeasuredAngle; }

		}


		// V1.2  Record errors (Nominal - actual)
		public double WidthError
		{
			get { return MeasuredWidthError; }
			set { MeasuredWidthError = value; }
		}

		public double HeightError
		{
			get { return MeasuredHeightError; }
			set { MeasuredHeightError = value; }
		}

		public double ThicknessError
		{
			get { return MeasuredThicknessError; }
			set { MeasuredThicknessError = value; }
		}

		public double AngleError
		{
			get { return MeasuredAngleError; }
			set { MeasuredAngleError = value; }
		}


		// V1.2 Record if part rotated.
		public bool PartWasRotated
		{
			get { return MeasuredRotated; }
			set { MeasuredRotated = value; }
		}



		public bool WidthInSpec
		{

			get { return WidthSpecificationStatus; }
			set { WidthSpecificationStatus = value; }
   
		}

		public bool HeightInSpec
		{

			get { return HeightSpecificationStatus; }
			set { HeightSpecificationStatus = value; }

		}

		public bool ThicknessInSpec
		{

			get { return ThicknessSpecificationStatus; }
			set { ThicknessSpecificationStatus = value; }
			
		}

		public bool AngleInSpec
		{
			get { return AngleSpecificationStatus; }
			set { AngleSpecificationStatus = value; }
		}


		public string MeasuredTime
		{

			get { return TimeDateStamp; }

		}

		public int PartInstanceNumber
		{
			get { return InstanceNumber; }
			set { InstanceNumber = value; }
		}


		public bool IsManualMeasurement
		{
			get { return ManualMeasurement; }
		}


		// Default constructor
		public MeasuredPartInstance()
		{


		}

		// The overloaded constructor
		public MeasuredPartInstance(double MWidth, double MHeight, double MThickness, double MAngle, MeasuringUnits CSVFileUnits, bool UseNorthAmericanTimeFormat, bool ManuallyMeasured)
		{

			// Passed MWidth and MHeight are ALWAYS in native hardware units (millimeters)
			// A conversion is required if CSVFileUnits are inches.

			// Save width if positive
			if (MWidth > 0.0)
			{
				// File data in mm
				if (CSVFileUnits == MeasuringUnits.Millimeters)
					MeasuredWidth = MWidth;

				// Convert to inches
				else
					MeasuredWidth = (MWidth / 25.4);
			}

			// Save height if positive
			if (MHeight > 0.0)
			{
				// file data in mm
				if (CSVFileUnits == MeasuringUnits.Millimeters)
					MeasuredHeight = MHeight;

				// Convert to inches
				else
					MeasuredHeight = (MHeight / 25.4);
			}

			// Save thickness (depth) if positive
			if (MThickness > 0.0)
			{
				// file data in mm
				if (CSVFileUnits == MeasuringUnits.Millimeters)
					MeasuredThickness = MThickness;

				// Convert to inches
				else
					MeasuredThickness = (MThickness / 25.4);
			}


			// Save angle
			MeasuredAngle = MAngle;
  
			// Was the measurement manually created
			ManualMeasurement = ManuallyMeasured;

	
			// Get current time
			DateTime Time = DateTime.Now;
			String DateStr;
			string MonthStr;
			string DayStr;
			string HourStr;
			string MinStr;
			string SecStr;

			
			if (Time.Month < 10)
				MonthStr = string.Format("0{0}", Time.Month);

			else
				MonthStr = Time.Month.ToString();


			if (Time.Day < 10)
				DayStr= string.Format("0{0}", Time.Day);

			else
				DayStr = Time.Day.ToString();



			if (UseNorthAmericanTimeFormat)
			{
				// Build Date string in north american format   (month-day-year)
				DateStr = string.Format("{0}-{1}-{2}", MonthStr, DayStr, Time.Year.ToString());

			}

			else
			{
				// Build string in European format (day-month-year)
				DateStr = string.Format("{0}-{1}-{2}", DayStr, MonthStr, Time.Year.ToString());
			}


			if (Time.Hour < 10)
				HourStr = string.Format("0{0}", Time.Hour);

			else
				HourStr = Time.Hour.ToString();

			if (Time.Minute < 10)
				MinStr = string.Format("0{0}", Time.Minute);

			else
				MinStr = Time.Minute.ToString();


			if (Time.Second < 10)
				SecStr = string.Format("0{0}", Time.Second);

			else
				SecStr = Time.Second.ToString();


			// Build Time string without delimiters
			String TimeStr = string.Format("{0}:{1}:{2}", HourStr, MinStr, SecStr);

			// Set the time stamp string
			TimeDateStamp = string.Format("{0}  {1}", DateStr, TimeStr);
			

		}

		// Swaps height and width values if part was rotated.
		public void SwapHeightAndWidth()
		{

			double Temp = MeasuredWidth;
			MeasuredWidth = MeasuredHeight;
			MeasuredHeight = Temp;

		}
	









	}
}
