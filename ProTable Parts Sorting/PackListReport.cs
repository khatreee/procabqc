using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ProCABQC11
{
	public partial class PackListReport : DevExpress.XtraReports.UI.XtraReport
	{


		private const int VerticalLineSpacing = 17;
		private const int FirstPageStartYPos = 275;
		private const int EndOfPage = 757;
		private int PageBreakRef = EndOfPage;


		public PackListReport()
		{
			InitializeComponent();

		}


		public void FormatOrder(Order CurrentOrder, PersistentParametersV1100 PParams)
		{

			// Populate the company data
			CompanyName.Text = PParams.CompanyNameStr;
			CompanyAddress1.Text = PParams.CompanyAddr1Str;
			CompanyAddress2.Text = PParams.CompanyAddr2Str;
			CompanyCity.Text = PParams.CompanyCityStr;
			CompanyState.Text = PParams.CompanyStateStr;
			CompanyZip.Text = PParams.CompanyZipStr;
			CompanyTelephone.Text = PParams.CompanyPhoneStr;

			// Load the logo if defined
			if (PParams.CompanyLogoFileStr != "")
			{
				try
				{

					Image LogoImage = new Bitmap(PParams.CompanyLogoFileStr);
					CompanyLogo.Image = LogoImage;
				}

				// Invalid image path/filename
				catch
				{

				}
			}


			// Populate the Bill To Customer Data
			CustomerInfo Customer;

			// Get the bill customer from the order
			CurrentOrder.GetCustomerInfoData(out Customer, CustomersTypes.Billing);

			// Populate the fields
			BillToCustomerName.Text = Customer.CustomerName;
			BillToAddress1.Text = Customer.Address1;
			BillToAddress2.Text = Customer.Address2;
			BillToCity.Text = Customer.City;
			BillToState.Text = Customer.State;
			BillToZip.Text = Customer.Zip;
			BillToCountry.Text = Customer.Country;
			BillToContact.Text = Customer.ContactName;


			// Get the Ship customer from the order
			CurrentOrder.GetCustomerInfoData(out Customer, CustomersTypes.Shipping);

			// Populate the fields
			ShipToCustomerName.Text = Customer.CustomerName;
			ShipToAddress1.Text = Customer.Address1;
			ShipToAddress2.Text = Customer.Address2;
			ShipToCity.Text = Customer.City;
			ShipToState.Text = Customer.State;
			ShipToZip.Text = Customer.Zip;
			ShipToCountry.Text = Customer.Country;
			ShipToContact.Text = Customer.ContactName;

			// Show the order ID
			OrderID.Text = CurrentOrder.OrderNumber;

			// Print the line item header
			int YPos = PrintLineItemHeader(new Point(0, FirstPageStartYPos));

			// For each line item in the order
			for (int u = 0; u < CurrentOrder.NumberOfLineItems(); u++)
			{
				LineItem LI;


				// Get the line item
				if (CurrentOrder.GetLineItem(out LI, u))
				{
					// Print the line item and its measurements
					YPos = FormatLineItem(LI, PParams, CurrentOrder, new Point(0, YPos));

				}
			}
 
		}

		private int FormatLineItem(LineItem LI, PersistentParametersV1100 PParams, Order CurrentOrder, Point Pt)
		{

			// Print Line item data here
			XRLabel LineItemNumber = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel PartID = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomWidth = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomHeight = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel Quantity = new DevExpress.XtraReports.UI.XRLabel();

			// Check if page break is required
			int YPos = CheckForPageBreak(Pt.Y);


			// 
			// LineItemNumber
			// 
			LineItemNumber.Font = new System.Drawing.Font("Arial", 10F);
			LineItemNumber.Location = new System.Drawing.Point(15, YPos);
			LineItemNumber.Name = "LineItemNumber";
			LineItemNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			LineItemNumber.Size = new System.Drawing.Size(58, 25);
			LineItemNumber.StylePriority.UseFont = false;
			LineItemNumber.Text = LI.LineNumber.ToString();
			// 
			// PartID
			// 
			PartID.Font = new System.Drawing.Font("Arial", 10F);
			PartID.Location = new System.Drawing.Point(67, YPos);
			PartID.Name = "PartID";
			PartID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			PartID.Size = new System.Drawing.Size(242, 25);
			PartID.StylePriority.UseFont = false;
			PartID.Text = LI.PartIdStr;
			// 
			// NomWidth
			// 
			NomWidth.Font = new System.Drawing.Font("Arial", 10F);
			NomWidth.Location = new System.Drawing.Point(342, YPos);
			NomWidth.Name = "NomWidth";
			NomWidth.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomWidth.Size = new System.Drawing.Size(58, 25);
			NomWidth.StylePriority.UseFont = false;
			NomWidth.Text = PositionConversion.ConvertUnits(LI.Width, CurrentOrder.CSVMeasurementUnits, CurrentOrder.CSVMeasurementUnits, PParams.MMPrecisionVal, PParams.InchPrecisionVal);
			// 
			// NomHeight
			// 
			NomHeight.Font = new System.Drawing.Font("Arial", 10F);
			NomHeight.Location = new System.Drawing.Point(450, YPos);
			NomHeight.Name = "NomHeight";
			NomHeight.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomHeight.Size = new System.Drawing.Size(58, 25);
			NomHeight.StylePriority.UseFont = false;
			NomHeight.Text = PositionConversion.ConvertUnits(LI.Height, CurrentOrder.CSVMeasurementUnits, CurrentOrder.CSVMeasurementUnits, PParams.MMPrecisionVal, PParams.InchPrecisionVal); ;
			// 
			// Quantity
			// 
			Quantity.Font = new System.Drawing.Font("Arial", 10F);
			Quantity.Location = new System.Drawing.Point(585, YPos);
			Quantity.Name = "Quantity";
			Quantity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			Quantity.Size = new System.Drawing.Size(58, 25);
			Quantity.StylePriority.UseFont = false;
			Quantity.Text = LI.QtyRequired.ToString();

			// Add it to the detail
			this.Detail.Controls.Add(LineItemNumber);
			this.Detail.Controls.Add(PartID);
			this.Detail.Controls.Add(NomWidth);
			this.Detail.Controls.Add(NomHeight);
			this.Detail.Controls.Add(Quantity);

			// Move down a line
			YPos += VerticalLineSpacing;

			// Print Part instance header data once per line item
			YPos = PrintPartHeader(new Point(0, YPos));


			// Loop through each measured part
			for(int u = 0; u < LI.GetMeasuredPartsCount(); u++)
			{

				MeasuredPartInstance Part;

				// Get the measured part
				if(LI.GetMeasuredPart(out Part, u))
				{
					YPos = FormatPartMeasurement(Part, PParams, CurrentOrder, new Point(0, YPos));
				}
			}

			// Add a blank line
			YPos += VerticalLineSpacing;

			return YPos;


		}

		private int FormatPartMeasurement(MeasuredPartInstance Part, PersistentParametersV1100 PParams, Order CurrentOrder, Point Pt)
		{


			// Print Line item data here
			XRLabel PartInstance = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel MeasuredWidth = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel WidthSpec = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel MeasuredHeight = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel HeightSpec = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel MeasureType = new XRLabel();


			// Check if page break is required
			int YPos = CheckForPageBreak(Pt.Y);


			// 
			// PartInstance
			// 
			PartInstance.Font = new System.Drawing.Font("Arial", 10F);
			PartInstance.Location = new System.Drawing.Point(45, YPos);
			PartInstance.Name = "PartInstance";
			PartInstance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			PartInstance.Size = new System.Drawing.Size(58, 25);
			PartInstance.StylePriority.UseFont = false;
			PartInstance.Text = Part.PartInstanceNumber.ToString();
			// 
			// MeasuredWidth
			// 
			MeasuredWidth.Font = new System.Drawing.Font("Arial", 10F);
			MeasuredWidth.Location = new System.Drawing.Point(125, YPos);
			MeasuredWidth.Name = "MeasuredWidth";
			MeasuredWidth.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			MeasuredWidth.Size = new System.Drawing.Size(58, 25);
			MeasuredWidth.StylePriority.UseFont = false;
			MeasuredWidth.Text = PositionConversion.ConvertUnits(Part.Width, CurrentOrder.CSVMeasurementUnits, CurrentOrder.CSVMeasurementUnits, PParams.MMPrecisionVal, PParams.InchPrecisionVal);
			// 
			// WidthSpec
			// 
			WidthSpec.Font = new System.Drawing.Font("Arial", 10F);
			WidthSpec.Location = new System.Drawing.Point(220, YPos);
			WidthSpec.Name = "WidthSpec";
			WidthSpec.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			WidthSpec.Size = new System.Drawing.Size(58, 25);
			WidthSpec.StylePriority.UseFont = false;
			WidthSpec.Text = Part.WidthInSpec.ToString();
			// 
			// MeasuredHeight
			// 
			MeasuredHeight.Font = new System.Drawing.Font("Arial", 10F);
			MeasuredHeight.Location = new System.Drawing.Point(325, YPos);
			MeasuredHeight.Name = "MeasuredHeight";
			MeasuredHeight.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			MeasuredHeight.Size = new System.Drawing.Size(58, 25);
			MeasuredHeight.StylePriority.UseFont = false;
			MeasuredHeight.Text = PositionConversion.ConvertUnits(Part.Height, CurrentOrder.CSVMeasurementUnits, CurrentOrder.CSVMeasurementUnits, PParams.MMPrecisionVal, PParams.InchPrecisionVal); ;
			// 
			// HeightSpec
			// 
			HeightSpec.Font = new System.Drawing.Font("Arial", 10F);
			HeightSpec.Location = new System.Drawing.Point(430, YPos);
			HeightSpec.Name = "HeightSpec";
			HeightSpec.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			HeightSpec.Size = new System.Drawing.Size(58, 25);
			HeightSpec.StylePriority.UseFont = false;
			HeightSpec.Text = Part.HeightInSpec.ToString();

			// 
			// Measure Type
			// 
			MeasureType.Font = new System.Drawing.Font("Arial", 10F);
			MeasureType.Location = new System.Drawing.Point(535, YPos);
			MeasureType.Name = "MeasureType";
			MeasureType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			MeasureType.Size = new System.Drawing.Size(58, 25);
			MeasureType.StylePriority.UseFont = false;
			if (Part.IsManualMeasurement)
				MeasureType.Text = "Manual";

			else
				MeasureType.Text = "System";


			// Add it to the detail
			this.Detail.Controls.Add(PartInstance);
			this.Detail.Controls.Add(MeasuredWidth);
			this.Detail.Controls.Add(WidthSpec);
			this.Detail.Controls.Add(MeasuredHeight);
			this.Detail.Controls.Add(HeightSpec);
			this.Detail.Controls.Add(MeasureType);


			return YPos + VerticalLineSpacing;

		}

		private int PrintLineItemHeader(Point Pt)
		{


			XRLabel LineIDHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel PartIDHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomWidthHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomHeightHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel QuantityHeader = new DevExpress.XtraReports.UI.XRLabel();

			// Check if page break is required
			int YPos = CheckForPageBreak(Pt.Y);


			// 
			// LineIDHeader
			// 
			LineIDHeader.Font = new System.Drawing.Font("Arial", 10F);
			LineIDHeader.Location = new System.Drawing.Point(0, YPos);
			LineIDHeader.Name = "LineIDHeader";
			LineIDHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			LineIDHeader.Size = new System.Drawing.Size(58, 25);
			LineIDHeader.StylePriority.UseFont = false;
			LineIDHeader.Text = "Line #";
			// 
			// PartIDHeader
			// 
			PartIDHeader.Font = new System.Drawing.Font("Arial", 10F);
			PartIDHeader.Location = new System.Drawing.Point(67, YPos);
			PartIDHeader.Name = "PartIDHeader";
			PartIDHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			PartIDHeader.Size = new System.Drawing.Size(67, 25);
			PartIDHeader.StylePriority.UseFont = false;
			PartIDHeader.Text = "Part ID";
			// 
			// NomWidthHeader
			// 
			NomWidthHeader.Font = new System.Drawing.Font("Arial", 10F);
			NomWidthHeader.Location = new System.Drawing.Point(325, YPos);
			NomWidthHeader.Name = "NomWidthHeader";
			NomWidthHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomWidthHeader.Size = new System.Drawing.Size(92, 25);
			NomWidthHeader.StylePriority.UseFont = false;
			NomWidthHeader.Text = "Nom. Width";
			// 
			// NomHeightHeader
			// 
			NomHeightHeader.Font = new System.Drawing.Font("Arial", 10F);
			NomHeightHeader.Location = new System.Drawing.Point(433, YPos);
			NomHeightHeader.Name = "NomHeightHeader";
			NomHeightHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomHeightHeader.Size = new System.Drawing.Size(108, 25);
			NomHeightHeader.StylePriority.UseFont = false;
			NomHeightHeader.Text = "Nom. Height";
			// 
			// QuantityHeader
			// 
			QuantityHeader.Font = new System.Drawing.Font("Arial", 10F);
			QuantityHeader.Location = new System.Drawing.Point(558, YPos);
			QuantityHeader.Name = "QuantityHeader";
			QuantityHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			QuantityHeader.Size = new System.Drawing.Size(75, 25);
			QuantityHeader.StylePriority.UseFont = false;
			QuantityHeader.Text = "Quantity";

			// Add it to the detail
			this.Detail.Controls.Add(LineIDHeader);
			this.Detail.Controls.Add(PartIDHeader);
			this.Detail.Controls.Add(NomWidthHeader);
			this.Detail.Controls.Add(NomHeightHeader);
			this.Detail.Controls.Add(QuantityHeader);


			return YPos + VerticalLineSpacing;



		}

		private int PrintPartHeader(Point Pt)
		{

			XRLabel PartHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel WidthHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel WidthSpecHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel HeightHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel HeightSpecHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel MeasureTypeHeader = new XRLabel();

			// Check if page break is required
			int YPos = CheckForPageBreak(Pt.Y);


			// 
			// PartHeader
			// 
			PartHeader.Font = new System.Drawing.Font("Arial", 10F);
			PartHeader.Location = new System.Drawing.Point(33, YPos);
			PartHeader.Name = "PartHeader";
			PartHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			PartHeader.Size = new System.Drawing.Size(58, 25);
			PartHeader.StylePriority.UseFont = false;
			PartHeader.Text = "Part #";
			// 
			// WidthHeader
			// 
			WidthHeader.Font = new System.Drawing.Font("Arial", 10F);
			WidthHeader.Location = new System.Drawing.Point(90, YPos);
			WidthHeader.Name = "WidthHeader";
			WidthHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			WidthHeader.Size = new System.Drawing.Size(133, 25);
			WidthHeader.StylePriority.UseFont = false;
			WidthHeader.Text = "Measured Width";
			// 
			// WidthSpecHeader
			// 
			WidthSpecHeader.Font = new System.Drawing.Font("Arial", 10F);
			WidthSpecHeader.Location = new System.Drawing.Point(210, YPos);
			WidthSpecHeader.Name = "WidthSpecHeader";
			WidthSpecHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			WidthSpecHeader.Size = new System.Drawing.Size(67, 25);
			WidthSpecHeader.StylePriority.UseFont = false;
			WidthSpecHeader.Text = "In Spec";
			// 
			// HeightHeader
			// 
			HeightHeader.Font = new System.Drawing.Font("Arial", 10F);
			HeightHeader.Location = new System.Drawing.Point(290, YPos);
			HeightHeader.Name = "HeightHeader";
			HeightHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			HeightHeader.Size = new System.Drawing.Size(133, 25);
			HeightHeader.StylePriority.UseFont = false;
			HeightHeader.Text = "Measured Height";
			// 
			// HeightSpecHeader
			// 
			HeightSpecHeader.Font = new System.Drawing.Font("Arial", 10F);
			HeightSpecHeader.Location = new System.Drawing.Point(420, YPos);
			HeightSpecHeader.Name = "HeightSpecHeader";
			HeightSpecHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			HeightSpecHeader.Size = new System.Drawing.Size(75, 25);
			HeightSpecHeader.StylePriority.UseFont = false;
			HeightSpecHeader.Text = "In Spec";

			// 
			// MeasureTypeHeader
			// 
			MeasureTypeHeader.Font = new System.Drawing.Font("Arial", 10F);
			MeasureTypeHeader.Location = new System.Drawing.Point(500, YPos);
			MeasureTypeHeader.Name = "MeasureTypeHeader";
			MeasureTypeHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			MeasureTypeHeader.Size = new System.Drawing.Size(135, 25);
			MeasureTypeHeader.StylePriority.UseFont = false;
			MeasureTypeHeader.Text = "Measurement Type";

			// Add it to the detail
			this.Detail.Controls.Add(PartHeader);
			this.Detail.Controls.Add(WidthHeader);
			this.Detail.Controls.Add(WidthSpecHeader);
			this.Detail.Controls.Add(HeightHeader);
			this.Detail.Controls.Add(HeightSpecHeader);
			this.Detail.Controls.Add(MeasureTypeHeader);

			return YPos + VerticalLineSpacing;


		}

		private int CheckForPageBreak(int YPos)
		{

			if (YPos > PageBreakRef)
			{

//				YPos = 0;

				// Do a page break
				XRPageBreak Break = new XRPageBreak();

				Break.Location = new Point(0, PageBreakRef);

				// Add it to the page
				this.Detail.Controls.Add(Break);

				PageBreakRef += EndOfPage;

			}

			return YPos;
		}



	}
}
