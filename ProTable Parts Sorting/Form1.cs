﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;
using System.Security.Permissions;
using System.IO;
using Microsoft.Win32;
using System.Drawing.Printing;

using ProCABQC11.Properties;
using Ekc.Comp;





/*
 * 
 *  8/27/14  Version 1.2.0.5  Added position update suppression to fix lockup bug when Options dialog opened while axis motion.
 *  
 *  10/2/14  Version 1.2.1.0  Added angle measurement capability, larger screen capability and allow for fp quantities.
 *  
 *  --/--	 Version 1.2.1.1  Unknown change
 * 
 *	3/9/15	 Version 1.2.1.2  Fixed parameter file shipped with new copies to fix import/export
 *	
 *  3/13/15  Version 1.2.1.3  Changed angle resolution from 1 dp to 2 dp.
 *  
 *  9/15/16  Version 1.2.1.4  Added additional import fields.  Added pop-up user message after measurement.  Fixed import file source location.
 *							  Added sorting capability to Job listing.
 *							  
 * 10/11/16	 Version 1.2.1.5  Changed security package from Xheo to Sciensoft ElecKey
 * 
 * 10/25/16  Version 1.2.1.6  Added User 3 field for line item data.  Added Line Item layout file to allow user to rename column text in
 *							  in Line Item list view.  Also allows columns to be re-ordered (moved) and saves state.
 *							  
 * 11/23/16  Version 1.2.1.7  Fixed Line Item Column state save so that it saves both by clicking tool bar exit or system exit.  Fixed 
 *							  data for user 3 in forms editor.
 *							  
 * 2/9/17	 Version 1.2.1.8  Add filter when importing CSV string to strip all quotation marks from raw data prior to conversion.
 * 
 * 4/7/17	 Version 1.2.1.9  Added the Style field to the Line Item grid. 
 * 
 * 
 * 
 * */


namespace ProCABQC11
{


	public partial class Form1 : Form
	{

		// Constant Strings
			
		private SystemOrders OrdersList = (SystemOrders)null;
		private Order CurrentOrder = (Order)null;

		private PersistentParametersV1100 Params;

		// The measurement Interface Hardware
		private MeasurementInterfaceHardware MeasurementHardware;

		private delegate void UpdateWidthCallback(string WidthPos);
		private delegate void UpdateHeightCallback(string HeightPos);
		private delegate void UpdateDepthCallback(string DepthPos);
		private delegate void UpdateAngleCallback(string AnglePos);

		private const string ExportFileName = "System Param Export.txt";
		private const string OutOfSquareDefectLabelFileName = "\\Out Of Square defect label.repx";
		private const string FinishDefectLabelFileName = "\\Finish defect label.repx";
		private const string MiscDefectLabelFileName = "\\Miscellaneous defect label.repx";
		private const string OutOfToleranceLabelFileName = "\\Out Of Tolerance label.repx";


		private Point PositionGroupBoxDefaultLocation;
		private Point UnitsGroupBoxDefaultLocation;
		private Point MeasureButtonDefaultLocation;
		private int JobsTitleDefaultYPos;
		private int JobsInstructionDefaultYPos;
		private Size LowerPanelDefaultSize;


		private ItemListLayout Layout;
		




		private enum LineItemListColumns
		{
			LineIDCol,
			PartIDCol,
			HeightCol,
			WidthCol,
			ThichnessCol,
			QtyReqCol,
			QtyMeasCol,
			TypeCol,
			MaterialCol,
			FinishCol,
			HingingCol,
			MachiningCol,
			AssemblyCol,
			CommentCol,
			UserDef1Col,
			UserDef2Col,
			UserDef3Col,
			LocationCol,
			StyleCol


		}




		public Form1()
		{

			try
			{

				InitializeComponent();

				configurationToolStripMenuItem.Visible = true;

				// Create program parameters object
				Params = new PersistentParametersV1100();

				// Check if params serial file exist.
				if (!Params.ReadParameters(ref Params))
					Params.SaveParameters();

				// Show manual measure menu button ONLY if manual measure entry allowed
				if (Params.AllowManualMeasurements)
				{
					simulateMeasurementToolStripMenuItem.Visible = true;
					ManualMeasurementToolButton.Visible = true;
				}

				else
					ManualMeasurementToolButton.Visible = false;


				// Show/hide process incomplete orders button based on state
				if (Params.AllowIncompleteOrdersToBeProcessed)
					ProcessAsCompleteToolButton.Visible = true;

				else
					ProcessAsCompleteToolButton.Visible = false;


				


			}


			catch (Exception e)
			{

				Application.Exit();

			}


			
 		}



		private void Form1_Shown(object sender, EventArgs e)
		{


			// Create orders list.  This may change with serialization
			OrdersList = new SystemOrders();

			// Check if serialized file exists.  If so, read it.
			if (OrdersList.ReadParameters(ref OrdersList))
			{
				foreach(Order Ord in OrdersList)
				{
					// Add the OrderID to the order listing object
					OrderListing.Items.Add(Ord.OrderNumber);
				}
			}


			// Update the Item List View
			UpdateItemListViewColumns();



			// Create the measurement hardware interface (inludes virtual axes)
			MeasurementHardware = new MeasurementInterfaceHardware(Params.MeasurementDevicePortStr, MeasurementInterfaceHardware.MeasureHardwareType.ProRF);

			// Set position update events
			if(!Params.SwapHeightAndWidthAxes)
			{
				MeasurementHardware.WidthUpdate += new MeasurementInterfaceHardware.UpdateWidthPosition(MeasurementHardware_WidthUpdate);
				MeasurementHardware.HeightUpdate += new MeasurementInterfaceHardware.UpdateHeightPosition(MeasurementHardware_HeightUpdate);

			}

			else
			{
				MeasurementHardware.WidthUpdate += new MeasurementInterfaceHardware.UpdateWidthPosition(MeasurementHardware_HeightUpdate);
				MeasurementHardware.HeightUpdate += new MeasurementInterfaceHardware.UpdateHeightPosition(MeasurementHardware_WidthUpdate);

			}

			// Depth Axis (Z) event handler
			MeasurementHardware.DepthUpdate += new MeasurementInterfaceHardware.UpdateDepthPosition(MeasurementHardware_DepthUpdate);

			// Angle Axis event handler
			MeasurementHardware.AngleUpdate += new MeasurementInterfaceHardware.UpdateAnglePosition(MeasurementHardware_AngleUpdate);


			// Set measuring units
			if (Params.SysUnits == MeasuringUnits.Inches)
				InchSelect.Checked = true;

			else
				MillimeterSelect.Checked = true;


			// If Z axis not enabled, hide related controls
			if (!Params.ZAxisInUse)
			{
				DepthLabel.Visible = false;
				DepthPosition.Visible = false;
				DatumDepthToolButton.Visible = false;
				setDepthDatumToolStripMenuItem.Visible = false;

				// Disable Z functionality in Measurement Hardware
				MeasurementHardware.UseZAxis = false;

			}

			// Z axis enabled
			else
			{
				DepthLabel.Visible = true;
				DepthPosition.Visible = true;
				DatumDepthToolButton.Visible = true;
				setDepthDatumToolStripMenuItem.Visible = true;

				MeasurementHardware.UseZAxis = true;
			}

			// Angle measurement disabled V1.2.0.6
			if (!Params.AngleAxisInUse)
			{
				AnglePosition.Visible = false;
				AngleLabel.Visible = false;
				DatumAngleToolButton.Visible = false;

				MeasurementHardware.UseAngleAxis = false;

			}

			// Angle axis enabled
			else
			{
				AnglePosition.Visible = true;
				AngleLabel.Visible = true;
				DatumAngleToolButton.Visible = true;

				MeasurementHardware.UseAngleAxis = true;
			}



			// try to open the measurement hardware port
			string Msg;

			if (!MeasurementHardware.OpenPort(out Msg))
			{
				MessageBox.Show(Msg, "Port Open Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);


			}

			// Port open successfully
			else
			{

				// Try to acquire last axes positions from receiver
				// X axis
				MeasurementHardware.SerialPortTransmitMessage("r 1");

				// Y axis
				MeasurementHardware.SerialPortTransmitMessage("r 2");

				// Z axis
				if(Params.ZAxisInUse)
					MeasurementHardware.SerialPortTransmitMessage("r 3");

				// Angle axis in use but not Z, use axis 3
				if ((Params.AngleAxisInUse) && (!Params.ZAxisInUse))
					MeasurementHardware.SerialPortTransmitMessage("r 3");

				// Angle in use and Z, use axis 4
				else if(Params.AngleAxisInUse)
					MeasurementHardware.SerialPortTransmitMessage("r 4");


			}


			// Version 1.2.0.6 and above.  Record the initial positions of the PositionGroupBox, UnitsGroupBox and MeasureButton
			PositionGroupBoxDefaultLocation = new Point();
			PositionGroupBoxDefaultLocation = PositionGroupBox.Location;

			UnitsGroupBoxDefaultLocation = new Point();
			UnitsGroupBoxDefaultLocation = UnitsGroupBox.Location;

			MeasureButtonDefaultLocation = new Point();
			MeasureButtonDefaultLocation = MeasureButton.Location;

			JobsTitleDefaultYPos = JobsTitleLabel.Location.Y;
			JobsInstructionDefaultYPos = JobsInstructionsLabel.Location.Y;

			LowerPanelDefaultSize.Width = splitContainer1.Panel2.Width;
			LowerPanelDefaultSize.Height = splitContainer1.Panel2.Height;


			// Create a timed dialog to show splash
			TimedDialog Dlg = new TimedDialog("Accurate Technology.jpg");

			Dlg.ShowDialog();


		}

		// Event handler called when new height measurement detected
		void MeasurementHardware_HeightUpdate()
		{

			string HeightPos;
			int Precision;

			if (Params.SysUnits == MeasuringUnits.Inches)
				Precision = Params.InchPrecisionVal;

			else
				Precision = Params.MMPrecisionVal;
			

			// Swap axes if necessary
			if (Params.SwapHeightAndWidthAxes)
			{
				HeightPos = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis].GetPositionString(Params.SysUnits, Precision);
			}

			// No Swap
			else
			{
				HeightPos = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis].GetPositionString(Params.SysUnits, Precision);
			}

			HeightPosition.Invoke(new UpdateHeightCallback(this.UpdateHeight), HeightPos);

		}

		// Event handler called when new width measurement detected
		void MeasurementHardware_WidthUpdate()
		{

			string WidthPos;
			int Precision;

			if (Params.SysUnits == MeasuringUnits.Inches)
				Precision = Params.InchPrecisionVal;

			else
				Precision = Params.MMPrecisionVal;

			// Swap axes if necessary
			if (Params.SwapHeightAndWidthAxes)
			{
				WidthPos = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis].GetPositionString(Params.SysUnits, Precision);
			}

			// No Swap
			else
			{
				WidthPos = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis].GetPositionString(Params.SysUnits, Precision);
			}

			WidthPosition.Invoke(new UpdateWidthCallback(this.UpdateWidth), WidthPos);

		}

		// Event handler called when new depth measurement detected
		void MeasurementHardware_DepthUpdate()
		{

			string DepthPos;
			int Precision;

			if (Params.SysUnits == MeasuringUnits.Inches)
				Precision = Params.InchPrecisionVal;

			else
				Precision = Params.MMPrecisionVal;


			DepthPos = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.ZAxis].GetPositionString(Params.SysUnits, Precision);
			

			DepthPosition.Invoke(new UpdateDepthCallback(this.UpdateDepth), DepthPos);

		}


		// Converts the linear Angle Axis reading into an angle value in degrees using ArcTan
		double ConvertAngleAxisToAngle()
		{

			double NativeAngleEncoderPos;
			double AngleResult;
			double ArctanValue;

			NativeAngleEncoderPos = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.AngleAxis].GetNativePosition();

			if (Math.Abs(NativeAngleEncoderPos) >= .01)
			{
				// Calculate arctan.  Vertical offset always in inches.  Convert native encoder pos to inches
				ArctanValue = 180 / Math.PI * Math.Atan(Params.AngleVerticalOffsetValue / (NativeAngleEncoderPos / 25.4));

				// Quadrant 2.  Find incremental difference and add to 90
				if (ArctanValue < 0.0)
					AngleResult = 90.0 + (90.0 - Math.Abs(ArctanValue));
			
			   // Quadrant 1.  Use as is.
				else
					AngleResult = ArctanValue;

			}

			else
				AngleResult = 90.0;


			return AngleResult;


		}


		// Event handler called when new depth measurement detected
		void MeasurementHardware_AngleUpdate()
		{

			
			double AngleResult;

			// Convert linear to angle
			AngleResult = ConvertAngleAxisToAngle();
									 			
			AnglePosition.Invoke(new UpdateDepthCallback(this.UpdateAngle), AngleResult.ToString("F2"));

		}

		// delegate function to update width using invoke when executed from a different thread
		private void UpdateWidth(string WidthPos)
		{

			WidthPosition.Text = WidthPos;


		}

		// delegate function to update height using invoke when executed from a different thread
		private void UpdateHeight(string HeightPos)
		{

			HeightPosition.Text = HeightPos;

		}


		// delegate function to update depth using invoke when executed from a different thread
		private void UpdateDepth(string DepthPos)
		{

			DepthPosition.Text = DepthPos;

		}

		// delegate function to update angle using invoke when executed from a different thread
		private void UpdateAngle(string AnglePos)
		{

			AnglePosition.Text = AnglePos;

		}



		private void InchSelect_CheckedChanged(object sender, EventArgs e)
		{

			if (InchSelect.Checked)
			{
				Params.SysUnits = MeasuringUnits.Inches;
			}

			else
			{
				Params.SysUnits = MeasuringUnits.Millimeters;
			}

			// Save the new settings
			Params.SaveParameters();

			// Update the Height and Width position fields
			MeasurementHardware_HeightUpdate();
			MeasurementHardware_WidthUpdate();
			MeasurementHardware_DepthUpdate();

			// Rebuild the line item listing
			ShowOrderLineItemList(CurrentOrder);

		}

		private void DeleteCurrentOrder()
		{

			if (CurrentOrder != (Order)null)
			{
				// Remove order from order list
				OrdersList.RemoveOrder(CurrentOrder);

				// Delete all of the current items in the line items listing if populated
				LineItemList.Items.Clear();

				// Remove the current order from the order list
				OrderListing.Items.Remove(CurrentOrder.OrderNumber);

				// Clear current order
				CurrentOrder = (Order)null;

				// Clear the customer data
				ClearCustomerData();

			}

			else
				MessageBox.Show("There is no active order selected", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}


		private void SaveOrderCSVFile()
		{

			if (CurrentOrder.GenerateOrderCSVFile(Params) == OrderCSVCreationResultCodes.Success)
			{

				// Delete the current order
				DeleteCurrentOrder();
									
				// Save system order state
				OrdersList.SaveParameters();
				

			}

		}


		private void ProcessCompletedOrder()
		{

			if (Params.ShowOrderCompleteMsg)
			{
				string PrintPacklistMsg = "";

				if ((Params.PrintPacklistOnOrderCompletion == PrintReportTypes.Always) ||
					((Params.PrintPacklistOnOrderCompletion == PrintReportTypes.ByTemplate) && (CurrentOrder.PacklistTemplateFieldDefined)))
				{
					PrintPacklistMsg = "Generating Packlist Preview\nOne moment please!";
				}

				// Say order complete
				string Message = string.Format("Order: {0}\n is complete\n\n{1}", CurrentOrder.OrderNumber, PrintPacklistMsg);

				TimedDialog MessageDialog = new TimedDialog(Message, "Order Complete", 3000);

				MessageDialog.ShowDialog();

			}

			// Print pack list if enabled 
			if ((Params.PrintPacklistOnOrderCompletion == PrintReportTypes.Always) ||
				((Params.PrintPacklistOnOrderCompletion == PrintReportTypes.ByTemplate) && (CurrentOrder.PacklistTemplateFieldDefined)))
			{

				// Only print if current order exists
				if (CurrentOrder != null)
				{

					if (Params.ReportPrinterNameStr != "")
					{
						if (IsNamedPrinterInstalled(Params.ReportPrinterNameStr))
							GenerateUserDefinedPackList();

						// Printer defined in system but not installed in OS
						else
							MessageBox.Show("Defined report printer NOT installed in Windows", "Printer Not Installed", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}

					else
						MessageBox.Show("Report Printing Enabled but\nNo Report Printer Defined", "Report Print Error", MessageBoxButtons.OK, MessageBoxIcon.Error);


				}
			}
	

			// Create Order CSV file including measurements and update system list/state
			// This saves the current order to a csv files with measurements, removes the 
			// order from the order list and updates the SystemOrder object which saves the 
			// state of the system
			SaveOrderCSVFile();


		}



		private void PopulateMeasuredPartDataSource(ref DataSet DataSource, MeasuredPartInstance Part, int LineItemNumber)
		{

			DataRow MeasuredPartsRow;

			// Create a new row in measured part data source
			MeasuredPartsRow = DataSource.Tables["Measurement"].NewRow();

			// Populate measured part instances for line item
			MeasuredPartsRow["MeasuredWidth"] = Part.Width;
			MeasuredPartsRow["MeasuredHeight"] = Part.Height;
			MeasuredPartsRow["MeasuredDepth"] = Part.Thickness;

			if (Part.WidthInSpec)
				MeasuredPartsRow["WidthInSpec"] = "OK";
			else
				MeasuredPartsRow["WidthInSpec"] = "FAIL";

			if (Part.HeightInSpec)
				MeasuredPartsRow["HeightInSpec"] = "OK";
			else
				MeasuredPartsRow["HeightInSpec"] = "FAIL";

			if (Part.ThicknessInSpec)
				MeasuredPartsRow["DepthInSpec"] = "OK";
			else
				MeasuredPartsRow["DepthInSpec"] = "FAIL";


			MeasuredPartsRow["TimeDate"] = Part.MeasuredTime;
			MeasuredPartsRow["PartInstanceNumber"] = Part.PartInstanceNumber;

			if (Part.IsManualMeasurement)
				MeasuredPartsRow["ManuallyMeasured"] = "Yes";
			else
				MeasuredPartsRow["ManuallyMeasured"] = "No";

			MeasuredPartsRow["LineItemNumber"] = LineItemNumber;



			// V1.2  Add new table elements for error and rotation
			MeasuredPartsRow["WidthError"] = Part.WidthError;

			MeasuredPartsRow["HeightError"] = Part.HeightError;

			MeasuredPartsRow["DepthError"] = Part.ThicknessError;

			MeasuredPartsRow["PartMeasuredRotated"] = Part.PartWasRotated;




			// Add row to table
			DataSource.Tables["Measurement"].Rows.Add(MeasuredPartsRow);


		}

		private void PopulateLineItemDataSource(ref DataSet DataSource, LineItem LI)
		{

			DataRow LineItemRow;

			// Create a new row in the line item data source table
			LineItemRow = DataSource.Tables["LineItems"].NewRow();

			if (LI != null)
			{

				// Populate line item columns in data source
				LineItemRow["LineItemNumber"] = LI.LineNumber.ToString();
				LineItemRow["PartID"] = LI.PartIdStr;
				LineItemRow["QuantityRequired"] = LI.QtyRequired;
				LineItemRow["QuantityMeasured"] = LI.QtyMeasured;
				LineItemRow["NominalWidth"] = LI.Width;
				LineItemRow["NominalHeight"] = LI.Height;
				LineItemRow["NominalThickness"] = LI.Thickness;
				LineItemRow["Material"] = LI.MaterialText;
				LineItemRow["Style"] = LI.StyleText;

				LineItemRow["Hinging"] = LI.HingingText;
				LineItemRow["Finish"] = LI.FinishText;
				LineItemRow["Type"] = LI.TypeText;
				LineItemRow["Location"] = LI.LocationText;
				LineItemRow["Machining"] = LI.MachiningText;
				LineItemRow["Assembly"] = LI.AssemblyText;
				LineItemRow["UserMsg"] = LI.UserMessageText;


				LineItemRow["Comment"] = LI.CommentText;
				LineItemRow["User1"] = LI.UserDef1Text;
				LineItemRow["User2"] = LI.UserDef2Text;
				LineItemRow["User3"] = LI.UserDef3Text;
			}

			// No line item found.  Use dummy data
			else
			{
				// Populate line item columns in data source
				LineItemRow["LineItemNumber"] = "0";
				LineItemRow["PartID"] = "Unknown";
				LineItemRow["QuantityRequired"] = 0;
				LineItemRow["QuantityMeasured"] = 0;
				LineItemRow["NominalWidth"] = 0.0;
				LineItemRow["NominalHeight"] = 0.0;
				LineItemRow["NominalThickness"] = 0.0;
				LineItemRow["Material"] = "Unknown";
				LineItemRow["Style"] = "Unknown";

				LineItemRow["Hinging"] = "Unknown";
				LineItemRow["Finish"] = "Unknown";
				LineItemRow["Type"] = "Unknown";
				LineItemRow["Location"] = "Unknown";
				LineItemRow["Machining"] = "Unknown";
				LineItemRow["Assembly"] = "Unknown";
				LineItemRow["UserMsg"] = "Unknown";

				LineItemRow["Comment"] = "Unknown";
				LineItemRow["User1"] = "Unknown";
				LineItemRow["User2"] = "Unknown";
				LineItemRow["User3"] = "Unknown";
			}


			// Add row to table
			DataSource.Tables["LineItems"].Rows.Add(LineItemRow);


		}


		private void GenerateUserDefinedPartLabel(LineItem LI, MeasuredPartInstance Part)
		{
			DataBoundReport report;

			report = new DataBoundReport();

			DataSet DataSource;

			report.GetDataSource(out DataSource);

			// Populate the data source with common report data
			PopulateReportCommonData(ref DataSource);

			// Populate the line item data source
			PopulateLineItemDataSource(ref DataSource, LI);

			// Populate the Measured part data source
			PopulateMeasuredPartDataSource(ref DataSource, Part, LI.LineNumber);

			// Update the report's data source
			report.SetDataSource(DataSource);



			try
			{

				string TemplateFile;

				// If a part label template is defined in the Order, use it.
				if (CurrentOrder.PartLabelTemplateText != "")
				{
					// Be sure that specified template file exists.
					if (File.Exists(Params.PartLabelTemplateFilePathStr + "\\" + CurrentOrder.PartLabelTemplateText))
						TemplateFile = Params.PartLabelTemplateFilePathStr + "\\" + CurrentOrder.PartLabelTemplateText;

					// If not, use default template
					else
						TemplateFile = Params.PartLabelTemplateFilePathStr + "\\Default Part Label.repx";
				}

				// If no part label template defined, use the default.
				else
					TemplateFile = Params.PartLabelTemplateFilePathStr + "\\Default Part Label.repx";

				// Load template file
				report.LoadLayout(TemplateFile);

				// Set label printer name
				report.PrinterName = Params.LabelPrinterNameStr;

				// Use preview if enabled
				if (Params.UseLabelPrintPreview)
				{
					report.ShowPreview();
				}

				else
				{
					// Print label with no preview
					report.Print();
				}

			}

			catch
			{
				MessageBox.Show("Unable to open a valid label template.  Printing aborted.", "Label Template Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}


		}

		private void GenerateDefectLabel(LineItem LI, MeasuredPartInstance Part, string DefectLabelTemplateFile)
		{

			// Be sure label printer defined
			if (Params.LabelPrinterNameStr == "")
			{
				MessageBox.Show("Label Printing Enabled but\nNo Label Printer Defined", "Label Print Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			else if (!IsNamedPrinterInstalled(Params.LabelPrinterNameStr))
			{
				MessageBox.Show("Defined label printer NOT installed in Windows", "Printer Not Installed", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			else
			{

				DataBoundReport report;

				report = new DataBoundReport();

				DataSet DataSource;

				report.GetDataSource(out DataSource);

				// Populate the data source with common report data
				PopulateReportCommonData(ref DataSource);

				// Populate the line item data source
				PopulateLineItemDataSource(ref DataSource, LI);

				// Populate the Measured part data source
				if(LI != null)			
					PopulateMeasuredPartDataSource(ref DataSource, Part, LI.LineNumber);

				else
					PopulateMeasuredPartDataSource(ref DataSource, Part, 0);

				// Update the report's data source
				report.SetDataSource(DataSource);



				try
				{

					string TemplateFile;

					if (File.Exists(DefectLabelTemplateFile))
					{
						TemplateFile = DefectLabelTemplateFile;

						// Load template file
						report.LoadLayout(TemplateFile);

						// Set label printer name
						report.PrinterName = Params.LabelPrinterNameStr;

						// Print label with no preview
						report.Print();
					}

					// Specified file does not exists
					else
						MessageBox.Show("Defect label template file does not exist.\nLabel printing aborted", "Label Template File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);


				}

				catch (Exception ex)
				{
					MessageBox.Show("Unable to open a valid label template.  Printing aborted.", "Label Template Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				}
			}

		}


		private bool IsNamedPrinterInstalled(string NamedPrinter)
		{
			// Printer defined. Make sure installed
			bool PrinterInstalled = false;

			for (int u = 0; u < PrinterSettings.InstalledPrinters.Count; u++)
			{
				if (NamedPrinter == PrinterSettings.InstalledPrinters[u])
				{
					PrinterInstalled = true;
					break;
				}

			}

			return PrinterInstalled;
		}


		private void ProcessItemAddedMeasurement(LineItem Item, bool Rotated)
		{

			// Show user dialog if enabled in configuration AND user message is not empty
			if ((Params.ShowUserMessageDlg)	 && (Item.UserMessageText != ""))
			{

				UserMessageDlg UserDlg = new UserMessageDlg(Item.UserMessageText);

				UserDlg.ShowDialog();
				
			}


			// Determine if quantity measured equals quantity required for the line item
			if (Item.IsQuantityComplete)
			{
				// Re-display the list with the completed line item
				ShowOrderLineItemList(CurrentOrder);

                string Message;

				// Show timed message that line item is complete
                if(!Rotated)
				    Message = string.Format("Line Item Number {0} is Complete!", Item.LineNumber);

                // Rotated part measurement
                else
                    Message = string.Format("     NOTICE:  Part was Rotated.\nLine Item Number {0} is Complete!", Item.LineNumber);


				TimedDialog MessageDialog = new TimedDialog(Message, "Line Item Complete");

				MessageDialog.ShowDialog();

			}

			// More parts required
			else
			{
                string Message;

				// Rebuild the line item listing
				ShowOrderLineItemList(CurrentOrder);
								
				// Show timed message that part was added
                if(!Rotated)
				    Message = string.Format("Measurement added for Line Item Number {0}", Item.LineNumber);

                // Rotated part measurement
                else
                    Message = string.Format("     NOTICE:  Part was Rotated.\nMeasurement added for Line Item Number {0}", Item.LineNumber);

				TimedDialog MessageDialog = new TimedDialog(Message, "Measurement Added");

				MessageDialog.ShowDialog();


			}

			MeasuredPartInstance Part;

			// Only print if at least one measured part exists AND label printing enabled.
			if((Item.GetLastMeasuredPart(out Part)) && ((Params.PrintMeasuredPartLabels == PrintLabelTypes.Always) || 
				((Params.PrintMeasuredPartLabels == PrintLabelTypes.ByTemplate) && (CurrentOrder.PartLabelTemplateFieldDefined))))
			{
				// be sure printer is defined
				if (Params.LabelPrinterNameStr != "")
				{
				 	// Print only if defined printer installed
					if (IsNamedPrinterInstalled(Params.LabelPrinterNameStr))
						GenerateUserDefinedPartLabel(Item, Part);

					// Printer defined in system but not installed in OS
					else
						MessageBox.Show("Defined label printer NOT installed in Windows", "Printer Not Installed", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}

				else
					MessageBox.Show("Label Printing Enabled but\nNo Label Printer Defined", "Label Print Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				
			}

			// Save system order state
			OrdersList.SaveParameters();

			// Save the state of the System order when a measurement completed
			StatusBarTimer.Enabled = true;
			SystemOrderStateLabel.Visible = true;

			// Determine if order is complete.  If so, process it
			if (CurrentOrder.IsOrderComplete() && !Params.DoNotProcessCompletedOrders)
				ProcessCompletedOrder();



		}


		// Look through an order for a line item that matches the passed measured part.
		// The measured part must be one of the following:
		//		Exact match (one and only one)
		//		Multiple matches (part ambiguity)
		//		No matches (part out of tolerance)
		//
		// If an exact match occurs, part is saved and line item list updated
		// If multiple matches or no matches, the user must choose a matching line item, if any
		// HEIGHT AND WIDTH TOLERANCES ARE ALWAYS IN MM
		// MeasuredPartInstance data are in the defined CSVUnits type
		private void AddMeasurementToOrder(ref MeasuredPartInstance Measure)
		{

			Tolerances OrderTol;

			if (CurrentOrder != null)
			{
				LineItemMatches Result;

				// A reference for a list of possible line items if multiple or no matches occur.
				List<LineItem> PossibleLineItems;

				// Determine if measurement results in one, multiple or no matches
				// If only one match, the measurement is saved to the line item
				Result = CurrentOrder.CheckLineItemsForPartMeasurement(out PossibleLineItems, ref Measure, false, out OrderTol, Params.CheckCompletedLIForMatch, Params.ZAxisInUse, Params.AngleAxisInUse);

				// An exact match (within tolerance).  Measurement is already stored.  Update measured quantity in list
				if ((Result == LineItemMatches.One) || (Result == LineItemMatches.OneRotated))
				{

                    bool Rotated = false;

                    if(Result == LineItemMatches.OneRotated)
                        Rotated = true;

					// create a line item reference based on the matched line item
					LineItem Item = PossibleLineItems[0];

					// Update the order and system as a result of adding the measurement
					ProcessItemAddedMeasurement(Item, Rotated);

				}

				// The line item has already been completed
				else if (Result == LineItemMatches.AlreadyCompleted)
				{
					MessageBox.Show("The matching line item has already been completed!", "Line Item Already Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);

				}

				// If allowable, ask user to determine line item if either multiple matches (ambiguity) or no matches
				else
				{
					string ContentMsg;
					string TitleMsg;
					MessageBoxButtons MBButtons;
					bool InvalidPartsAllowed = true;



					// Multiple matches (part ambiguity)
					if ((Result == LineItemMatches.Multiple) || (Result == LineItemMatches.MultipleRotated))
					{
						// Multiple matches but none are rotated
						if (Result == LineItemMatches.Multiple)
							ContentMsg = "There are multiple matching line items.\nPlease select the correct line item from the following.";

						// Multiple matches and at least one is rotated
						else
							ContentMsg = "There are multiple matching line items, at least one is rotated.\nPlease select the correct line item from the following.";

						TitleMsg = "Multiple Line Item Matches";
						MBButtons = MessageBoxButtons.OKCancel;

					}

					// No matches.  Out of spec?  Only if allowed
					else if (Params.OutOfTolerancePartsAccepted)
					{

						ContentMsg = "There are NO line items that match the\nmeasured part within tolerance.\nDo you wish to choose a line item anyway?";
						TitleMsg = "No Matching Parts Within Tolerance";
						MBButtons = MessageBoxButtons.OKCancel;

					}

					// No matches and invalid matches not allowed
					else
					{
						ContentMsg = "There are NO line items that match the\nmeasured part within tolerance.\nOut of Tolerance parts are NOT allowed!";
						TitleMsg = "No Matching Parts Within Tolerance";
						MBButtons = MessageBoxButtons.OK;
						InvalidPartsAllowed = false;

					}


					if ((MessageBox.Show(ContentMsg, TitleMsg, MBButtons, MessageBoxIcon.Warning) == DialogResult.OK) && (InvalidPartsAllowed))
					{
						bool PartFound = false;

						// Check each line item in possible line item list
						for (int u = 0; u < PossibleLineItems.Count; u++)
						{

							double WidthError;
							double HeightError;
							double ThicknessError;
							double RotWidthError;
							double RotHeightError;

							LineItem Item = PossibleLineItems[u];

							// Get the measurement error for the measurement compared to the line item specs
							Item.GetMeasurementError(ref Measure, out WidthError, out HeightError, out RotWidthError, out RotHeightError, out ThicknessError);

							// Pass the LineItem to a new instance of the Full Line Item Description dialog.  
							// Use the Yes and No buttons
							FullLineItemDialog Dlg = new FullLineItemDialog(ref Item, true, HeightError, WidthError, RotHeightError, RotWidthError, ThicknessError, CurrentOrder.TestThicknessDimFieldDefined && Params.ZAxisInUse);

							// Show the dialog.
							// If Ok, user has selected this line item as the target for the measurement
							if (Dlg.ShowDialog() == DialogResult.OK)
							{

								// The quantity has already been met.  Warn user.
								if (Item.IsQuantityComplete)
								{
									// Warn user.  If Yes, save part measurement anyway
									if (MessageBox.Show("Warning:  The part quantity for the line item has ALREADY been met.\nDo you want to add the part to the line item anyway?", "Exceeding Required Quantity", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
									{
										// Save the measurement to the Line Item
										Item.AddMeasuredPart(ref Measure, OrderTol, CurrentOrder.TestThicknessDimFieldDefined, Params.AngleAxisInUse);

										// Mark part as found
										PartFound = true;

										// Update the order and system as a result of adding the measurement
										ProcessItemAddedMeasurement(Item, false);

										break;
									}

									// User selected NO from warning.  Just break
									else
										break;


								}

								// Quantity not met, just add
								else
								{

									// Save the measurement to the Line Item
									Item.AddMeasuredPart(ref Measure, OrderTol, CurrentOrder.TestThicknessDimFieldDefined, Params.AngleAxisInUse);
									// Mark part as found
									PartFound = true;

									// Update the order and system as a result of adding the measurement
									ProcessItemAddedMeasurement(Item, false);

									break;
								}


							}
						}

						if (!PartFound)
						{
							MessageBox.Show("No matching line item selected.\nThe part measurement was NOT stored", "No Line Item Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}

					// The user has cancelled the operation. (not from invalid parts allowed)
					else if (InvalidPartsAllowed)
					{
						MessageBox.Show("The measured part will NOT be stored", "Measured Part Not Stored", MessageBoxButtons.OK, MessageBoxIcon.Warning);

					}

					// Invalid parts NOT allowed AND Print out of spec label enabled
					else if ((!InvalidPartsAllowed) && (Params.PrintOutOfSpecLabel))
					{

						// Create a dummy line item and set to null since none was found
						LineItem LI = null;

						// Print the out of tolerance label
						GenerateDefectLabel(LI, Measure, Params.PartLabelTemplateFilePathStr + OutOfToleranceLabelFileName);
					   

					}
				}
			}

			// No Order selected
			else
			{

				MessageBox.Show("No Order Currently Selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);


			}
		

		}




		private LineItem LocateDefectLineItemInOrder(ref MeasuredPartInstance Measure)
		{

			LineItem LineItemFound = null;

			Tolerances OrderTol;

			if (CurrentOrder != null)
			{
				LineItemMatches Result;


				// A reference for a list of possible line items if multiple or no matches occur.
				List<LineItem> PossibleLineItems;

				// Determine if measurement results in one, multiple or no matches
				// If only one match, the measurement is saved to the line item
				// Third parameter is true to indicate a defect test
				Result = CurrentOrder.CheckLineItemsForPartMeasurement(out PossibleLineItems, ref Measure, true, out OrderTol, Params.CheckCompletedLIForMatch, Params.ZAxisInUse, Params.AngleAxisInUse);

				// An exact match (within tolerance).  Measurement is already stored.  Update measured quantity in list
				if ((Result == LineItemMatches.One) || (Result == LineItemMatches.OneRotated))
				{

					// Return the matching line item.
					LineItemFound = PossibleLineItems[0];

				}

				// If allowable, ask user to determine line item if either multiple matches (ambiguity) or no matches
				else
				{
					string ContentMsg;
					string TitleMsg;
					MessageBoxButtons MBButtons;
					bool MultipleMatches = false;

					// Multiple matches (part ambiguity)
					if ((Result == LineItemMatches.Multiple) || (Result == LineItemMatches.MultipleRotated))
					{
						// Multiple matches but none are rotated
						if (Result == LineItemMatches.Multiple)
							ContentMsg = "There are multiple matching line items.\nPlease select the correct line item from the following.";

						// Multiple matches and at least one is rotated
						else
							ContentMsg = "There are multiple matching line items, at least one is rotated.\nPlease select the correct line item from the following.";

						TitleMsg = "Multiple Line Item Matches";
						MBButtons = MessageBoxButtons.OKCancel;
						MultipleMatches = true;

					}

		
					// No matches and invalid matches not allowed
					else
					{
						ContentMsg = "There are NO line items that match the\nmeasured part within tolerance.";
						TitleMsg = "No Matching Parts Within Tolerance";
						MBButtons = MessageBoxButtons.OK;
						MultipleMatches = false;

					}


					if ((MessageBox.Show(ContentMsg, TitleMsg, MBButtons, MessageBoxIcon.Warning) == DialogResult.OK) && (MultipleMatches))
					{
						bool PartFound = false;

						// Check each line item in possible line item list
						for (int u = 0; u < PossibleLineItems.Count; u++)
						{

							double WidthError;
							double HeightError;
							double ThicknessError;
							double RotWidthError;
							double RotHeightError;

							LineItem Item = PossibleLineItems[u];

							// Get the measurement error for the measurement compared to the line item specs
							Item.GetMeasurementError(ref Measure, out WidthError, out HeightError, out RotWidthError, out RotHeightError, out ThicknessError);

							// Pass the LineItem to a new instance of the Full Line Item Description dialog.  
							// Use the Yes and No buttons
							FullLineItemDialog Dlg = new FullLineItemDialog(ref Item, true, HeightError, WidthError, RotHeightError, RotWidthError, ThicknessError, CurrentOrder.TestThicknessDimFieldDefined && Params.ZAxisInUse);

							// Show the dialog.
							// If Ok, user has selected this line item as the target for the measurement
							if (Dlg.ShowDialog() == DialogResult.OK)
							{

								LineItemFound = Item;

							}
						}

						if (!PartFound)
						{
							MessageBox.Show("No matching line item selected.\nPlease double click on the desired line item.", "No Line Item Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}

			// No Order selected
			else
			{

				MessageBox.Show("No Order Currently Selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);


			}

			return LineItemFound;


		}





		private void BuildListViewLineItem(ref LineItem Item)
		{

			// Create list line item showing line item number
			ListViewItem LVItem = new ListViewItem();

			int Col;
			int u;
			Font Ft = new Font(FontFamily.GenericSansSerif, 18.0F);

		

			// Add a number of "blank" entries
			for (u = 0; u < LineItemList.Columns.Count; u++)
				LVItem.SubItems.Add(" ", Color.Black, Color.Blue, Ft);

			

			// Line Number
			ListViewItem.ListViewSubItem LineNumberSI = new ListViewItem.ListViewSubItem();
			LineNumberSI.Text = Item.LineNumber.ToString();
			Col = LocateLineItemColumnByName("LineItemHeader");
			LVItem.SubItems[Col] = LineNumberSI;

			// Part ID
			ListViewItem.ListViewSubItem PartIDSI = new ListViewItem.ListViewSubItem();
			PartIDSI.Text = Item.PartIdStr;
			Col = LocateLineItemColumnByName("PartIDHeader");
			LVItem.SubItems[Col] = PartIDSI;

			// Width
			ListViewItem.ListViewSubItem WidthSI = new ListViewItem.ListViewSubItem();
			WidthSI.Text = PositionConversion.ConvertUnits(Item.Width, CurrentOrder.CSVMeasurementUnits, Params.SysUnits, Params.MMPrecisionVal, Params.InchPrecisionVal);
			Col = LocateLineItemColumnByName("widthHeader");
			LVItem.SubItems[Col] = WidthSI;
	

			// Height
			ListViewItem.ListViewSubItem HeightSI = new ListViewItem.ListViewSubItem();
			HeightSI.Text = PositionConversion.ConvertUnits(Item.Height, CurrentOrder.CSVMeasurementUnits, Params.SysUnits, Params.MMPrecisionVal, Params.InchPrecisionVal);
			Col = LocateLineItemColumnByName("heightHeader");
			LVItem.SubItems[Col] = HeightSI;


			// Depth
			ListViewItem.ListViewSubItem DepthSI = new ListViewItem.ListViewSubItem();
			DepthSI.Text = PositionConversion.ConvertUnits(Item.Thickness, CurrentOrder.CSVMeasurementUnits, Params.SysUnits, Params.MMPrecisionVal, Params.InchPrecisionVal);
			Col = LocateLineItemColumnByName("depthHeader");
			LVItem.SubItems[Col] = DepthSI;

			// Qty Required
			ListViewItem.ListViewSubItem QtyReqSI = new ListViewItem.ListViewSubItem();
			QtyReqSI.Text = Item.QtyRequired.ToString();
			Col = LocateLineItemColumnByName("QtyReqHeader");
			LVItem.SubItems[Col] = QtyReqSI;

			// Qty Measured
			ListViewItem.ListViewSubItem QtyMeasSI = new ListViewItem.ListViewSubItem();
			QtyMeasSI.Text = Item.QtyMeasured.ToString();
			Col = LocateLineItemColumnByName("QtyMeasuredHeader");
			LVItem.SubItems[Col] = QtyMeasSI;

			// Type
			ListViewItem.ListViewSubItem TypeSI = new ListViewItem.ListViewSubItem();
			TypeSI.Text = Item.TypeText;
			Col = LocateLineItemColumnByName("TypeHeader");
			LVItem.SubItems[Col] = TypeSI;

			// Material
			ListViewItem.ListViewSubItem MaterialSI = new ListViewItem.ListViewSubItem();
			MaterialSI.Text = Item.MaterialText;
			Col = LocateLineItemColumnByName("MaterialHeader");
			LVItem.SubItems[Col] = MaterialSI;

			// Finish
			ListViewItem.ListViewSubItem FinishSI = new ListViewItem.ListViewSubItem();
			FinishSI.Text = Item.FinishText;
			Col = LocateLineItemColumnByName("FinishHeader");
			LVItem.SubItems[Col] = FinishSI;

			// Hinging
			ListViewItem.ListViewSubItem HingingSI = new ListViewItem.ListViewSubItem();
			HingingSI.Text = Item.HingingText;
			Col = LocateLineItemColumnByName("HingingHeader");
			LVItem.SubItems[Col] = HingingSI;

			// Machining
			ListViewItem.ListViewSubItem MachiningSI = new ListViewItem.ListViewSubItem();
			MachiningSI.Text = Item.MaterialText;
			Col = LocateLineItemColumnByName("MachiningHeader");
			LVItem.SubItems[Col] = MachiningSI;

			// Assembly
			ListViewItem.ListViewSubItem AssemblySI = new ListViewItem.ListViewSubItem();
			AssemblySI.Text = Item.AssemblyText;
			Col = LocateLineItemColumnByName("AssemblyHeader");
			LVItem.SubItems[Col] = AssemblySI;

			// Comments
			ListViewItem.ListViewSubItem CommentsSI = new ListViewItem.ListViewSubItem();
			CommentsSI.Text = Item.CommentText;
			Col = LocateLineItemColumnByName("CommentsHeader");
			LVItem.SubItems[Col] = CommentsSI;

			// User Def 1
			ListViewItem.ListViewSubItem Def1SI = new ListViewItem.ListViewSubItem();
			Def1SI.Text = Item.UserDef1Text;
			Col = LocateLineItemColumnByName("User1Header");
			LVItem.SubItems[Col] = Def1SI;

			// User Def 2
			ListViewItem.ListViewSubItem Def2SI = new ListViewItem.ListViewSubItem();
			Def2SI.Text = Item.UserDef2Text;
			Col = LocateLineItemColumnByName("User2Header");
			LVItem.SubItems[Col] = Def2SI;

			// User Def 3
			ListViewItem.ListViewSubItem Def3SI = new ListViewItem.ListViewSubItem();
			Def3SI.Text = Item.UserDef3Text;
			Col = LocateLineItemColumnByName("User3Header");
			LVItem.SubItems[Col] = Def3SI;

			// Location
			ListViewItem.ListViewSubItem LocationSI = new ListViewItem.ListViewSubItem();
			LocationSI.Text = Item.LocationText;
			Col = LocateLineItemColumnByName("LocationHeader");
			LVItem.SubItems[Col] = LocationSI;

			// Style
			ListViewItem.ListViewSubItem StyleSI = new ListViewItem.ListViewSubItem();
			StyleSI.Text = Item.StyleText;
			Col = LocateLineItemColumnByName("StyleHeader");
			LVItem.SubItems[Col] = StyleSI;


			// Add the list view item to the line item listing
			LineItemList.Items.Add(LVItem);

			// Set line font and size
			LVItem.Font = new Font(FontFamily.GenericSansSerif, 12.0F);

			// If line item complete, change to green
			if (Item.QtyMeasured >= Item.QtyRequired)
				LVItem.BackColor = Color.LightGreen;

			// If not complete but at least one part measured, changed to yellow.  Otherwise, default is white
			else if (Item.QtyMeasured > 0)
				LVItem.BackColor = Color.Yellow;

		}

		private int LocateLineItemColumnByName(String ColName)
		{

			foreach (ColumnHeader header in LineItemList.Columns)
			{
				if ((string)(header.Tag) == ColName)
				{
					int Index = header.Index;
					return Index;
				}
			}

			return -1;


		}

		// Locates the index of the LineItem in the ListView
		private int LocateLineItemIndexInList(ref LineItem Item)
		{

			int Index = -1;

			for (int u = 0; u < LineItemList.Items.Count; u++)
			{
				// Find line number in list that matches Line Item and return its index
				if (Item.LineNumber == Convert.ToInt32(LineItemList.Items[u].SubItems[0].Text))
					Index = u;
			}

			return Index;


		}


		private void LineItemList_ColumnReordered(object sender, ColumnReorderedEventArgs e)
		{



		}

		// Updates the Item List View object with column data and layout
		private void UpdateItemListViewColumns()
		{

			Layout = new ItemListLayout();

			// Read configuration file here....
			Layout.OpenItemListLayout();


			int ColumnCount = 0;

			foreach (ColumnHeader header in LineItemList.Columns)
			{

				header.Text = Layout.GetColumnTitle(ColumnCount);
				header.Width = Layout.GetColumnWidth(ColumnCount);
				header.DisplayIndex = Layout.GetColumnDisplayIndex(ColumnCount);
				header.Tag = Layout.GetColumnTag(ColumnCount);

				ColumnCount++;

			}




		}

		// Saves Item List View Column data to Layout data
		private void SaveItemListViewColumns()
		{


			int ColumnCount = 0;

			// Update layout data class with column data
			foreach (ColumnHeader header in LineItemList.Columns)
			{

				Layout.SetColumnTitle(ColumnCount, header.Text);
				Layout.SetColumnWidth(ColumnCount, header.Width); 
				Layout.SetColumnDisplayIndex(ColumnCount, header.DisplayIndex);

				ColumnCount++;

			}

			// Save the data to the config file
			Layout.SaveItemListLayout();



		}


		private void openOrderFileToolStripMenuItem_Click(object sender, EventArgs e)
		{

			OpenFileDialog OpenFileDlg = new OpenFileDialog();

			OpenFileDlg.InitialDirectory = Params.CSVFilePathStr;
			OpenFileDlg.Filter = "CSV Files (*.csv)|*.csv";
			OpenFileDlg.FilterIndex = 1;
			OpenFileDlg.RestoreDirectory = true;

			// Ok pressed from dialog
			if (OpenFileDlg.ShowDialog() == DialogResult.OK)
			{
				// File name not null
				if (OpenFileDlg.FileName.Length != 0)
				{
					// List for error log
					List<string> ErrorLog = new List<string>();

					// An empty list of orders to pass to the reader to hold parsed orders
					SystemOrders NewOrders = new SystemOrders();

					int SelectedIndex = -1;

					// Using cutlist type format
					if (Params.CSVFileTypeInUse == CSVFileType.CutlistFile)
					{

						// Check if more than one template available.  If so, user must select template
						if (Params.AreMultipleMOTemplates())
						{
							TemplateIndexSelectionDialog TemplateDlg = new TemplateIndexSelectionDialog(Params);

							if (TemplateDlg.ShowDialog() == DialogResult.OK)
								SelectedIndex = TemplateDlg.TemplateIndex;
						}

						else
							SelectedIndex = 0;

						// If NOT cancelled, process CSV file
						if (SelectedIndex >= 0)
						{

							// Create a cutlist reader
							CutlistCSVReader CSVReader = new CutlistCSVReader();

							// Open the file and parse
							CSVReaderResultCodes Result = CSVReader.OpenCSVFile(OpenFileDlg.FileName, ref Params, ref ErrorLog, ref NewOrders, SelectedIndex);

							// File opening error, say it.
							if (Result == CSVReaderResultCodes.FileOpenError)
								MessageBox.Show(ErrorLog[0].ToString(), "File Open Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

							// Parsing errors occured.  Show error list in dialog box
							else if (Result == CSVReaderResultCodes.ParsingErrors)
							{
								ErrorListDialog ErrorDlg = new ErrorListDialog(ref ErrorLog);

								ErrorDlg.ShowDialog();

							}

							// Import successful, try to add local order list to system order list
							else
							{

								// Add order ID to order listing and new orders to order lists
								for (int u = 0; u < NewOrders.NumberOfOrders(); u++)
								{
									// Get the order from the list
									Order OrderItem;
									NewOrders.GetExistingOrder(out OrderItem, u);

									// Make sure order is valid
									if (OrderItem != null)
									{
										// Add the individual order to the application's list of orders in memory
										// If not duplicate, add order ID to order listing
										if (OrdersList.AddOrder(ref OrderItem))
										{
											// Add the OrderID to the order listing object
//											OrderListing.Items.Add(OrderItem.OrderNumber);
											OrderListing.Items.Clear();

											for (int v = 0; v < OrdersList.Count(); v++)
												OrderListing.Items.Add(OrdersList.ElementAt(v).OrderNumber);

										}

										else
										{
											string Msg = string.Format("Error - Order {0} already exists in the list of orders and was not duplicated", OrderItem.OrderNumber);

											ErrorLog.Add(Msg);
										}
									}
								}

								// No duplicates.  Say success
								if (ErrorLog.Count == 0)
								{

									TimedDialog Dlg = new TimedDialog(string.Format("{0}\nImported successfully!", OpenFileDlg.SafeFileName), "CSV Import Successful", 2000);

									Dlg.ShowDialog();

									
								}

								// One or more errors
								else
								{
									// Create error dialog
									ErrorListDialog ErrorDlg = new ErrorListDialog(ref ErrorLog);

									ErrorDlg.ShowDialog();

								}
							}
						}
					}

					// Single order template type
					else if (Params.CSVFileTypeInUse == CSVFileType.SingleOrderFile)
					{

						// Check if more than one template available.  If so, user must select template
						if (Params.AreMultipleSOTemplates())
						{
							TemplateIndexSelectionDialog TemplateDlg = new TemplateIndexSelectionDialog(Params);

							if (TemplateDlg.ShowDialog() == DialogResult.OK)
								SelectedIndex = TemplateDlg.TemplateIndex;
						}

						// Only the default template in use
						else
							SelectedIndex = 0;

						// If NOT cancelled, process CSV file
						if (SelectedIndex >= 0)
						{
							// Create a single order reader
							SingleOrderCSVReader CSVReader = new SingleOrderCSVReader();

							// Open the file and parse
							CSVReaderResultCodes Result = CSVReader.OpenCSVFile(OpenFileDlg.FileName, ref Params, ref ErrorLog, ref NewOrders, SelectedIndex);

							// File opening error, say it.
							if (Result == CSVReaderResultCodes.FileOpenError)
								MessageBox.Show(ErrorLog[0].ToString(), "File Open Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

							// Parsing errors occured.  Show error list in dialog box
							else if (Result == CSVReaderResultCodes.ParsingErrors)
							{
								ErrorListDialog ErrorDlg = new ErrorListDialog(ref ErrorLog);

								ErrorDlg.ShowDialog();

							}

							// Import successful, say it
							else
							{

								// Add order ID to order listing and new orders to order lists (in this case, only one order in list)
								for (int u = 0; u < NewOrders.NumberOfOrders(); u++)
								{
									// Get the order from the list
									Order OrderItem;
									NewOrders.GetExistingOrder(out OrderItem, u);

									// Make sure order is valid
									if (OrderItem != null)
									{
										// Add the individual order to the application's list of orders in memory
										// If not duplicate, add order ID to order listing
										if (OrdersList.AddOrder(ref OrderItem))
										{
											// Add the OrderID to the order listing object
//											OrderListing.Items.Add(OrderItem.OrderNumber);

											OrderListing.Items.Clear();

											for (int v = 0; v < OrdersList.Count(); v++)
												OrderListing.Items.Add(OrdersList.ElementAt(v).OrderNumber);

											TimedDialog Dlg = new TimedDialog(string.Format("{0}\nImported successfully!", OpenFileDlg.SafeFileName), "CSV Import Successful", 2000);

											Dlg.ShowDialog();
										}

										// a duplicate order
										else
										{
											string Msg = string.Format("Error - Order {0} already exists in the list of orders and was not duplicated", OrderItem.OrderNumber);

											ErrorLog.Add(Msg);

											ErrorListDialog ErrorDlg = new ErrorListDialog(ref ErrorLog);

											ErrorDlg.ShowDialog();
										}

									}
								}
							}
						}
   					}
				}
			}
		}



		private void ShowOrderLineItemList(Order CurOrder)
		{

			// Only execute if valid order
			if (CurrentOrder != null)
			{

				// Delete all of the current items in the line items listing if populated
				LineItemList.Items.Clear();

				// Do for all line items 
				for (int u = 0; u < CurOrder.NumberOfLineItems(); u++)
				{
					LineItem Item;

					//  Get line item
					if (CurOrder.GetLineItem(out Item, u))
					{
						// Line Item complete.  If DeleteLineItemOnQuantity NOT set, change color of line item
						if ((!Params.DeleteLineItemWhenComplete) || (Item.QtyMeasured < Item.QtyRequired))
						{
							// Build the line item in the list view
							BuildListViewLineItem(ref Item);

						}
					}
				}

				LineItemList.Refresh();
			}
		}

		

	
		private void OrderListing_SelectedValueChanged(object sender, EventArgs e)
		{

			// Make sure a valid item selected
			if (OrderListing.SelectedIndex != -1)
			{


				// Make sure a valid order exists
				if (OrdersList.GetExistingOrder(out CurrentOrder, OrderListing.SelectedIndex))
				{

					// Show the line item list
					ShowOrderLineItemList(CurrentOrder);

					// Populate customer bill and ship fields
					CustomerInfo Customer;

					// Get Bill To customer data
					if (CurrentOrder.GetCustomerInfoData(out Customer, CustomersTypes.Billing))
					{
						BillCustomer.Text = Customer.CustomerName;
						BillAddr1.Text = Customer.Address1;
						BillAddr2.Text = Customer.Address2;
						BillCity.Text = Customer.City;
						BillState.Text = Customer.State;
						BillZip.Text = Customer.Zip;
						BillCountry.Text = Customer.Country;
						BillContact.Text = Customer.ContactName;
						BillTelephone.Text = Customer.TelephoneNumber;
						BillEmail.Text = Customer.Email;

					}

					// Ship To customer
					if (CurrentOrder.GetCustomerInfoData(out Customer, CustomersTypes.Shipping))
					{
						ShipCustomer.Text = Customer.CustomerName;
						ShipAddr1.Text = Customer.Address1;
						ShipAddr2.Text = Customer.Address2;
						ShipCity.Text = Customer.City;
						ShipState.Text = Customer.State;
						ShipZip.Text = Customer.Zip;
						ShipCountry.Text = Customer.Country;
						ShipContact.Text = Customer.ContactName;
						ShipTelephone.Text = Customer.TelephoneNumber;
						ShipEmail.Text = Customer.Email;

					}

					if ((CurrentOrder.TestThicknessDimFieldDefined) && (Params.ZAxisInUse))
						OrderUsesDepthLabel.Visible = true;

					

					else
						OrderUsesDepthLabel.Visible = false;



 				}
 			}
 		}




		private void LineItemList_MouseDoubleClick(object sender, MouseEventArgs e)
		{

			if (e.Button == MouseButtons.Left)
			{

				LineItemContextOptions.Show(LineItemList, e.Location);

			}

		}





		private void LineItemDetailToolButton_Click(object sender, EventArgs e)
		{

			ListView.SelectedIndexCollection Indices = LineItemList.SelectedIndices;

			if (Indices.Count == 1)
			{
				int Index = Indices[0];


				// Get the line number text of the selected line
				string LineItemStr = LineItemList.Items[Index].Text;

				// Get the line item based on the line item number string
				LineItem Item;
				if (CurrentOrder.GetLineItem(out Item, LineItemStr))
				{
					FullLineItemDialog Dlg = new FullLineItemDialog(ref Item, false, 0.0, 0.0, 0.0, 0.0, 0.0, CurrentOrder.TestThicknessDimFieldDefined && Params.ZAxisInUse);

					// Show the dialog to show the full line item description
					Dlg.ShowDialog();
				}
			}

			else
				MessageBox.Show("An order and line item must first be selected", "Select Line Item", MessageBoxButtons.OK, MessageBoxIcon.Error);


		}



		private void DeleteMeasurementToolButton_Click(object sender, EventArgs e)
		{

			
			ListView.SelectedIndexCollection Indices = LineItemList.SelectedIndices;

			if (Indices.Count == 1)
			{
				int Index = Indices[0];


				// Get the line number text of the selected line
				string LineItemStr = LineItemList.Items[Index].Text;

				// Get the line item based on the line item number string
				LineItem Item;
				if (CurrentOrder.GetLineItem(out Item, LineItemStr))
				{

					if (Item.MeasuredPartsCount == 0)
						MessageBox.Show(string.Format("No measured parts for line item: {0}", LineItemStr), "No Measured Parts", MessageBoxButtons.OK, MessageBoxIcon.Information);

					else
					{

						if (MessageBox.Show(string.Format("WARNING:  YOU ARE ABOUT TO DELETE THE LAST MEASURED PART FOR LINE ITEM: {0}", LineItemStr) + "\nTHIS CANNOT BE UNDONE!  CONTINUE?", "Delete Measurement", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.Yes)
						{

							// Delete the last measurement for this line item
							Item.RemoveLastMeasuredPart();

							// Rebuild the line item listing
							ShowOrderLineItemList(CurrentOrder);

							// Determine if order is complete.  If so, process it
							if (CurrentOrder.IsOrderComplete() && !Params.DoNotProcessCompletedOrders)
								ProcessCompletedOrder();

							// Tell user that item measurement was removed
							MessageBox.Show(string.Format("Last measurement removed for line item: {0}", LineItemStr), "Measurement Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
			}

			else
				MessageBox.Show("An order and line item must first be selected", "Select Line Item", MessageBoxButtons.OK, MessageBoxIcon.Error);


		}






		private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{

			LineItemContextOptions.Close();

			string Operation = e.ClickedItem.Name;

			ListView.SelectedIndexCollection Indices = LineItemList.SelectedIndices;

			int Index = Indices[0];

			// Get the line number text of the selected line
			string LineItemStr = LineItemList.Items[Index].Text;

			// Get the line item based on the line item number string
			LineItem Item;
			if (CurrentOrder.GetLineItem(out Item, LineItemStr))
			{


				// Edit the line item
				if (Operation == "EditLineItem")
				{

					LineItemEditorDialog Dlg = new LineItemEditorDialog(ref Item, Params);

					// Show the dialog
					if (Dlg.ShowDialog() == DialogResult.OK)
					{

						// Rebuild the line item listing
						ShowOrderLineItemList(CurrentOrder);

						// Determine if order is complete.  If so, process it
						if (CurrentOrder.IsOrderComplete() && !Params.DoNotProcessCompletedOrders)
							ProcessCompletedOrder();

					}
				}

				else if (Operation == "ViewFullLineItem")
				{

					FullLineItemDialog Dlg = new FullLineItemDialog(ref Item, false, 0.0, 0.0, 0.0, 0.0, 0.0, CurrentOrder.TestThicknessDimFieldDefined && Params.ZAxisInUse);

					// Show the dialog to show the full line item description
					Dlg.ShowDialog();


				}

				else if (Operation == "ViewMeasuredParts")
				{

					if (Item.MeasuredPartsCount == 0)
						MessageBox.Show(string.Format("No measured parts for line item: {0}", LineItemStr), "No Measured Parts", MessageBoxButtons.OK, MessageBoxIcon.Information);

					else
					{
						ViewMeasuredPartsDialog Dlg = new ViewMeasuredPartsDialog(ref Item);

						// Show the dialog
						Dlg.ShowDialog();
					}
				}

				else if(Operation == "deleteLastLineItemMeasurement")
				{

					if (Item.MeasuredPartsCount == 0)
						MessageBox.Show(string.Format("No measured parts for line item: {0}", LineItemStr), "No Measured Parts", MessageBoxButtons.OK, MessageBoxIcon.Information);

					else
					{

						if (MessageBox.Show(string.Format("WARNING:  YOU ARE ABOUT TO DELETE THE LAST MEASURED PART FOR LINE ITEM: {0}", LineItemStr) + "\nTHIS CANNOT BE UNDONE!  CONTINUE?", "Delete Measurement", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.Yes)
						{

							// Delete the last measurement for this line item
							Item.RemoveLastMeasuredPart();

							// Rebuild the line item listing
							ShowOrderLineItemList(CurrentOrder);

							// Determine if order is complete.  If so, process it
							if (CurrentOrder.IsOrderComplete() && !Params.DoNotProcessCompletedOrders)
								ProcessCompletedOrder();

							// Tell user that item measurement was removed
							MessageBox.Show(string.Format("Last measurement removed for line item: {0}", LineItemStr), "Measurement Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}


			}

			else
			{
				MessageBox.Show("Internal Error: Line number not found", "Internal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			

		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{

			Application.Exit();


		}


		private void ShowOptionsDialog()
		{


//			OptionsDialog Dlg = new OptionsDialog(Params, MeasurementHardware, MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis], MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis], MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.ZAxis], MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.AngleAxis], _license);
			OptionsDialog Dlg = new OptionsDialog(Params, MeasurementHardware, MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis], MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis], MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.ZAxis], MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.AngleAxis]);

			if (Dlg.ShowDialog() == DialogResult.OK)
			{
				// Pass the original data objects and save them with the updated data
				Dlg.SaveParameterData(ref Params, ref MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis], ref MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis], ref MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.ZAxis], ref MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.AngleAxis]);

				// Update the line item list view to reflect any changes in system options.
				if (CurrentOrder != null)
					ShowOrderLineItemList(CurrentOrder);

				// Show manual measure menu button ONLY if manual measure entry allowed
				if (Params.AllowManualMeasurements)
				{
					simulateMeasurementToolStripMenuItem.Visible = true;
					ManualMeasurementToolButton.Visible = true;
				}

				else
				{
					simulateMeasurementToolStripMenuItem.Visible = false;
					ManualMeasurementToolButton.Visible = false;
				}


				// If Z axis not enabled, hide related controls
				if (!Params.ZAxisInUse)
				{
					DepthLabel.Visible = false;
					DepthPosition.Visible = false;
					DatumDepthToolButton.Visible = false;
					setDepthDatumToolStripMenuItem.Visible = false;

				}

				// Show Z
				else
				{
					DepthLabel.Visible = true;
					DepthPosition.Visible = true;
					DatumDepthToolButton.Visible = true;
					setDepthDatumToolStripMenuItem.Visible = true;
				}

				// If Angle enabled, show field and label
				if (Params.AngleAxisInUse)
				{
					AngleLabel.Visible = true;
					AnglePosition.Visible = true;
					DatumAngleToolButton.Visible = true;

					MeasurementHardware.UseAngleAxis = true;


				}

				// Angle not enabled
				else
				{
					AngleLabel.Visible = false;
					AnglePosition.Visible = false;
					DatumAngleToolButton.Visible = false;

					MeasurementHardware.UseAngleAxis = false;
				}




				// Show/hide process incomplete orders button based on state
				if (Params.AllowIncompleteOrdersToBeProcessed)
					ProcessAsCompleteToolButton.Visible = true;

				else
					ProcessAsCompleteToolButton.Visible = false;


				partErrorLabelsToolStripMenuItem.Visible = true;

				// Handle swapping of axes
				if (!Params.SwapHeightAndWidthAxes)
				{

					MeasurementHardware.WidthUpdate -= new MeasurementInterfaceHardware.UpdateWidthPosition(MeasurementHardware_HeightUpdate);
					MeasurementHardware.HeightUpdate -= new MeasurementInterfaceHardware.UpdateHeightPosition(MeasurementHardware_WidthUpdate);
				
					MeasurementHardware.WidthUpdate += new MeasurementInterfaceHardware.UpdateWidthPosition(MeasurementHardware_WidthUpdate);
					MeasurementHardware.HeightUpdate += new MeasurementInterfaceHardware.UpdateHeightPosition(MeasurementHardware_HeightUpdate);

				}

				else
				{

					MeasurementHardware.WidthUpdate -= new MeasurementInterfaceHardware.UpdateWidthPosition(MeasurementHardware_WidthUpdate);
					MeasurementHardware.HeightUpdate -= new MeasurementInterfaceHardware.UpdateHeightPosition(MeasurementHardware_HeightUpdate);


					MeasurementHardware.WidthUpdate += new MeasurementInterfaceHardware.UpdateWidthPosition(MeasurementHardware_HeightUpdate);
					MeasurementHardware.HeightUpdate += new MeasurementInterfaceHardware.UpdateHeightPosition(MeasurementHardware_WidthUpdate);
				}

				

			}

			// If the user deactivated the software, exit the app NOW.
			if(Dlg.Deactivated)
				Application.Exit();

			// Allow updates
//			MeasurementHardware.DisableDisplayUpdate(false);

		}

		private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// Suppress updates while cloning
			MeasurementHardware.DisableDisplayUpdate(true);

			OptionLauncherTimer.Start();

		
		}


		private void MeasureButton_Click(object sender, EventArgs e)
		{

			if (CurrentOrder != null)
			{
				// Create a measurement reference
				MeasuredPartInstance Measurement = CreateMeasurement();

				// Attempt to add to order
				AddMeasurementToOrder(ref Measurement);
			}

			else
			{
				MessageBox.Show("An order must be selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			
  		}

		private void openBatchOrderFileToolStripMenuItem_Click(object sender, EventArgs e)
		{

			StreamReader BatchFileStreamIn;

			OpenFileDialog OpenFileDlg = new OpenFileDialog();

			string CSVFilePath;

			OpenFileDlg.InitialDirectory = Params.CSVBatchFilePathStr;
			OpenFileDlg.Filter = "TXT Files (*.txt)|*.txt";
			OpenFileDlg.FilterIndex = 1;
			OpenFileDlg.RestoreDirectory = true;

			// Ok pressed from dialog
			if (OpenFileDlg.ShowDialog() == DialogResult.OK)
			{
				// File name not null
				if (OpenFileDlg.FileName.Length != 0)
				{

					// List for error log
					List<string> ErrorLog = new List<string>();

					// Try to open batch file
					try
					{
						BatchFileStreamIn = File.OpenText(OpenFileDlg.FileName);

						string BatchLine = "";

						// Read each line of batch file.  Each line is name of single order CSV file
						while ((BatchLine = BatchFileStreamIn.ReadLine()) != null)
						{

							// Create an array of strings to pass to the line parser
							string[] Columns;

							Char Delimit = ',';

							Columns = BatchLine.Split(Delimit);

							string TemplateName = Columns[1].Trim();

							// If default path ends with backslash, don't concatentate with another
							if (Params.CSVFilePathStr.EndsWith(@"\"))
							{

								// Build the default CSV file path + relative file name
								CSVFilePath = string.Format(@"{0}{1}", Params.CSVFilePathStr, Columns[0]);
							}

							// Add backslash
							else
							{

								// Build the default CSV file path + relative file name
								CSVFilePath = string.Format(@"{0}\{1}", Params.CSVFilePathStr, Columns[0]);
							}


							// An empty list of orders to pass to the reader to hold parsed orders
							SystemOrders NewOrders = new SystemOrders();

							// Create a single order reader
							SingleOrderCSVReader CSVReader = new SingleOrderCSVReader();

							// Open the file and parse
							CSVReaderResultCodes Result = CSVReader.OpenCSVFile(CSVFilePath, ref Params, ref ErrorLog, ref NewOrders, Params.GetSOTemplateIndex(TemplateName));

							if (Result == CSVReaderResultCodes.Success)
							{
								// Add order ID to order listing and new orders to order lists
								for (int u = 0; u < NewOrders.NumberOfOrders(); u++)
								{
									// Get the order from the list
									Order OrderItem;
									NewOrders.GetExistingOrder(out OrderItem, u);

									// Make sure order is valid
									if (OrderItem != null)
									{
										// Add the individual order to the application's list of orders in memory
										// If not duplicate, add order ID to order listing
										if (OrdersList.AddOrder(ref OrderItem))
										{
											// Add the OrderID to the order listing object
//											OrderListing.Items.Add(OrderItem.OrderNumber);
											OrderListing.Items.Clear();

											for (int v = 0; v < OrdersList.Count(); v++)
												OrderListing.Items.Add(OrdersList.ElementAt(v).OrderNumber);

										}

										// A duplicate order
										else
										{
											// Build duplicate error message string
											string Msg = string.Format("Error - Order {0} already exists in the list of orders and was not duplicated", OrderItem.OrderNumber);

											// Add it to the log
											ErrorLog.Add(Msg);
										}
									}
								}
							}

						}

						// Close the batch file
						BatchFileStreamIn.Close();

						// Check if any errors in log.  If so, show in dialog
						if (ErrorLog.Count > 0)
						{
							ErrorListDialog ErrorDlg = new ErrorListDialog(ref ErrorLog);

							ErrorDlg.ShowDialog();

						}

						// All files imported sucessfully
						else
						{
							MessageBox.Show(string.Format("CSV Batch file: {0} imported successfully.", OpenFileDlg.FileName), "Import Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}

					}

					// A problem opening the batch file
					catch (Exception ex)
					{
						ErrorLog.Add(ex.Message);
					}

				}

			}
		}

		private void fileToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
		{

			// Only show batch file menu option when single order template selected
			if (Params.CSVFileTypeInUse == CSVFileType.SingleOrderFile)
				openBatchOrderFileToolStripMenuItem.Visible = true;

			else
				openBatchOrderFileToolStripMenuItem.Visible = false;

		}


		private MeasuredPartInstance CreateMeasurement()
		{

			double Width;
			double Height;
			double Thickness;
			double Angle;

			// Swap Width and Height axes if necessary
			if (Params.SwapHeightAndWidthAxes)
			{

				// Get the current positions for the measurement hardware
				Width = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis].GetNativePosition();
				Height = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis].GetNativePosition();

				// Only get data from hardware if enabled
				if (Params.ZAxisInUse)
					Thickness = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.ZAxis].GetNativePosition();

				// Not enabled
				else
					Thickness = 0.0;

				// Add angle here...
				if (Params.AngleAxisInUse)
					Angle = ConvertAngleAxisToAngle();

				else
					Angle = 90.0;


			}

			// No Swap
			else
			{

				// Get the current positions for the measurement hardware
				Width = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis].GetNativePosition();
				Height = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis].GetNativePosition();

				// Only get data from hardware if enabled
				if (Params.ZAxisInUse)
					Thickness = MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.ZAxis].GetNativePosition();

				// Not enabled
				else
					Thickness = 0.0;


				// Add angle here...
				if (Params.AngleAxisInUse)
					Angle = ConvertAngleAxisToAngle();

				else
					Angle = 90.0;


			}
			// Create a new Measurement
			MeasuredPartInstance Measurement = new MeasuredPartInstance(Width, Height, Thickness, Angle, CurrentOrder.CSVMeasurementUnits, Params.UseNorthAmericanDateFormat, false);

			return Measurement;
			
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{

			// Save OrdersList when closing
			OrdersList.SaveParameters();

			// Save the layout for the Item List View.
			SaveItemListViewColumns();


		}


		private void IncompleteOrderToolButton_Click(object sender, EventArgs e)
		{

			if (OrderListing.SelectedIndex != -1)
			{

				CreateIncompleteOrderReport();

			}

			else
				MessageBox.Show("An Order must first be selected.", "Select Order", MessageBoxButtons.OK, MessageBoxIcon.Error);

		}


		private void OrderListing_MouseDoubleClick(object sender, MouseEventArgs e)
		{

			if (e.Button == MouseButtons.Left)
			{

				if (OrderListing.SelectedIndex != -1)
				{

					// Get the rectangle for the selected area
					Rectangle R = OrderListing.GetItemRectangle(OrderListing.SelectedIndex);

					// Make sure the mouse is in the selected area
					if (R.Contains(e.Location))
					{
						// Check if individual orders can be deleted.  Set enable/disable appropriately
						if (Params.IndividualOrderDeletionAllowed)
							OrdersContextOptions.Items[0].Visible = true;

						else
							OrdersContextOptions.Items[0].Visible = false;

						// Prior to poping context menu, check if CurrentOrder (selected)
						// is in a state of complete (but not saved since it still exists in the list)
						// If so, enable the "save complete order item in the context menu.  If not
						// complete, disable this item
						if (CurrentOrder.IsOrderComplete())
						{
							saveCompletedOrderToolStripMenuItem.Visible = true;
						}

						else
						{
							saveCompletedOrderToolStripMenuItem.Visible = false;
						}



						// Show the popup menu
						OrdersContextOptions.Show(OrderListing, e.Location);
					}
				}

			}

		}


		private void deleteOrderToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// Delete the current order
			DeleteCurrentOrder();
			

		}

		private void deleteCurrentOrderToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// Delete the current order
			DeleteCurrentOrder();

		}

		private void ordersToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
		{

			// Enable/disable the menu item for the individual order delete based on the param
			if (Params.IndividualOrderDeletionAllowed)
				deleteCurrentOrderToolStripMenuItem.Enabled = true;

			else
				deleteCurrentOrderToolStripMenuItem.Enabled = false;


			// Enable/disable the menu item for the Delete All Orders
			if (Params.AllOrderDeletionAllowed)
				deleteAllOrdersToolStripMenuItem.Enabled = true;

			else
				deleteAllOrdersToolStripMenuItem.Enabled = false;


			// Check if Process Incomplete orders enabled
			processOrderAsCompleteToolStripMenuItem.Enabled = Params.AllowIncompleteOrdersToBeProcessed;


		}


		// Clears the customer info fields when an order is deleted/complete
		private void ClearCustomerData()
		{

			BillCustomer.Text = "";
			BillAddr1.Text = "";
			BillAddr2.Text = "";
			BillCity.Text = "";
			BillState.Text = "";
			BillZip.Text = "";
			BillCountry.Text = "";
			BillContact.Text = "";
			BillTelephone.Text = "";


			ShipCustomer.Text = "";
			ShipAddr1.Text = "";
			ShipAddr2.Text = "";
			ShipCity.Text = "";
			ShipState.Text = "";
			ShipZip.Text = "";
			ShipCountry.Text = "";
			ShipContact.Text = "";
			ShipTelephone.Text = "";

		}


		private void deleteAllOrdersToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (OrdersList.NumberOfOrders() > 0)
			{
				// Remove all orders from memory
				OrdersList.RemoveAllOrders();

				// Delete all of the current items in the line items listing if populated
				LineItemList.Items.Clear();

				// Remove all of the orders from the order list
				OrderListing.Items.Clear();

				// Clear current order
				CurrentOrder = (Order)null;

				// Clear the customer data
				ClearCustomerData();

			}

		}

	

		private void saveCompletedOrderToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// Attempt to save the order file again
			SaveOrderCSVFile();

		}

		// Open password entry dialog to log into configuration editor
		private void logInToolStripMenuItem_Click(object sender, EventArgs e)
		{

			PasswordDialog Dlg = new PasswordDialog();

			if (Dlg.ShowDialog() == DialogResult.OK)
			{
				if (Dlg.PasswordVal == Params.PasswordVal)
				{
					logOutToolStripMenuItem.Enabled = true;
					logInToolStripMenuItem.Enabled = false;
					optionsToolStripMenuItem.Enabled = true;


					// Suppress updates while cloning
					MeasurementHardware.DisableDisplayUpdate(true);

					// Start timer to show the options dialog.
					OptionLauncherTimer.Start();


				}
			}
		}

		// Log out of configuration editor
		private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
		{

			logInToolStripMenuItem.Enabled = true;
			logOutToolStripMenuItem.Enabled = false;
			optionsToolStripMenuItem.Enabled = false;


		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// Determine the version type
			string VersionStr = "";

			AboutBox1 Dlg = new AboutBox1(VersionStr);

			Dlg.ShowDialog();

		}

		private void axesDatumToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
		{
			if (Params.ZAxisInUse)
				setDepthDatumToolStripMenuItem.Visible = true;

			else
				setDepthDatumToolStripMenuItem.Visible = false;
		}


		private void setWidthDatumToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (MessageBox.Show("Confirm to set Width Datum?", "Set Width Datum", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
			{

				if (!Params.SwapHeightAndWidthAxes)
				{
					// Update the datum
					MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis].ApplyDatumPreset();
				}

				// Axes swapped
				else
				{
					// Update the datum
					MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis].ApplyDatumPreset();

				}
					
				// Update the readout
				MeasurementHardware_WidthUpdate();
			}
		}

		private void setHeightDatumToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (MessageBox.Show("Confirm to set Height Datum?", "Set Height Datum", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
			{
				if (!Params.SwapHeightAndWidthAxes)
				{
					// Update the datum
					MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.YAxis].ApplyDatumPreset();
				}


				// Axes swapped
				else
				{
					// Update the datum
					MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.XAxis].ApplyDatumPreset();
				}

				// Update the readout
				MeasurementHardware_HeightUpdate();	

			}

		}

		private void setDepthDatumToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (MessageBox.Show("Confirm to set Depth Datum?", "Set Depth Datum", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
			{
				// Update the datum
				MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.ZAxis].ApplyDatumPreset();

				// Update the readout
				MeasurementHardware_DepthUpdate();

			}

		}

		private void DatumAngleToolButton_Click(object sender, EventArgs e)
		{

			if (MessageBox.Show("Confirm to set Angle Datum?", "Set Angle Datum", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
			{
				// Update the datum
				MeasurementHardware.Axes[(int)MeasurementInterfaceHardware.AxisNames.AngleAxis].ApplyDatumPreset();

				// Update the readout
				MeasurementHardware_AngleUpdate();

			}

		}

		// Creates a simulated part measurement for use in trial simulation
		private void simulateMeasurementToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (CurrentOrder != null)
			{
				bool UseThickness = false;

				// Check if to use thickness field
				if ((Params.ZAxisInUse) && (CurrentOrder.TestThicknessDimFieldDefined))
					UseThickness = true;
				
				SimulatedMeasurementDialog Dlg = new SimulatedMeasurementDialog(Params, UseThickness);

				if (Dlg.ShowDialog() == DialogResult.OK)
				{
					// Create a simulated measurement from the dialog fields
					MeasuredPartInstance Measurement = new MeasuredPartInstance(Dlg.SimWidth, Dlg.SimHeight, Dlg.SimThickness, 90.0, CurrentOrder.CSVMeasurementUnits, Params.UseNorthAmericanDateFormat, true);

					// Attempt to add to order
					AddMeasurementToOrder(ref Measurement);
				}
			}

			// No current order active
			else
			{
				MessageBox.Show("There is no active order selected", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			SendKeys.Send("{F1}");
		}


		private void GenerateUserDefinedIncompleteOrderReport()
		{

			DataBoundReport report;

			report = new DataBoundReport();

			DataSet DataSource;

			report.GetDataSource(out DataSource);

			// Populate the data source with common report data
			PopulateReportCommonData(ref DataSource);


			// For each line item in order....
			for (int u = 0; u < CurrentOrder.NumberOfLineItems(); u++)
			{
				LineItem LI;

				// Get the line item
				if (CurrentOrder.GetLineItem(out LI, u))
				{
					// If the line item quantity != measured quantity...
					if (!LI.IsQuantityComplete)
					{
						// Populate the line item data source
						PopulateLineItemDataSource(ref DataSource, LI);

						// Create an empty part measurement.  Data not used here
						MeasuredPartInstance Part = new MeasuredPartInstance();

						// Populate the Measured part data source
						PopulateMeasuredPartDataSource(ref DataSource, Part, LI.LineNumber);
					}
				}
			}

			// Update the report's data source
			report.SetDataSource(DataSource);



			try
			{

				string TemplateFile = Params.PackListTemplateFilePathStr + "\\Incomplete Order Report.repx";

				if (File.Exists(TemplateFile))
				{
					// Load template file
					report.LoadLayout(TemplateFile);

					// Set report printer name
					report.PrinterName = Params.ReportPrinterNameStr;

					// If using print preview, tell user and start preview
					if (Params.UseReportPrintPreview)
					{
						// Show a time dialog telling user to wait for report manager to open
						TimedDialog Dlg = new TimedDialog("Print Preview Starting.  Please wait.", "Starting Print Preview", 1500);
						Dlg.ShowDialog();

						report.ShowPreviewDialog();
					}

					// else, just print
					else
					{
						report.Print();
					}
				}

				// Specified file does not exists
				else
					MessageBox.Show("Incomplete Report template file does not exist.\nReport printing aborted", "Incomplete Report Template File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);


			}

			catch
			{
				MessageBox.Show("Unable to open a valid Incomplete Order report template.  Printing aborted.", "Incomplete Order Report Template Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}

		}

		private void CreateIncompleteOrderReport()
		{

			if (CurrentOrder != (Order)null)
			{

				GenerateUserDefinedIncompleteOrderReport();

			}

			else
				MessageBox.Show("There is no active order selected", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}



		private void incompleteOrderReportToolStripMenuItem_Click(object sender, EventArgs e)
		{


			CreateIncompleteOrderReport();

		}

		private void incompleteOrderReportToolStripMenuItem1_Click(object sender, EventArgs e)
		{

			CreateIncompleteOrderReport();

		}

		private void outOfSquareToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (CurrentOrder != null)
			{

				if (MessageBox.Show("Move height and width gantries against part.\nPress OK.", "Process Defective Part for Out Of Square", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
				{

					// Create a measurement reference
					MeasuredPartInstance Measurement = CreateMeasurement();

					LineItem LI;

					LI = LocateDefectLineItemInOrder(ref Measurement);

					if ((LI) != null)
					{
						GenerateDefectLabel(LI, Measurement, Params.PartLabelTemplateFilePathStr + OutOfSquareDefectLabelFileName);
					}

					else
					{
						if (MessageBox.Show("No line item found.  Do you wish\nto print a defect label anyway?", "No Line Item Found", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							GenerateDefectLabel(LI, Measurement, Params.PartLabelTemplateFilePathStr + OutOfSquareDefectLabelFileName);
						}
					}

				}
			}

			else
				MessageBox.Show("An Order must be selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);

		}

		private void finishDefectToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (CurrentOrder != null)
			{
				if (MessageBox.Show("Move height and width gantries against part.\nPress OK.", "Process Defective Part for Finish Defect", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
				{

					// Create a measurement reference
					MeasuredPartInstance Measurement = CreateMeasurement();

					LineItem LI;

					LI = LocateDefectLineItemInOrder(ref Measurement);

					if ((LI) != null)
					{
						GenerateDefectLabel(LI, Measurement, Params.PartLabelTemplateFilePathStr + FinishDefectLabelFileName);
					}

					else
					{
						if (MessageBox.Show("No line item found.  Do you wish\nto print a defect label anyway?", "No Line Item Found", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							GenerateDefectLabel(LI, Measurement, Params.PartLabelTemplateFilePathStr + FinishDefectLabelFileName);
						}
					}

				}
			}

			else
				MessageBox.Show("An Order must be selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);


		}

		private void otherToolStripMenuItem_Click(object sender, EventArgs e)
		{

			if (CurrentOrder != null)
			{
				if (MessageBox.Show("Move height and width gantries against part.\nPress OK.", "Process Defective Part for Other Defect", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
				{

					// Create a measurement reference
					MeasuredPartInstance Measurement = CreateMeasurement();

					LineItem LI;

					LI = LocateDefectLineItemInOrder(ref Measurement);

					if ((LI) != null)
					{
						GenerateDefectLabel(LI, Measurement, Params.PartLabelTemplateFilePathStr + MiscDefectLabelFileName);
					}

					else
					{
						if (MessageBox.Show("No line item found.  Do you wish\nto print a defect label anyway?", "No Line Item Found", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							GenerateDefectLabel(LI, Measurement, Params.PartLabelTemplateFilePathStr + MiscDefectLabelFileName);
						}
					}
				}
			}

			else
				MessageBox.Show("An Order must be selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);

			
		}

		public string GetCurrentDateString()
		{


			// Get current time
			DateTime Time = DateTime.Now;
			String DateStr;
			string MonthStr;
			string DayStr;


			if (Time.Month < 10)
				MonthStr = string.Format("0{0}", Time.Month);

			else
				MonthStr = Time.Month.ToString();


			if (Time.Day < 10)
				DayStr = string.Format("0{0}", Time.Day);

			else
				DayStr = Time.Day.ToString();



			if (Params.UseNorthAmericanDateFormat)
			{
				// Build Date string in north american format   (month-day-year)
				DateStr = string.Format("{0}-{1}-{2}", MonthStr, DayStr, Time.Year.ToString());

			}

			else
			{
				// Build string in European format (day-month-year)
				DateStr = string.Format("{0}-{1}-{2}", DayStr, MonthStr, Time.Year.ToString());
			}

			return DateStr;


		}

		private void PopulateReportCommonData(ref DataSet DataSource)
		{

			// *** CUSTOMER SHIP DATA ***

			DataRow ShipRow = DataSource.Tables["CustomerShipInfo"].NewRow();

			// Populate the Ship To Customer Data
			CustomerInfo Customer;

			// Get the bill customer from the order
			CurrentOrder.GetCustomerInfoData(out Customer, CustomersTypes.Shipping);

			ShipRow["ShipName"] = Customer.CustomerName;
			ShipRow["ShipAddress1"] = Customer.Address1;
			ShipRow["ShipAddress2"] = Customer.Address2;
			ShipRow["ShipCity"] = Customer.City;
			ShipRow["ShipState"] = Customer.State;
			ShipRow["ShipZip"] = Customer.Zip;
			ShipRow["ShipCountry"] = Customer.Country;
			ShipRow["ShipContact"] = Customer.ContactName;
			ShipRow["ShipTelephone"] = Customer.TelephoneNumber;
			ShipRow["ShipEmail"] = Customer.Email;

			DataSource.Tables["CustomerShipInfo"].Rows.Add(ShipRow);


			// *** CUSTOMER BILL DATA ***

			DataRow BillRow = DataSource.Tables["CustomerBillInfo"].NewRow();

			// Get the bill customer from the order
			CurrentOrder.GetCustomerInfoData(out Customer, CustomersTypes.Billing);

			BillRow["BillName"] = Customer.CustomerName;
			BillRow["BillAddress1"] = Customer.Address1;
			BillRow["BillAddress2"] = Customer.Address2;
			BillRow["BillCity"] = Customer.City;
			BillRow["BillState"] = Customer.State;
			BillRow["BillZip"] = Customer.Zip;
			BillRow["BillCountry"] = Customer.Country;
			BillRow["BillContact"] = Customer.ContactName;
			BillRow["BillTelephone"] = Customer.TelephoneNumber;
			BillRow["BillEmail"] = Customer.Email;

			DataSource.Tables["CustomerBillInfo"].Rows.Add(BillRow);


			// *** COMPANY DATA ***

			DataRow CompanyRow = DataSource.Tables["CompanyData"].NewRow();

			// Populate the data source with the company data info
			CompanyRow["CompanyName"] = Params.CompanyNameStr;
			CompanyRow["CompanyAddress1"] = Params.CompanyAddr1Str;
			CompanyRow["CompanyAddress2"] = Params.CompanyAddr2Str;
			CompanyRow["CompanyCity"] = Params.CompanyCityStr;
			CompanyRow["CompanyState"] = Params.CompanyStateStr;
			CompanyRow["CompanyZip"] = Params.CompanyZipStr;
			CompanyRow["CompanyTelephone"] = Params.CompanyPhoneStr;
			CompanyRow["CompanyLogoFileName"] = Params.CompanyLogoFileStr;
			CompanyRow["CompanyURL"] = Params.CompanyURLStr;
			CompanyRow["CompanyCountry"] = Params.CompanyCountryStr;


			DataSource.Tables["CompanyData"].Rows.Add(CompanyRow);



			// *** ORDER DATA ***

			DataRow OrderRow = DataSource.Tables["OrderData"].NewRow();

			// Populate the data source with order data
			OrderRow["OrderID"] = CurrentOrder.OrderNumber;

			// Set string for units type
			if (CurrentOrder.CSVMeasurementUnits == MeasuringUnits.Inches)
				OrderRow["MeasurementUnits"] = "IN";

			else
				OrderRow["MeasurementUnits"] = "MM";


			// Create the measurement date
			OrderRow["MeasurementCompletionDate"] = GetCurrentDateString();

			// Create order user defined 1
			OrderRow["OrderUserDef1"] = CurrentOrder.UserDefined1Text;

			DataSource.Tables["OrderData"].Rows.Add(OrderRow);

		}


		private void GenerateUserDefinedPackList()
		{

			try
			{

				DataBoundReport report;

				report = new DataBoundReport();

				DataSet DataSource;

				report.GetDataSource(out DataSource);

				try
				{

					// Populate the data source with common report data
					PopulateReportCommonData(ref DataSource);


					try
					{
						DataRow LineItemRow;
						DataRow MeasuredPartsRow;

						// For each line item in order, populate line item data in DataSource
						for (int u = 0; u < CurrentOrder.NumberOfLineItems(); u++)
						{
							LineItem LI;

							// Get the line item
							if (CurrentOrder.GetLineItem(out LI, u))
							{

								// Create a new row in the line item data source table
								LineItemRow = DataSource.Tables["LineItems"].NewRow();

								// Populate line item columns in data source
								LineItemRow["LineItemNumber"] = LI.LineNumber.ToString();
								LineItemRow["PartID"] = LI.PartIdStr;
								LineItemRow["QuantityRequired"] = LI.QtyRequired;
								LineItemRow["QuantityMeasured"] = LI.QtyMeasured;
								LineItemRow["NominalWidth"] = LI.Width;
								LineItemRow["NominalHeight"] = LI.Height;
								LineItemRow["NominalThickness"] = LI.Thickness;
								LineItemRow["Material"] = LI.MaterialText;
								LineItemRow["Style"] = LI.StyleText;

								LineItemRow["Hinging"] = LI.HingingText;
								LineItemRow["Finish"] = LI.FinishText;
								LineItemRow["Type"] = LI.TypeText;
								LineItemRow["Location"] = LI.LocationText;
								LineItemRow["Machining"] = LI.MachiningText;
								LineItemRow["Assembly"] = LI.AssemblyText;
								LineItemRow["UserMsg"] = LI.UserMessageText;

								LineItemRow["Comment"] = LI.CommentText;
								LineItemRow["User1"] = LI.UserDef1Text;
								LineItemRow["User2"] = LI.UserDef2Text;
								LineItemRow["User3"] = LI.UserDef3Text;

								// Add row to table
								DataSource.Tables["LineItems"].Rows.Add(LineItemRow);


								try
								{
									// For each measured part in line item, add it
									for (int v = 0; v < LI.GetMeasuredPartsCount(); v++)
									{

										MeasuredPartInstance Part;

										// Get the measured part
										if (LI.GetMeasuredPart(out Part, v))
										{
											// Create a new row in measured part data source
											MeasuredPartsRow = DataSource.Tables["Measurement"].NewRow();

											// Populate measured part instances for line item
											MeasuredPartsRow["MeasuredWidth"] = Part.Width;

											MeasuredPartsRow["MeasuredHeight"] = Part.Height;
											MeasuredPartsRow["MeasuredDepth"] = Part.Thickness;

											if (Part.WidthInSpec)
												MeasuredPartsRow["WidthInSpec"] = "OK";
											else
												MeasuredPartsRow["WidthInSpec"] = "FAIL";

											if (Part.HeightInSpec)
												MeasuredPartsRow["HeightInSpec"] = "OK";
											else
												MeasuredPartsRow["HeightInSpec"] = "FAIL";

											if (Part.ThicknessInSpec)
												MeasuredPartsRow["DepthInSpec"] = "OK";
											else
												MeasuredPartsRow["DepthInSpec"] = "FAIL";

											MeasuredPartsRow["TimeDate"] = Part.MeasuredTime;
											MeasuredPartsRow["PartInstanceNumber"] = Part.PartInstanceNumber;

											if (Part.IsManualMeasurement)
												MeasuredPartsRow["ManuallyMeasured"] = "Yes";
											else
												MeasuredPartsRow["ManuallyMeasured"] = "No";

											MeasuredPartsRow["LineItemNumber"] = LI.LineNumber;

											// Add row to table
											DataSource.Tables["Measurement"].Rows.Add(MeasuredPartsRow);


										}
									}
								}

								catch
								{
									MessageBox.Show("Exception in Measurement Report Data");

								}

							}
						}

					}

					catch
					{

						MessageBox.Show("Exception in Line Item Report Data");

					}

						// Update the report's data source
						report.SetDataSource(DataSource);



					try
					{

						string TemplateFile;

						// If a packlist template is defined in the Order, use it.
						if (CurrentOrder.PackListTemplateText != "")
						{
							// Be sure that specified template file exists.
							if (File.Exists(Params.PackListTemplateFilePathStr + "\\" + CurrentOrder.PackListTemplateText))
								TemplateFile = Params.PackListTemplateFilePathStr + "\\" + CurrentOrder.PackListTemplateText;

							// If not, use default template
							else
								TemplateFile = Params.PackListTemplateFilePathStr + "\\Default Pack List.repx";

						}

						// If no packlist template defined, use the default.
						else
							TemplateFile = Params.PackListTemplateFilePathStr + "\\Default Pack List.repx";

						report.LoadLayout(TemplateFile);

						// Set report printer name
						report.PrinterName = Params.ReportPrinterNameStr;

						// If using print preview, tell user and start preview
						if (Params.UseReportPrintPreview)
						{
							// Show a time dialog telling user to wait for report manager to open
							TimedDialog Dlg = new TimedDialog("Print Preview Starting.  Please wait.", "Starting Print Preview", 1500);
							Dlg.ShowDialog();

							// Show the print preview
							report.ShowPreviewDialog();
						}

						// else, just print
						else
						{
							report.Print();
						}



					}

					catch
					{
						MessageBox.Show("Unable to open a valid report template.  Printing aborted.", "Report Template Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}

					


				}

				catch
				{
					MessageBox.Show("Exception in report common data");
				}

			}

			catch
			{
				MessageBox.Show("Exception getting report or data source");
			}

				
		}



		private void processOrderAsCompletePopupToolStripMenuItem_Click(object sender, EventArgs e)
		{

			ProcessOrderAsComplete();

		}

		private void ProcessAsCompleteToolButton_Click(object sender, EventArgs e)
		{
			if(CurrentOrder != null)
				ProcessOrderAsComplete();

			// No order currently selected
			else
			{
				MessageBox.Show("A order must first be selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}



		private void processOrderAsCompleteToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// Process only if an order selected
			if (CurrentOrder != null)
				ProcessOrderAsComplete();

			// No order currently selected
			else
			{
				MessageBox.Show("A order must first be selected!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}


		private void ProcessOrderAsComplete()
		{

			// Only process if order is NOT complete
			if (!CurrentOrder.IsOrderComplete())
			{
				// Warn that order is not complete
				if (MessageBox.Show("Order " + CurrentOrder.OrderNumber + " is NOT complete.\nThere are one or more line items unfulfilled.\nDo you want to process as complete?", "Incomplete Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{

					// Ask if incomplete order report to be generated
					if(MessageBox.Show("Do you wish to print an Incomplete Order Report?", "Print Incomplete Order Report", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						CreateIncompleteOrderReport();
					
					// Process the order as complete
					ProcessCompletedOrder();

					
				}


			}

			// Order completed but still on list.  This would occur if the order was processed but unable to be saved.
			// Don't allow processing again.
			else
			{
				MessageBox.Show("Order is already complete but has not been saved.\nUse the \"Save Completed Order\" option to save the file.", "Order Already Completed", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		
		}

		private void OrdersContextOptions_Opening(object sender, CancelEventArgs e)
		{
			// Determine if process incomplete order option allowed
			processOrderAsCompletePopupToolStripMenuItem.Enabled = Params.AllowIncompleteOrdersToBeProcessed;

		}

		private void StatusBarTimer_Tick(object sender, EventArgs e)
		{

			SystemOrderStateLabel.Visible = false;
			StatusBarTimer.Enabled = false;
			

		}

		private void ExitToolButton_Click(object sender, EventArgs e)
		{
			// Save OrdersList when closing
//			OrdersList.SaveParameters();

			// Save the layout for the Item List View.
//			SaveItemListViewColumns();

			Application.Exit();
			
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void OptionLauncher_Tick(object sender, EventArgs e)
		{

			OptionLauncherTimer.Stop();

			ShowOptionsDialog();



		}


		private void RepositionObject(Control Obj, Point DefaultLocation, ref Point NewPos)
		{

			int NewX;
			int NewY;
	
			double Ratio = (DefaultLocation.X + (Obj.Width / 2.0)) / (LowerPanelDefaultSize.Width * 1.0);
		

			// Calculate the ratio from the minimum size to the current size
			NewX = (int) (Ratio * splitContainer1.Panel2.Width - (Obj.Width / 2.0));
			NewY = (int)((splitContainer1.Panel2.Height - Obj.Height) / 2) - 16;	 // 16 is fudge?

			Point Pos = new Point(NewX, NewY);

			 NewPos = Pos;


		}



		private void Form1_Resize(object sender, EventArgs e)
		{

			Point NewPos = new Point();

			// Reposition Width field
			RepositionObject(PositionGroupBox, PositionGroupBoxDefaultLocation, ref NewPos);
			PositionGroupBox.Location = NewPos;

			// Repositon Units
			RepositionObject(UnitsGroupBox, UnitsGroupBoxDefaultLocation, ref NewPos);
			UnitsGroupBox.Location = NewPos;

			// Reposition measure button
			RepositionObject(MeasureButton, MeasureButtonDefaultLocation, ref NewPos);
			MeasureButton.Location = NewPos;


			// Reposition Jobs label
			NewPos.Y = JobsTitleDefaultYPos;
			NewPos.X = (splitContainer2.Panel2.Width - JobsTitleLabel.Size.Width) / 2;

			JobsTitleLabel.Location = NewPos;

			// Reposition Jobs instruction label
			NewPos.Y = JobsInstructionDefaultYPos;
			NewPos.X = (splitContainer2.Panel2.Width - JobsInstructionsLabel.Size.Width) / 2;

			JobsInstructionsLabel.Location = NewPos;
		


		}

		private void Form1_ResizeEnd(object sender, EventArgs e)
		{



		}

		private void SortButton_Click(object sender, EventArgs e)
		{

			// Reverse the sort direction of the orders list
			OrdersList.ReverseList();

			OrderListing.Items.Clear();

			for (int v = 0; v < OrdersList.Count(); v++)
				OrderListing.Items.Add(OrdersList.ElementAt(v).OrderNumber);


		}

		private void instantProtection1_OnDetectionError(object sender, DetectionErrorEventArgs e)
		{
			if (e.ErrStr != "")
				MessageBox.Show(e.ErrStr, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			e.Terminated = true;
		}







	}
}
