﻿namespace ProCABQC11
{
	partial class OptionsDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsDialog));
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.AxisParameters = new System.Windows.Forms.TabPage();
            this.PivotDistVal = new System.Windows.Forms.TextBox();
            this.PivotDistLabel = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.EncoderNegDirSelect = new System.Windows.Forms.RadioButton();
            this.EncoderPosDirSelect = new System.Windows.Forms.RadioButton();
            this.ScaleFactorText = new System.Windows.Forms.TextBox();
            this.DatumPresetText = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.AxisSelect = new System.Windows.Forms.ComboBox();
            this.InterfaceHardware = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.LabelPrinterComboBox = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.ReportPrinterComboBox = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.UseAngleCheckBox = new System.Windows.Forms.CheckBox();
            this.UseZAxisCheckBox = new System.Windows.Forms.CheckBox();
            this.AutoConfigureBut = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ProMuxSelect = new System.Windows.Forms.RadioButton();
            this.ProRFSelect = new System.Windows.Forms.RadioButton();
            this.CommunicationsPort = new System.Windows.Forms.ComboBox();
            this.SystemSettings = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.BrowsePrintLabelTemplateFolder = new System.Windows.Forms.Button();
            this.PartLabelTemplateFolderText = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.BrowsePackListTemplateFolder = new System.Windows.Forms.Button();
            this.PackListTemplateFolderText = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.MMPrec2 = new System.Windows.Forms.RadioButton();
            this.MMPrec1 = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.InchPrec3 = new System.Windows.Forms.RadioButton();
            this.InchPrec2 = new System.Windows.Forms.RadioButton();
            this.InchPrec1 = new System.Windows.Forms.RadioButton();
            this.PasswordText = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.BrowseCompletedFolder = new System.Windows.Forms.Button();
            this.CompletedOrderFolderText = new System.Windows.Forms.TextBox();
            this.DefaultBatchCSVFolderText = new System.Windows.Forms.TextBox();
            this.DefaultCSVFolderText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BrowseCSVBatchFolder = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.BrowseCSVFolder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EditTemplateBut = new System.Windows.Forms.Button();
            this.CutlistSelect = new System.Windows.Forms.RadioButton();
            this.SingleOrderSelect = new System.Windows.Forms.RadioButton();
            this.OptionsTabControl = new System.Windows.Forms.TabControl();
            this.MeasurementTab = new System.Windows.Forms.TabPage();
            this.ShowUserMsgDlgChkBtn = new System.Windows.Forms.CheckBox();
            this.DoNotAutoSaveOrderWhenComplete = new System.Windows.Forms.CheckBox();
            this.UseLabelPreviewSel = new System.Windows.Forms.CheckBox();
            this.SwapAxesSel = new System.Windows.Forms.CheckBox();
            this.AllowIncompleteOrdersSel = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.PrintPackListSel = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.PrintLabelsOptionSel = new System.Windows.Forms.ComboBox();
            this.UseReportPreviewSel = new System.Windows.Forms.CheckBox();
            this.TestCompletedLineItemsSel = new System.Windows.Forms.CheckBox();
            this.PrintOutOfSpecLabelSel = new System.Windows.Forms.CheckBox();
            this.ManualMeasureSel = new System.Windows.Forms.CheckBox();
            this.UseNorthAmericanDateSel = new System.Windows.Forms.CheckBox();
            this.ShowOrderComplete = new System.Windows.Forms.CheckBox();
            this.LineItemQuantityEdit = new System.Windows.Forms.CheckBox();
            this.LineItemWidthEdit = new System.Windows.Forms.CheckBox();
            this.LineItemHeightEdit = new System.Windows.Forms.CheckBox();
            this.AllowAllOrderDel = new System.Windows.Forms.CheckBox();
            this.AllowIndividualOrderDel = new System.Windows.Forms.CheckBox();
            this.DeleteLIUponQuantity = new System.Windows.Forms.CheckBox();
            this.AllowOutOfTolParts = new System.Windows.Forms.CheckBox();
            this.CompanyInfo = new System.Windows.Forms.TabPage();
            this.CompanyCountry = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.CompanyURL = new System.Windows.Forms.TextBox();
            this.LogoFileDialog = new System.Windows.Forms.Button();
            this.LogoFileName = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.CompanyPhone = new System.Windows.Forms.TextBox();
            this.CompanyZip = new System.Windows.Forms.TextBox();
            this.CompanyState = new System.Windows.Forms.TextBox();
            this.CompanyCity = new System.Windows.Forms.TextBox();
            this.CompanyAddr2 = new System.Windows.Forms.TextBox();
            this.CompanyAddr1 = new System.Windows.Forms.TextBox();
            this.CompanyNameField = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.CompanyLogo = new System.Windows.Forms.PictureBox();
            this.SystemMaintenance = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ConfigurationImportButton = new System.Windows.Forms.Button();
            this.ConfigurationExportButton = new System.Windows.Forms.Button();
            this.CancelBut = new System.Windows.Forms.Button();
            this.OkBut = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.UseOutOfSquare = new System.Windows.Forms.CheckBox();
            this.AxisParameters.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.InterfaceHardware.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SystemSettings.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.OptionsTabControl.SuspendLayout();
            this.MeasurementTab.SuspendLayout();
            this.CompanyInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyLogo)).BeginInit();
            this.SystemMaintenance.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // AxisParameters
            // 
            this.AxisParameters.BackColor = System.Drawing.SystemColors.ControlLight;
            this.AxisParameters.Controls.Add(this.PivotDistVal);
            this.AxisParameters.Controls.Add(this.PivotDistLabel);
            this.AxisParameters.Controls.Add(this.groupBox4);
            this.AxisParameters.Controls.Add(this.ScaleFactorText);
            this.AxisParameters.Controls.Add(this.DatumPresetText);
            this.AxisParameters.Controls.Add(this.label10);
            this.AxisParameters.Controls.Add(this.label9);
            this.AxisParameters.Controls.Add(this.label8);
            this.AxisParameters.Controls.Add(this.AxisSelect);
            this.AxisParameters.Location = new System.Drawing.Point(4, 25);
            this.AxisParameters.Name = "AxisParameters";
            this.AxisParameters.Padding = new System.Windows.Forms.Padding(3);
            this.AxisParameters.Size = new System.Drawing.Size(683, 355);
            this.AxisParameters.TabIndex = 2;
            this.AxisParameters.Text = "Axis Parameters";
            this.AxisParameters.ToolTipText = "Configures axis parameter data";
            // 
            // PivotDistVal
            // 
            this.PivotDistVal.Location = new System.Drawing.Point(144, 186);
            this.PivotDistVal.Name = "PivotDistVal";
            this.PivotDistVal.Size = new System.Drawing.Size(130, 22);
            this.PivotDistVal.TabIndex = 8;
            this.PivotDistVal.Visible = false;
            // 
            // PivotDistLabel
            // 
            this.PivotDistLabel.AutoSize = true;
            this.PivotDistLabel.Location = new System.Drawing.Point(39, 189);
            this.PivotDistLabel.Name = "PivotDistLabel";
            this.PivotDistLabel.Size = new System.Drawing.Size(94, 16);
            this.PivotDistLabel.TabIndex = 7;
            this.PivotDistLabel.Text = "Pivot Distance";
            this.PivotDistLabel.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.EncoderNegDirSelect);
            this.groupBox4.Controls.Add(this.EncoderPosDirSelect);
            this.groupBox4.Location = new System.Drawing.Point(385, 99);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(147, 80);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Encoder Direction";
            // 
            // EncoderNegDirSelect
            // 
            this.EncoderNegDirSelect.AutoSize = true;
            this.EncoderNegDirSelect.Location = new System.Drawing.Point(32, 47);
            this.EncoderNegDirSelect.Name = "EncoderNegDirSelect";
            this.EncoderNegDirSelect.Size = new System.Drawing.Size(81, 20);
            this.EncoderNegDirSelect.TabIndex = 1;
            this.EncoderNegDirSelect.TabStop = true;
            this.EncoderNegDirSelect.Text = "Negative";
            this.EncoderNegDirSelect.UseVisualStyleBackColor = true;
            // 
            // EncoderPosDirSelect
            // 
            this.EncoderPosDirSelect.AutoSize = true;
            this.EncoderPosDirSelect.Location = new System.Drawing.Point(32, 21);
            this.EncoderPosDirSelect.Name = "EncoderPosDirSelect";
            this.EncoderPosDirSelect.Size = new System.Drawing.Size(74, 20);
            this.EncoderPosDirSelect.TabIndex = 0;
            this.EncoderPosDirSelect.TabStop = true;
            this.EncoderPosDirSelect.Text = "Positive";
            this.EncoderPosDirSelect.UseVisualStyleBackColor = true;
            // 
            // ScaleFactorText
            // 
            this.ScaleFactorText.Location = new System.Drawing.Point(144, 147);
            this.ScaleFactorText.Name = "ScaleFactorText";
            this.ScaleFactorText.Size = new System.Drawing.Size(130, 22);
            this.ScaleFactorText.TabIndex = 5;
            // 
            // DatumPresetText
            // 
            this.DatumPresetText.Location = new System.Drawing.Point(144, 108);
            this.DatumPresetText.Name = "DatumPresetText";
            this.DatumPresetText.Size = new System.Drawing.Size(130, 22);
            this.DatumPresetText.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(39, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 16);
            this.label10.TabIndex = 4;
            this.label10.Text = "Scaling Factor";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 111);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Datum Preset";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 1;
            this.label8.Text = "Select Axis To Edit";
            // 
            // AxisSelect
            // 
            this.AxisSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AxisSelect.Items.AddRange(new object[] {
            "Width Axis",
            "Height Axis",
            "Depth Axis",
            "Angle Axis"});
            this.AxisSelect.Location = new System.Drawing.Point(34, 46);
            this.AxisSelect.Name = "AxisSelect";
            this.AxisSelect.Size = new System.Drawing.Size(160, 24);
            this.AxisSelect.TabIndex = 0;
            this.AxisSelect.DropDown += new System.EventHandler(this.AxisSelect_DropDown);
            this.AxisSelect.SelectionChangeCommitted += new System.EventHandler(this.AxisSelect_SelectionChangeCommitted);
            this.AxisSelect.SelectedValueChanged += new System.EventHandler(this.AxisSelect_SelectedValueChanged);
            // 
            // InterfaceHardware
            // 
            this.InterfaceHardware.BackColor = System.Drawing.SystemColors.ControlLight;
            this.InterfaceHardware.Controls.Add(this.panel2);
            this.InterfaceHardware.Controls.Add(this.panel1);
            this.InterfaceHardware.Location = new System.Drawing.Point(4, 25);
            this.InterfaceHardware.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.InterfaceHardware.Name = "InterfaceHardware";
            this.InterfaceHardware.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.InterfaceHardware.Size = new System.Drawing.Size(683, 355);
            this.InterfaceHardware.TabIndex = 1;
            this.InterfaceHardware.Text = "Interface Hardware";
            this.InterfaceHardware.ToolTipText = "Configures interface hardware";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.LabelPrinterComboBox);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.ReportPrinterComboBox);
            this.panel2.Location = new System.Drawing.Point(324, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(352, 293);
            this.panel2.TabIndex = 3;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(34, 105);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(83, 16);
            this.label48.TabIndex = 3;
            this.label48.Text = "Label Printer";
            // 
            // LabelPrinterComboBox
            // 
            this.LabelPrinterComboBox.FormattingEnabled = true;
            this.LabelPrinterComboBox.Location = new System.Drawing.Point(37, 124);
            this.LabelPrinterComboBox.Name = "LabelPrinterComboBox";
            this.LabelPrinterComboBox.Size = new System.Drawing.Size(276, 24);
            this.LabelPrinterComboBox.TabIndex = 2;
            this.LabelPrinterComboBox.SelectedValueChanged += new System.EventHandler(this.LabelPrinterComboBox_SelectedValueChanged);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(34, 38);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(90, 16);
            this.label47.TabIndex = 1;
            this.label47.Text = "Report Printer";
            // 
            // ReportPrinterComboBox
            // 
            this.ReportPrinterComboBox.FormattingEnabled = true;
            this.ReportPrinterComboBox.Location = new System.Drawing.Point(37, 57);
            this.ReportPrinterComboBox.Name = "ReportPrinterComboBox";
            this.ReportPrinterComboBox.Size = new System.Drawing.Size(276, 24);
            this.ReportPrinterComboBox.TabIndex = 0;
            this.ReportPrinterComboBox.SelectedValueChanged += new System.EventHandler(this.ReportPrinterComboBox_SelectedValueChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.UseOutOfSquare);
            this.panel1.Controls.Add(this.UseAngleCheckBox);
            this.panel1.Controls.Add(this.UseZAxisCheckBox);
            this.panel1.Controls.Add(this.AutoConfigureBut);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.CommunicationsPort);
            this.panel1.Location = new System.Drawing.Point(7, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 293);
            this.panel1.TabIndex = 2;
            // 
            // UseAngleCheckBox
            // 
            this.UseAngleCheckBox.AutoSize = true;
            this.UseAngleCheckBox.Location = new System.Drawing.Point(8, 213);
            this.UseAngleCheckBox.Name = "UseAngleCheckBox";
            this.UseAngleCheckBox.Size = new System.Drawing.Size(146, 20);
            this.UseAngleCheckBox.TabIndex = 6;
            this.UseAngleCheckBox.Text = "Use Angle Measure";
            this.UseAngleCheckBox.UseVisualStyleBackColor = true;
            this.UseAngleCheckBox.Click += new System.EventHandler(this.UseAngleCheckBox_Click);
            // 
            // UseZAxisCheckBox
            // 
            this.UseZAxisCheckBox.AutoSize = true;
            this.UseZAxisCheckBox.Location = new System.Drawing.Point(8, 187);
            this.UseZAxisCheckBox.Name = "UseZAxisCheckBox";
            this.UseZAxisCheckBox.Size = new System.Drawing.Size(119, 20);
            this.UseZAxisCheckBox.TabIndex = 5;
            this.UseZAxisCheckBox.Text = "Use Depth Axis";
            this.UseZAxisCheckBox.UseVisualStyleBackColor = true;
            this.UseZAxisCheckBox.Click += new System.EventHandler(this.UseZAxisCheckBox_Click);
            // 
            // AutoConfigureBut
            // 
            this.AutoConfigureBut.Location = new System.Drawing.Point(92, 237);
            this.AutoConfigureBut.Name = "AutoConfigureBut";
            this.AutoConfigureBut.Size = new System.Drawing.Size(114, 34);
            this.AutoConfigureBut.TabIndex = 4;
            this.AutoConfigureBut.Text = "Auto Configure";
            this.AutoConfigureBut.UseVisualStyleBackColor = true;
            this.AutoConfigureBut.Click += new System.EventHandler(this.AutoConfigureBut_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(51, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(196, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "Measurement Interface";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "Port Setting";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ProMuxSelect);
            this.groupBox3.Controls.Add(this.ProRFSelect);
            this.groupBox3.Location = new System.Drawing.Point(70, 105);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(159, 76);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Interface Type";
            // 
            // ProMuxSelect
            // 
            this.ProMuxSelect.AutoSize = true;
            this.ProMuxSelect.Location = new System.Drawing.Point(25, 47);
            this.ProMuxSelect.Name = "ProMuxSelect";
            this.ProMuxSelect.Size = new System.Drawing.Size(86, 20);
            this.ProMuxSelect.TabIndex = 1;
            this.ProMuxSelect.TabStop = true;
            this.ProMuxSelect.Text = "ProMUX 3";
            this.ProMuxSelect.UseVisualStyleBackColor = true;
            this.ProMuxSelect.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ProMuxSelect_MouseClick);
            // 
            // ProRFSelect
            // 
            this.ProRFSelect.AutoSize = true;
            this.ProRFSelect.Location = new System.Drawing.Point(25, 21);
            this.ProRFSelect.Name = "ProRFSelect";
            this.ProRFSelect.Size = new System.Drawing.Size(123, 20);
            this.ProRFSelect.TabIndex = 0;
            this.ProRFSelect.TabStop = true;
            this.ProRFSelect.Text = "ProRF Receiver";
            this.ProRFSelect.UseVisualStyleBackColor = true;
            this.ProRFSelect.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ProRFSelect_MouseClick);
            // 
            // CommunicationsPort
            // 
            this.CommunicationsPort.FormattingEnabled = true;
            this.CommunicationsPort.Location = new System.Drawing.Point(117, 56);
            this.CommunicationsPort.Name = "CommunicationsPort";
            this.CommunicationsPort.Size = new System.Drawing.Size(121, 24);
            this.CommunicationsPort.TabIndex = 0;
            this.CommunicationsPort.SelectedValueChanged += new System.EventHandler(this.CommunicationsPort_SelectedValueChanged);
            // 
            // SystemSettings
            // 
            this.SystemSettings.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SystemSettings.Controls.Add(this.button8);
            this.SystemSettings.Controls.Add(this.button7);
            this.SystemSettings.Controls.Add(this.BrowsePrintLabelTemplateFolder);
            this.SystemSettings.Controls.Add(this.PartLabelTemplateFolderText);
            this.SystemSettings.Controls.Add(this.label46);
            this.SystemSettings.Controls.Add(this.BrowsePackListTemplateFolder);
            this.SystemSettings.Controls.Add(this.PackListTemplateFolderText);
            this.SystemSettings.Controls.Add(this.label45);
            this.SystemSettings.Controls.Add(this.groupBox8);
            this.SystemSettings.Controls.Add(this.groupBox7);
            this.SystemSettings.Controls.Add(this.PasswordText);
            this.SystemSettings.Controls.Add(this.label38);
            this.SystemSettings.Controls.Add(this.BrowseCompletedFolder);
            this.SystemSettings.Controls.Add(this.CompletedOrderFolderText);
            this.SystemSettings.Controls.Add(this.DefaultBatchCSVFolderText);
            this.SystemSettings.Controls.Add(this.DefaultCSVFolderText);
            this.SystemSettings.Controls.Add(this.label3);
            this.SystemSettings.Controls.Add(this.BrowseCSVBatchFolder);
            this.SystemSettings.Controls.Add(this.label2);
            this.SystemSettings.Controls.Add(this.BrowseCSVFolder);
            this.SystemSettings.Controls.Add(this.label1);
            this.SystemSettings.Controls.Add(this.groupBox1);
            this.SystemSettings.Location = new System.Drawing.Point(4, 25);
            this.SystemSettings.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SystemSettings.Name = "SystemSettings";
            this.SystemSettings.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SystemSettings.Size = new System.Drawing.Size(683, 355);
            this.SystemSettings.TabIndex = 0;
            this.SystemSettings.Text = "System Settings";
            this.SystemSettings.ToolTipText = "System Related Settings";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(466, 254);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(112, 47);
            this.button8.TabIndex = 28;
            this.button8.Text = "Edit Part Label Templates";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.PartLabelTemplateEditClick);
            // 
            // button7
            // 
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button7.Location = new System.Drawing.Point(329, 254);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(113, 47);
            this.button7.TabIndex = 27;
            this.button7.Text = "Edit Report Templates";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.PackListTemplateEditClick);
            // 
            // BrowsePrintLabelTemplateFolder
            // 
            this.BrowsePrintLabelTemplateFolder.BackgroundImage = global::ProCABQC11.Properties.Resources.folder_full_48x48;
            this.BrowsePrintLabelTemplateFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BrowsePrintLabelTemplateFolder.Location = new System.Drawing.Point(603, 172);
            this.BrowsePrintLabelTemplateFolder.Name = "BrowsePrintLabelTemplateFolder";
            this.BrowsePrintLabelTemplateFolder.Size = new System.Drawing.Size(32, 32);
            this.BrowsePrintLabelTemplateFolder.TabIndex = 26;
            this.BrowsePrintLabelTemplateFolder.UseVisualStyleBackColor = true;
            this.BrowsePrintLabelTemplateFolder.Click += new System.EventHandler(this.BrowsePrintLabelTemplateFolder_Click);
            // 
            // PartLabelTemplateFolderText
            // 
            this.PartLabelTemplateFolderText.Location = new System.Drawing.Point(319, 177);
            this.PartLabelTemplateFolderText.Name = "PartLabelTemplateFolderText";
            this.PartLabelTemplateFolderText.Size = new System.Drawing.Size(269, 22);
            this.PartLabelTemplateFolderText.TabIndex = 25;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(202, 170);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(111, 37);
            this.label46.TabIndex = 24;
            this.label46.Text = "Part Label Template Folder";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BrowsePackListTemplateFolder
            // 
            this.BrowsePackListTemplateFolder.BackgroundImage = global::ProCABQC11.Properties.Resources.folder_full_48x48;
            this.BrowsePackListTemplateFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BrowsePackListTemplateFolder.Location = new System.Drawing.Point(603, 133);
            this.BrowsePackListTemplateFolder.Name = "BrowsePackListTemplateFolder";
            this.BrowsePackListTemplateFolder.Size = new System.Drawing.Size(32, 32);
            this.BrowsePackListTemplateFolder.TabIndex = 23;
            this.BrowsePackListTemplateFolder.UseVisualStyleBackColor = true;
            this.BrowsePackListTemplateFolder.Click += new System.EventHandler(this.BrowsePackListTemplateFolder_Click);
            // 
            // PackListTemplateFolderText
            // 
            this.PackListTemplateFolderText.Location = new System.Drawing.Point(319, 138);
            this.PackListTemplateFolderText.Name = "PackListTemplateFolderText";
            this.PackListTemplateFolderText.Size = new System.Drawing.Size(269, 22);
            this.PackListTemplateFolderText.TabIndex = 22;
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(203, 131);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(119, 37);
            this.label45.TabIndex = 21;
            this.label45.Text = "Pack List Template Folder";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.MMPrec2);
            this.groupBox8.Controls.Add(this.MMPrec1);
            this.groupBox8.Location = new System.Drawing.Point(9, 233);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(187, 52);
            this.groupBox8.TabIndex = 20;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "MM Precision";
            // 
            // MMPrec2
            // 
            this.MMPrec2.AutoSize = true;
            this.MMPrec2.Location = new System.Drawing.Point(96, 21);
            this.MMPrec2.Name = "MMPrec2";
            this.MMPrec2.Size = new System.Drawing.Size(52, 20);
            this.MMPrec2.TabIndex = 1;
            this.MMPrec2.TabStop = true;
            this.MMPrec2.Text = "2 dp";
            this.MMPrec2.UseVisualStyleBackColor = true;
            this.MMPrec2.CheckedChanged += new System.EventHandler(this.MMPrec2_CheckedChanged);
            // 
            // MMPrec1
            // 
            this.MMPrec1.AutoSize = true;
            this.MMPrec1.Location = new System.Drawing.Point(26, 21);
            this.MMPrec1.Name = "MMPrec1";
            this.MMPrec1.Size = new System.Drawing.Size(52, 20);
            this.MMPrec1.TabIndex = 0;
            this.MMPrec1.TabStop = true;
            this.MMPrec1.Text = "1 dp";
            this.MMPrec1.UseVisualStyleBackColor = true;
            this.MMPrec1.CheckedChanged += new System.EventHandler(this.MMPrec1_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.InchPrec3);
            this.groupBox7.Controls.Add(this.InchPrec2);
            this.groupBox7.Controls.Add(this.InchPrec1);
            this.groupBox7.Location = new System.Drawing.Point(9, 170);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(187, 57);
            this.groupBox7.TabIndex = 19;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Inch Precision";
            // 
            // InchPrec3
            // 
            this.InchPrec3.AutoSize = true;
            this.InchPrec3.Location = new System.Drawing.Point(122, 21);
            this.InchPrec3.Name = "InchPrec3";
            this.InchPrec3.Size = new System.Drawing.Size(52, 20);
            this.InchPrec3.TabIndex = 2;
            this.InchPrec3.TabStop = true;
            this.InchPrec3.Text = "3 dp";
            this.InchPrec3.UseVisualStyleBackColor = true;
            this.InchPrec3.CheckedChanged += new System.EventHandler(this.InchPrec3_CheckedChanged);
            // 
            // InchPrec2
            // 
            this.InchPrec2.AutoSize = true;
            this.InchPrec2.Location = new System.Drawing.Point(64, 21);
            this.InchPrec2.Name = "InchPrec2";
            this.InchPrec2.Size = new System.Drawing.Size(52, 20);
            this.InchPrec2.TabIndex = 1;
            this.InchPrec2.TabStop = true;
            this.InchPrec2.Text = "2 dp";
            this.InchPrec2.UseVisualStyleBackColor = true;
            this.InchPrec2.CheckedChanged += new System.EventHandler(this.InchPrec2_CheckedChanged);
            // 
            // InchPrec1
            // 
            this.InchPrec1.AutoSize = true;
            this.InchPrec1.Location = new System.Drawing.Point(6, 21);
            this.InchPrec1.Name = "InchPrec1";
            this.InchPrec1.Size = new System.Drawing.Size(52, 20);
            this.InchPrec1.TabIndex = 0;
            this.InchPrec1.TabStop = true;
            this.InchPrec1.Text = "1 dp";
            this.InchPrec1.UseVisualStyleBackColor = true;
            this.InchPrec1.CheckedChanged += new System.EventHandler(this.InchPrec1_CheckedChanged);
            // 
            // PasswordText
            // 
            this.PasswordText.Location = new System.Drawing.Point(319, 216);
            this.PasswordText.Name = "PasswordText";
            this.PasswordText.Size = new System.Drawing.Size(269, 22);
            this.PasswordText.TabIndex = 18;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(217, 209);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(96, 37);
            this.label38.TabIndex = 17;
            this.label38.Text = "Configuration Password";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BrowseCompletedFolder
            // 
            this.BrowseCompletedFolder.BackgroundImage = global::ProCABQC11.Properties.Resources.folder_full_48x48;
            this.BrowseCompletedFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BrowseCompletedFolder.Location = new System.Drawing.Point(603, 94);
            this.BrowseCompletedFolder.Name = "BrowseCompletedFolder";
            this.BrowseCompletedFolder.Size = new System.Drawing.Size(32, 32);
            this.BrowseCompletedFolder.TabIndex = 14;
            this.BrowseCompletedFolder.UseVisualStyleBackColor = true;
            this.BrowseCompletedFolder.Click += new System.EventHandler(this.BrowseCompletedFolder_Click);
            // 
            // CompletedOrderFolderText
            // 
            this.CompletedOrderFolderText.Location = new System.Drawing.Point(319, 99);
            this.CompletedOrderFolderText.Name = "CompletedOrderFolderText";
            this.CompletedOrderFolderText.Size = new System.Drawing.Size(269, 22);
            this.CompletedOrderFolderText.TabIndex = 13;
            // 
            // DefaultBatchCSVFolderText
            // 
            this.DefaultBatchCSVFolderText.Location = new System.Drawing.Point(319, 60);
            this.DefaultBatchCSVFolderText.Name = "DefaultBatchCSVFolderText";
            this.DefaultBatchCSVFolderText.Size = new System.Drawing.Size(269, 22);
            this.DefaultBatchCSVFolderText.TabIndex = 5;
            // 
            // DefaultCSVFolderText
            // 
            this.DefaultCSVFolderText.Location = new System.Drawing.Point(319, 21);
            this.DefaultCSVFolderText.Name = "DefaultCSVFolderText";
            this.DefaultCSVFolderText.Size = new System.Drawing.Size(270, 22);
            this.DefaultCSVFolderText.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(215, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 37);
            this.label3.TabIndex = 12;
            this.label3.Text = "Completed Order Folder";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BrowseCSVBatchFolder
            // 
            this.BrowseCSVBatchFolder.BackgroundImage = global::ProCABQC11.Properties.Resources.folder_full_48x48;
            this.BrowseCSVBatchFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BrowseCSVBatchFolder.Location = new System.Drawing.Point(603, 55);
            this.BrowseCSVBatchFolder.Name = "BrowseCSVBatchFolder";
            this.BrowseCSVBatchFolder.Size = new System.Drawing.Size(32, 32);
            this.BrowseCSVBatchFolder.TabIndex = 6;
            this.BrowseCSVBatchFolder.UseVisualStyleBackColor = true;
            this.BrowseCSVBatchFolder.Click += new System.EventHandler(this.BrowseCSVBatchFolder_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(223, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 37);
            this.label2.TabIndex = 4;
            this.label2.Text = "Default CSV Batch Folder";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BrowseCSVFolder
            // 
            this.BrowseCSVFolder.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BrowseCSVFolder.BackgroundImage")));
            this.BrowseCSVFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BrowseCSVFolder.Location = new System.Drawing.Point(603, 16);
            this.BrowseCSVFolder.Name = "BrowseCSVFolder";
            this.BrowseCSVFolder.Size = new System.Drawing.Size(32, 32);
            this.BrowseCSVFolder.TabIndex = 3;
            this.BrowseCSVFolder.UseVisualStyleBackColor = true;
            this.BrowseCSVFolder.Click += new System.EventHandler(this.BrowseCSVFolder_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(223, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 35);
            this.label1.TabIndex = 1;
            this.label1.Text = "Default CSV Folder";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EditTemplateBut);
            this.groupBox1.Controls.Add(this.CutlistSelect);
            this.groupBox1.Controls.Add(this.SingleOrderSelect);
            this.groupBox1.Location = new System.Drawing.Point(9, 21);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(187, 126);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CSV Import Template Style";
            // 
            // EditTemplateBut
            // 
            this.EditTemplateBut.Location = new System.Drawing.Point(34, 86);
            this.EditTemplateBut.Name = "EditTemplateBut";
            this.EditTemplateBut.Size = new System.Drawing.Size(113, 27);
            this.EditTemplateBut.TabIndex = 2;
            this.EditTemplateBut.Text = "Edit Template";
            this.EditTemplateBut.UseVisualStyleBackColor = true;
            this.EditTemplateBut.Click += new System.EventHandler(this.EditTemplateBut_Click);
            // 
            // CutlistSelect
            // 
            this.CutlistSelect.AutoSize = true;
            this.CutlistSelect.Location = new System.Drawing.Point(19, 53);
            this.CutlistSelect.Name = "CutlistSelect";
            this.CutlistSelect.Size = new System.Drawing.Size(90, 20);
            this.CutlistSelect.TabIndex = 1;
            this.CutlistSelect.TabStop = true;
            this.CutlistSelect.Text = "Multi Order";
            this.CutlistSelect.UseVisualStyleBackColor = true;
            this.CutlistSelect.CheckedChanged += new System.EventHandler(this.CutlistSelect_CheckedChanged);
            // 
            // SingleOrderSelect
            // 
            this.SingleOrderSelect.AutoSize = true;
            this.SingleOrderSelect.Location = new System.Drawing.Point(19, 25);
            this.SingleOrderSelect.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SingleOrderSelect.Name = "SingleOrderSelect";
            this.SingleOrderSelect.Size = new System.Drawing.Size(101, 20);
            this.SingleOrderSelect.TabIndex = 0;
            this.SingleOrderSelect.TabStop = true;
            this.SingleOrderSelect.Text = "Single Order";
            this.SingleOrderSelect.UseVisualStyleBackColor = true;
            this.SingleOrderSelect.CheckedChanged += new System.EventHandler(this.SingleOrderSelect_CheckedChanged);
            // 
            // OptionsTabControl
            // 
            this.OptionsTabControl.Controls.Add(this.SystemSettings);
            this.OptionsTabControl.Controls.Add(this.MeasurementTab);
            this.OptionsTabControl.Controls.Add(this.InterfaceHardware);
            this.OptionsTabControl.Controls.Add(this.AxisParameters);
            this.OptionsTabControl.Controls.Add(this.CompanyInfo);
            this.OptionsTabControl.Controls.Add(this.SystemMaintenance);
            this.OptionsTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionsTabControl.Location = new System.Drawing.Point(1, 14);
            this.OptionsTabControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.OptionsTabControl.Name = "OptionsTabControl";
            this.OptionsTabControl.SelectedIndex = 0;
            this.OptionsTabControl.ShowToolTips = true;
            this.OptionsTabControl.Size = new System.Drawing.Size(691, 384);
            this.OptionsTabControl.TabIndex = 0;
            // 
            // MeasurementTab
            // 
            this.MeasurementTab.BackColor = System.Drawing.SystemColors.ControlLight;
            this.MeasurementTab.Controls.Add(this.ShowUserMsgDlgChkBtn);
            this.MeasurementTab.Controls.Add(this.DoNotAutoSaveOrderWhenComplete);
            this.MeasurementTab.Controls.Add(this.UseLabelPreviewSel);
            this.MeasurementTab.Controls.Add(this.SwapAxesSel);
            this.MeasurementTab.Controls.Add(this.AllowIncompleteOrdersSel);
            this.MeasurementTab.Controls.Add(this.label14);
            this.MeasurementTab.Controls.Add(this.PrintPackListSel);
            this.MeasurementTab.Controls.Add(this.label13);
            this.MeasurementTab.Controls.Add(this.PrintLabelsOptionSel);
            this.MeasurementTab.Controls.Add(this.UseReportPreviewSel);
            this.MeasurementTab.Controls.Add(this.TestCompletedLineItemsSel);
            this.MeasurementTab.Controls.Add(this.PrintOutOfSpecLabelSel);
            this.MeasurementTab.Controls.Add(this.ManualMeasureSel);
            this.MeasurementTab.Controls.Add(this.UseNorthAmericanDateSel);
            this.MeasurementTab.Controls.Add(this.ShowOrderComplete);
            this.MeasurementTab.Controls.Add(this.LineItemQuantityEdit);
            this.MeasurementTab.Controls.Add(this.LineItemWidthEdit);
            this.MeasurementTab.Controls.Add(this.LineItemHeightEdit);
            this.MeasurementTab.Controls.Add(this.AllowAllOrderDel);
            this.MeasurementTab.Controls.Add(this.AllowIndividualOrderDel);
            this.MeasurementTab.Controls.Add(this.DeleteLIUponQuantity);
            this.MeasurementTab.Controls.Add(this.AllowOutOfTolParts);
            this.MeasurementTab.Location = new System.Drawing.Point(4, 25);
            this.MeasurementTab.Name = "MeasurementTab";
            this.MeasurementTab.Padding = new System.Windows.Forms.Padding(3);
            this.MeasurementTab.Size = new System.Drawing.Size(683, 355);
            this.MeasurementTab.TabIndex = 3;
            this.MeasurementTab.Text = "Measurement Options";
            // 
            // ShowUserMsgDlgChkBtn
            // 
            this.ShowUserMsgDlgChkBtn.AutoSize = true;
            this.ShowUserMsgDlgChkBtn.Location = new System.Drawing.Point(20, 285);
            this.ShowUserMsgDlgChkBtn.Name = "ShowUserMsgDlgChkBtn";
            this.ShowUserMsgDlgChkBtn.Size = new System.Drawing.Size(282, 20);
            this.ShowUserMsgDlgChkBtn.TabIndex = 40;
            this.ShowUserMsgDlgChkBtn.Text = "Show User Message Dialog if data present";
            this.ShowUserMsgDlgChkBtn.UseVisualStyleBackColor = true;
            // 
            // DoNotAutoSaveOrderWhenComplete
            // 
            this.DoNotAutoSaveOrderWhenComplete.AutoSize = true;
            this.DoNotAutoSaveOrderWhenComplete.Location = new System.Drawing.Point(20, 259);
            this.DoNotAutoSaveOrderWhenComplete.Name = "DoNotAutoSaveOrderWhenComplete";
            this.DoNotAutoSaveOrderWhenComplete.Size = new System.Drawing.Size(270, 20);
            this.DoNotAutoSaveOrderWhenComplete.TabIndex = 39;
            this.DoNotAutoSaveOrderWhenComplete.Text = "Do Not Auto Save Order When Complete";
            this.DoNotAutoSaveOrderWhenComplete.UseVisualStyleBackColor = true;
            // 
            // UseLabelPreviewSel
            // 
            this.UseLabelPreviewSel.AutoSize = true;
            this.UseLabelPreviewSel.Location = new System.Drawing.Point(362, 259);
            this.UseLabelPreviewSel.Name = "UseLabelPreviewSel";
            this.UseLabelPreviewSel.Size = new System.Drawing.Size(169, 20);
            this.UseLabelPreviewSel.TabIndex = 38;
            this.UseLabelPreviewSel.Text = "Use Label Print Preview";
            this.UseLabelPreviewSel.UseVisualStyleBackColor = true;
            // 
            // SwapAxesSel
            // 
            this.SwapAxesSel.AutoSize = true;
            this.SwapAxesSel.Location = new System.Drawing.Point(20, 233);
            this.SwapAxesSel.Name = "SwapAxesSel";
            this.SwapAxesSel.Size = new System.Drawing.Size(199, 20);
            this.SwapAxesSel.TabIndex = 37;
            this.SwapAxesSel.Text = "Swap Width and Height Axes";
            this.SwapAxesSel.UseVisualStyleBackColor = true;
            // 
            // AllowIncompleteOrdersSel
            // 
            this.AllowIncompleteOrdersSel.AutoSize = true;
            this.AllowIncompleteOrdersSel.Location = new System.Drawing.Point(20, 207);
            this.AllowIncompleteOrdersSel.Name = "AllowIncompleteOrdersSel";
            this.AllowIncompleteOrdersSel.Size = new System.Drawing.Size(274, 20);
            this.AllowIncompleteOrdersSel.TabIndex = 36;
            this.AllowIncompleteOrdersSel.Text = "Allow Incomplete Orders to be Processed";
            this.AllowIncompleteOrdersSel.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(360, 197);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 16);
            this.label14.TabIndex = 35;
            this.label14.Text = "Print Pack Lists";
            // 
            // PrintPackListSel
            // 
            this.PrintPackListSel.FormattingEnabled = true;
            this.PrintPackListSel.Items.AddRange(new object[] {
            "Never",
            "Always",
            "By Template"});
            this.PrintPackListSel.Location = new System.Drawing.Point(474, 194);
            this.PrintPackListSel.Name = "PrintPackListSel";
            this.PrintPackListSel.Size = new System.Drawing.Size(121, 24);
            this.PrintPackListSel.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(360, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 16);
            this.label13.TabIndex = 33;
            this.label13.Text = "Print Part Labels:";
            // 
            // PrintLabelsOptionSel
            // 
            this.PrintLabelsOptionSel.FormattingEnabled = true;
            this.PrintLabelsOptionSel.Items.AddRange(new object[] {
            "Never",
            "Always",
            "By Template"});
            this.PrintLabelsOptionSel.Location = new System.Drawing.Point(474, 161);
            this.PrintLabelsOptionSel.Name = "PrintLabelsOptionSel";
            this.PrintLabelsOptionSel.Size = new System.Drawing.Size(121, 24);
            this.PrintLabelsOptionSel.TabIndex = 32;
            // 
            // UseReportPreviewSel
            // 
            this.UseReportPreviewSel.AutoSize = true;
            this.UseReportPreviewSel.Location = new System.Drawing.Point(362, 233);
            this.UseReportPreviewSel.Name = "UseReportPreviewSel";
            this.UseReportPreviewSel.Size = new System.Drawing.Size(176, 20);
            this.UseReportPreviewSel.TabIndex = 31;
            this.UseReportPreviewSel.Text = "Use Report Print Preview";
            this.UseReportPreviewSel.UseVisualStyleBackColor = true;
            // 
            // TestCompletedLineItemsSel
            // 
            this.TestCompletedLineItemsSel.AutoSize = true;
            this.TestCompletedLineItemsSel.Location = new System.Drawing.Point(362, 131);
            this.TestCompletedLineItemsSel.Name = "TestCompletedLineItemsSel";
            this.TestCompletedLineItemsSel.Size = new System.Drawing.Size(317, 20);
            this.TestCompletedLineItemsSel.TabIndex = 28;
            this.TestCompletedLineItemsSel.Text = "Test completed line items on new measurements";
            this.TestCompletedLineItemsSel.UseVisualStyleBackColor = true;
            // 
            // PrintOutOfSpecLabelSel
            // 
            this.PrintOutOfSpecLabelSel.AutoSize = true;
            this.PrintOutOfSpecLabelSel.Location = new System.Drawing.Point(362, 105);
            this.PrintOutOfSpecLabelSel.Name = "PrintOutOfSpecLabelSel";
            this.PrintOutOfSpecLabelSel.Size = new System.Drawing.Size(298, 20);
            this.PrintOutOfSpecLabelSel.TabIndex = 27;
            this.PrintOutOfSpecLabelSel.Text = "Print \"Out Of Spec\" label if no part match found";
            this.PrintOutOfSpecLabelSel.UseVisualStyleBackColor = true;
            // 
            // ManualMeasureSel
            // 
            this.ManualMeasureSel.AutoSize = true;
            this.ManualMeasureSel.Location = new System.Drawing.Point(362, 77);
            this.ManualMeasureSel.Name = "ManualMeasureSel";
            this.ManualMeasureSel.Size = new System.Drawing.Size(224, 20);
            this.ManualMeasureSel.TabIndex = 26;
            this.ManualMeasureSel.Text = "Allow Manual Measurement Entry";
            this.ManualMeasureSel.UseVisualStyleBackColor = true;
            // 
            // UseNorthAmericanDateSel
            // 
            this.UseNorthAmericanDateSel.AutoSize = true;
            this.UseNorthAmericanDateSel.Location = new System.Drawing.Point(362, 51);
            this.UseNorthAmericanDateSel.Name = "UseNorthAmericanDateSel";
            this.UseNorthAmericanDateSel.Size = new System.Drawing.Size(295, 20);
            this.UseNorthAmericanDateSel.TabIndex = 25;
            this.UseNorthAmericanDateSel.Text = "Use North American Date Format (mm/dd/yy)";
            this.UseNorthAmericanDateSel.UseVisualStyleBackColor = true;
            // 
            // ShowOrderComplete
            // 
            this.ShowOrderComplete.AutoSize = true;
            this.ShowOrderComplete.Location = new System.Drawing.Point(362, 25);
            this.ShowOrderComplete.Name = "ShowOrderComplete";
            this.ShowOrderComplete.Size = new System.Drawing.Size(218, 20);
            this.ShowOrderComplete.TabIndex = 24;
            this.ShowOrderComplete.Text = "Show Order Complete Message";
            this.ShowOrderComplete.UseVisualStyleBackColor = true;
            // 
            // LineItemQuantityEdit
            // 
            this.LineItemQuantityEdit.AutoSize = true;
            this.LineItemQuantityEdit.Location = new System.Drawing.Point(20, 181);
            this.LineItemQuantityEdit.Name = "LineItemQuantityEdit";
            this.LineItemQuantityEdit.Size = new System.Drawing.Size(192, 20);
            this.LineItemQuantityEdit.TabIndex = 23;
            this.LineItemQuantityEdit.Text = "Allow Line Item Quantity Edit";
            this.LineItemQuantityEdit.UseVisualStyleBackColor = true;
            // 
            // LineItemWidthEdit
            // 
            this.LineItemWidthEdit.AutoSize = true;
            this.LineItemWidthEdit.Location = new System.Drawing.Point(20, 155);
            this.LineItemWidthEdit.Name = "LineItemWidthEdit";
            this.LineItemWidthEdit.Size = new System.Drawing.Size(178, 20);
            this.LineItemWidthEdit.TabIndex = 22;
            this.LineItemWidthEdit.Text = "Allow Line Item Width Edit";
            this.LineItemWidthEdit.UseVisualStyleBackColor = true;
            // 
            // LineItemHeightEdit
            // 
            this.LineItemHeightEdit.AutoSize = true;
            this.LineItemHeightEdit.Location = new System.Drawing.Point(20, 129);
            this.LineItemHeightEdit.Name = "LineItemHeightEdit";
            this.LineItemHeightEdit.Size = new System.Drawing.Size(183, 20);
            this.LineItemHeightEdit.TabIndex = 21;
            this.LineItemHeightEdit.Text = "Allow Line Item Height Edit";
            this.LineItemHeightEdit.UseVisualStyleBackColor = true;
            // 
            // AllowAllOrderDel
            // 
            this.AllowAllOrderDel.AutoSize = true;
            this.AllowAllOrderDel.Location = new System.Drawing.Point(20, 103);
            this.AllowAllOrderDel.Name = "AllowAllOrderDel";
            this.AllowAllOrderDel.Size = new System.Drawing.Size(212, 20);
            this.AllowAllOrderDel.TabIndex = 20;
            this.AllowAllOrderDel.Text = "Allow All Orders To Be Deleted";
            this.AllowAllOrderDel.UseVisualStyleBackColor = true;
            // 
            // AllowIndividualOrderDel
            // 
            this.AllowIndividualOrderDel.AutoSize = true;
            this.AllowIndividualOrderDel.Location = new System.Drawing.Point(20, 77);
            this.AllowIndividualOrderDel.Name = "AllowIndividualOrderDel";
            this.AllowIndividualOrderDel.Size = new System.Drawing.Size(209, 20);
            this.AllowIndividualOrderDel.TabIndex = 19;
            this.AllowIndividualOrderDel.Text = "Allow Individual Order Deletion";
            this.AllowIndividualOrderDel.UseVisualStyleBackColor = true;
            // 
            // DeleteLIUponQuantity
            // 
            this.DeleteLIUponQuantity.AutoSize = true;
            this.DeleteLIUponQuantity.Location = new System.Drawing.Point(20, 51);
            this.DeleteLIUponQuantity.Name = "DeleteLIUponQuantity";
            this.DeleteLIUponQuantity.Size = new System.Drawing.Size(307, 20);
            this.DeleteLIUponQuantity.TabIndex = 18;
            this.DeleteLIUponQuantity.Text = "Delete Line Item from list when quantity reached";
            this.DeleteLIUponQuantity.UseVisualStyleBackColor = true;
            // 
            // AllowOutOfTolParts
            // 
            this.AllowOutOfTolParts.AutoSize = true;
            this.AllowOutOfTolParts.Location = new System.Drawing.Point(20, 25);
            this.AllowOutOfTolParts.Name = "AllowOutOfTolParts";
            this.AllowOutOfTolParts.Size = new System.Drawing.Size(197, 20);
            this.AllowOutOfTolParts.TabIndex = 17;
            this.AllowOutOfTolParts.Text = "Allow Out Of Tolerance Parts";
            this.AllowOutOfTolParts.UseVisualStyleBackColor = true;
            // 
            // CompanyInfo
            // 
            this.CompanyInfo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.CompanyInfo.Controls.Add(this.CompanyCountry);
            this.CompanyInfo.Controls.Add(this.label50);
            this.CompanyInfo.Controls.Add(this.label49);
            this.CompanyInfo.Controls.Add(this.CompanyURL);
            this.CompanyInfo.Controls.Add(this.LogoFileDialog);
            this.CompanyInfo.Controls.Add(this.LogoFileName);
            this.CompanyInfo.Controls.Add(this.label33);
            this.CompanyInfo.Controls.Add(this.label32);
            this.CompanyInfo.Controls.Add(this.CompanyPhone);
            this.CompanyInfo.Controls.Add(this.CompanyZip);
            this.CompanyInfo.Controls.Add(this.CompanyState);
            this.CompanyInfo.Controls.Add(this.CompanyCity);
            this.CompanyInfo.Controls.Add(this.CompanyAddr2);
            this.CompanyInfo.Controls.Add(this.CompanyAddr1);
            this.CompanyInfo.Controls.Add(this.CompanyNameField);
            this.CompanyInfo.Controls.Add(this.label31);
            this.CompanyInfo.Controls.Add(this.label30);
            this.CompanyInfo.Controls.Add(this.label29);
            this.CompanyInfo.Controls.Add(this.label28);
            this.CompanyInfo.Controls.Add(this.label27);
            this.CompanyInfo.Controls.Add(this.label26);
            this.CompanyInfo.Controls.Add(this.label25);
            this.CompanyInfo.Controls.Add(this.CompanyLogo);
            this.CompanyInfo.Location = new System.Drawing.Point(4, 25);
            this.CompanyInfo.Name = "CompanyInfo";
            this.CompanyInfo.Padding = new System.Windows.Forms.Padding(3);
            this.CompanyInfo.Size = new System.Drawing.Size(683, 355);
            this.CompanyInfo.TabIndex = 5;
            this.CompanyInfo.Text = "Company Info";
            // 
            // CompanyCountry
            // 
            this.CompanyCountry.Location = new System.Drawing.Point(121, 193);
            this.CompanyCountry.Name = "CompanyCountry";
            this.CompanyCountry.Size = new System.Drawing.Size(163, 22);
            this.CompanyCountry.TabIndex = 6;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(12, 196);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(53, 16);
            this.label50.TabIndex = 6;
            this.label50.Text = "Country";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(12, 255);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(88, 16);
            this.label49.TabIndex = 20;
            this.label49.Text = "Website URL";
            // 
            // CompanyURL
            // 
            this.CompanyURL.Location = new System.Drawing.Point(121, 252);
            this.CompanyURL.Name = "CompanyURL";
            this.CompanyURL.Size = new System.Drawing.Size(163, 22);
            this.CompanyURL.TabIndex = 8;
            // 
            // LogoFileDialog
            // 
            this.LogoFileDialog.BackgroundImage = global::ProCABQC11.Properties.Resources.folder_full_48x48;
            this.LogoFileDialog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LogoFileDialog.Location = new System.Drawing.Point(590, 238);
            this.LogoFileDialog.Name = "LogoFileDialog";
            this.LogoFileDialog.Size = new System.Drawing.Size(49, 51);
            this.LogoFileDialog.TabIndex = 18;
            this.LogoFileDialog.UseVisualStyleBackColor = true;
            this.LogoFileDialog.Click += new System.EventHandler(this.LogoFileDialog_Click);
            // 
            // LogoFileName
            // 
            this.LogoFileName.Location = new System.Drawing.Point(363, 252);
            this.LogoFileName.Name = "LogoFileName";
            this.LogoFileName.Size = new System.Drawing.Size(216, 22);
            this.LogoFileName.TabIndex = 9;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(387, 25);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(39, 16);
            this.label33.TabIndex = 16;
            this.label33.Text = "Logo";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(360, 227);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(104, 16);
            this.label32.TabIndex = 15;
            this.label32.Text = "Logo File Name";
            // 
            // CompanyPhone
            // 
            this.CompanyPhone.Location = new System.Drawing.Point(121, 221);
            this.CompanyPhone.Name = "CompanyPhone";
            this.CompanyPhone.Size = new System.Drawing.Size(163, 22);
            this.CompanyPhone.TabIndex = 7;
            // 
            // CompanyZip
            // 
            this.CompanyZip.Location = new System.Drawing.Point(121, 165);
            this.CompanyZip.Name = "CompanyZip";
            this.CompanyZip.Size = new System.Drawing.Size(125, 22);
            this.CompanyZip.TabIndex = 5;
            // 
            // CompanyState
            // 
            this.CompanyState.Location = new System.Drawing.Point(121, 137);
            this.CompanyState.Name = "CompanyState";
            this.CompanyState.Size = new System.Drawing.Size(125, 22);
            this.CompanyState.TabIndex = 4;
            // 
            // CompanyCity
            // 
            this.CompanyCity.Location = new System.Drawing.Point(121, 109);
            this.CompanyCity.Name = "CompanyCity";
            this.CompanyCity.Size = new System.Drawing.Size(213, 22);
            this.CompanyCity.TabIndex = 3;
            // 
            // CompanyAddr2
            // 
            this.CompanyAddr2.Location = new System.Drawing.Point(121, 81);
            this.CompanyAddr2.Name = "CompanyAddr2";
            this.CompanyAddr2.Size = new System.Drawing.Size(213, 22);
            this.CompanyAddr2.TabIndex = 2;
            // 
            // CompanyAddr1
            // 
            this.CompanyAddr1.Location = new System.Drawing.Point(121, 53);
            this.CompanyAddr1.Name = "CompanyAddr1";
            this.CompanyAddr1.Size = new System.Drawing.Size(213, 22);
            this.CompanyAddr1.TabIndex = 1;
            // 
            // CompanyNameField
            // 
            this.CompanyNameField.Location = new System.Drawing.Point(121, 25);
            this.CompanyNameField.Name = "CompanyNameField";
            this.CompanyNameField.Size = new System.Drawing.Size(213, 22);
            this.CompanyNameField.TabIndex = 0;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 224);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(74, 16);
            this.label31.TabIndex = 6;
            this.label31.Text = "Telephone";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 168);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 16);
            this.label30.TabIndex = 5;
            this.label30.Text = "Zip";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 140);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 16);
            this.label29.TabIndex = 4;
            this.label29.Text = "State/Province";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 112);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(30, 16);
            this.label28.TabIndex = 3;
            this.label28.Text = "City";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 84);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(69, 16);
            this.label27.TabIndex = 2;
            this.label27.Text = "Address 2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 56);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(69, 16);
            this.label26.TabIndex = 1;
            this.label26.Text = "Address 1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(106, 16);
            this.label25.TabIndex = 0;
            this.label25.Text = "Company Name";
            // 
            // CompanyLogo
            // 
            this.CompanyLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CompanyLogo.Location = new System.Drawing.Point(390, 44);
            this.CompanyLogo.Name = "CompanyLogo";
            this.CompanyLogo.Size = new System.Drawing.Size(249, 165);
            this.CompanyLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CompanyLogo.TabIndex = 14;
            this.CompanyLogo.TabStop = false;
            // 
            // SystemMaintenance
            // 
            this.SystemMaintenance.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SystemMaintenance.Controls.Add(this.label5);
            this.SystemMaintenance.Controls.Add(this.label4);
            this.SystemMaintenance.Controls.Add(this.ConfigurationImportButton);
            this.SystemMaintenance.Controls.Add(this.ConfigurationExportButton);
            this.SystemMaintenance.Location = new System.Drawing.Point(4, 25);
            this.SystemMaintenance.Name = "SystemMaintenance";
            this.SystemMaintenance.Padding = new System.Windows.Forms.Padding(3);
            this.SystemMaintenance.Size = new System.Drawing.Size(683, 355);
            this.SystemMaintenance.TabIndex = 6;
            this.SystemMaintenance.Text = "Import/Export";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(255, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(359, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Allows user to IMPORT new system configuration.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(255, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(384, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Allows user to EXPORT current system configuration.";
            // 
            // ConfigurationImportButton
            // 
            this.ConfigurationImportButton.Location = new System.Drawing.Point(60, 125);
            this.ConfigurationImportButton.Name = "ConfigurationImportButton";
            this.ConfigurationImportButton.Size = new System.Drawing.Size(150, 40);
            this.ConfigurationImportButton.TabIndex = 7;
            this.ConfigurationImportButton.Text = "Import Confuguration";
            this.ConfigurationImportButton.UseVisualStyleBackColor = true;
            this.ConfigurationImportButton.Click += new System.EventHandler(this.button6_Click);
            // 
            // ConfigurationExportButton
            // 
            this.ConfigurationExportButton.Location = new System.Drawing.Point(60, 46);
            this.ConfigurationExportButton.Name = "ConfigurationExportButton";
            this.ConfigurationExportButton.Size = new System.Drawing.Size(150, 40);
            this.ConfigurationExportButton.TabIndex = 6;
            this.ConfigurationExportButton.Text = "Export Configuration";
            this.ConfigurationExportButton.UseVisualStyleBackColor = true;
            this.ConfigurationExportButton.Click += new System.EventHandler(this.ExportClick);
            // 
            // CancelBut
            // 
            this.CancelBut.BackgroundImage = global::ProCABQC11.Properties.Resources.remove_48x48;
            this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBut.Location = new System.Drawing.Point(422, 406);
            this.CancelBut.Name = "CancelBut";
            this.CancelBut.Size = new System.Drawing.Size(75, 48);
            this.CancelBut.TabIndex = 2;
            this.CancelBut.UseVisualStyleBackColor = true;
            // 
            // OkBut
            // 
            this.OkBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
            this.OkBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.OkBut.Location = new System.Drawing.Point(194, 406);
            this.OkBut.Name = "OkBut";
            this.OkBut.Size = new System.Drawing.Size(75, 48);
            this.OkBut.TabIndex = 1;
            this.OkBut.UseVisualStyleBackColor = true;
            this.OkBut.Click += new System.EventHandler(this.OkBut_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Location = new System.Drawing.Point(21, 227);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(187, 52);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MM Precision";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(96, 21);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(46, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "2 dp";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(26, 21);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(46, 17);
            this.radioButton2.TabIndex = 0;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "1 dp";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButton3);
            this.groupBox5.Controls.Add(this.radioButton4);
            this.groupBox5.Controls.Add(this.radioButton5);
            this.groupBox5.Location = new System.Drawing.Point(21, 170);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(187, 57);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Inch Precision";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(122, 21);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(46, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "3 dp";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(64, 21);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(46, 17);
            this.radioButton4.TabIndex = 1;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "2 dp";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(6, 21);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(46, 17);
            this.radioButton5.TabIndex = 0;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "1 dp";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(326, 218);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(269, 20);
            this.textBox1.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(227, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 37);
            this.label11.TabIndex = 17;
            this.label11.Text = "Configuration Password";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::ProCABQC11.Properties.Resources.folder_full_48x48;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(613, 104);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 40);
            this.button1.TabIndex = 14;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(326, 112);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(269, 20);
            this.textBox2.TabIndex = 13;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(325, 66);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(269, 20);
            this.textBox3.TabIndex = 5;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(324, 21);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(270, 20);
            this.textBox4.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(227, 104);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 37);
            this.label12.TabIndex = 12;
            this.label12.Text = "Completed Order Folder";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::ProCABQC11.Properties.Resources.folder_full_48x48;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button2.Location = new System.Drawing.Point(613, 58);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(42, 40);
            this.button2.TabIndex = 6;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(231, 61);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(87, 37);
            this.label43.TabIndex = 4;
            this.label43.Text = "Default CSV Batch Folder";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button3
            // 
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button3.Location = new System.Drawing.Point(613, 10);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(42, 40);
            this.button3.TabIndex = 3;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(232, 13);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(86, 35);
            this.label44.TabIndex = 1;
            this.label44.Text = "Default CSV Folder";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.button4);
            this.groupBox9.Controls.Add(this.radioButton6);
            this.groupBox9.Controls.Add(this.radioButton7);
            this.groupBox9.Location = new System.Drawing.Point(21, 21);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox9.Size = new System.Drawing.Size(187, 126);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "CSV Import Template Style";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(34, 86);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(113, 27);
            this.button4.TabIndex = 2;
            this.button4.Text = "Edit Template";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(19, 53);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(76, 17);
            this.radioButton6.TabIndex = 1;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Multi Order";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(19, 25);
            this.radioButton7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(83, 17);
            this.radioButton7.TabIndex = 0;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Single Order";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // UseOutOfSquare
            // 
            this.UseOutOfSquare.AutoSize = true;
            this.UseOutOfSquare.Location = new System.Drawing.Point(149, 187);
            this.UseOutOfSquare.Name = "UseOutOfSquare";
            this.UseOutOfSquare.Size = new System.Drawing.Size(119, 20);
            this.UseOutOfSquare.TabIndex = 7;
            this.UseOutOfSquare.Text = "Use Out-of-Square";
            this.UseOutOfSquare.UseVisualStyleBackColor = true;
            // 
            // OptionsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(690, 466);
            this.Controls.Add(this.CancelBut);
            this.Controls.Add(this.OkBut);
            this.Controls.Add(this.OptionsTabControl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "System Options";
            this.Load += new System.EventHandler(this.OptionsDialog_Load);
            this.AxisParameters.ResumeLayout(false);
            this.AxisParameters.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.InterfaceHardware.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.SystemSettings.ResumeLayout(false);
            this.SystemSettings.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.OptionsTabControl.ResumeLayout(false);
            this.MeasurementTab.ResumeLayout(false);
            this.MeasurementTab.PerformLayout();
            this.CompanyInfo.ResumeLayout(false);
            this.CompanyInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyLogo)).EndInit();
            this.SystemMaintenance.ResumeLayout(false);
            this.SystemMaintenance.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button OkBut;
		private System.Windows.Forms.Button CancelBut;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.TabPage AxisParameters;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.RadioButton EncoderNegDirSelect;
		private System.Windows.Forms.RadioButton EncoderPosDirSelect;
		private System.Windows.Forms.TextBox ScaleFactorText;
		private System.Windows.Forms.TextBox DatumPresetText;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ComboBox AxisSelect;
		private System.Windows.Forms.TabPage InterfaceHardware;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button AutoConfigureBut;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.RadioButton ProMuxSelect;
		private System.Windows.Forms.RadioButton ProRFSelect;
		private System.Windows.Forms.ComboBox CommunicationsPort;
		private System.Windows.Forms.TabPage SystemSettings;
		private System.Windows.Forms.Button BrowseCompletedFolder;
		private System.Windows.Forms.TextBox CompletedOrderFolderText;
		private System.Windows.Forms.TextBox DefaultBatchCSVFolderText;
		private System.Windows.Forms.TextBox DefaultCSVFolderText;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button BrowseCSVBatchFolder;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button BrowseCSVFolder;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button EditTemplateBut;
		private System.Windows.Forms.RadioButton CutlistSelect;
		private System.Windows.Forms.RadioButton SingleOrderSelect;
		private System.Windows.Forms.TabControl OptionsTabControl;
		private System.Windows.Forms.TabPage MeasurementTab;
		private System.Windows.Forms.CheckBox AllowOutOfTolParts;
		private System.Windows.Forms.CheckBox DeleteLIUponQuantity;
		private System.Windows.Forms.CheckBox AllowAllOrderDel;
		private System.Windows.Forms.CheckBox AllowIndividualOrderDel;
		private System.Windows.Forms.TabPage CompanyInfo;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox LogoFileName;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.PictureBox CompanyLogo;
		private System.Windows.Forms.TextBox CompanyPhone;
		private System.Windows.Forms.TextBox CompanyZip;
		private System.Windows.Forms.TextBox CompanyState;
		private System.Windows.Forms.TextBox CompanyCity;
		private System.Windows.Forms.TextBox CompanyAddr2;
		private System.Windows.Forms.TextBox CompanyAddr1;
		private System.Windows.Forms.TextBox CompanyNameField;
		private System.Windows.Forms.Button LogoFileDialog;
		private System.Windows.Forms.CheckBox LineItemQuantityEdit;
		private System.Windows.Forms.CheckBox LineItemWidthEdit;
		private System.Windows.Forms.CheckBox LineItemHeightEdit;
		private System.Windows.Forms.CheckBox ShowOrderComplete;
		private System.Windows.Forms.TextBox PasswordText;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.RadioButton InchPrec1;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.RadioButton MMPrec2;
		private System.Windows.Forms.RadioButton MMPrec1;
		private System.Windows.Forms.RadioButton InchPrec3;
		private System.Windows.Forms.RadioButton InchPrec2;
		private System.Windows.Forms.TabPage SystemMaintenance;
		private System.Windows.Forms.CheckBox UseNorthAmericanDateSel;
		private System.Windows.Forms.CheckBox ManualMeasureSel;
		private System.Windows.Forms.CheckBox PrintOutOfSpecLabelSel;
		private System.Windows.Forms.CheckBox TestCompletedLineItemsSel;
		private System.Windows.Forms.Button BrowsePrintLabelTemplateFolder;
		private System.Windows.Forms.TextBox PartLabelTemplateFolderText;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Button BrowsePackListTemplateFolder;
		private System.Windows.Forms.TextBox PackListTemplateFolderText;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.RadioButton radioButton7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.ComboBox LabelPrinterComboBox;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.ComboBox ReportPrinterComboBox;
		private System.Windows.Forms.CheckBox UseReportPreviewSel;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.TextBox CompanyURL;
		private System.Windows.Forms.TextBox CompanyCountry;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button ConfigurationImportButton;
		private System.Windows.Forms.Button ConfigurationExportButton;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.ComboBox PrintLabelsOptionSel;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox PrintPackListSel;
		private System.Windows.Forms.CheckBox AllowIncompleteOrdersSel;
		private System.Windows.Forms.CheckBox SwapAxesSel;
		private System.Windows.Forms.CheckBox UseZAxisCheckBox;
		private System.Windows.Forms.CheckBox UseLabelPreviewSel;
		private System.Windows.Forms.TextBox PivotDistVal;
		private System.Windows.Forms.Label PivotDistLabel;
		private System.Windows.Forms.CheckBox UseAngleCheckBox;
		private System.Windows.Forms.CheckBox DoNotAutoSaveOrderWhenComplete;
		private System.Windows.Forms.CheckBox ShowUserMsgDlgChkBtn;
        private System.Windows.Forms.CheckBox UseOutOfSquare;
    }
}