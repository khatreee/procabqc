﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Windows.Forms;





namespace ProCABQC11 
{

	public enum CSVFileType
	{

		SingleOrderFile,
		CutlistFile

	}

	public enum MeasuringUnits
	{

		Inches,
		Millimeters,
		Degrees

	}

	public enum TemplateDataType
	{

		None,
		SOData,
		MOData

	}

	public enum PrintReportTypes
	{
		None,
		Always,
		ByTemplate
	}

	public enum PrintLabelTypes
	{
		None,
		Always,
		ByTemplate
	}





	[Serializable]
	public struct CellSettingsType
	{
		public int Column;
		public int Row;

	}

	[Serializable]
	public class MeasuringTolerance
	{

		private double Tolerance;
		bool LinearType;

		public MeasuringTolerance()
		{
			// Defaults to 1/32"
			Tolerance = .79375;

			LinearType = true;

		}

		public MeasuringTolerance(bool LinearAxis)
		{
			if (LinearAxis)
			{
				// Defaults to 1/32"
				Tolerance = .79375;

				LinearType = true;

			}

			// Angle axis
			else
			{
				Tolerance = .5;

				LinearType = false;
			}
		}



		public double ToleranceVal
		{
			get { return Tolerance; }
			set { Tolerance = value; }

		}

		public string GetToleranceStr(MeasuringUnits SystemUnits, int MMPrecision, int InchPrecision)
		{
			if(LinearType )
				return PositionConversion.ConvertUnits(Tolerance, MeasuringUnits.Millimeters, SystemUnits, MMPrecision, InchPrecision);

			// Angle type, fixed to 2dp
			else
				return Tolerance.ToString("F2");

		}

		// Tries to set the tolerance
		public bool SetToleranceStr(string Val, MeasuringUnits SystemUnits)
		{

			// Try to convert string to double
			try
			{
				double Temp = Convert.ToDouble(Val);

				if (LinearType)
				{
					if (SystemUnits != MeasuringUnits.Degrees)
					{

						// Check if system units currently inches.  If so, convert to mm
						if (SystemUnits == MeasuringUnits.Inches)
							Temp *= 25.4;

						// Save if conversion successful (Always in mm)
						Tolerance = Temp;

					}
				}

				// Degrees
				else
					Tolerance = Temp;

				return true;

			}

			catch
			{

				MessageBox.Show("Invalid data format.  Not floating point data", "Invalid Data Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;

			}

		}
	}



	[Serializable]
	public class CutlistTemplateDataV1100
	{

		// Data relative to cutlist CSV formats
		// Line item start for cutlist
		private int CutlistLineItemStart = 0;

		// Column settings
		private int[] CutlistColumnSettings = new int[(int)CutlistLineItemColumnIDV1100.MaxItems];

		// Name of template
		private string TemplateName = "";

		// CSV file units move to template in V0003
		private MeasuringUnits CSVMeasurementData = MeasuringUnits.Inches;

		// Measuring tolerance moved to template on V0003
		public MeasuringTolerance WidthPlusTolerance = new MeasuringTolerance();
		public MeasuringTolerance WidthMinusTolerance = new MeasuringTolerance();
		public MeasuringTolerance HeightPlusTolerance = new MeasuringTolerance();
		public MeasuringTolerance HeightMinusTolerance = new MeasuringTolerance();
		public MeasuringTolerance ThicknessPlusTolerance = new MeasuringTolerance();
		public MeasuringTolerance ThicknessMinusTolerance = new MeasuringTolerance();

		// V1.2.1.0
		public MeasuringTolerance AnglePlusTolerance = new MeasuringTolerance(false);	// False for angle measurement
		public MeasuringTolerance AngleMinusTolerance = new MeasuringTolerance(false);



		public int LineItemStart
		{
			get { return CutlistLineItemStart; }
			set { CutlistLineItemStart = value; }
		}

		public string Name
		{
			get { return TemplateName; }
			set	// Do not allow default to be renamed
			{
				if (TemplateName != "Default")
					TemplateName = value;

			}
		}


		public int GetColumnSettings(CutlistLineItemColumnIDV1100 Index)
		{

			if ((Index >= 0) && (Index < CutlistLineItemColumnIDV1100.MaxItems))
			{
				return CutlistColumnSettings[(int)Index];

			}

			else
			{
				return -1;
				
			}
		}

		public bool SetColumnSettings(CutlistLineItemColumnIDV1100 Index, int Val)
		{

			if ((Index >= 0) && (Index < CutlistLineItemColumnIDV1100.MaxItems))
			{
				CutlistColumnSettings[(int)Index] = Val;
				return true;
			}

			else
				return false;
		}

		
		public CutlistTemplateDataV1100(string Name)
		{
			
			// Set default cutlist columns values (no column defined)
			for (int u = 0; u < (int)CutlistLineItemColumnIDV1100.MaxItems; u++)
			{
				CutlistColumnSettings[u] = -1;
			}

			// Set template name
			TemplateName = Name;


		}

		public MeasuringUnits CSVMeasureUnits
		{

			get { return CSVMeasurementData; }
			set { CSVMeasurementData = value; }
		}


	}


	[Serializable]
	public class SingleOrderTemplateDataV1100
	{

		// Data relative to single order CSV formats
		// Header cell array
		private CellSettingsType[] SingleOrderHeaderCellSettings = new CellSettingsType[(int)SingleOrderHeaderCellIDV1100.MaxItems];

		// Line item column Settings
		private int[] SingleOrderColumnSettings = new int[(int)SingleOrderLineItemColumnIDV1100.MaxItems];

		// Line item start for single order
		private int SingleOrderLineItemStart = 0;

		// Name of template
		private string TemplateName = "";

		// CSV file units move to template in V0003
		private MeasuringUnits CSVMeasurementData = MeasuringUnits.Inches;

		// Measuring tolerance moved to template on V0003
		public MeasuringTolerance WidthPlusTolerance = new MeasuringTolerance();
		public MeasuringTolerance WidthMinusTolerance = new MeasuringTolerance();
		public MeasuringTolerance HeightPlusTolerance = new MeasuringTolerance();
		public MeasuringTolerance HeightMinusTolerance = new MeasuringTolerance();
		public MeasuringTolerance ThicknessPlusTolerance = new MeasuringTolerance();
		public MeasuringTolerance ThicknessMinusTolerance = new MeasuringTolerance();

		// V1.2.1.0
		public MeasuringTolerance AnglePlusTolerance = new MeasuringTolerance(false);
		public MeasuringTolerance AngleMinusTolerance = new MeasuringTolerance(false);


		// Accessors
		public int LineItemStart
		{
			get { return SingleOrderLineItemStart; }
			set { SingleOrderLineItemStart = value; }
		}


		public string Name
		{
			get { return TemplateName; }
			set	// Do not allow default to be renamed
			{
				if (TemplateName != "Default")
					TemplateName = value;

			}
		}

		public int GetColumnSetting(SingleOrderLineItemColumnIDV1100 Index)
		{
			if (Index >= 0 && Index < SingleOrderLineItemColumnIDV1100.MaxItems)
			{
				return SingleOrderColumnSettings[(int)Index];
				
			}

			else
			{
				return -1;
			}
		}

		public bool SetColumnSettings(SingleOrderLineItemColumnIDV1100 Index, int Val)
		{
			if (Index >= 0 && Index < SingleOrderLineItemColumnIDV1100.MaxItems)
			{
				SingleOrderColumnSettings[(int)Index] = Val;
				return true;
			}

			else
				return false;
		}

		public CellSettingsType GetCellSettings(SingleOrderHeaderCellIDV1100 Index)
		{

			if ((Index >= 0) && (Index < SingleOrderHeaderCellIDV1100.MaxItems))
			{

				return SingleOrderHeaderCellSettings[(int)Index];

			}

			else
			{
				CellSettingsType CellData = new CellSettingsType();
				CellData.Column = -1;
				CellData.Row = -1;
				return CellData;
				
			}
		}

		public bool SetCellSettings(SingleOrderHeaderCellIDV1100 Index, int Column, int Row)
		{
			if ((Index >= 0) && (Index < SingleOrderHeaderCellIDV1100.MaxItems))
			{
				SingleOrderHeaderCellSettings[(int)Index].Column = Column;
				SingleOrderHeaderCellSettings[(int)Index].Row = Row;

				return true;
			}

			else
				return false;
		}



		public SingleOrderTemplateDataV1100(string Name)
		{

			// Set default for Single Order line item column values
			for (int u = 0; u < (int)SingleOrderLineItemColumnIDV1100.MaxItems; u++)
			{
				SingleOrderColumnSettings[u] = -1;
			}

			// Set default for Single Order header cell values
			for (int u = 0; u < (int)SingleOrderHeaderCellIDV1100.MaxItems; u++)
			{
				SingleOrderHeaderCellSettings[u] = new CellSettingsType();
				SingleOrderHeaderCellSettings[u].Column = -1;
				SingleOrderHeaderCellSettings[u].Row = -1;
			}


			// Set template name
			TemplateName = Name;

		}

		public MeasuringUnits CSVMeasureUnits
		{

			get { return CSVMeasurementData; }
			set { CSVMeasurementData = value; }
		}




	}



	[Serializable]
	public class PersistentParametersV1100 : ICloneable
	{



		public object Clone()
		{

			MemoryStream ms = new MemoryStream();

			BinaryFormatter bf = new BinaryFormatter();

			bf.Serialize(ms, this);

			ms.Position = 0;

			object obj = bf.Deserialize(ms);

			ms.Close();

			return obj;


		}


	

       private string [] ParameterNameStrings = new string[] 
	   {

            "#PARAMETERS START",
            "CSV File Type Used",
            "Edit Line Item Height",
            "Edit Line Item Width",
			"Edit Line Item Required Qty",
			"System Units",
            "Inch Precision",
            "MM Precision",
            "CSV File Path",
            "CSV Batch Path",
            "Completed Order Path",
            "Pack List Template Path",
            "Part Label Template Path",
			"Print Pack Lists",
			"Allow Invalid Part Matches",
			"Delete Line Item When Complete",
			"Allow Individual Order Deletion",	  
			"Allow All Order Deletion",
			"Show Order Complete",
			"Use North American Date",
			"Allow Manual Measurements",
			"Print Out Of Spec Label",
			"Compare Completed Line Items",
			"Print Measured Part Labels",
			"Use Report Print Preview",	   
			"Use Label Print Preview",
			"Measurement Device Port Name",
			"Measurement Device Type",
			"Use Z Axis",
			"Report Printer Name",
			"Label Printer Name",
			"Company Name",
			"Company Addr1",
			"Company Addr2",
			"Company City",
			"Company State",
			"Company Zip",
			"Company Phone",
			"Company Logo File Name",
			"Company Country",
			"Company URL",
			"Use Angle Axis",
			"Angle Vertical Offset",
			"Password",
			"Allow Incomplete Orders To be Processed",
			"Swap Height and Width Axes",
			"Do not process order when complete",
			"Show User Message Dialog",
			"#PARAMETERS END"
		};



		public enum GeneralParameterNames
		{
			ParamStart,
			CSVFileTypeUsed,
			EditLineItemHeight,
			EditLineItemWidth,
			EditLineItemQty,
			SystemUnits,
			InchPrecision,
			MMPrecision,
			CSVFilePath,
			CSVBatchPath,
			CompletedOrderPath,
			PackListTemplatePath,
			PartLabelTemplatePath,
			PrintPackList,
			AllowInvalidPartMatches,
			DeleteLineItemWhenComplete,
			AllowIndividualOrderDeletion,
			AllowAllOrderDeletion,
			ShowOrderComplete,
			UseNorthAmericanDate,
			AllowManualMeasurements,
			PrintOutOfSpecLabel,
			CompareCompletedLineItems,
			PrintMeasuredPartLabels,
			UseReportPrintPreview,
			UseLabelPrintPreview,
			MeasurementDevicePortName,
			MeasurementDeviceType,
			UseZAxisFunction,
			ReportPrinterName,
			LabelPrinterName,
			CompanyName,
			CompanyAddr1,
			CompanyAddr2,
			CompanyCity,
			CompanyState,
			CompanyZip,
			CompanyPhone,
			CompanyLogoFileName,
			CompanyCountry,
			CompanyURL,
			UseAngleAxisFunction,
			AngleVerticalOffsetVal,
			Password,
			AllowIncompleteOrdersToBeProcessed,
			SwapHeightWidthAxes,
			DoNotProcessOrderWhenComplete,
			ShowUserMessageDialog,
			ParamEnd
		}


		public string GetParamsStartString()
		{
			return ParameterNameStrings[(int)GeneralParameterNames.ParamStart];

		}


		public void ExportSystemParameters(string Filename, ref MeasurementInterfaceHardware MeasurementHardware)
		{

			StreamWriter ExportStream;

			try
			{

				// Try to open the stream for the CSV write
				using (ExportStream = new StreamWriter(Filename))
				{

					// Export system parameters
					ExportParamsValues(ExportStream);

					// Export order Templates if they exist
					ExportOrderTemplates(ExportStream);

					// Export axis data
					MeasurementHardware.ExportAxisData(ExportStream);
					
				}

				ExportStream.Close();
			}

			catch (Exception e)
			{
				MessageBox.Show("Unable to export system parameter data:\n" + e.Message, "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}
		}

		private void ExportParamsValues(StreamWriter Stream)
		{



			// Export string data of parameters
			// Write start
			Stream.WriteLine(String.Format("{0}", ParameterNameStrings[(int)GeneralParameterNames.ParamStart]));

			// CSV type
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CSVFileTypeUsed], CSVFileTypeUsed.ToString()));

			// Edit line Item Height
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.EditLineItemHeight], EditLineItemHeight.ToString()));

			// Edit line Item Width
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.EditLineItemWidth], EditLineItemWidth.ToString()));

			// Edit line Item Qty
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.EditLineItemQty], EditLineItemQuantityReq.ToString()));

			// System Units
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.SystemUnits], SystemUnits.ToString()));

			// Inch Precision
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.InchPrecision], InchPrecision.ToString()));

			// Metric Precision
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.MMPrecision], MMPrecision.ToString()));

			// CSV File Path
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CSVFilePath], CSVFilePath.ToString()));

			// CSV Batch File Path
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CSVBatchPath], CSVBatchFilePath.ToString()));

			// Complete Order File Path
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompletedOrderPath], CompletedOrderFilePath.ToString()));

			// Report template file path
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.PackListTemplatePath], PackListTemplateFilePath.ToString()));

			// Part Label template file path
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.PartLabelTemplatePath], PartLabelTemplateFilePath.ToString()));

			// Print Packlist Checkbox
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.PrintPackList], PrintPacklist.ToString()));
			
			// Allow invalid part matches
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.AllowInvalidPartMatches], AllowInvalidPartMatches.ToString()));

			// Delete line item from displayed list when order complete
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.DeleteLineItemWhenComplete], DeleteLineItemWhenComplete.ToString()));

			// Allow individual order deletion
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.AllowIndividualOrderDeletion], AllowIndividualOrderDeletion.ToString()));

			// Allow all order deletion
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.AllowAllOrderDeletion], AllowAllOrderDeletion.ToString()));

			// Show order complete message when order completed
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.ShowOrderComplete], ShowOrderComplete.ToString()));

			// Use North American Date Format
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.UseNorthAmericanDate], UseNAmericanDate.ToString()));

			// Allow manual measurements to be completed
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.AllowManualMeasurements], AllowManualMeasurements.ToString()));

			// Print Out Of Spec Labels
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.PrintOutOfSpecLabel], PrintOutOfSpecLab.ToString()));

			// Check completed line items when conducting match test
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompareCompletedLineItems], CheckCompletedLI.ToString()));

			// Print Measured Part Labels
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.PrintMeasuredPartLabels], PrintMPartLabels.ToString()));

			// Use print preview on reports
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.UseReportPrintPreview], ReportPrintPreview.ToString()));

			// Use print preview on labels
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.UseLabelPrintPreview], LabelPrintPreview.ToString()));

			// Measurement device port name
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.MeasurementDevicePortName], MeasurementDevicePortName.ToString()));

			// Measurement Device Type
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.MeasurementDeviceType], MeasurementDeviceType.ToString()));

			// Use Z Axis
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.UseZAxisFunction], ZAxisInUse.ToString()));

			// Report Printer Name
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.ReportPrinterName], ReportPrinterName.ToString()));

			// Label Printer Name
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.LabelPrinterName], LabelPrinterName.ToString()));

			// Company Name
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyName], CompanyName.ToString()));

			// Company Addr1
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyAddr1], CompanyAddress1.ToString()));

			// Company Addr2
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyAddr2], CompanyAddress2.ToString()));

			// Company City
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyCity], CompanyCity.ToString()));

			// Company State
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyState], CompanyState.ToString()));

			// Company Zip
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyZip], CompanyZip.ToString()));

			// Company Phone
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyPhone], CompanyPhone.ToString()));

			// Company Logo File name
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyLogoFileName], CompanyLogoFile.ToString()));

			// Company URL
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyURL], CompanyURL.ToString()));

			// Company Country
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.CompanyCountry], CompanyCountry.ToString()));

			// Use angle axis
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.UseAngleAxisFunction], AngleAxisInUse.ToString()));

			// Angle Vertical Offset
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.AngleVerticalOffsetVal], AngleVerticalOffsetValue.ToString()));

			// Current Password
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.Password], Password.ToString()));
 
			// Allow incomplete orders to be processed
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.AllowIncompleteOrdersToBeProcessed], AllowIncompleteOrderProcessing.ToString()));

			// Swap height and width axes
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.SwapHeightWidthAxes], SwapHeightandWidth.ToString()));

			// Do not process order when complete
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.DoNotProcessOrderWhenComplete], DoNotProcessOrdersWhenComplete.ToString()));

			// Show User Message Dialog
			Stream.WriteLine(String.Format("{0},{1}", ParameterNameStrings[(int)GeneralParameterNames.ShowUserMessageDialog], ShowUserMessageDialog.ToString()));


			// End of Parameters
			Stream.WriteLine(String.Format("{0}", ParameterNameStrings[(int)GeneralParameterNames.ParamEnd]));


 
		}


		public bool ImportParamsValues(StreamReader Stream, ref int LineNumber)
		{

			// Create an array of strings to pass to the line parser
			string[] Columns;

			Char Delimit = ',';
			bool Loop = true;
			bool Result = true;


			while(Loop)
			{

				string InputLine = "";

				// Read a line from the stream
				InputLine = Stream.ReadLine();

				LineNumber++;

				// If eof, set return status as false (error, eof reached) and break
				if (InputLine == null)
				{
					Loop = false;
					Result = false;
				}

				// Not EOF.  Process imported string
				else
				{
					// Split the line into columns delimited by comma
					Columns = InputLine.Split(Delimit);


					// Check if at end of parameters
					if (Columns[0].Equals(ParameterNameStrings[(int)GeneralParameterNames.ParamEnd]))
						Loop = false;

					// Save parameter based on parameter ID
					else
					{


						// Import the parameter based on the field name
						UpdateParameterFromImport(Columns[0], Columns[1]);

					}


				}

			}


			return Result;


		}


		// Locates the enumerated value of the parameter based on the passed string
		private bool LookupParameterID(string ParameterID, out GeneralParameterNames ID)
		{
			ID = GeneralParameterNames.ParamEnd;
			bool Result = false;

			for (GeneralParameterNames u = GeneralParameterNames.ParamStart; u < GeneralParameterNames.ParamEnd; u++)
			{

				if (ParameterNameStrings[(int)u].Equals(ParameterID))
				{
					ID = u;
					Result = true;
					break;
				}
 
			}

			return Result;
			
		}



		private void UpdateParameterFromImport(string ParameterID, string Data)
		{

			GeneralParameterNames Index;

			// Locate the associated enumerated parameter ID based on the passed string
			if (LookupParameterID(ParameterID, out Index))
			{
				// if valid ID found, save data to appropriate data member
				switch (Index)
				{
					case GeneralParameterNames.CSVFileTypeUsed:

						if (Data.Equals("SingleOrderFile"))
							CSVFileTypeUsed = CSVFileType.SingleOrderFile;

						else
							CSVFileTypeUsed = CSVFileType.CutlistFile;

						break;


					case GeneralParameterNames.EditLineItemHeight:

						EditLineItemHeight = bool.Parse(Data);

						break;


					case GeneralParameterNames.EditLineItemWidth:

						EditLineItemWidth = bool.Parse(Data);

						break;


					case GeneralParameterNames.EditLineItemQty:

						EditLineItemQuantityReq = bool.Parse(Data);

						break;


					case GeneralParameterNames.SystemUnits:

						if (Data.Equals("Inches"))
							SystemUnits = MeasuringUnits.Inches;

						else
							SystemUnits = MeasuringUnits.Millimeters;

						break;


					case GeneralParameterNames.InchPrecision:

						InchPrecision = Convert.ToInt32(Data);

						break;


					case GeneralParameterNames.MMPrecision:

						MMPrecision = Convert.ToInt32(Data);

						break;


					case GeneralParameterNames.CSVFilePath:

						CSVFilePath = Data;

						break;


					case GeneralParameterNames.CSVBatchPath:

						CSVBatchFilePath = Data;

						break;


					case GeneralParameterNames.CompletedOrderPath:

						CompletedOrderFilePath = Data;

						break;


					case GeneralParameterNames.PackListTemplatePath:

						PackListTemplateFilePath = Data;

						break;


					case GeneralParameterNames.PartLabelTemplatePath:

						PartLabelTemplateFilePath = Data;

						break;


					case GeneralParameterNames.PrintPackList:

						if (Data.Equals("None"))
							PrintPacklist = PrintReportTypes.None;

						else if (Data.Equals("Always"))
							PrintPacklist = PrintReportTypes.Always;

						else
							PrintPacklist = PrintReportTypes.ByTemplate;


						break;


					case GeneralParameterNames.AllowInvalidPartMatches:

						AllowInvalidPartMatches = bool.Parse(Data);

						break;


					case GeneralParameterNames.DeleteLineItemWhenComplete:

						DeleteLIWhenComplete = bool.Parse(Data);

						break;


					case GeneralParameterNames.AllowIndividualOrderDeletion:

						AllowIndividualOrderDeletion = bool.Parse(Data);

						break;


					case GeneralParameterNames.AllowAllOrderDeletion:

						AllowAllOrderDeletion = bool.Parse(Data);

						break;


					case GeneralParameterNames.ShowOrderComplete:

						ShowOrderComplete = bool.Parse(Data);

						break;


					case GeneralParameterNames.UseNorthAmericanDate:

						UseNAmericanDate = bool.Parse(Data);

						break;


					case GeneralParameterNames.AllowManualMeasurements:

						AllowManMeasurements = bool.Parse(Data);

						break;


					case GeneralParameterNames.PrintOutOfSpecLabel:

						PrintOutOfSpecLab = bool.Parse(Data);

						break;


					case GeneralParameterNames.CompareCompletedLineItems:

						CheckCompletedLI = bool.Parse(Data);

						break;


					case GeneralParameterNames.PrintMeasuredPartLabels:

						if(Data.Equals("None"))
							PrintMPartLabels = PrintLabelTypes.None;

						else if(Data.Equals("Always"))
							PrintMPartLabels = PrintLabelTypes.Always;

						else
							PrintMPartLabels = PrintLabelTypes.ByTemplate;

						break;


					case GeneralParameterNames.UseReportPrintPreview:

						ReportPrintPreview = bool.Parse(Data);

						break;


					case GeneralParameterNames.UseLabelPrintPreview:

						LabelPrintPreview = bool.Parse(Data);

						break;


					case GeneralParameterNames.MeasurementDevicePortName:

						int v = Data.Length;

						MeasurementDevicePortName = Data;

						break;


					case GeneralParameterNames.MeasurementDeviceType:

						if (Data.Equals("ProMUX"))
							MeasurementDeviceType = MeasurementInterfaceHardware.MeasureHardwareType.ProMUX;

						else
							MeasurementDeviceType = MeasurementInterfaceHardware.MeasureHardwareType.ProRF;


						break;


					case GeneralParameterNames.UseZAxisFunction:

						ZAxisInUse = bool.Parse(Data);

						break;


					case GeneralParameterNames.ReportPrinterName:

						ReportPrinterName = Data;

						break;


					case GeneralParameterNames.LabelPrinterName:

						LabelPrinterName = Data;

						break;


					case GeneralParameterNames.CompanyName:

						CompanyName = Data;

						break;


					case GeneralParameterNames.CompanyAddr1:

						CompanyAddress1 = Data;

						break;


					case GeneralParameterNames.CompanyAddr2:

						CompanyAddress2 = Data;

						break;


					case GeneralParameterNames.CompanyCity:

						CompanyCity = Data;

						break;


					case GeneralParameterNames.CompanyState:

						CompanyState = Data;

						break;


					case GeneralParameterNames.CompanyZip:

						CompanyZip = Data;

						break;


					case GeneralParameterNames.CompanyLogoFileName:

						CompanyLogoFile = Data;

						break;


					case GeneralParameterNames.CompanyURL:

						CompanyURL = Data;

						break;


					case GeneralParameterNames.CompanyPhone:

						CompanyPhone = Data;

						break;


					case GeneralParameterNames.CompanyCountry:

						CompanyCountry = Data;

						break;


					case GeneralParameterNames.UseAngleAxisFunction:

						AngleAxisInUse = bool.Parse(Data);

						break;

					case GeneralParameterNames.AngleVerticalOffsetVal:

						AngleVerticalOffsetValue = double.Parse(Data);

						break;


					case GeneralParameterNames.Password:

						Password = Data;

						break;


					case GeneralParameterNames.AllowIncompleteOrdersToBeProcessed:

						AllowIncompleteOrderProcessing = bool.Parse(Data);

						break;


					case GeneralParameterNames.SwapHeightWidthAxes:

						SwapHeightandWidth = bool.Parse(Data);

						break;


					case GeneralParameterNames.DoNotProcessOrderWhenComplete:

						DoNotProcessOrdersWhenComplete = bool.Parse(Data);

						break;


					case GeneralParameterNames.ShowUserMessageDialog:

						ShowUserMessageDialog = bool.Parse(Data);

						break;


						






				}
			}
		}



		private string[] OrderTemplateNameStrings = new string[]
		{
			"#ORDER TEMPLATES START",
			"#SINGLE ORDER TEMPLATE START",
			"#SINGLE ORDER TEMPLATE END",
			"#MULTI ORDER TEMPLATE START",
			"#MULTI ORDER TEMPLATE END",
			"Template Name",
			"Line Item Start",
			"CSV Units",
			"Width Tolerance Min",
			"Width Tolerance Max",
			"Height Tolerance Min",
			"Height Tolerance Max",
			"Thickness Tolerance Min",
			"Thickness Tolerance Max",
			"Angle Tolerance Min",
			"Angle Tolerance Max",
			"#ORDER TEMPLATES END"

		};


		public enum OrderTemplateCommonFields
		{
			OrderTemplatesStart,
			SingleOrderTemplateStart,
			SingleOrderTemplateEnd,
			MultiOrderTemplateStart,
			MultiOrderTemplateEnd,
			TemplateName,
			LineItemStart,
			CSVUnits,
			WidthTolMin,
			WidthTolMax,
			HeightTolMin,
			HeightTolMax,
			ThicknessTolMin,
			ThicknessTolMax,
			AngleTolMin,
			AngleTolMax,
			OrderTemplatesEnd
		}



		// Return the string that designates the start of order templates
		public string GetOrderTemplatesStartString()
		{
			return OrderTemplateNameStrings[(int)OrderTemplateCommonFields.OrderTemplatesStart];

		}


		private void ExportOrderTemplates(StreamWriter Stream)
		{

			// Write template start only if one or more templates exist
			if ((SOTemplates.Count > 0) || (MOTemplates.Count > 0))
			{
				// Write start of templates for export.  This is only done once per export
				Stream.WriteLine(string.Format("{0}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.OrderTemplatesStart]));

				// Export the Single Order templates if any exists
				for (int u = 0; u < SOTemplates.Count; u++)
				{
					// Export each single order template
					ExportSingleOrderTemplate(Stream, u);
				}

				// Export the Multi Order templates if any exists
				for (int u = 0; u < MOTemplates.Count; u++)
				{

					// Export each multi order template
					ExportMultiOrderTemplate(Stream, u);
					
				}

				// Write the end of templates file marker
				Stream.WriteLine(string.Format("{0}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.OrderTemplatesEnd]));

			}
		}

		private void ExportSingleOrderTemplate(StreamWriter Stream, int TemplateIndex)
		{

			// Write start of single order template export
			Stream.WriteLine(string.Format("{0}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.SingleOrderTemplateStart]));

			// The template common fields
			// Write the template name
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.TemplateName], SOTemplates[TemplateIndex].Name));

			// Write the line item line start
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.LineItemStart], SOTemplates[TemplateIndex].LineItemStart));

			// Write the CSV Units
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.CSVUnits], SOTemplates[TemplateIndex].CSVMeasureUnits.ToString()));

			// Write the Width min tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.WidthTolMin], SOTemplates[TemplateIndex].WidthMinusTolerance.ToleranceVal));

			// Write the Width max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.WidthTolMax], SOTemplates[TemplateIndex].WidthPlusTolerance.ToleranceVal));

			// Write the Height min tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.HeightTolMin], SOTemplates[TemplateIndex].HeightMinusTolerance.ToleranceVal));

			// Write the Width max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.HeightTolMax], SOTemplates[TemplateIndex].HeightPlusTolerance.ToleranceVal));

			// Write the Thickness (depth) min tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.ThicknessTolMin], SOTemplates[TemplateIndex].ThicknessMinusTolerance.ToleranceVal));

			// Write the Thickness max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.ThicknessTolMax], SOTemplates[TemplateIndex].ThicknessPlusTolerance.ToleranceVal));

			// Write the angle min tolerance  V1.2.0.6
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.AngleTolMin], SOTemplates[TemplateIndex].AngleMinusTolerance.ToleranceVal));

			// Write the angle max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.AngleTolMax], SOTemplates[TemplateIndex].AnglePlusTolerance.ToleranceVal));



			// The template HEADER data
			SingleOrderHeaderTemplate Template = new SingleOrderHeaderTemplate();

			for (SingleOrderHeaderCellIDV1100 u = SingleOrderHeaderCellIDV1100.OrderID; u < SingleOrderHeaderCellIDV1100.MaxItems; u++)
			{

				string FieldName;
				CellSettingsType Cell;


				// Get the field text description
				Template.GetHeaderFieldText(out FieldName, (int)u);

				// Get the cell settings
				Cell = SOTemplates[TemplateIndex].GetCellSettings(u);


				// Write the header cell data export (column then row)
				Stream.WriteLine(string.Format("{0},{1},{2}", FieldName, Cell.Column, Cell.Row));


			}

			// The template line item data
			SingleOrderLineItemTemplate LITemplate = new SingleOrderLineItemTemplate();

			for (SingleOrderLineItemColumnIDV1100 u = SingleOrderLineItemColumnIDV1100.LineItemNumber; u < SingleOrderLineItemColumnIDV1100.MaxItems; u++)
			{

				string FieldName;
				int Column;

				// Get the field text description
				LITemplate.GetLineItemFieldText(out FieldName, (int)u);

				// Get the column data
				Column = SOTemplates[TemplateIndex].GetColumnSetting(u);

				// Write the line item data export
				Stream.WriteLine(string.Format("{0},{1}", FieldName, Column));
			}

			// Write end of single order template export
			Stream.WriteLine(string.Format("{0}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.SingleOrderTemplateEnd]));

		}



		private void ExportMultiOrderTemplate(StreamWriter Stream, int TemplateIndex)
		{

			// Write start of multi order template export
			Stream.WriteLine(string.Format("{0}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.MultiOrderTemplateStart]));

			// The template common fields
			// Write the template name
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.TemplateName], MOTemplates[TemplateIndex].Name));

			// Write the line item line start
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.LineItemStart], MOTemplates[TemplateIndex].LineItemStart));

			// Write the CSV Units
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.CSVUnits], MOTemplates[TemplateIndex].CSVMeasureUnits.ToString()));

			// Write the Width min tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.WidthTolMin], MOTemplates[TemplateIndex].WidthMinusTolerance.ToleranceVal));

			// Write the Width max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.WidthTolMax], MOTemplates[TemplateIndex].WidthPlusTolerance.ToleranceVal));

			// Write the Height min tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.HeightTolMin], MOTemplates[TemplateIndex].HeightMinusTolerance.ToleranceVal));

			// Write the Width max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.HeightTolMax], MOTemplates[TemplateIndex].HeightPlusTolerance.ToleranceVal));

			// Write the Thickness min tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.ThicknessTolMin], MOTemplates[TemplateIndex].ThicknessMinusTolerance.ToleranceVal));

			// Write the Thickness max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.ThicknessTolMax], MOTemplates[TemplateIndex].ThicknessPlusTolerance.ToleranceVal));

			// Write the Thickness min tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.AngleTolMin], MOTemplates[TemplateIndex].AngleMinusTolerance.ToleranceVal));

			// Write the Thickness max tolerance
			Stream.WriteLine(string.Format("{0},{1}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.ThicknessTolMax], MOTemplates[TemplateIndex].AnglePlusTolerance.ToleranceVal));



			
			// The template line item data
			CutlistTemplate LITemplate = new CutlistTemplate();

			for (CutlistLineItemColumnIDV1100 u = CutlistLineItemColumnIDV1100.LineItemNumber; u < CutlistLineItemColumnIDV1100.MaxItems; u++)
			{

				string FieldName;
				int Column;

				// Get the field text description
				LITemplate.GetLineItemFieldText(out FieldName, (int)u);

				// Get the column data
				Column = MOTemplates[TemplateIndex].GetColumnSettings(u);

				// Write the line item data export
				Stream.WriteLine(string.Format("{0},{1}", FieldName, Column));
			}

			// Write end of single order template export
			Stream.WriteLine(string.Format("{0}", OrderTemplateNameStrings[(int)OrderTemplateCommonFields.MultiOrderTemplateEnd]));

		}


		// Import Template Data methods
		public bool ImportOrderTemplateValues(StreamReader Stream, ref int LineNumber)
		{

			// Create an array of strings to pass to the line parser
			string[] Columns;

			Char Delimit = ',';
			bool Loop = true;
			bool Result = true;

			// Delete the current list of both style templates
			SOTemplates.Clear();
			MOTemplates.Clear();

			// Template objects
			SingleOrderTemplateDataV1100 SOData = new SingleOrderTemplateDataV1100("");
			CutlistTemplateDataV1100 MOData = new CutlistTemplateDataV1100("");

			// Current Template Data being read
			TemplateDataType DataType = TemplateDataType.None;


			while (Loop)
			{

				string InputLine = "";

				// Read a line from the stream
				InputLine = Stream.ReadLine();

				LineNumber++;

				// If eof, set return status as false (error, eof reached) and break
				if (InputLine == null)
				{
					Loop = false;
					Result = false;
				}

				// Not EOF.  Process imported string
				else
				{
					// Split the line into columns delimited by comma
					Columns = InputLine.Split(Delimit);

					// Begin testing for possible control headers within template section

					// Test for new Single Order template (start)
					if(Columns[0].Equals(OrderTemplateNameStrings[(int)OrderTemplateCommonFields.SingleOrderTemplateStart]))
					{
						// Create new SO template item
						SOData = new SingleOrderTemplateDataV1100("");

						// Save the current type of file being read
						DataType = TemplateDataType.SOData;
	
					}

					// Check if at end of this SO template data
					else if (Columns[0].Equals(OrderTemplateNameStrings[(int)OrderTemplateCommonFields.SingleOrderTemplateEnd]))
					{

						// Add it to the list of SO templates
						SOTemplates.Add(SOData);

						// Set data type to none
						DataType = TemplateDataType.None;

					}

					// Save SO parameter based on parameter ID
					else if(DataType == TemplateDataType.SOData)
					{
						// Check size of Columns[].  If three parameters, 
						// Check if header is part of common data.  If so, save it.  If not, then template header or line item data
						if (!UpdateSingleOrderTemplateCommonDataFromImport(SOData, Columns[0], Columns[1]))
						{
							// If three parameters, parse as template header info
							if (Columns.Length == 3)
							{
								// Import header data based in header
								UpdateSingleOrderTemplateHeaderFromImport(SOData, Columns[0], Columns[1], Columns[2]);
							}

							// If two parameters, parse as template line item data
							else if (Columns.Length == 2)
							{
								// Import template line item data based on header
								UpdateSingleOrderTemplateLineItemFromImport(SOData, Columns[0], Columns[1]);
							}

						}

					}

					// Test for new Multi Order template (start)
					if (Columns[0].Equals(OrderTemplateNameStrings[(int)OrderTemplateCommonFields.MultiOrderTemplateStart]))
					{
						// Create new SO template item
						MOData = new CutlistTemplateDataV1100("");

						// Save the current type of file being read
						DataType = TemplateDataType.MOData;

					}

					// Check if at end of this MO template data
					else if (Columns[0].Equals(OrderTemplateNameStrings[(int)OrderTemplateCommonFields.MultiOrderTemplateEnd]))
					{

						// Add it to the list of SO templates
						MOTemplates.Add(MOData);

						// Set data type to none
						DataType = TemplateDataType.None;

					}

					// Save MO parameter based on parameter ID
					else if (DataType == TemplateDataType.MOData)
					{
						// Test for correct number of line parameters
						if (Columns.Length == 2)
						{
							// Check if header is part of common data.  If so, save it.  If not, then template header or line item data
							if (!UpdateMultiOrderTemplateCommonDataFromImport(MOData, Columns[0], Columns[1]))
							{
								// Import the parameter based on the field name
								UpdateMultiOrderTemplateFromImport(MOData, Columns[0], Columns[1]);

							}
						}

					}

					// Check if at end of all templates.  If so, break loop and return
					else if (Columns[0].Equals(OrderTemplateNameStrings[(int)OrderTemplateCommonFields.OrderTemplatesEnd]))
					{

						Loop = false;

					}

				}

			}


			return Result;


		}


		// Attempts to locate header string in common Order header strings.  If found return true and index
		private bool LocateOrderCommonDataIndex(string HeaderStr, out OrderTemplateCommonFields Index)
		{

			bool Result = false;
			Index = OrderTemplateCommonFields.OrderTemplatesEnd;


			for (OrderTemplateCommonFields u = OrderTemplateCommonFields.TemplateName; u < OrderTemplateCommonFields.OrderTemplatesEnd; u++)
			{
				if (HeaderStr.Equals(OrderTemplateNameStrings[(int)u]))
				{
					Index = u;
					Result = true;
					break;
				}
			}

			return Result;
		}


		// Updates the Single Order template common data
		private bool UpdateSingleOrderTemplateCommonDataFromImport(SingleOrderTemplateDataV1100 TemplateData, string Header, string Data)
		{

			bool Result = true;
			OrderTemplateCommonFields Index;

			// Test if passed header is part of order common data.  If so, parse and return true
			if(LocateOrderCommonDataIndex(Header, out Index))
			{
				// Save based on index
				switch (Index)
				{
					case OrderTemplateCommonFields.TemplateName :

						TemplateData.Name = Data;
						break;


					case OrderTemplateCommonFields.LineItemStart :

						TemplateData.LineItemStart = Convert.ToInt32(Data);
						break;


					case OrderTemplateCommonFields.CSVUnits :

						if(Data.Equals(MeasuringUnits.Inches.ToString()))
							TemplateData.CSVMeasureUnits = MeasuringUnits.Inches;

						else
							TemplateData.CSVMeasureUnits = MeasuringUnits.Millimeters;

						break;


					case OrderTemplateCommonFields.HeightTolMin :

						TemplateData.HeightMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.HeightTolMax :

						TemplateData.HeightPlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.WidthTolMin :

						TemplateData.WidthMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.WidthTolMax :

						TemplateData.WidthPlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.ThicknessTolMin :

						TemplateData.ThicknessMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.ThicknessTolMax :

						TemplateData.ThicknessPlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.AngleTolMin:

						TemplateData.AngleMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.AngleTolMax:

						TemplateData.AnglePlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;

				}
			}

			// Common order template header not found.  Report false find
			else
				Result = false;

			return Result;
		}
			

		
		// Updates the single order template header data
		private void UpdateSingleOrderTemplateHeaderFromImport(SingleOrderTemplateDataV1100 TemplateData, string FieldName, string Value1, string Value2)
		{

			SingleOrderHeaderTemplate HeaderTemplate = new SingleOrderHeaderTemplate();
			SingleOrderHeaderCellIDV1100 HeaderID;

			// Check for the header information first...
			if (HeaderTemplate.LocateHeaderFieldNameIndex(FieldName, out HeaderID))
			{

				int Column = Convert.ToInt32(Value1);
				int Row = Convert.ToInt32(Value2);

				TemplateData.SetCellSettings(HeaderID, Column, Row);
	
			}

		}


		// Updates the single order template Line item data
		private void UpdateSingleOrderTemplateLineItemFromImport(SingleOrderTemplateDataV1100 TemplateData, string FieldName, string Value1)
		{

			SingleOrderLineItemTemplate LITemplate = new SingleOrderLineItemTemplate();
			SingleOrderLineItemColumnIDV1100 LineItemID;

			// If not Header, check for Row info
			if (LITemplate.LocateLineItemFieldNameIndex(FieldName, out LineItemID))
			{

				int Column = Convert.ToInt32(Value1);

				TemplateData.SetColumnSettings(LineItemID, Column);

			}
		}



		// Updates the Multi Order template common data
		private bool UpdateMultiOrderTemplateCommonDataFromImport(CutlistTemplateDataV1100 TemplateData, string Header, string Data)
		{

			bool Result = true;
			OrderTemplateCommonFields Index;

			// Test if passed header is part of order common data.  If so, parse and return true
			if (LocateOrderCommonDataIndex(Header, out Index))
			{
				// Save based on index
				switch (Index)
				{
					case OrderTemplateCommonFields.TemplateName:

						TemplateData.Name = Data;
						break;


					case OrderTemplateCommonFields.LineItemStart:

						TemplateData.LineItemStart = Convert.ToInt32(Data);
						break;


					case OrderTemplateCommonFields.CSVUnits:

						if (Data.Equals(MeasuringUnits.Inches))
							TemplateData.CSVMeasureUnits = MeasuringUnits.Inches;

						else
							TemplateData.CSVMeasureUnits = MeasuringUnits.Millimeters;

						break;


					case OrderTemplateCommonFields.HeightTolMin:

						TemplateData.HeightMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.HeightTolMax:

						TemplateData.HeightPlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.WidthTolMin:

						TemplateData.WidthMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.WidthTolMax:

						TemplateData.WidthPlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.ThicknessTolMax:

						TemplateData.ThicknessPlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.ThicknessTolMin:

						TemplateData.ThicknessMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.AngleTolMax:

						TemplateData.AnglePlusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


					case OrderTemplateCommonFields.AngleTolMin:

						TemplateData.AngleMinusTolerance.ToleranceVal = Convert.ToDouble(Data);
						break;


				}
			}

			// Common order template header not found.  Report false find
			else
				Result = false;

			return Result;
		}



		// Update the Multi order template data
		private void UpdateMultiOrderTemplateFromImport(CutlistTemplateDataV1100 TemplateData, string FieldName, string Value1)
		{

			CutlistTemplate LITemplate = new CutlistTemplate();
			CutlistLineItemColumnIDV1100 LineItemID;



			// Line Item info
			if (LITemplate.LocateLineItemFieldNameIndex(FieldName, out LineItemID))
			{

				int Column = Convert.ToInt32(Value1);

				TemplateData.SetColumnSettings(LineItemID, Column);

			}


		}




		private const string FileName = "ParamsV1100.dat";
		private const int CurrentVersionDefine = 1002;


		// General system data
		private CSVFileType CSVFileTypeUsed = CSVFileType.CutlistFile;

		private bool EditLineItemHeight = false;
		private bool EditLineItemWidth = false;
		private bool EditLineItemQuantityReq = true;

		private MeasuringUnits SystemUnits = MeasuringUnits.Inches;
		
		private int InchPrecision = 3;
		private int MMPrecision = 2;


		private string CSVFilePath = @"C:\";
		private string CSVBatchFilePath = @"C:\";
		private string CompletedOrderFilePath = @"C:\";

		// Version 1.1
		private string PackListTemplateFilePath = @"C:\";
		private string PartLabelTemplateFilePath = @"C:\";


		private bool AllowInvalidPartMatches = false;
		private bool DeleteLIWhenComplete = false;
		private bool AllowIndividualOrderDeletion = false;
		private bool AllowAllOrderDeletion = false;
		private bool ShowOrderComplete = false;
		private bool UseNAmericanDate = true;
		private bool AllowManMeasurements = true;
		private bool PrintOutOfSpecLab = false;
		private bool CheckCompletedLI = false;
		private PrintLabelTypes PrintMPartLabels = PrintLabelTypes.None;
		private PrintReportTypes PrintPacklist = PrintReportTypes.None;

		// Version 1.1
		private bool ReportPrintPreview = true;
		private bool LabelPrintPreview = false;
		private bool AllowIncompleteOrderProcessing = false;
		private bool SwapHeightandWidth = false;

		private string MeasurementDevicePortName = "";
		private MeasurementInterfaceHardware.MeasureHardwareType MeasurementDeviceType = MeasurementInterfaceHardware.MeasureHardwareType.ProRF;

		// Version 2
		private bool UseZAxis = false;

		// Version 1.1
		private string ReportPrinterName = "";
		private string LabelPrinterName = "";						   

		private string CompanyName = "";
		private string CompanyAddress1 = "";
		private string CompanyAddress2 = "";
		private string CompanyCity = "";
		private string CompanyState = "";
		private string CompanyZip = "";
		private string CompanyPhone = "";
		private string CompanyLogoFile = "";

		// Version 1.1
		private string CompanyURL = "";
		private string CompanyCountry = "";


		// Version 1.2.1.0
		private bool UseAngleAxis = false;
		private double AngleVerticalOffset = 3.5;
		private bool DoNotProcessOrdersWhenComplete = false;
		private bool ShowUserMessageDialog = false;

        // Version 1.2.2.0
        private bool UseOutOfSquare = false;
        //private double OutOfSquareVerticalOffset = 3.5;
        //private bool DoNotProcessOrdersWhenComplete = false;
        //private bool ShowUserMessageDialog = false;

        private string Password = "1234";

		private int ParamVersion = CurrentVersionDefine;


		// Single order template data
		private List<SingleOrderTemplateDataV1100> SOTemplates;

		// Multi order template data
		private List<CutlistTemplateDataV1100> MOTemplates;


               


		// Assign properties

		public MeasuringUnits SysUnits
		{

			get { return SystemUnits; }
			set { SystemUnits = value; }
		}



		public CSVFileType CSVFileTypeInUse
		{
			get { return CSVFileTypeUsed; }
			set { CSVFileTypeUsed = value; }
		}

		public bool CanEditLineItemHeight
		{
 			get { return EditLineItemHeight; }
			set { EditLineItemHeight = value; }
		}

		public bool CanEditLineItemWidth
		{
			get { return EditLineItemWidth; }
			set { EditLineItemWidth = value; }

		}

		public bool CanEditLineItemQuantityReq
		{
 			get { return EditLineItemQuantityReq; }
			set { EditLineItemQuantityReq = value; }
		}


		public string CSVFilePathStr
		{
			get { return CSVFilePath; }
			set { CSVFilePath = value; }
		}

		public string CSVBatchFilePathStr
		{
			get { return CSVBatchFilePath; }
			set { CSVBatchFilePath = value; }
		}

 		public string CompletedOrderFilePathStr
		{
			get { return CompletedOrderFilePath; }
			set { CompletedOrderFilePath = value; }
		}

		public string PackListTemplateFilePathStr
		{
			get { return PackListTemplateFilePath; }
			set { PackListTemplateFilePath = value; }
		}

		public string PartLabelTemplateFilePathStr
		{
			get { return PartLabelTemplateFilePath; }
			set { PartLabelTemplateFilePath = value; }
		}

		
		public string InchPrecisionStr
		{
			get { return InchPrecision.ToString(); }

		}

		public bool SetInchPrecisionStr(string Val)
		{
			// Try to convert string to integer
			try
			{
				int Temp = Convert.ToInt32(Val);

				if ((Temp > 0) && (Temp <= 4))
				{
					InchPrecision = Temp;
					return true;
				}

				else
				{
					MessageBox.Show("Inch precision must be between 1 and 4", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return false;
				}
			}

			catch
			{

				MessageBox.Show("Invalid data format.  Not integer data", "Invalid Data Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;

			}
		}
	

		public int InchPrecisionVal
		{
			get { return InchPrecision; }
		}



		public string MMPrecisionStr
		{
			get { return MMPrecision.ToString(); }
		}

		public bool SetMMPrecisionStr(string Val)
		{

			// Try to convert string to integer
			try
			{
				int Temp = Convert.ToInt32(Val);

				if ((Temp > 0) && (Temp <= 2))
				{
					MMPrecision = Temp;
					return true;
				}

				else
				{
					MessageBox.Show("Millimeter precision must be between 1 and 2", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return false;
				}
			}

			catch
			{

				MessageBox.Show("Invalid data format.  Not integer data", "Invalid Data Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;

			}
		}
	

		public int MMPrecisionVal
		{
			get { return MMPrecision;}

		}


		public PrintReportTypes PrintPacklistOnOrderCompletion
		{
			get { return PrintPacklist; }
			set { PrintPacklist = value; }
		}


		public string MeasurementDevicePortStr
		{
			get { return MeasurementDevicePortName; }
			set { MeasurementDevicePortName = value; }
		}

		public MeasurementInterfaceHardware.MeasureHardwareType MeasurementInterfaceType
		{
			get { return MeasurementDeviceType; }
			set { MeasurementDeviceType = value; }
		}

		public bool ZAxisInUse
		{
			get { return UseZAxis; }
			set { UseZAxis = value; }
		}


		public string ReportPrinterNameStr
		{
			get { return ReportPrinterName; }
			set { ReportPrinterName = value; }
		}

		public string LabelPrinterNameStr
		{
			get { return LabelPrinterName; }
			set { LabelPrinterName = value; }
		}

			   

		public bool OutOfTolerancePartsAccepted
		{
			get { return AllowInvalidPartMatches; }
			set { AllowInvalidPartMatches = value; }
		}

		public bool DeleteLineItemWhenComplete
		{
			get { return DeleteLIWhenComplete; }
			set { DeleteLIWhenComplete = value; }
		}


		public bool IndividualOrderDeletionAllowed
		{
			get { return AllowIndividualOrderDeletion; }
			set { AllowIndividualOrderDeletion = value; }
		}

		public bool AllOrderDeletionAllowed
		{
			get { return AllowAllOrderDeletion; }
			set { AllowAllOrderDeletion = value; }
		}


		public bool ShowOrderCompleteMsg
		{
			get { return ShowOrderComplete; }
			set { ShowOrderComplete = value; }
		}

		public bool UseNorthAmericanDateFormat
		{
			get { return UseNAmericanDate; }
			set { UseNAmericanDate = value; }
		}

		public bool AllowManualMeasurements
		{
			get { return AllowManMeasurements; }
			set { AllowManMeasurements = value; }
		}

		public bool PrintOutOfSpecLabel
		{
			get { return PrintOutOfSpecLab; }
			set { PrintOutOfSpecLab = value; }
		}


		public bool CheckCompletedLIForMatch
		{
			get { return CheckCompletedLI; }
			set { CheckCompletedLI = value; }
		}

		public PrintLabelTypes PrintMeasuredPartLabels
		{
			get { return PrintMPartLabels; }
			set { PrintMPartLabels = value; }
		}


		public bool UseReportPrintPreview
		{
			get { return ReportPrintPreview; }
			set { ReportPrintPreview = value; }
		}


		public bool UseLabelPrintPreview
		{
			get { return LabelPrintPreview; }
			set { LabelPrintPreview = value; }
		}


		public string CompanyNameStr
		{
			get { return CompanyName; }
			set { CompanyName = value; }
		}

		public string CompanyAddr1Str
		{
			get { return CompanyAddress1; }
			set { CompanyAddress1 = value; }
		}

		public string CompanyAddr2Str
		{
			get { return CompanyAddress2; }
			set { CompanyAddress2 = value; }
		}

		public string CompanyCityStr
		{
			get { return CompanyCity; }
			set { CompanyCity = value; }
		}

		public string CompanyStateStr
		{
			get { return CompanyState; }
			set { CompanyState = value; }
		}

		public string CompanyZipStr
		{
			get { return CompanyZip; }
			set { CompanyZip = value; }
		}

		public string CompanyPhoneStr
		{
			get { return CompanyPhone; }
			set { CompanyPhone = value; }
		}

		public string CompanyLogoFileStr
		{
			get { return CompanyLogoFile; }
			set { CompanyLogoFile = value; }
		}

		public string CompanyURLStr
		{
			get { return CompanyURL; }
			set { CompanyURL = value; }
		}

		public string CompanyCountryStr
		{
			get { return CompanyCountry; }
			set { CompanyCountry = value; }
		}


		public bool AngleAxisInUse
		{
			get { return UseAngleAxis; }
			set { UseAngleAxis = value; }
		}

        public bool OutOfSquareInUse
        {
            get { return UseOutOfSquare; }
            set { UseOutOfSquare = value; }
        }

        public double AngleVerticalOffsetValue
		{
			get { return AngleVerticalOffset; }
			set { AngleVerticalOffset = value; }

		}
		
		 
		public string PasswordVal
		{
			get { return Password; }
			set { Password = value; }
		}


		public bool AllowIncompleteOrdersToBeProcessed
		{

			get { return AllowIncompleteOrderProcessing; }
			set { AllowIncompleteOrderProcessing = value; }
		}

		public bool SwapHeightAndWidthAxes
		{
			get { return SwapHeightandWidth; }
			set { SwapHeightandWidth = value; }
		}


		public bool DoNotProcessCompletedOrders
		{
			get { return DoNotProcessOrdersWhenComplete; }
			set { DoNotProcessOrdersWhenComplete = value; }
		}


		public bool ShowUserMessageDlg
		{
			get { return ShowUserMessageDialog; }
			set { ShowUserMessageDialog = value; }
		}



		public int ParameterVersion
		{
			get { return ParamVersion; }
		}



		// Constructor
		public PersistentParametersV1100()
		{



			// Create new template lists
			SOTemplates = new List<SingleOrderTemplateDataV1100>();
			MOTemplates = new List<CutlistTemplateDataV1100>();

			// Create new Single Order and MultiOrder default templates
			CreateSOTemplateData("Default");
			CreateMOTemplateData("Default");




           		

		}


		// Member Functions

		// Save parameters to disk in local directory
		public void SaveParameters()
		{

			BinaryFormatter Formatter = new BinaryFormatter();

			using (Stream fstream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				Formatter.Serialize(fstream, this);
			}

		}

		// Attempt to read parameter data file
		// Reads data if file exists and returns true
		// Returns false if no data file exists
		public bool ReadParameters(ref PersistentParametersV1100 Params)
		{

			// Save the latest version number
			int CurrentVersion = ParamVersion;

			// Check if data file exists.  If so, open it and return true.  If not, false
			if (File.Exists(FileName))
			{

				BinaryFormatter Formatter = new BinaryFormatter();

				using (Stream fstream = File.OpenRead(FileName))
				{
					Params = (PersistentParametersV1100)Formatter.Deserialize(fstream);
				}


				return true;

			}

			else
			{

				return false;

			}

		}



		// Determines if the selected template has its required fields configured.
		// If all required fields configured, return true.  Else, return false
		public bool IsSelectedTemplateConfigured(out string TypeStr, int TemplateIndex)
		{

			// If Multi-order (cutlist) template currently selected.
			if (CSVFileTypeUsed == CSVFileType.CutlistFile)
			{

				if (TemplateIndex < MOTemplates.Count)
				{
					TypeStr = "Multi-Order";

					// Required fields
					if ((MOTemplates[TemplateIndex].GetColumnSettings(CutlistLineItemColumnIDV1100.NominalHeight) == -1) ||
						(MOTemplates[TemplateIndex].GetColumnSettings(CutlistLineItemColumnIDV1100.NominalWidth) == -1) ||
						(MOTemplates[TemplateIndex].GetColumnSettings(CutlistLineItemColumnIDV1100.PartID) == -1) ||
						(MOTemplates[TemplateIndex].GetColumnSettings(CutlistLineItemColumnIDV1100.QuantityRequired) == -1) ||
						(MOTemplates[TemplateIndex].GetColumnSettings(CutlistLineItemColumnIDV1100.OrderID) == -1))
					{

						return false;
					}

					else
					{
						return true;
					}
				}

				// Invalid Index.  This should normally not happen
				else
				{
					TypeStr = "Internal Error:  Invalid template index";
					return false;
				}

			}

			// Single order template currently selected.
			else if (CSVFileTypeUsed == CSVFileType.SingleOrderFile)
			{

				if (TemplateIndex < SOTemplates.Count)
				{
					TypeStr = "Single Order";

					// Required fields
					if ((SOTemplates[TemplateIndex].GetColumnSetting(SingleOrderLineItemColumnIDV1100.NominalHeight) == -1) ||
						(SOTemplates[TemplateIndex].GetColumnSetting(SingleOrderLineItemColumnIDV1100.NominalWidth) == -1) ||
						(SOTemplates[TemplateIndex].GetColumnSetting(SingleOrderLineItemColumnIDV1100.PartID) == -1) ||
						(SOTemplates[TemplateIndex].GetColumnSetting(SingleOrderLineItemColumnIDV1100.QuantityRequired) == -1))
					{
						return false;
					}

					else
					{
						return true;
					}
				}

				else
				{
					TypeStr = "Internal Error:  Invalid template index";
					return false;
				}

			}

			else
			{
				TypeStr = "Internal Error:  Invalid Template";
				return false;
			}
								
		}


		// Gets a Single Order template based on index
		public bool GetSOTemplateData(int TemplateIndex, out SingleOrderTemplateDataV1100 Template)
		{

			if ((TemplateIndex >= 0) && (TemplateIndex < SOTemplates.Count))
			{
				Template = SOTemplates[TemplateIndex];
				return true;
			}

			else
			{
				Template = (SingleOrderTemplateDataV1100)null;
				return false;
			}
		}

		// Create a new blank SO template and add to SOTemplates list
		public void CreateSOTemplateData(string Name)
		{

			SingleOrderTemplateDataV1100 Template = new SingleOrderTemplateDataV1100(Name);

			SOTemplates.Add(Template);
		
		}

		public bool DeleteSOTemplateData(int TemplateIndex)
		{

			if ((TemplateIndex > 0) && (TemplateIndex < SOTemplates.Count))
			{
				SOTemplates.RemoveAt(TemplateIndex);
				return true;
			}

			else
				return false;
		}

		public bool SaveSOTemplateData(int TemplateIndex, SingleOrderTemplateDataV1100 Template)
		{

			if((TemplateIndex >= 0) && (TemplateIndex < SOTemplates.Count))
			{
				SOTemplates[TemplateIndex] = Template;
				return true;
			}

			else
				return false;
		}

		public bool RenameSOTemplateData(int TemplateIndex, string NewName)
		{

			if ((TemplateIndex >= 0) && (TemplateIndex < SOTemplates.Count))
			{
				SOTemplates[TemplateIndex].Name = NewName;
				return true;
			}

			else
				return false;
		}


		public bool AreMultipleSOTemplates()
		{
			if (SOTemplates.Count > 1)
				return true;

			else
				return false;
		}

		public void GetSOTemplateNames(out List<string> NameStr)
		{

			NameStr = new List<string>();

			for (int u = 0; u < SOTemplates.Count; u++)
			{
				NameStr.Add(SOTemplates[u].Name);
			}
			
		}

		public int GetSOTemplateIndex(string Name)
		{

			int Index = -1;

			for (int u = 0; u < SOTemplates.Count; u++)
			{
				if (SOTemplates[u].Name == Name)
					Index = u;
			}

			// returns located index.  If index not found, returns -1
			return Index;
		}









		// Gets a Multi Order template (cutlist type) based on the index
		public bool GetMOTemplateData(int TemplateIndex, out CutlistTemplateDataV1100 Template)
		{

			if ((TemplateIndex >= 0) && (TemplateIndex < MOTemplates.Count))
			{
				Template = MOTemplates[TemplateIndex];
				return true;
			}

			else
			{
				Template = (CutlistTemplateDataV1100)null;
				return false;
			}


		}

		// Create a new blank MO template and add to MOTemplates list
		public void CreateMOTemplateData(string Name)
		{

			CutlistTemplateDataV1100 Template = new CutlistTemplateDataV1100(Name);

			MOTemplates.Add(Template);

		}

		public bool DeleteMOTemplateData(int TemplateIndex)
		{

			if ((TemplateIndex > 0) && (TemplateIndex < MOTemplates.Count))
			{
				MOTemplates.RemoveAt(TemplateIndex);
				return true;
			}

			else
				return false;
		}

		public bool SaveMOTemplateData(int TemplateIndex, CutlistTemplateDataV1100 Template)
		{

			if ((TemplateIndex >= 0) && (TemplateIndex < MOTemplates.Count))
			{
				MOTemplates[TemplateIndex] = Template;
				return true;
			}

			else
				return false;
		}

		public bool RenameMOTemplateData(int TemplateIndex, string NewName)
		{

			if ((TemplateIndex >= 0) && (TemplateIndex < MOTemplates.Count))
			{
				MOTemplates[TemplateIndex].Name = NewName;
				return true;
			}

			else
				return false;
		}

		public bool AreMultipleMOTemplates()
		{
			if (MOTemplates.Count > 1)
				return true;

			else
				return false;
		}


		public void GetMOTemplateNames(out List<string> NameStr)
		{

			NameStr = new List<string>();

			for (int u = 0; u < MOTemplates.Count; u++)
			{
				NameStr.Add(MOTemplates[u].Name);
			}

		}

		public int GetMOTemplateIndex(string Name)
		{

			int Index = -1;

			for (int u = 0; u < MOTemplates.Count; u++)
			{
				if (MOTemplates[u].Name == Name)
					Index = u;
			}

			// returns located index.  If index not found, returns -1
			return Index;
		}
		



	}
}
