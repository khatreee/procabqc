﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Drawing.Printing;




namespace ProCABQC11
{
	public partial class OptionsDialog : Form
	{

		PersistentParametersV1100 LocalParams;
		MeasurementInterfaceHardware LocalHardware;

		VirtualAxis WidthAxis;
		VirtualAxis HeightAxis;
		VirtualAxis ThicknessAxis;
		VirtualAxis AngleAxis;

		

		int LastIndex;

		private bool DeactiveStatus = false;

		DataBoundReport report;

		private const string ExportFileName = "System Param Export.txt";

 
		public bool Deactivated
		{
			get {return DeactiveStatus;}
		}



		public OptionsDialog()
		{
			InitializeComponent();
		}

		public void LoadDialogControls()
		{

			// Initial fields on the System Page

			DefaultCSVFolderText.Text = LocalParams.CSVFilePathStr;
			DefaultBatchCSVFolderText.Text = LocalParams.CSVBatchFilePathStr;
			CompletedOrderFolderText.Text = LocalParams.CompletedOrderFilePathStr;
			PackListTemplateFolderText.Text = LocalParams.PackListTemplateFilePathStr;
			PartLabelTemplateFolderText.Text = LocalParams.PartLabelTemplateFilePathStr;

			PasswordText.Text = LocalParams.PasswordVal;

			// Set precision buttons for inch
			if (LocalParams.InchPrecisionStr == "1")
				InchPrec1.Checked = true;

			else if (LocalParams.InchPrecisionStr == "2")
				InchPrec2.Checked = true;

			else if (LocalParams.InchPrecisionStr == "3")
				InchPrec3.Checked = true;

			else
				InchPrec3.Checked = true;

			// Set precision buttons for mm
			if (LocalParams.MMPrecisionStr == "1")
				MMPrec1.Checked = true;

			else if (LocalParams.MMPrecisionStr == "2")
				MMPrec2.Checked = true;

			else
				MMPrec2.Checked = true;



			if (LocalParams.CSVFileTypeInUse == CSVFileType.SingleOrderFile)
				SingleOrderSelect.Checked = true;

			else
				CutlistSelect.Checked = true;





			// *** Measurement Options

			// Set packlist option
			PrintPackListSel.SelectedIndex = (int)LocalParams.PrintPacklistOnOrderCompletion;

			// Allow unmatched measured parts
			AllowOutOfTolParts.Checked = LocalParams.OutOfTolerancePartsAccepted;

			// Delete line item from order list when measured quantity reached
			DeleteLIUponQuantity.Checked = LocalParams.DeleteLineItemWhenComplete;

			// Individual order deletion
			AllowIndividualOrderDel.Checked = LocalParams.IndividualOrderDeletionAllowed;

			// All Order Deletion
			AllowAllOrderDel.Checked = LocalParams.AllOrderDeletionAllowed; 

			// Line item width edit
			LineItemWidthEdit.Checked = LocalParams.CanEditLineItemWidth;

			// Line Item height edit
			LineItemHeightEdit.Checked = LocalParams.CanEditLineItemHeight;

			// Line Item Quantity Edit
			LineItemQuantityEdit.Checked = LocalParams.CanEditLineItemQuantityReq;

			// Show order complete message
			ShowOrderComplete.Checked = LocalParams.ShowOrderCompleteMsg;

			// North american date format
			UseNorthAmericanDateSel.Checked = LocalParams.UseNorthAmericanDateFormat;

			// Manual measurement
			ManualMeasureSel.Checked = LocalParams.AllowManualMeasurements;

			// Out of spec label
			PrintOutOfSpecLabelSel.Checked = LocalParams.PrintOutOfSpecLabel;

			// Test completed line items on new measurements
			TestCompletedLineItemsSel.Checked = LocalParams.CheckCompletedLIForMatch;

			// Preset combo box for measurement labels print selection
			PrintLabelsOptionSel.SelectedIndex = (int)LocalParams.PrintMeasuredPartLabels;			

			// Report Print Preview
			UseReportPreviewSel.Checked = LocalParams.UseReportPrintPreview;

			// Use label preview (V2)
			UseLabelPreviewSel.Checked = LocalParams.UseLabelPrintPreview;

			// Allow Incomplete Orders
			AllowIncompleteOrdersSel.Checked = LocalParams.AllowIncompleteOrdersToBeProcessed;

			// Swap axes
			SwapAxesSel.Checked = LocalParams.SwapHeightAndWidthAxes;

			// Auto Save order when complete
			DoNotAutoSaveOrderWhenComplete.Checked = LocalParams.DoNotProcessCompletedOrders;

			// User message diaalog
			ShowUserMsgDlgChkBtn.Checked = LocalParams.ShowUserMessageDlg;




			// *** Hardware interface page

			// Currently selected comm port
			String[] PortNames;

			PortNames = SerialPort.GetPortNames();

			foreach (string Name in PortNames)
			{
				CommunicationsPort.Items.Add(Name);
			}

			if (LocalParams.MeasurementDevicePortStr != "")
			{
				int Index = CommunicationsPort.FindString(LocalParams.MeasurementDevicePortStr);

				CommunicationsPort.SelectedIndex = Index;
			}
			
			else
				CommunicationsPort.SelectedIndex = -1;


			// Hardware selection
			if (LocalParams.MeasurementInterfaceType == MeasurementInterfaceHardware.MeasureHardwareType.ProRF)
				ProRFSelect.Checked = true;

			else
				ProMuxSelect.Checked = true;


			// Z axis use
			UseZAxisCheckBox.Checked = LocalParams.ZAxisInUse;

			// Angle Use
			UseAngleCheckBox.Checked = LocalParams.AngleAxisInUse;



			string PrinterName;

			// Build list for available printers and populate the report printer combo box
			for (int u = 0; u < PrinterSettings.InstalledPrinters.Count; u++)
			{
				PrinterName = PrinterSettings.InstalledPrinters[u];
				ReportPrinterComboBox.Items.Add(PrinterName);
			}

			// set the report printer if defined
			if (LocalParams.ReportPrinterNameStr != "")
			{
				int Index = ReportPrinterComboBox.FindString(LocalParams.ReportPrinterNameStr);
				ReportPrinterComboBox.SelectedIndex = Index;
			}

			else
				ReportPrinterComboBox.SelectedIndex = -1;


			// Build list for available printers and populate the label printer combo box
			for (int u = 0; u < PrinterSettings.InstalledPrinters.Count; u++)
			{
				PrinterName = PrinterSettings.InstalledPrinters[u];
				LabelPrinterComboBox.Items.Add(PrinterName);
			}

			// set the label printer if defined
			if (LocalParams.LabelPrinterNameStr != "")
			{
				int Index = LabelPrinterComboBox.FindString(LocalParams.LabelPrinterNameStr);
				LabelPrinterComboBox.SelectedIndex = Index;
			}

			else
				LabelPrinterComboBox.SelectedIndex = -1;
			
							
	  

			// *** Axis parameters

			// Set initial axis to width (0)
			AxisSelect.SelectedIndex = 0;

			ReloadAxisData(AxisSelect.SelectedIndex);



			// *** Company Data

			CompanyNameField.Text = LocalParams.CompanyNameStr;
			CompanyAddr1.Text = LocalParams.CompanyAddr1Str;
			CompanyAddr2.Text = LocalParams.CompanyAddr2Str;
			CompanyCity.Text = LocalParams.CompanyCityStr;
			CompanyState.Text = LocalParams.CompanyStateStr;
			CompanyZip.Text = LocalParams.CompanyZipStr;
			CompanyPhone.Text = LocalParams.CompanyPhoneStr;
			LogoFileName.Text = LocalParams.CompanyLogoFileStr;
			CompanyURL.Text = LocalParams.CompanyURLStr;
			CompanyCountry.Text = LocalParams.CompanyCountryStr;


			// Try to load logo file if filename not null
			if(LogoFileName.Text != "")
				CompanyLogo.ImageLocation = LogoFileName.Text;



		}


//		public OptionsDialog(PersistentParametersV1100 PParams, MeasurementInterfaceHardware MeasureHardware, VirtualAxis XAxis, VirtualAxis YAxis, VirtualAxis ZAxis, VirtualAxis AAxis, SecureLicense Lic)
		public OptionsDialog(PersistentParametersV1100 PParams, MeasurementInterfaceHardware MeasureHardware, VirtualAxis XAxis, VirtualAxis YAxis, VirtualAxis ZAxis, VirtualAxis AAxis)
		{


			InitializeComponent();

			// Make a local copy
			LocalParams = (PersistentParametersV1100)PParams.Clone();

			// Make a local hardware reference
			LocalHardware = MeasureHardware;


			WidthAxis = (VirtualAxis)XAxis.Clone();
			HeightAxis = (VirtualAxis)YAxis.Clone();
			ThicknessAxis = (VirtualAxis)ZAxis.Clone();
			AngleAxis = (VirtualAxis)AAxis.Clone();


			// Load the controls
			LoadDialogControls();


			MeasureHardware.DisableDisplayUpdate(false);

			
	
		}


		// String to integer converter.  Catches conversion exceptions
		// returns true if successful or false if failure.
		private bool ConvertStringToInt(string Str, out int Val)
		{

			try
			{

				Val = Convert.ToInt32(Str);
				return true;
			}

			catch  (Exception e)
			{

				MessageBox.Show(string.Format("Conversion Error: ", e.Message), "Conversion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				Val = 0;

				return false;

			}


		}





		private void OkBut_Click(object sender, EventArgs e)
		{

			bool Result = true;

			// Inspect and save data back to params

			// CSV file type
			if (SingleOrderSelect.Checked)
				LocalParams.CSVFileTypeInUse = CSVFileType.SingleOrderFile;

			else
				LocalParams.CSVFileTypeInUse = CSVFileType.CutlistFile;

			// Precision
			// Inch
			if (InchPrec1.Checked)
			{
				if (!LocalParams.SetInchPrecisionStr("1"))
					Result = false;
			}

			else if(InchPrec2.Checked)
			{
				if (!LocalParams.SetInchPrecisionStr("2"))
					Result = false;
			}

			else
			{
				if (!LocalParams.SetInchPrecisionStr("3"))
					Result = false;
			}


			// MM
			if (MMPrec1.Checked)
			{
				if (!LocalParams.SetMMPrecisionStr("1"))
					Result = false;
			}

			else
			{
				if (!LocalParams.SetMMPrecisionStr("2"))
					Result = false;
			}


			// Save default file paths
			LocalParams.CSVFilePathStr = DefaultCSVFolderText.Text;
			LocalParams.CSVBatchFilePathStr = DefaultBatchCSVFolderText.Text;
			LocalParams.CompletedOrderFilePathStr = CompletedOrderFolderText.Text;
			LocalParams.PackListTemplateFilePathStr = PackListTemplateFolderText.Text;
			LocalParams.PartLabelTemplateFilePathStr = PartLabelTemplateFolderText.Text;

			LocalParams.PasswordVal = PasswordText.Text;




			// *** Measurement Options

			// Save pack list status
			LocalParams.PrintPacklistOnOrderCompletion = (PrintReportTypes)PrintPackListSel.SelectedIndex;

			// Save allowed out of tolerance measurements 
			LocalParams.OutOfTolerancePartsAccepted = AllowOutOfTolParts.Checked;

			// Delete line item from order list when measured quantity reached
			LocalParams.DeleteLineItemWhenComplete = DeleteLIUponQuantity.Checked;

			// Individual order deletion
			LocalParams.IndividualOrderDeletionAllowed = AllowIndividualOrderDel.Checked;

			// All Order deletion
			LocalParams.AllOrderDeletionAllowed = AllowAllOrderDel.Checked;

			// Line Item width edit
			LocalParams.CanEditLineItemWidth = LineItemWidthEdit.Checked;

			// Line Item height edit
			LocalParams.CanEditLineItemHeight = LineItemHeightEdit.Checked;

			// Line item quantity edit
			LocalParams.CanEditLineItemQuantityReq = LineItemQuantityEdit.Checked;

			// Show order complete dialog
			LocalParams.ShowOrderCompleteMsg = ShowOrderComplete.Checked;

			// North american date format
			LocalParams.UseNorthAmericanDateFormat = UseNorthAmericanDateSel.Checked;

			// Manual measurements
			LocalParams.AllowManualMeasurements = ManualMeasureSel.Checked;

			// Out of spec label
			LocalParams.PrintOutOfSpecLabel = PrintOutOfSpecLabelSel.Checked;

			// Test completed line items
			LocalParams.CheckCompletedLIForMatch = TestCompletedLineItemsSel.Checked;

			// Save the defined label print option
			LocalParams.PrintMeasuredPartLabels = (PrintLabelTypes)PrintLabelsOptionSel.SelectedIndex;

			// Report Print Preview
			LocalParams.UseReportPrintPreview = UseReportPreviewSel.Checked;

			// Label preview (V2)
			LocalParams.UseLabelPrintPreview = UseLabelPreviewSel.Checked;

			// Allow incomplete orders
			LocalParams.AllowIncompleteOrdersToBeProcessed = AllowIncompleteOrdersSel.Checked;

			// Swap Axes
			LocalParams.SwapHeightAndWidthAxes = SwapAxesSel.Checked;

			LocalParams.DoNotProcessCompletedOrders = DoNotAutoSaveOrderWhenComplete.Checked;

			// User message dialog
			LocalParams.ShowUserMessageDlg = ShowUserMsgDlgChkBtn.Checked;






			// Hardware interface page
			
			// Measurement Hardware Selection
			if (ProRFSelect.Checked)
				LocalParams.MeasurementInterfaceType = MeasurementInterfaceHardware.MeasureHardwareType.ProRF;

			else
				LocalParams.MeasurementInterfaceType = MeasurementInterfaceHardware.MeasureHardwareType.ProMUX;


			if (LabelPrinterComboBox.SelectedIndex == -1)
			{
				LocalParams.LabelPrinterNameStr = "";

			}

			
			// AXIS Configuration Data

			// Get the axis data from the dialog and save to the local copy of the axis
			ReadAxisFields(AxisSelect.SelectedIndex);


			bool FieldResults = true;



			// *** Company Data
			LocalParams.CompanyNameStr = CompanyNameField.Text;
			LocalParams.CompanyAddr1Str = CompanyAddr1.Text;
			LocalParams.CompanyAddr2Str = CompanyAddr2.Text;
			LocalParams.CompanyCityStr = CompanyCity.Text;
			LocalParams.CompanyStateStr = CompanyState.Text;
			LocalParams.CompanyZipStr = CompanyZip.Text;
			LocalParams.CompanyPhoneStr = CompanyPhone.Text;
			LocalParams.CompanyLogoFileStr = LogoFileName.Text;
			LocalParams.CompanyURLStr = CompanyURL.Text;
			LocalParams.CompanyCountryStr = CompanyCountry.Text;






			// Check if any failures.  If none, close dialog
			if (Result && FieldResults)
				this.DialogResult = DialogResult.OK;


		}

		public void SaveParameterData(ref PersistentParametersV1100 PParams, ref VirtualAxis XAxis, ref VirtualAxis YAxis, ref VirtualAxis ZAxis, ref VirtualAxis AAxis)
		{


			// Copy the data
			PParams = LocalParams;
			XAxis = WidthAxis;
			YAxis = HeightAxis;
			ZAxis = ThicknessAxis;
			AAxis = AngleAxis;
			


			// Save the data
			PParams.SaveParameters();
			XAxis.SaveParameters();
			YAxis.SaveParameters();
			ZAxis.SaveParameters();
			AAxis.SaveParameters();


		}

		private void BrowseCSVFolder_Click(object sender, EventArgs e)
		{

			FolderBrowserDialog Dlg = new FolderBrowserDialog();

			Dlg.Description = "Select default CSV file folder";
			Dlg.SelectedPath = DefaultCSVFolderText.Text;

			if (Dlg.ShowDialog() == DialogResult.OK)
				DefaultCSVFolderText.Text = Dlg.SelectedPath;

		}

		private void BrowseCSVBatchFolder_Click(object sender, EventArgs e)
		{

			FolderBrowserDialog Dlg = new FolderBrowserDialog();

			Dlg.Description = "Select default CSV Batch file folder";
			Dlg.SelectedPath = DefaultBatchCSVFolderText.Text;

			if (Dlg.ShowDialog() == DialogResult.OK)
				DefaultBatchCSVFolderText.Text = Dlg.SelectedPath;
		}

		private void BrowseCompletedFolder_Click(object sender, EventArgs e)
		{

			FolderBrowserDialog Dlg = new FolderBrowserDialog();

			Dlg.Description = "Select completed order file folder";
			Dlg.SelectedPath = CompletedOrderFolderText.Text;

			if (Dlg.ShowDialog() == DialogResult.OK)
				CompletedOrderFolderText.Text = Dlg.SelectedPath;

		}



		private void CommunicationsPort_SelectedValueChanged(object sender, EventArgs e)
		{

			// Set new port name
			LocalParams.MeasurementDevicePortStr = CommunicationsPort.SelectedItem.ToString();

			// Set new port name
			LocalHardware.SetPortComName(LocalParams.MeasurementDevicePortStr);

			string OpenPortErrorMessage;

			// Try to open the port with the new port name
			if (!LocalHardware.OpenPort(out OpenPortErrorMessage))
			{
				MessageBox.Show(OpenPortErrorMessage, "Unable to Open Communications Port", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}


		}



		private void AxisSelect_SelectedValueChanged(object sender, EventArgs e)
		{

			int Index = AxisSelect.SelectedIndex;

		

		}

		private void ReloadAxisData(int Index)
		{

			int Precision;

			if(LocalParams.SysUnits == MeasuringUnits.Inches)
				Precision = LocalParams.InchPrecisionVal;

			else
				Precision = LocalParams.MMPrecisionVal;

			if (Index == 0)
			{
				DatumPresetText.Text = WidthAxis.GetDatumPresetStr(LocalParams.SysUnits, Precision);
				ScaleFactorText.Text = WidthAxis.ScaleFactor;

				if (WidthAxis.GetEncoderDirStr() == "1")
					EncoderPosDirSelect.Checked = true;

				else
					EncoderNegDirSelect.Checked = true;

			}

			else if(Index == 1)
			{
				DatumPresetText.Text = HeightAxis.GetDatumPresetStr(LocalParams.SysUnits, Precision);
				ScaleFactorText.Text = HeightAxis.ScaleFactor;

				if (HeightAxis.GetEncoderDirStr() == "1")
					EncoderPosDirSelect.Checked = true;

				else
					EncoderNegDirSelect.Checked = true;

			}

			else if(Index == 2)
			{
				DatumPresetText.Text = ThicknessAxis.GetDatumPresetStr(LocalParams.SysUnits, Precision);
				ScaleFactorText.Text = ThicknessAxis.ScaleFactor;

				if (ThicknessAxis.GetEncoderDirStr() == "1")
					EncoderPosDirSelect.Checked = true;

				else
					EncoderNegDirSelect.Checked = true;

			}

			else if (Index == 3)
			{

				DatumPresetText.Text = AngleAxis.GetDatumPresetStr(LocalParams.SysUnits, Precision);
				ScaleFactorText.Text = AngleAxis.ScaleFactor;


				if (AngleAxis.GetEncoderDirStr() == "1")
					EncoderPosDirSelect.Checked = true;

				else
					EncoderNegDirSelect.Checked = true;


				PivotDistLabel.Visible = true;
				PivotDistVal.Visible = true;
				PivotDistVal.Text = LocalParams.AngleVerticalOffsetValue.ToString();



			}



		}


		private void ReadAxisFields(int Index)
		{

			// Save the previous settings if last axis was X (width)
			if (Index == (int)MeasurementInterfaceHardware.AxisNames.XAxis)
			{

				// Scale factor
				WidthAxis.ScaleFactor = ScaleFactorText.Text;

				// Datum Preset
				WidthAxis.SetDatumPreset(DatumPresetText.Text, LocalParams.SysUnits);

				// EncoderDirection
				if (EncoderPosDirSelect.Checked)
					WidthAxis.SetEncoderDir("1");

				else
					WidthAxis.SetEncoderDir("0");

			}

			// axis was Y (height)
			else if (Index == (int)MeasurementInterfaceHardware.AxisNames.YAxis)
			{
				// Scale factor
				HeightAxis.ScaleFactor = ScaleFactorText.Text;

				// Datum Preset
				HeightAxis.SetDatumPreset(DatumPresetText.Text, LocalParams.SysUnits);

				// EncoderDirection
				if (EncoderPosDirSelect.Checked)
					HeightAxis.SetEncoderDir("1");

				else
					HeightAxis.SetEncoderDir("0");

			}

			// Last axis was Z (thickness)
			else if (Index == (int)MeasurementInterfaceHardware.AxisNames.ZAxis)
			{
				// Scale factor
				ThicknessAxis.ScaleFactor = ScaleFactorText.Text;

				// Datum Preset
				ThicknessAxis.SetDatumPreset(DatumPresetText.Text, LocalParams.SysUnits);

				// EncoderDirection
				if (EncoderPosDirSelect.Checked)
					ThicknessAxis.SetEncoderDir("1");

				else
					ThicknessAxis.SetEncoderDir("0");

			}

			// Angle Axis
			else if (Index == (int)MeasurementInterfaceHardware.AxisNames.AngleAxis)
			{
				// Scale factor
				AngleAxis.ScaleFactor = ScaleFactorText.Text;

				// Encoder Direction
				if (EncoderPosDirSelect.Checked)
					AngleAxis.SetEncoderDir("1");

				else
					AngleAxis.SetEncoderDir("0");


				// Read Pivot Distance
				LocalParams.AngleVerticalOffsetValue = Convert.ToDouble(PivotDistVal.Text);
			}





		}

		private void AxisSelect_SelectionChangeCommitted(object sender, EventArgs e)
		{
			int Index = AxisSelect.SelectedIndex;

			
			// Reload the dialog fields with the newly selected axis data
			ReloadAxisData(Index);

			if (Index != 3)
			{
				PivotDistLabel.Visible = false;
				PivotDistVal.Visible = false;
			}


		}

		private void AxisSelect_DropDown(object sender, EventArgs e)
		{
			// The previous selected axis
			LastIndex = AxisSelect.SelectedIndex;

			// Get the axis data from the dialog and save to the local copy of the axis
			ReadAxisFields(LastIndex);




		}

		private void EditTemplateBut_Click(object sender, EventArgs e)
		{

			if (LocalParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
			{

				TemplateIndexEditDialog EditDlg = new TemplateIndexEditDialog(LocalParams);

				EditDlg.ShowDialog();
						
			}

			else if (LocalParams.CSVFileTypeInUse == CSVFileType.SingleOrderFile)
			{

				TemplateIndexEditDialog EditDlg = new TemplateIndexEditDialog(LocalParams);

				EditDlg.ShowDialog();

			}

		}

		private void AutoConfigureBut_Click(object sender, EventArgs e)
		{

			// Create auto hardware config dialog
			HardwareConfigForm Dlg = new HardwareConfigForm(LocalParams, LocalHardware);
 
			// pop it up
			Dlg.ShowDialog();

			

		}


		private void HardwareTypeChanged()
		{

			if (ProRFSelect.Checked)
				LocalParams.MeasurementInterfaceType = MeasurementInterfaceHardware.MeasureHardwareType.ProRF;

			else
				LocalParams.MeasurementInterfaceType = MeasurementInterfaceHardware.MeasureHardwareType.ProMUX;
		}


		private void ProRFSelect_MouseClick(object sender, MouseEventArgs e)
		{

			HardwareTypeChanged();


		}

		private void ProMuxSelect_MouseClick(object sender, MouseEventArgs e)
		{

			HardwareTypeChanged();

		}





		private void LogoFileDialog_Click(object sender, EventArgs e)
		{

			OpenFileDialog Dlg = new OpenFileDialog();

			Dlg.InitialDirectory = "\\";
			Dlg.Title = "Choose Company Logo Image File";
			Dlg.Filter = "Image Files|*.bmp;*.jpg;*.gif;*.png";
			Dlg.FileName = LogoFileName.Text;
			Dlg.RestoreDirectory = true;

			if (Dlg.ShowDialog() == DialogResult.OK)
			{
				LogoFileName.Text = Dlg.FileName;

				// Try to load image file
				CompanyLogo.ImageLocation = LogoFileName.Text;

			}

		}


		// Updates the positional data when the precision is changed
		private void UpdatePrecisionChange()
		{

			// Axis Data
			ReloadAxisData(AxisSelect.SelectedIndex);
			
		}

		// Inch 1dp precision selected
		private void InchPrec1_CheckedChanged(object sender, EventArgs e)
		{

			LocalParams.SetInchPrecisionStr("1");
			UpdatePrecisionChange();

		}

		// Inch 2dp precision selected
		private void InchPrec2_CheckedChanged(object sender, EventArgs e)
		{

			LocalParams.SetInchPrecisionStr("2");
			UpdatePrecisionChange();

		}

		// Inch 3dp precision selected
		private void InchPrec3_CheckedChanged(object sender, EventArgs e)
		{
			LocalParams.SetInchPrecisionStr("3");
			UpdatePrecisionChange();
		}

		// MM 1dp precision selected
		private void MMPrec1_CheckedChanged(object sender, EventArgs e)
		{

			LocalParams.SetMMPrecisionStr("1");
			UpdatePrecisionChange();

		}

		// MM 2dp precision selected
		private void MMPrec2_CheckedChanged(object sender, EventArgs e)
		{

			LocalParams.SetMMPrecisionStr("2");
			UpdatePrecisionChange();
		}



		private void DeactivateBut_Click(object sender, EventArgs e)
		{


	
		}

		private void SingleOrderSelect_CheckedChanged(object sender, EventArgs e)
		{

			LocalParams.CSVFileTypeInUse = CSVFileType.SingleOrderFile;

		}

		private void CutlistSelect_CheckedChanged(object sender, EventArgs e)
		{

			LocalParams.CSVFileTypeInUse = CSVFileType.CutlistFile;
		}

		private void PopulateReportCommonData(ref DataSet DataSource)
		{

			// *** CUSTOMER SHIP DATA ***

			DataRow ShipRow = DataSource.Tables["CustomerShipInfo"].NewRow();

			// Add dummy ship to data
			ShipRow["ShipName"] = "Midtown Construction Inc.";
			ShipRow["ShipAddress1"] = "459 South St.";
			ShipRow["ShipAddress2"] = "Suite A";
			ShipRow["ShipCity"] = "Hendersonville";
			ShipRow["ShipState"] = "NC";
			ShipRow["ShipZip"] = "28777";
			ShipRow["ShipCountry"] = "USA";
			ShipRow["ShipContact"] = "Tim Smith";
			ShipRow["ShipTelephone"] = "888-555-1212";
			ShipRow["ShipEmail"] = "TSmith@Acme.com";

			DataSource.Tables["CustomerShipInfo"].Rows.Add(ShipRow);


			// *** CUSTOMER BILL DATA ***

			DataRow BillRow = DataSource.Tables["CustomerBillInfo"].NewRow();

			// Add dummy Bill to data
			BillRow["BillName"] = "Acme Cabinets Inc.";
			BillRow["BillAddress1"] = "429 Front St.";
			BillRow["BillAddress2"] = "Receiving Dock A";
			BillRow["BillCity"] = "Johnson City";
			BillRow["BillState"] = "TN";
			BillRow["BillZip"] = "29301";
			BillRow["BillCountry"] = "USA";
			BillRow["BillContact"] = "John Hanes";
			BillRow["BillTelephone"] = "887-211-2375";
			BillRow["BillEmail"] = "John@Midtown.com";

			DataSource.Tables["CustomerBillInfo"].Rows.Add(BillRow);


			// *** COMPANY DATA ***

			DataRow CompanyRow = DataSource.Tables["CompanyData"].NewRow();

			// Populate the data source with the company data info
			CompanyRow["CompanyName"] = LocalParams.CompanyNameStr;
			CompanyRow["CompanyAddress1"] = LocalParams.CompanyAddr1Str;
			CompanyRow["CompanyAddress2"] = LocalParams.CompanyAddr2Str;
			CompanyRow["CompanyCity"] = LocalParams.CompanyCityStr;
			CompanyRow["CompanyState"] = LocalParams.CompanyStateStr;
			CompanyRow["CompanyZip"] = LocalParams.CompanyZipStr;
			CompanyRow["CompanyTelephone"] = LocalParams.CompanyPhoneStr;
			CompanyRow["CompanyLogoFileName"] = LocalParams.CompanyLogoFileStr;
			CompanyRow["CompanyURL"] = LocalParams.CompanyURLStr;
			CompanyRow["CompanyCountry"] = LocalParams.CompanyCountryStr;

			DataSource.Tables["CompanyData"].Rows.Add(CompanyRow);



			// *** ORDER DATA ***

			DataRow OrderRow = DataSource.Tables["OrderData"].NewRow();

			// Populate the data source with order data
			OrderRow["OrderID"] = "34R297";

			// Set string for units type
			OrderRow["MeasurementUnits"] = "IN";

			// Set string for Order user def 1
			OrderRow["OrderUserDef1"] = "Order User Def";
			
			
			// Create the measurement date

			// Get current time
			DateTime Time = DateTime.Now;
			String DateStr;
			string MonthStr;
			string DayStr;


			if (Time.Month < 10)
				MonthStr = string.Format("0{0}", Time.Month);

			else
				MonthStr = Time.Month.ToString();


			if (Time.Day < 10)
				DayStr = string.Format("0{0}", Time.Day);

			else
				DayStr = Time.Day.ToString();



			if (LocalParams.UseNorthAmericanDateFormat)
			{
				// Build Date string in north american format   (month-day-year)
				DateStr = string.Format("{0}-{1}-{2}", MonthStr, DayStr, Time.Year.ToString());

			}

			else
			{
				// Build string in European format (day-month-year)
				DateStr = string.Format("{0}-{1}-{2}", DayStr, MonthStr, Time.Year.ToString());
			}

			OrderRow["MeasurementCompletionDate"] = DateStr;

			DataSource.Tables["OrderData"].Rows.Add(OrderRow);

		}

		private void PackListTemplateEditClick(object sender, EventArgs e)
		{
			if (ReportPrinterComboBox.SelectedIndex != -1)
			{

				report = new DataBoundReport();

				DataSet DataSource;

				report.GetDataSource(out DataSource);

				// Populate the data source with common report data
				PopulateReportCommonData(ref DataSource);

				DataRow LineItemRow;
				DataRow MeasuredPartsRow;

				int PartID = 1263;
				double StartingWidth = 12.0;
				double StartingHeight = 14.0;
				int MeasuredPartsCnt = 1;

				// For each line item in order, populate line item data in DataSource
				for (int u = 0; u < 10; u++)
				{

					// Create a new row in the line item data source table
					LineItemRow = DataSource.Tables["LineItems"].NewRow();

					// Populate line item columns in data source
					LineItemRow["LineItemNumber"] = u + 1;
					LineItemRow["PartID"] = (PartID + u).ToString();
					LineItemRow["QuantityRequired"] = MeasuredPartsCnt;
					LineItemRow["QuantityMeasured"] = MeasuredPartsCnt;
					LineItemRow["NominalWidth"] = StartingWidth;
					LineItemRow["NominalHeight"] = StartingHeight;
					LineItemRow["NominalThickness"] = 1.0;
					LineItemRow["Material"] = "Oak";
					LineItemRow["Style"] = "Door";

					LineItemRow["Hinging"] = "Yes";
					LineItemRow["Finish"] = "Poly";
					LineItemRow["Type"] = "Raised Panel";
					LineItemRow["Location"] = "Kitchen";
					LineItemRow["Machining"] = "Edge Band";
					LineItemRow["Assembly"] = "386924";
					LineItemRow["UserMsg"] = "Install Hinges";


					LineItemRow["Comment"] = "Part Comment";
					LineItemRow["User1"] = "User Text 1";
					LineItemRow["User2"] = "User Text 2";
					LineItemRow["User3"] = "User Text 3";

					// Add row to table
					DataSource.Tables["LineItems"].Rows.Add(LineItemRow);

					// For each measured part in line item, add it
					for (int v = 0; v < MeasuredPartsCnt; v++)
					{

						// Create a new row in measured part data source
						MeasuredPartsRow = DataSource.Tables["Measurement"].NewRow();

						// Populate measured part instances for line item
						MeasuredPartsRow["MeasuredWidth"] = StartingWidth + .01;
						MeasuredPartsRow["MeasuredHeight"] = StartingHeight + .01;
						MeasuredPartsRow["MeasuredDepth"] = 1.0;
						MeasuredPartsRow["WidthInSpec"] = "OK";
						MeasuredPartsRow["HeightInSpec"] = "OK";
						MeasuredPartsRow["DepthInSpec"] = "OK";
						MeasuredPartsRow["TimeDate"] = "08-01-2010  8:23:47";
						MeasuredPartsRow["PartInstanceNumber"] = v + 1;
						MeasuredPartsRow["ManuallyMeasured"] = "No";
						MeasuredPartsRow["LineItemNumber"] = u + 1;

						// V1.2  Add new table elements for error and rotation
						MeasuredPartsRow["WidthError"] = .015;

						MeasuredPartsRow["HeightError"] = .008;

						MeasuredPartsRow["DepthError"] = .021;

						MeasuredPartsRow["PartMeasuredRotated"] = "False";

						// Add row to table
						DataSource.Tables["Measurement"].Rows.Add(MeasuredPartsRow);

						StartingHeight += .01;
						StartingWidth += .01;
					}


					MeasuredPartsCnt++;



				}


				// Update data source for report
				report.SetDataSource(DataSource);

				// try to open default pack list
				try
				{

					report.LoadLayout(PackListTemplateFolderText.Text + "\\Default Pack List.repx");


				}

				catch
				{
					MessageBox.Show("Default Pack List file cannot be found.  Using blank template", "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				}


				// Show a time dialog telling user to wait for report manager to open
				TimedDialog Dlg = new TimedDialog("Report Manager starting.  Please wait.", "Starting Report Manager", 1500);
				Dlg.ShowDialog();

				// Set printer name for pack list
				report.PrinterName = LocalParams.ReportPrinterNameStr;

				report.ShowDesignerDialog();
			}

			// No valid printer selected
			else
			{
				MessageBox.Show("The report printer must be selected\nbefore using this function", "No Report Printer Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			


		}

		private void BrowsePackListTemplateFolder_Click(object sender, EventArgs e)
		{

			FolderBrowserDialog Dlg = new FolderBrowserDialog();

			Dlg.Description = "Select print pack list template file folder";
			Dlg.SelectedPath = PackListTemplateFolderText.Text;

			if (Dlg.ShowDialog() == DialogResult.OK)
				PackListTemplateFolderText.Text = Dlg.SelectedPath;

		}

		private void BrowsePrintLabelTemplateFolder_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog Dlg = new FolderBrowserDialog();

			Dlg.Description = "Select part label template file folder";
			Dlg.SelectedPath = PartLabelTemplateFolderText.Text;

			if (Dlg.ShowDialog() == DialogResult.OK)
				PartLabelTemplateFolderText.Text = Dlg.SelectedPath;


		}

		private void ReportPrinterComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			// Sets the new report printer name when one selected.
			LocalParams.ReportPrinterNameStr = ReportPrinterComboBox.SelectedItem.ToString();
		}

		private void LabelPrinterComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			// Sets the new label printer name when one selected.
			LocalParams.LabelPrinterNameStr = LabelPrinterComboBox.SelectedItem.ToString();
		}

		private void PartLabelTemplateEditClick(object sender, EventArgs e)
		{

			if (LabelPrinterComboBox.SelectedIndex != -1)
			{


				report = new DataBoundReport();

				DataSet DataSource;

				report.GetDataSource(out DataSource);
  						

				// Populate the data source with common report data
				PopulateReportCommonData(ref DataSource);

				DataRow LineItemRow;
				DataRow MeasuredPartsRow;

				int PartID = 1263;
				double StartingWidth = 12.0;
				double StartingHeight = 14.0;
				int MeasuredPartsCnt = 1;
				
				// For each line item in order, populate line item data in DataSource
				for (int u = 0; u < 1; u++)
				{

					// Create a new row in the line item data source table
					LineItemRow = DataSource.Tables["LineItems"].NewRow();

					// Populate line item columns in data source
					LineItemRow["LineItemNumber"] = u + 1;
					LineItemRow["PartID"] = (PartID + u).ToString();
					LineItemRow["QuantityRequired"] = MeasuredPartsCnt;
					LineItemRow["QuantityMeasured"] = MeasuredPartsCnt;
					LineItemRow["NominalWidth"] = StartingWidth;
					LineItemRow["NominalHeight"] = StartingHeight;
					LineItemRow["NominalThickness"] = 1.0;
					LineItemRow["Material"] = "Oak";
					LineItemRow["Style"] = "Door";

					LineItemRow["Hinging"] = "Yes";
					LineItemRow["Finish"] = "Poly";
					LineItemRow["Type"] = "Raised Panel";
					LineItemRow["Location"] = "Kitchen";
					LineItemRow["Machining"] = "Edge Band";
					LineItemRow["Assembly"] = "386924";
					LineItemRow["UserMsg"] = "Install Hinges";


					LineItemRow["Comment"] = "Part Comment";
					LineItemRow["User1"] = string.Copy("User 1");
					LineItemRow["User2"] = "User Text 2";
					LineItemRow["User3"] = "User Text 3";

					// Add row to table
					DataSource.Tables["LineItems"].Rows.Add(LineItemRow);

					// For each measured part in line item, add it
					for (int v = 0; v < MeasuredPartsCnt; v++)
					{

						// Create a new row in measured part data source
						MeasuredPartsRow = DataSource.Tables["Measurement"].NewRow();

						// Populate measured part instances for line item
						MeasuredPartsRow["MeasuredWidth"] = StartingWidth + .01;
						MeasuredPartsRow["MeasuredHeight"] = StartingHeight + .01;
						MeasuredPartsRow["MeasuredDepth"] = 1.0;
						MeasuredPartsRow["WidthInSpec"] = "OK";
						MeasuredPartsRow["HeightInSpec"] = "OK";
						MeasuredPartsRow["DepthInSpec"] = "OK";
						MeasuredPartsRow["TimeDate"] = "08-01-2010  8:23:47";
						MeasuredPartsRow["PartInstanceNumber"] = v + 1;
						MeasuredPartsRow["ManuallyMeasured"] = "No";
						MeasuredPartsRow["LineItemNumber"] = u + 1;

						// V1.2  Add new table elements for error and rotation
						MeasuredPartsRow["WidthError"] = .015;

						MeasuredPartsRow["HeightError"] = .008;

						MeasuredPartsRow["DepthError"] = .021;

						MeasuredPartsRow["PartMeasuredRotated"] = "False";

						// Add row to table
						DataSource.Tables["Measurement"].Rows.Add(MeasuredPartsRow);

						StartingHeight += .01;
						StartingWidth += .01;
					}


					MeasuredPartsCnt++;



				}



				// Update data source for report
				report.SetDataSource(DataSource);

				// try to open default pack list
				try
				{

					report.LoadLayout(PartLabelTemplateFolderText.Text + "\\Default Part Label.repx");

				}

				catch
				{
					MessageBox.Show("Default Part Label file cannot be found.  Using blank template", "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				}


				// Show a time dialog telling user to wait for report manager to open
				TimedDialog Dlg = new TimedDialog("Report Manager starting.  Please wait.", "Starting Report Manager", 1500);
				Dlg.ShowDialog();

				report.PrinterName = LocalParams.LabelPrinterNameStr;
				
			
				report.ShowDesignerDialog();
			}

			else
			{
				MessageBox.Show("The label printer must be selected\nbefore using this function", "No Label Printer Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}

		}

		private void button6_Click(object sender, EventArgs e)
		{

			if (MessageBox.Show("WARNING:  THIS OPERATION WILL OVERWRITE ALL OF\nYOUR CURRENT SYSTEM CONFIGURATION DATA\nDO YOU WISH TO CONTINUE?", "System Configuration Import", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{

				FolderBrowserDialog Dlg = new FolderBrowserDialog();
				int LineNumber = 0;

				Dlg.Description = "Select the Import Target Directory";
				Dlg.SelectedPath = Directory.GetCurrentDirectory();

				if (Dlg.ShowDialog() == DialogResult.OK)
				{
					// Importing....
					try
					{
						StreamReader ImportStream;
						string PathFilename = Dlg.SelectedPath + "\\" + ExportFileName;

						// Check if file exists in specified directory
						if (File.Exists(PathFilename))
						{
							bool PrematureEOF = false;

							// Try to open the stream for import 
							using (ImportStream = new StreamReader(PathFilename))
							{

								string InputStr = "";


								while (((InputStr = ImportStream.ReadLine()) != null))
								{

									// Check if params start header
									if (InputStr.Equals(LocalParams.GetParamsStartString()))
									{
										LineNumber++;

										if (!LocalParams.ImportParamsValues(ImportStream, ref LineNumber))
											PrematureEOF = true;
									}

									// Check if Template start header
									else if (InputStr.Equals(LocalParams.GetOrderTemplatesStartString()))
									{
										LineNumber++;

										if (!LocalParams.ImportOrderTemplateValues(ImportStream, ref LineNumber))
											PrematureEOF = true;
									}

									// Check for Axis start header
									if (InputStr.Equals(LocalHardware.GetAxesStartString()))
									{
										LineNumber++;

										if (!LocalHardware.ImportAxisData(ImportStream, ref LineNumber))
											PrematureEOF = true;

									}

								}

							}

							ImportStream.Close();

							// Check if premature EOF detected during section read.  If so, complain
							if (PrematureEOF)
								MessageBox.Show("CONFIGURATION FILE IMPORT FAILURE!\nEnd of file before completion.\n\nRegenerate configuration export.", "Configuration Import Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);

							// Configuration imported OK
							else
							{
								// Reload the controls after the import
								LoadDialogControls();

								MessageBox.Show("Configuration Import Complete!", "System Configuration Import", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}

						// Unable to locate the file in the specified directory
						else
							MessageBox.Show("Unable to locate configuration import\nfile in the specified directory.\n\nConfiguration import failed!", "Configuration Import Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);


					}

					// An exception occured....
					catch (Exception Ex)
					{
						MessageBox.Show("Unable to Import Configuration Data:\n" + Ex.Message + "\nLine Number: " + LineNumber, "Configuration Import Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}
				}
			}

		
		}

		private void ExportClick(object sender, EventArgs e)
		{

			if (MessageBox.Show("This operation will export\nthe current system configuration.\n\nDo you want to continue?", "Export Configuration", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
			{

				// Select the directory to export to.  Preset to current directory
				FolderBrowserDialog Dlg = new FolderBrowserDialog();

				Dlg.Description = "Select the Export Target Directory";
				Dlg.SelectedPath = Directory.GetCurrentDirectory();

				// Show the directory dialog.  If user clicks ok, execute export
				if (Dlg.ShowDialog() == DialogResult.OK)
				{
					string FilePath;

					FilePath = Dlg.SelectedPath + "\\" + ExportFileName;

					// Export the configuration parameters.  Pass the Measurement Hardware reference to access the object
					LocalParams.ExportSystemParameters(FilePath, ref LocalHardware);

					MessageBox.Show("Configuration Export Completed!", "Export Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);

				}
			}

		}

	


		private void UseZAxisCheckBox_Click(object sender, EventArgs e)
		{
			LocalParams.ZAxisInUse = UseZAxisCheckBox.Checked;

			if (UseZAxisCheckBox.Checked)
			{
				MessageBox.Show("If this is the first time the Depth axis has been enabled,\nyou must use the Auto Configure button to\nenabled the RF hardware.", "Depth Axis Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void OptionsDialog_Load(object sender, EventArgs e)
		{

		}


		private void UseAngleCheckBox_Click(object sender, EventArgs e)
		{

			LocalParams.AngleAxisInUse = UseAngleCheckBox.Checked;


		}




	}
}
