﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProCABQC11
{
	public class CSVReader
	{

		protected int LineItemStart = 0;
		protected StreamReader CSVFileStreamIn;
		protected int CSVLineNumber = 0;
		protected string BaseFileName = "";
		protected bool ParseErrors = false;
		
		protected const string FileNotFoundErrorMsg = "does not exist.";
		protected const string InvalidProcessCall = "The virtual 'ProcessCSVFile()' call not allowed";
		

		protected CSVReader()
		{

		}


		public CSVReaderResultCodes OpenCSVFile(string CSVFileName, ref PersistentParametersV1100 PParams, ref List<string> ErrorLog, ref SystemOrders OrdersList, int TemplateIndex)
		{

			// Make sure that passed files exists.  If not, write error log and return failure
			if (!File.Exists(CSVFileName))
			{
				string Msg = string.Format("Error - File: {0} {1}", CSVFileName, FileNotFoundErrorMsg);
				ErrorLog.Add(Msg);
				return CSVReaderResultCodes.FileOpenError;

			}

			
			else
			{
				// Verify that template index is valid
				if (TemplateIndex != -1)
				{

					string TemplateType;

					// Confirm that the appropriate template has the required fields configured.
					if (PParams.IsSelectedTemplateConfigured(out TemplateType, TemplateIndex))
					{

						// File exists, open it.  Call the respective ProcessCSVFile operation
						try
						{

							// Assign the stream reader to the file
							CSVFileStreamIn = File.OpenText(CSVFileName);

							// Save the base file name (no path)
							string FileName = CSVFileName.Substring(CSVFileName.LastIndexOf(@"\") + 1);
							BaseFileName = FileName.Substring(0, FileName.Length - 4);

							// Call the processor.  Return its status
							return ProcessCSVFile(ref PParams, ref ErrorLog, ref OrdersList, TemplateIndex);
						}

						// A problem opening the file
						catch (Exception e)
						{
							ErrorLog.Add(e.Message);
							return CSVReaderResultCodes.FileOpenError;


						}
					}

					// Template Not configured.  Add error message to log
					else
					{
						string Msg = string.Format("{0} template does not have all required fields configured.", TemplateType);
						ErrorLog.Add(Msg);
						return CSVReaderResultCodes.ParsingErrors;

					}
				}

				// An invalid template index, probably caused by an invalid template name in the batch file.
				else
				{
					string Msg = "Invalid Template defined in batch file";
					ErrorLog.Add(Msg);
					return CSVReaderResultCodes.ParsingErrors;
				}
			}
		}


		protected void CloseCSVFile()
		{

			CSVFileStreamIn.Close();

		}



		virtual protected CSVReaderResultCodes ProcessCSVFile(ref PersistentParametersV1100 PParams, ref List<string> ErrorLog, ref SystemOrders SystemOrders, int TemplateIndex)
		{

			string Msg = string.Format("Error - {0}", InvalidProcessCall);
			ErrorLog.Add(Msg);
			return CSVReaderResultCodes.ParsingErrors;

		}

	}
}
