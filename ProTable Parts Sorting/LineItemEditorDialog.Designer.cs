﻿namespace ProCABQC11
{
	partial class LineItemEditorDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LineItemEditorDialog));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.LineNumberText = new System.Windows.Forms.TextBox();
			this.PartIDText = new System.Windows.Forms.TextBox();
			this.WidthText = new System.Windows.Forms.TextBox();
			this.HeightText = new System.Windows.Forms.TextBox();
			this.QtyReqText = new System.Windows.Forms.TextBox();
			this.OKBut = new System.Windows.Forms.Button();
			this.CancelBut = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(22, 104);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(50, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Width";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(22, 142);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 20);
			this.label2.TabIndex = 1;
			this.label2.Text = "Height";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(22, 180);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(106, 20);
			this.label3.TabIndex = 2;
			this.label3.Text = "Qty. Required";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(22, 28);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(99, 20);
			this.label4.TabIndex = 3;
			this.label4.Text = "Line Number";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(22, 66);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(59, 20);
			this.label5.TabIndex = 4;
			this.label5.Text = "Part ID";
			// 
			// LineNumberText
			// 
			this.LineNumberText.Location = new System.Drawing.Point(167, 25);
			this.LineNumberText.Name = "LineNumberText";
			this.LineNumberText.ReadOnly = true;
			this.LineNumberText.Size = new System.Drawing.Size(77, 26);
			this.LineNumberText.TabIndex = 5;
			this.LineNumberText.TabStop = false;
			// 
			// PartIDText
			// 
			this.PartIDText.Location = new System.Drawing.Point(167, 63);
			this.PartIDText.Name = "PartIDText";
			this.PartIDText.ReadOnly = true;
			this.PartIDText.Size = new System.Drawing.Size(184, 26);
			this.PartIDText.TabIndex = 6;
			this.PartIDText.TabStop = false;
			// 
			// WidthText
			// 
			this.WidthText.Location = new System.Drawing.Point(167, 101);
			this.WidthText.Name = "WidthText";
			this.WidthText.Size = new System.Drawing.Size(114, 26);
			this.WidthText.TabIndex = 7;
			this.WidthText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WidthText_KeyPress);
			// 
			// HeightText
			// 
			this.HeightText.Location = new System.Drawing.Point(167, 139);
			this.HeightText.Name = "HeightText";
			this.HeightText.Size = new System.Drawing.Size(114, 26);
			this.HeightText.TabIndex = 8;
			this.HeightText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HeightText_KeyPress);
			// 
			// QtyReqText
			// 
			this.QtyReqText.Location = new System.Drawing.Point(167, 177);
			this.QtyReqText.Name = "QtyReqText";
			this.QtyReqText.Size = new System.Drawing.Size(77, 26);
			this.QtyReqText.TabIndex = 9;
			this.QtyReqText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.QtyReqText_KeyPress);
			// 
			// OKBut
			// 
			this.OKBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OKBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OKBut.Location = new System.Drawing.Point(77, 238);
			this.OKBut.Name = "OKBut";
			this.OKBut.Size = new System.Drawing.Size(77, 40);
			this.OKBut.TabIndex = 10;
			this.OKBut.UseVisualStyleBackColor = true;
			this.OKBut.Click += new System.EventHandler(this.OKBut_Click);
			// 
			// CancelBut
			// 
			this.CancelBut.BackgroundImage = global::ProCABQC11.Properties.Resources.remove_48x48;
			this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBut.Location = new System.Drawing.Point(238, 238);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(75, 40);
			this.CancelBut.TabIndex = 11;
			this.CancelBut.UseVisualStyleBackColor = true;
			// 
			// LineItemEditorDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(394, 295);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.OKBut);
			this.Controls.Add(this.QtyReqText);
			this.Controls.Add(this.HeightText);
			this.Controls.Add(this.WidthText);
			this.Controls.Add(this.PartIDText);
			this.Controls.Add(this.LineNumberText);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LineItemEditorDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Line Item Editor";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox LineNumberText;
		private System.Windows.Forms.TextBox PartIDText;
		private System.Windows.Forms.TextBox WidthText;
		private System.Windows.Forms.TextBox HeightText;
		private System.Windows.Forms.TextBox QtyReqText;
		private System.Windows.Forms.Button OKBut;
		private System.Windows.Forms.Button CancelBut;
	}
}