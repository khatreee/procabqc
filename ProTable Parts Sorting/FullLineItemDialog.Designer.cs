﻿namespace ProCABQC11
{
	partial class FullLineItemDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FullLineItemDialog));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.LineNumberText = new System.Windows.Forms.TextBox();
			this.PartIDText = new System.Windows.Forms.TextBox();
			this.HeightText = new System.Windows.Forms.TextBox();
			this.WidthText = new System.Windows.Forms.TextBox();
			this.ThicknessText = new System.Windows.Forms.TextBox();
			this.MaterialText = new System.Windows.Forms.TextBox();
			this.PartStyleText = new System.Windows.Forms.TextBox();
			this.CommentText = new System.Windows.Forms.TextBox();
			this.QtyReqText = new System.Windows.Forms.TextBox();
			this.OkBut = new System.Windows.Forms.Button();
			this.CancelBut = new System.Windows.Forms.Button();
			this.QuestionText = new System.Windows.Forms.Label();
			this.NormalHeightErrorLabel = new System.Windows.Forms.Label();
			this.NormalWidthErrorLabel = new System.Windows.Forms.Label();
			this.NormalHeightError = new System.Windows.Forms.TextBox();
			this.NormalWidthError = new System.Windows.Forms.TextBox();
			this.UserDef1Text = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.UserDef2Text = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.ThicknessError = new System.Windows.Forms.TextBox();
			this.ThicknessErrorLabel = new System.Windows.Forms.Label();
			this.RotatedHeightErrorLabel = new System.Windows.Forms.Label();
			this.RotatedWidthErrorLabel = new System.Windows.Forms.Label();
			this.RotatedHeightError = new System.Windows.Forms.TextBox();
			this.RotatedWidthError = new System.Windows.Forms.TextBox();
			this.UserDef3Text = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.TypeText = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.MachiningText = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.HingingText = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.LocationText = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.FinishText = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.AssemblyText = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.UserMsgText = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 31);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(67, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Line No.";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 66);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(59, 20);
			this.label2.TabIndex = 2;
			this.label2.Text = "Part ID";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 101);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 20);
			this.label3.TabIndex = 3;
			this.label3.Text = "Height";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 136);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(50, 20);
			this.label4.TabIndex = 4;
			this.label4.Text = "Width";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 171);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 20);
			this.label5.TabIndex = 5;
			this.label5.Text = "Thickness";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 206);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(65, 20);
			this.label6.TabIndex = 6;
			this.label6.Text = "Material";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 241);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(77, 20);
			this.label7.TabIndex = 7;
			this.label7.Text = "Part Style";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(12, 276);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(78, 20);
			this.label8.TabIndex = 8;
			this.label8.Text = "Comment";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(12, 431);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(106, 20);
			this.label9.TabIndex = 9;
			this.label9.Text = "Qty. Required";
			// 
			// LineNumberText
			// 
			this.LineNumberText.Location = new System.Drawing.Point(124, 31);
			this.LineNumberText.Name = "LineNumberText";
			this.LineNumberText.ReadOnly = true;
			this.LineNumberText.Size = new System.Drawing.Size(341, 26);
			this.LineNumberText.TabIndex = 10;
			// 
			// PartIDText
			// 
			this.PartIDText.Location = new System.Drawing.Point(124, 66);
			this.PartIDText.Name = "PartIDText";
			this.PartIDText.ReadOnly = true;
			this.PartIDText.Size = new System.Drawing.Size(341, 26);
			this.PartIDText.TabIndex = 11;
			// 
			// HeightText
			// 
			this.HeightText.Location = new System.Drawing.Point(124, 101);
			this.HeightText.Name = "HeightText";
			this.HeightText.ReadOnly = true;
			this.HeightText.Size = new System.Drawing.Size(81, 26);
			this.HeightText.TabIndex = 12;
			// 
			// WidthText
			// 
			this.WidthText.Location = new System.Drawing.Point(124, 136);
			this.WidthText.Name = "WidthText";
			this.WidthText.ReadOnly = true;
			this.WidthText.Size = new System.Drawing.Size(81, 26);
			this.WidthText.TabIndex = 13;
			// 
			// ThicknessText
			// 
			this.ThicknessText.Location = new System.Drawing.Point(124, 171);
			this.ThicknessText.Name = "ThicknessText";
			this.ThicknessText.ReadOnly = true;
			this.ThicknessText.Size = new System.Drawing.Size(81, 26);
			this.ThicknessText.TabIndex = 14;
			// 
			// MaterialText
			// 
			this.MaterialText.Location = new System.Drawing.Point(124, 206);
			this.MaterialText.Name = "MaterialText";
			this.MaterialText.ReadOnly = true;
			this.MaterialText.Size = new System.Drawing.Size(341, 26);
			this.MaterialText.TabIndex = 15;
			// 
			// PartStyleText
			// 
			this.PartStyleText.Location = new System.Drawing.Point(124, 241);
			this.PartStyleText.Name = "PartStyleText";
			this.PartStyleText.ReadOnly = true;
			this.PartStyleText.Size = new System.Drawing.Size(341, 26);
			this.PartStyleText.TabIndex = 16;
			// 
			// CommentText
			// 
			this.CommentText.Location = new System.Drawing.Point(124, 276);
			this.CommentText.Name = "CommentText";
			this.CommentText.ReadOnly = true;
			this.CommentText.Size = new System.Drawing.Size(341, 26);
			this.CommentText.TabIndex = 17;
			// 
			// QtyReqText
			// 
			this.QtyReqText.Location = new System.Drawing.Point(124, 431);
			this.QtyReqText.Name = "QtyReqText";
			this.QtyReqText.ReadOnly = true;
			this.QtyReqText.Size = new System.Drawing.Size(66, 26);
			this.QtyReqText.TabIndex = 18;
			// 
			// OkBut
			// 
			this.OkBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OkBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OkBut.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OkBut.Location = new System.Drawing.Point(381, 550);
			this.OkBut.Name = "OkBut";
			this.OkBut.Size = new System.Drawing.Size(75, 41);
			this.OkBut.TabIndex = 0;
			this.OkBut.UseVisualStyleBackColor = true;
			// 
			// CancelBut
			// 
			this.CancelBut.BackgroundImage = global::ProCABQC11.Properties.Resources.remove_48x48;
			this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBut.Location = new System.Drawing.Point(542, 550);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(75, 41);
			this.CancelBut.TabIndex = 1;
			this.CancelBut.UseVisualStyleBackColor = true;
			// 
			// QuestionText
			// 
			this.QuestionText.AutoSize = true;
			this.QuestionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.QuestionText.ForeColor = System.Drawing.Color.Red;
			this.QuestionText.Location = new System.Drawing.Point(385, 506);
			this.QuestionText.Name = "QuestionText";
			this.QuestionText.Size = new System.Drawing.Size(232, 20);
			this.QuestionText.TabIndex = 21;
			this.QuestionText.Text = "Is this the correct line item?";
			this.QuestionText.Visible = false;
			// 
			// NormalHeightErrorLabel
			// 
			this.NormalHeightErrorLabel.AutoSize = true;
			this.NormalHeightErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NormalHeightErrorLabel.Location = new System.Drawing.Point(211, 101);
			this.NormalHeightErrorLabel.Name = "NormalHeightErrorLabel";
			this.NormalHeightErrorLabel.Size = new System.Drawing.Size(40, 26);
			this.NormalHeightErrorLabel.TabIndex = 22;
			this.NormalHeightErrorLabel.Text = "Normal\r\nError";
			this.NormalHeightErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// NormalWidthErrorLabel
			// 
			this.NormalWidthErrorLabel.AutoSize = true;
			this.NormalWidthErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NormalWidthErrorLabel.Location = new System.Drawing.Point(211, 137);
			this.NormalWidthErrorLabel.Name = "NormalWidthErrorLabel";
			this.NormalWidthErrorLabel.Size = new System.Drawing.Size(40, 26);
			this.NormalWidthErrorLabel.TabIndex = 23;
			this.NormalWidthErrorLabel.Text = "Normal\r\nError";
			this.NormalWidthErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// NormalHeightError
			// 
			this.NormalHeightError.Location = new System.Drawing.Point(257, 100);
			this.NormalHeightError.Name = "NormalHeightError";
			this.NormalHeightError.ReadOnly = true;
			this.NormalHeightError.Size = new System.Drawing.Size(68, 26);
			this.NormalHeightError.TabIndex = 24;
			// 
			// NormalWidthError
			// 
			this.NormalWidthError.Location = new System.Drawing.Point(257, 136);
			this.NormalWidthError.Name = "NormalWidthError";
			this.NormalWidthError.ReadOnly = true;
			this.NormalWidthError.Size = new System.Drawing.Size(68, 26);
			this.NormalWidthError.TabIndex = 25;
			// 
			// UserDef1Text
			// 
			this.UserDef1Text.Location = new System.Drawing.Point(124, 312);
			this.UserDef1Text.Name = "UserDef1Text";
			this.UserDef1Text.ReadOnly = true;
			this.UserDef1Text.Size = new System.Drawing.Size(341, 26);
			this.UserDef1Text.TabIndex = 27;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(12, 312);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(86, 20);
			this.label10.TabIndex = 26;
			this.label10.Text = "User Def 1";
			// 
			// UserDef2Text
			// 
			this.UserDef2Text.Location = new System.Drawing.Point(124, 350);
			this.UserDef2Text.Name = "UserDef2Text";
			this.UserDef2Text.ReadOnly = true;
			this.UserDef2Text.Size = new System.Drawing.Size(341, 26);
			this.UserDef2Text.TabIndex = 29;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(12, 350);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(86, 20);
			this.label11.TabIndex = 28;
			this.label11.Text = "User Def 2";
			// 
			// ThicknessError
			// 
			this.ThicknessError.Location = new System.Drawing.Point(257, 171);
			this.ThicknessError.Name = "ThicknessError";
			this.ThicknessError.ReadOnly = true;
			this.ThicknessError.Size = new System.Drawing.Size(68, 26);
			this.ThicknessError.TabIndex = 31;
			// 
			// ThicknessErrorLabel
			// 
			this.ThicknessErrorLabel.AutoSize = true;
			this.ThicknessErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ThicknessErrorLabel.Location = new System.Drawing.Point(211, 174);
			this.ThicknessErrorLabel.Name = "ThicknessErrorLabel";
			this.ThicknessErrorLabel.Size = new System.Drawing.Size(44, 20);
			this.ThicknessErrorLabel.TabIndex = 30;
			this.ThicknessErrorLabel.Text = "Error";
			// 
			// RotatedHeightErrorLabel
			// 
			this.RotatedHeightErrorLabel.AutoSize = true;
			this.RotatedHeightErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RotatedHeightErrorLabel.Location = new System.Drawing.Point(346, 102);
			this.RotatedHeightErrorLabel.Name = "RotatedHeightErrorLabel";
			this.RotatedHeightErrorLabel.Size = new System.Drawing.Size(45, 26);
			this.RotatedHeightErrorLabel.TabIndex = 32;
			this.RotatedHeightErrorLabel.Text = "Rotated\r\nError";
			this.RotatedHeightErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// RotatedWidthErrorLabel
			// 
			this.RotatedWidthErrorLabel.AutoSize = true;
			this.RotatedWidthErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RotatedWidthErrorLabel.Location = new System.Drawing.Point(346, 136);
			this.RotatedWidthErrorLabel.Name = "RotatedWidthErrorLabel";
			this.RotatedWidthErrorLabel.Size = new System.Drawing.Size(45, 26);
			this.RotatedWidthErrorLabel.TabIndex = 33;
			this.RotatedWidthErrorLabel.Text = "Rotated\r\nError";
			this.RotatedWidthErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// RotatedHeightError
			// 
			this.RotatedHeightError.Location = new System.Drawing.Point(397, 102);
			this.RotatedHeightError.Name = "RotatedHeightError";
			this.RotatedHeightError.ReadOnly = true;
			this.RotatedHeightError.Size = new System.Drawing.Size(68, 26);
			this.RotatedHeightError.TabIndex = 34;
			// 
			// RotatedWidthError
			// 
			this.RotatedWidthError.Location = new System.Drawing.Point(397, 137);
			this.RotatedWidthError.Name = "RotatedWidthError";
			this.RotatedWidthError.ReadOnly = true;
			this.RotatedWidthError.Size = new System.Drawing.Size(68, 26);
			this.RotatedWidthError.TabIndex = 35;
			// 
			// UserDef3Text
			// 
			this.UserDef3Text.Location = new System.Drawing.Point(125, 388);
			this.UserDef3Text.Name = "UserDef3Text";
			this.UserDef3Text.ReadOnly = true;
			this.UserDef3Text.Size = new System.Drawing.Size(341, 26);
			this.UserDef3Text.TabIndex = 37;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(13, 388);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(86, 20);
			this.label12.TabIndex = 36;
			this.label12.Text = "User Def 3";
			// 
			// TypeText
			// 
			this.TypeText.Location = new System.Drawing.Point(621, 31);
			this.TypeText.Name = "TypeText";
			this.TypeText.ReadOnly = true;
			this.TypeText.Size = new System.Drawing.Size(341, 26);
			this.TypeText.TabIndex = 39;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(509, 31);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(43, 20);
			this.label13.TabIndex = 38;
			this.label13.Text = "Type";
			// 
			// MachiningText
			// 
			this.MachiningText.Location = new System.Drawing.Point(621, 66);
			this.MachiningText.Name = "MachiningText";
			this.MachiningText.ReadOnly = true;
			this.MachiningText.Size = new System.Drawing.Size(341, 26);
			this.MachiningText.TabIndex = 41;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(509, 66);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(81, 20);
			this.label14.TabIndex = 40;
			this.label14.Text = "Machining";
			// 
			// HingingText
			// 
			this.HingingText.Location = new System.Drawing.Point(621, 102);
			this.HingingText.Name = "HingingText";
			this.HingingText.ReadOnly = true;
			this.HingingText.Size = new System.Drawing.Size(341, 26);
			this.HingingText.TabIndex = 43;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(509, 102);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(63, 20);
			this.label15.TabIndex = 42;
			this.label15.Text = "Hinging";
			// 
			// LocationText
			// 
			this.LocationText.Location = new System.Drawing.Point(621, 137);
			this.LocationText.Name = "LocationText";
			this.LocationText.ReadOnly = true;
			this.LocationText.Size = new System.Drawing.Size(341, 26);
			this.LocationText.TabIndex = 45;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(509, 137);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(70, 20);
			this.label16.TabIndex = 44;
			this.label16.Text = "Location";
			// 
			// FinishText
			// 
			this.FinishText.Location = new System.Drawing.Point(621, 171);
			this.FinishText.Name = "FinishText";
			this.FinishText.ReadOnly = true;
			this.FinishText.Size = new System.Drawing.Size(341, 26);
			this.FinishText.TabIndex = 47;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(509, 171);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(51, 20);
			this.label17.TabIndex = 46;
			this.label17.Text = "Finish";
			// 
			// AssemblyText
			// 
			this.AssemblyText.Location = new System.Drawing.Point(621, 206);
			this.AssemblyText.Name = "AssemblyText";
			this.AssemblyText.ReadOnly = true;
			this.AssemblyText.Size = new System.Drawing.Size(341, 26);
			this.AssemblyText.TabIndex = 49;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(509, 209);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(77, 20);
			this.label18.TabIndex = 48;
			this.label18.Text = "Assembly";
			// 
			// UserMsgText
			// 
			this.UserMsgText.Location = new System.Drawing.Point(621, 241);
			this.UserMsgText.Name = "UserMsgText";
			this.UserMsgText.ReadOnly = true;
			this.UserMsgText.Size = new System.Drawing.Size(341, 26);
			this.UserMsgText.TabIndex = 51;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(509, 244);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(77, 20);
			this.label19.TabIndex = 50;
			this.label19.Text = "User Msg";
			// 
			// FullLineItemDialog
			// 
			this.AcceptButton = this.CancelBut;
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.CancelButton = this.CancelBut;
			this.ClientSize = new System.Drawing.Size(998, 628);
			this.Controls.Add(this.UserMsgText);
			this.Controls.Add(this.label19);
			this.Controls.Add(this.AssemblyText);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.FinishText);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.LocationText);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.HingingText);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.MachiningText);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.TypeText);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.UserDef3Text);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.RotatedWidthError);
			this.Controls.Add(this.RotatedHeightError);
			this.Controls.Add(this.RotatedWidthErrorLabel);
			this.Controls.Add(this.RotatedHeightErrorLabel);
			this.Controls.Add(this.ThicknessError);
			this.Controls.Add(this.ThicknessErrorLabel);
			this.Controls.Add(this.UserDef2Text);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.UserDef1Text);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.NormalWidthError);
			this.Controls.Add(this.NormalHeightError);
			this.Controls.Add(this.NormalWidthErrorLabel);
			this.Controls.Add(this.NormalHeightErrorLabel);
			this.Controls.Add(this.QuestionText);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.OkBut);
			this.Controls.Add(this.QtyReqText);
			this.Controls.Add(this.CommentText);
			this.Controls.Add(this.PartStyleText);
			this.Controls.Add(this.MaterialText);
			this.Controls.Add(this.ThicknessText);
			this.Controls.Add(this.WidthText);
			this.Controls.Add(this.HeightText);
			this.Controls.Add(this.PartIDText);
			this.Controls.Add(this.LineNumberText);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FullLineItemDialog";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Line Item Description";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox LineNumberText;
		private System.Windows.Forms.TextBox PartIDText;
		private System.Windows.Forms.TextBox HeightText;
		private System.Windows.Forms.TextBox WidthText;
		private System.Windows.Forms.TextBox ThicknessText;
		private System.Windows.Forms.TextBox MaterialText;
		private System.Windows.Forms.TextBox PartStyleText;
		private System.Windows.Forms.TextBox CommentText;
		private System.Windows.Forms.TextBox QtyReqText;
		private System.Windows.Forms.Button OkBut;
		private System.Windows.Forms.Button CancelBut;
		private System.Windows.Forms.Label QuestionText;
		private System.Windows.Forms.Label NormalHeightErrorLabel;
		private System.Windows.Forms.Label NormalWidthErrorLabel;
		private System.Windows.Forms.TextBox NormalHeightError;
		private System.Windows.Forms.TextBox NormalWidthError;
		private System.Windows.Forms.TextBox UserDef1Text;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox UserDef2Text;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox ThicknessError;
		private System.Windows.Forms.Label ThicknessErrorLabel;
		private System.Windows.Forms.Label RotatedHeightErrorLabel;
		private System.Windows.Forms.Label RotatedWidthErrorLabel;
		private System.Windows.Forms.TextBox RotatedHeightError;
		private System.Windows.Forms.TextBox RotatedWidthError;
		private System.Windows.Forms.TextBox UserDef3Text;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox TypeText;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox MachiningText;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox HingingText;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox LocationText;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox FinishText;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox AssemblyText;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox UserMsgText;
		private System.Windows.Forms.Label label19;
	}
}