﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class ErrorListDialog : Form
	{
		public ErrorListDialog()
		{
			InitializeComponent();
		}

		public ErrorListDialog(ref List<string> ErrorMsg)
		{

			InitializeComponent();

			// Add each error string to the list
			foreach (string Str in ErrorMsg)
				ErrorList.Items.Add(Str);

			
		}
	}
}
