﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.IO;

namespace ProCABQC11
{
	public class MeasurementInterfaceHardware
	{

		public enum AxisNames
		{
			XAxis,
			YAxis,
			ZAxis,
			AngleAxis,
			MaxAxes
		}

		public enum MeasureHardwareType
		{
			ProRF,
			ProMUX
		}


		private System.IO.Ports.SerialPort serialPort1;

		// Create the array of virtual axes, not the virtual axes themselves
		public VirtualAxis[] Axes = new VirtualAxis[(int)AxisNames.MaxAxes];

		private String ReceiveMsg;
		private String TransmitString = null;
		private MeasureHardwareType DeviceType;
		private int SystemNumAxes = 4;
		private string PortName = "";
		private bool SuppressUpdates = false;
		private bool UseZ = false;
		private bool UseAngle = false;

		// AXIS parameter import and export
		private const string AxisParamSectionStartString = "#AXIS PARAMETERS START";
		private const string AxisParamSectionEndString = "#AXIS PARAMETERS END";





		private bool ConfigReceiveState = false;
//		private HardwareConfigForm HardwareConfig;


		public delegate void UpdateWidthPosition();
		public event UpdateWidthPosition WidthUpdate;

		public delegate void UpdateHeightPosition();
		public event UpdateHeightPosition HeightUpdate;

		public delegate void UpdateDepthPosition();
		public event UpdateDepthPosition DepthUpdate;

		public delegate void UpdateAnglePosition();
		public event UpdateAnglePosition AngleUpdate;

		public delegate void ConfigurationMessage(string Message);
		public event ConfigurationMessage ConfigMessage;


		public bool ConfigurationState
		{
			set { ConfigReceiveState = value; }
			get { return ConfigReceiveState; }
		}


		// V1.2.0.6
		public int NumberOfAxes
		{
			set { SystemNumAxes = value; }
			get { return SystemNumAxes; }

		}

		public bool UseZAxis
		{
			set { UseZ = value; }
			get { return UseZ; }

		}

		public bool UseAngleAxis
		{
			set { UseAngle = value; }
			get { return UseAngle; }

		}


		public MeasurementInterfaceHardware()
		{


		}

		public MeasurementInterfaceHardware(string PortNameStr, MeasureHardwareType Type)
		{

			// Instantiate the virtual X axis
			Axes[(int)AxisNames.XAxis] = new VirtualAxis();

			// Try to read the axis object state.  If no data file exists, write a new one base on the defaults
			if (!Axes[(int)AxisNames.XAxis].ReadParameters(ref Axes[(int)AxisNames.XAxis]))
				Axes[(int)AxisNames.XAxis].SaveParameters();

			// Instantiate the virtual Y axis
			Axes[(int)AxisNames.YAxis] = new VirtualAxis();

			// Try to read the axis object state.  If no data file exists, write a new one base on the defaults
			if (!Axes[(int)AxisNames.YAxis].ReadParameters(ref Axes[(int)AxisNames.YAxis]))
				Axes[(int)AxisNames.YAxis].SaveParameters();

			// Instantiate the virtual Z axis
			Axes[(int)AxisNames.ZAxis] = new VirtualAxis();

			// Try to read the axis object state.  If no data file exists, write a new one base on the defaults
			if (!Axes[(int)AxisNames.ZAxis].ReadParameters(ref Axes[(int)AxisNames.ZAxis]))
				Axes[(int)AxisNames.ZAxis].SaveParameters();
   
			// Instantiate the virtual Angle axis
			Axes[(int)AxisNames.AngleAxis] = new VirtualAxis();

			// Try to read the axis object state.  If no data file exists, write a new one base on the defaults
			if (!Axes[(int)AxisNames.AngleAxis].ReadParameters(ref Axes[(int)AxisNames.AngleAxis]))
				Axes[(int)AxisNames.AngleAxis].SaveParameters();



			// Instantiate port
			serialPort1 = new SerialPort();

			// set event handler
			this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);

			// Save the current hardware type
			DeviceType = Type;

			// Set port name if not null string
			if (PortNameStr != "")
			{
				serialPort1.PortName = PortNameStr;
				PortName = PortNameStr;
			
			}


		}


		// Sets a new port name for the serial port
		public bool SetPortComName(string Name)
		{

			if (Name != null)
			{
				// If currently open, close it first.
				if(serialPort1.IsOpen)
					serialPort1.Close();

				serialPort1.PortName = Name;
				PortName = Name;

				return true;
			}

			else
			{
				return false;
			}

		}


		

		public bool OpenPort(out string Msg)
		{

			if (PortName != "")
			{
				// Try to open the port
				try
				{
					// Close port first if open
					if (serialPort1.IsOpen)
						serialPort1.Close();

					// try to open port
					serialPort1.Open();
					Msg = "";
					return true;

				}

				catch (UnauthorizedAccessException)
				{

					Msg = "The Communications Port could not be opened.\nThe port is in use by another device!";
					return false;


				}

				catch (Exception oEx)
				{

					Msg = "The Communications Port could not be opened.\n" + oEx.Message;
					return false;

				}
			}

			else
			{
				Msg = "The Measurement Communications Port has not yet been defined.\n";
				return false;
			}
		}


		public void ClosePort()
		{

			try
			{
				if (serialPort1.IsOpen)
					serialPort1.Close();

			}


			catch
			{

			}
		}

		private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
		{

			try
			{
				while (serialPort1.BytesToRead > 0)
					AddCharToInputString(serialPort1.ReadChar());

			}

			catch
			{

			}

		}


		private void AddCharToInputString(int Data)
		{


			if (Data == 0x0D)
			{

				if (ConfigReceiveState == false)
				{
					// ProRF receiver
					if (DeviceType == MeasureHardwareType.ProRF)
					{

						try
						{
							// Only update if not suppressed
							if (!SuppressUpdates)
							{
								Char Delimit = Convert.ToChar(9);
								String[] Fields = ReceiveMsg.Split(Delimit);

								// X axis
								if (Fields[2] == "1")
								{
									Axes[(int)AxisNames.XAxis].SetRawPosition(Convert.ToDouble(Fields[0]));

									// Call the event
									WidthUpdate();

								}

								// Y axis
								if ((SystemNumAxes == 2) || (SystemNumAxes == 3) || (SystemNumAxes == 4))
								{
									if (Fields[2] == "2")
									{
										Axes[(int)AxisNames.YAxis].SetRawPosition(Convert.ToDouble(Fields[0]));

										// Call the event
										HeightUpdate();
									}
								}

								// Z axis
								if ((SystemNumAxes == 3) || (SystemNumAxes == 4))
								{
									if (Fields[2] == "3")
									{

										// If using Z...
										if (UseZ)
										{
											Axes[(int)AxisNames.ZAxis].SetRawPosition(Convert.ToDouble(Fields[0]));

											// Call the event
											DepthUpdate();
										}

										// Angular measurement if Z NOT in use
										else if (UseAngle)
										{

											Axes[(int)AxisNames.AngleAxis].SetRawPosition(Convert.ToDouble(Fields[0]));

											// Call the event
											AngleUpdate();

										}
									}
								}

								// Angular measurement if Z in use
								if (SystemNumAxes == 4)
								{
									if (Fields[2] == "4")
									{

										// Using angle measurement
										if (UseAngle)
										{

											Axes[(int)AxisNames.AngleAxis].SetRawPosition(Convert.ToDouble(Fields[0]));

											// Call the event
											AngleUpdate();

										}
									}
								}
							}
						}

						catch
						{
						}

					}

					// ProMUX 3
					else
					{

						// Make sure minimum string length and starts with * (ensure from ProMux)
						if ((ReceiveMsg.Length >= 27) && (ReceiveMsg.Substring(0, 1) == "*"))
						{

							try
							{
								String XStr, XDecimal;

								// Get the sub-strings for each axis from ReceiveMsg
								XStr = ReceiveMsg.Substring(2, 8);

								XDecimal = XStr.Substring(5, 1);

								// If decimal point in correct position, save X
								if (XDecimal == ".")
								{

									// Save X position
									Axes[(int)AxisNames.XAxis].SetRawPosition(Convert.ToDouble(XStr));

									// Call the event
									WidthUpdate();

								}


								if (SystemNumAxes >= 2)
								{
									String YStr, YDecimal;

									YStr = ReceiveMsg.Substring(18, 8);

									YDecimal = YStr.Substring(5, 1);

									// If decimal point in correct position, save Y
									if (YDecimal == ".")
									{
										// Save Y position 
										Axes[(int)AxisNames.YAxis].SetRawPosition(Convert.ToDouble(YStr));

										// Call the event
										HeightUpdate();

									}
								}
							}

							catch
							{


							}
						}


					}
				}

				// In hardware configuration mode...
				else
				{

					// Check if echo.  If not echo, forward received message
					if (ReceiveMsg != TransmitString)
					{

						// ProRF type
						if (DeviceType == MeasureHardwareType.ProRF)
						{
							String FirstChar = ReceiveMsg.Substring(0, 1);
							char[] FirstData = FirstChar.ToCharArray();

							// filter out any measurement data sent by receiver
							if ((FirstChar != " ") && (FirstChar != "-") && (!Char.IsDigit(FirstData[0])))
							{
								// Send the message to the dialog
								ConfigMessage(ReceiveMsg);


							}

						}

						// ProMUX type
						else
						{

							// Send the message to the dialog
							ConfigMessage(ReceiveMsg);

						}

					}

				}

				// Delete last message
				ReceiveMsg = "";

			}

			// Not CR, save character to string
			else
			{
				// If not LF, save data.  Else, discard
				if (Data != 0x0A)
					ReceiveMsg += Convert.ToString(Convert.ToChar(Data));
			}
		}

		public void SerialPortTransmitMessage(String Msg)
		{

			string WriteMsg = Msg + Convert.ToString(Convert.ToChar(0x0d));

			serialPort1.Write(WriteMsg);

		}


		public void ExportAxisData(StreamWriter Stream)
		{

			// Add start header
			Stream.WriteLine(string.Format("{0}", AxisParamSectionStartString));

			for (AxisNames u = AxisNames.XAxis; u < AxisNames.MaxAxes; u++)
			{
				Axes[(int)u].ExportParameters(Stream);
			}

  			// Add end header
			Stream.WriteLine(string.Format("{0}", AxisParamSectionEndString));


		}

		public string GetAxesStartString()
		{
			return AxisParamSectionStartString;

		}

		public bool ImportAxisData(StreamReader Stream, ref int LineNumber)
		{


			// Create an array of strings to pass to the line parser
			string[] Columns;

			Char Delimit = ',';
			bool Loop = true;
			bool Result = true;


			while (Loop)
			{

				string InputLine = "";

				// Read a line from the stream
				InputLine = Stream.ReadLine();

				LineNumber++;

				// If eof, set return status as false (error, eof reached) and break
				if (InputLine == null)
				{
					Loop = false;
					Result = false;
				}

				// Not EOF.  Process imported string
				else
				{
					// Split the line into columns delimited by comma
					Columns = InputLine.Split(Delimit);

					// Check if at end of axes
					if (Columns[0].Equals(AxisParamSectionEndString))
						Loop = false;

					// Save parameter based on Axis ID
					else
					{

						// Identify the particular Axis ID string
						for (AxisNames u = AxisNames.XAxis; u < AxisNames.MaxAxes; u++)
						{
							// If match, import data
							if (Columns[0].Equals(Axes[(int)u].GetExportAxisIDString()))
								Axes[(int)u].ImportParamsValues(Stream);
						}
						
					}

				}

			}

			return Result;

		}


		public void DisableDisplayUpdate(bool State)
		{

			SuppressUpdates = State;


		}





	}
}
