﻿namespace ProCABQC11
{
	partial class CutlistTemplateConfigDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CutlistTemplateConfigDialog));
			this.OkButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.MillimetersSelect = new System.Windows.Forms.RadioButton();
			this.InchSelect = new System.Windows.Forms.RadioButton();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.AngleMinusAccuracyText = new System.Windows.Forms.TextBox();
			this.AnglePlusAccuracyText = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.ThicknessMinusAccuracyText = new System.Windows.Forms.TextBox();
			this.ThicknessPlusAccuracyText = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label44 = new System.Windows.Forms.Label();
			this.HeightMinusAccuracyText = new System.Windows.Forms.TextBox();
			this.WidthMinusAccuracyText = new System.Windows.Forms.TextBox();
			this.label43 = new System.Windows.Forms.Label();
			this.HeightPlusAccuracyText = new System.Windows.Forms.TextBox();
			this.WidthPlusAccuracyText = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox2.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// OkButton
			// 
			this.OkButton.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OkButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OkButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.OkButton.Location = new System.Drawing.Point(355, 765);
			this.OkButton.Name = "OkButton";
			this.OkButton.Size = new System.Drawing.Size(75, 48);
			this.OkButton.TabIndex = 0;
			this.OkButton.UseVisualStyleBackColor = true;
			this.OkButton.Click += new System.EventHandler(this.button1_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(34, 793);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(192, 20);
			this.label1.TabIndex = 1;
			this.label1.Text = "* Indicates a required field";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.MillimetersSelect);
			this.groupBox2.Controls.Add(this.InchSelect);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(14, 728);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(187, 50);
			this.groupBox2.TabIndex = 20;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "CSV Measurement Data";
			// 
			// MillimetersSelect
			// 
			this.MillimetersSelect.AutoSize = true;
			this.MillimetersSelect.Location = new System.Drawing.Point(84, 21);
			this.MillimetersSelect.Name = "MillimetersSelect";
			this.MillimetersSelect.Size = new System.Drawing.Size(97, 22);
			this.MillimetersSelect.TabIndex = 1;
			this.MillimetersSelect.TabStop = true;
			this.MillimetersSelect.Text = "Millimeters";
			this.MillimetersSelect.UseVisualStyleBackColor = true;
			// 
			// InchSelect
			// 
			this.InchSelect.AutoSize = true;
			this.InchSelect.Location = new System.Drawing.Point(8, 21);
			this.InchSelect.Name = "InchSelect";
			this.InchSelect.Size = new System.Drawing.Size(69, 22);
			this.InchSelect.TabIndex = 0;
			this.InchSelect.TabStop = true;
			this.InchSelect.Text = "Inches";
			this.InchSelect.UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.AngleMinusAccuracyText);
			this.groupBox5.Controls.Add(this.AnglePlusAccuracyText);
			this.groupBox5.Controls.Add(this.label3);
			this.groupBox5.Controls.Add(this.ThicknessMinusAccuracyText);
			this.groupBox5.Controls.Add(this.ThicknessPlusAccuracyText);
			this.groupBox5.Controls.Add(this.label2);
			this.groupBox5.Controls.Add(this.label44);
			this.groupBox5.Controls.Add(this.HeightMinusAccuracyText);
			this.groupBox5.Controls.Add(this.WidthMinusAccuracyText);
			this.groupBox5.Controls.Add(this.label43);
			this.groupBox5.Controls.Add(this.HeightPlusAccuracyText);
			this.groupBox5.Controls.Add(this.WidthPlusAccuracyText);
			this.groupBox5.Controls.Add(this.label12);
			this.groupBox5.Controls.Add(this.label11);
			this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox5.Location = new System.Drawing.Point(535, 380);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(237, 206);
			this.groupBox5.TabIndex = 19;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Allowable Measuring Tolerance";
			// 
			// AngleMinusAccuracyText
			// 
			this.AngleMinusAccuracyText.Location = new System.Drawing.Point(155, 137);
			this.AngleMinusAccuracyText.Name = "AngleMinusAccuracyText";
			this.AngleMinusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.AngleMinusAccuracyText.TabIndex = 13;
			// 
			// AnglePlusAccuracyText
			// 
			this.AnglePlusAccuracyText.Location = new System.Drawing.Point(66, 137);
			this.AnglePlusAccuracyText.Name = "AnglePlusAccuracyText";
			this.AnglePlusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.AnglePlusAccuracyText.TabIndex = 12;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(13, 139);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 18);
			this.label3.TabIndex = 11;
			this.label3.Text = "Angle";
			// 
			// ThicknessMinusAccuracyText
			// 
			this.ThicknessMinusAccuracyText.Location = new System.Drawing.Point(155, 107);
			this.ThicknessMinusAccuracyText.Name = "ThicknessMinusAccuracyText";
			this.ThicknessMinusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.ThicknessMinusAccuracyText.TabIndex = 10;
			// 
			// ThicknessPlusAccuracyText
			// 
			this.ThicknessPlusAccuracyText.Location = new System.Drawing.Point(66, 107);
			this.ThicknessPlusAccuracyText.Name = "ThicknessPlusAccuracyText";
			this.ThicknessPlusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.ThicknessPlusAccuracyText.TabIndex = 9;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 109);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(47, 18);
			this.label2.TabIndex = 8;
			this.label2.Text = "Depth";
			// 
			// label44
			// 
			this.label44.AutoSize = true;
			this.label44.Location = new System.Drawing.Point(165, 24);
			this.label44.Name = "label44";
			this.label44.Size = new System.Drawing.Size(48, 18);
			this.label44.TabIndex = 7;
			this.label44.Text = "Minus";
			// 
			// HeightMinusAccuracyText
			// 
			this.HeightMinusAccuracyText.Location = new System.Drawing.Point(155, 77);
			this.HeightMinusAccuracyText.Name = "HeightMinusAccuracyText";
			this.HeightMinusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.HeightMinusAccuracyText.TabIndex = 6;
			// 
			// WidthMinusAccuracyText
			// 
			this.WidthMinusAccuracyText.Location = new System.Drawing.Point(155, 48);
			this.WidthMinusAccuracyText.Name = "WidthMinusAccuracyText";
			this.WidthMinusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.WidthMinusAccuracyText.TabIndex = 5;
			// 
			// label43
			// 
			this.label43.AutoSize = true;
			this.label43.Location = new System.Drawing.Point(82, 24);
			this.label43.Name = "label43";
			this.label43.Size = new System.Drawing.Size(37, 18);
			this.label43.TabIndex = 4;
			this.label43.Text = "Plus";
			// 
			// HeightPlusAccuracyText
			// 
			this.HeightPlusAccuracyText.Location = new System.Drawing.Point(66, 77);
			this.HeightPlusAccuracyText.Name = "HeightPlusAccuracyText";
			this.HeightPlusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.HeightPlusAccuracyText.TabIndex = 3;
			// 
			// WidthPlusAccuracyText
			// 
			this.WidthPlusAccuracyText.Location = new System.Drawing.Point(66, 48);
			this.WidthPlusAccuracyText.Name = "WidthPlusAccuracyText";
			this.WidthPlusAccuracyText.Size = new System.Drawing.Size(63, 24);
			this.WidthPlusAccuracyText.TabIndex = 2;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(13, 79);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(50, 18);
			this.label12.TabIndex = 1;
			this.label12.Text = "Height";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(13, 51);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(46, 18);
			this.label11.TabIndex = 0;
			this.label11.Text = "Width";
			// 
			// CutlistTemplateConfigDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(785, 848);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.OkButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CutlistTemplateConfigDialog";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Multi Order Template Configuration";
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button OkButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton MillimetersSelect;
		private System.Windows.Forms.RadioButton InchSelect;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.TextBox HeightMinusAccuracyText;
		private System.Windows.Forms.TextBox WidthMinusAccuracyText;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.TextBox HeightPlusAccuracyText;
		private System.Windows.Forms.TextBox WidthPlusAccuracyText;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox ThicknessMinusAccuracyText;
		private System.Windows.Forms.TextBox ThicknessPlusAccuracyText;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox AngleMinusAccuracyText;
		private System.Windows.Forms.TextBox AnglePlusAccuracyText;
		private System.Windows.Forms.Label label3;
	}
}