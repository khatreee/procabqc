﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class TimedDialog : Form
	{

		private const int DefaultTime = 2500;
		private Bitmap MyImage;



		public TimedDialog()
		{
			InitializeComponent();


		}




		public TimedDialog(string DialogMessage, string DialogTitle, int UserTime)
		{

			InitDialog(DialogMessage, DialogTitle);

			// Set user defined time in ms
			timer1.Interval = UserTime;

			// Start timer
			timer1.Start();

		}

		public TimedDialog(string DialogMessage, string DialogTitle)
		{


			InitDialog(DialogMessage, DialogTitle);

			// Use default time
			timer1.Interval = DefaultTime;

			// Start timer
			timer1.Start();

		}

		public TimedDialog(string ImageFileName)
		{

			InitializeComponent();

			this.FormBorderStyle = FormBorderStyle.None;

			MyImage = new Bitmap(ImageFileName);

			pictureBox1.Image = (Image)MyImage;

			pictureBox1.Visible = true;

			// Use default time
			timer1.Interval = DefaultTime;

			// Start timer
			timer1.Start();

		}



		private void InitDialog(string DialogMessage, string DialogTitle)
		{
			InitializeComponent();


			// Set message
			DialogInfoText.Text = DialogMessage;

			// Set dialog title
			Text = DialogTitle;

			Size TextSize = DialogInfoText.Size;

			Size DialogSize = this.Size;

			int Width = (DialogSize.Width - TextSize.Width) / 2;
			int Height = ((DialogSize.Height - 35) - TextSize.Height) / 2;

			// Set the position of the message
			DialogInfoText.Location = new Point(Width, Height);


			this.StartPosition = FormStartPosition.CenterParent;


		}



		private void timer1_Tick(object sender, EventArgs e)
		{

		

			

			// Close the dialog automatically
			this.Close();

			this.Update();


		}




		


	}
}
