﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace ProCABQC11
{






	public class TemplateColumn
	{

		protected int Column;

				
		public string GetStringValue
		{
			get 
			{
				Byte[] CharArray;
				string Result;

				// If column not defined, return empty string
				if (Column == -1)
				{
					Result = "";

				}

				// Defined
				else
				{
					// Single letters Column values 0 - 25 = A to Z
					if (Column < 26)
					{
						CharArray = new Byte[1];
						CharArray[0] = (Byte)((Column % 26) + 'A');
						Result = ASCIIEncoding.ASCII.GetString(CharArray);
					}

					// Double letters (Column values 26 - 701 = AA to ZZ)
					else if (Column < 676 + 26)
					{
						CharArray = new Byte[2];
						int FirstChar = (Convert.ToInt32(Column / 26)) - 1;
						CharArray[0] = (Byte)(FirstChar + 'A');
						CharArray[1] = (Byte)((Column % 26) + 'A');
						Result = ASCIIEncoding.ASCII.GetString(CharArray);

					}

					// failsafe.  A column number larger than 701.  This should never happen
					else
					{
						Result = "";
						Column = -1;
					}


					
				}


				return Result;


	
			}
			
		}

		// A set function for Column.  Returns a result, thus standard set function not applicable
		public bool SetStringValue(string Str, string Name, out string ErrorMsg)
		{

			// Complete a series of test to filter out bad data

			// Test string length, 1 or 2 characters only
			if (Str.Length > 2)
			{
				ErrorMsg = string.Format("Error:  {0} - Input Too Long.  Must be A to ZZ", Name);
				return false;
			}

			// Empty string.  Set to undefined
			else if (Str.Length == 0)
			{
				Column = -1;

				ErrorMsg = "";

				return true;
			}

			else
			{

				// Check that characters are letters
				for (int u = 0; u < Str.Length; u++)
				{
					if (!Char.IsLetter(Str[u]))
					{
						ErrorMsg = string.Format("Error:  {0} - Invalid characters in input", Name);
						return false;
					}

				}

				// All characters are letters.  Convert to upper
				string VerifiedStr = Str.ToUpper();


				// Calculate new Column value
				byte[] ByteValues = ASCIIEncoding.ASCII.GetBytes(VerifiedStr);

				if (VerifiedStr.Length == 1)
					Column = ByteValues[0] - 65;

				else
					Column = ((((ByteValues[0] - 65) + 1) * 26) + (ByteValues[1] - 65));

				ErrorMsg = "";

				return true;

			}

		}


		public int GetColumnNumber
		{
 			get { return Column; }
		}


		// The default constructor
		public TemplateColumn()
		{

		}
  
	
		// Overridden constructor
		public TemplateColumn(int ColumnReference)
		{

			Column = ColumnReference;


		}








		 
	}
}
