﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace ProCABQC11
{


	[Serializable]
	public class SystemOrders : IEnumerable<Order>
	{
		
		private const string NoOrderFoundErrMsg = "No Order found with specified Order number";
		private const string FileName = "SystemOrders.dat";
		

		private List<Order> Orders = new List<Order>();


		IEnumerator<Order> IEnumerable<Order>.GetEnumerator()
		{

			return Orders.GetEnumerator();
	

		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return Orders.GetEnumerator();

		}


		
		public SystemOrders()
		{

			
		}

		protected int LocateOrderByID(string OrderID)
		{

			// Check through list to see if an order exists with the orderID
			for (int u = 0; u < Orders.Count; u++)
			{
				// If duplicate found, return index.
				if (Orders[u].OrderNumber == OrderID)
					return u;

			}

			// if reached, no matching ID found
			return -1;
		}



		public bool AddOrder(ref Order NewOrder)
		{

			int Index;

			// Check through list to see if there are duplicate OrderIDs
			if ((Index = LocateOrderByID(NewOrder.OrderNumber)) != -1)
				return false;

			else
			{
				// No duplicates found.  Add new order to list
				Orders.Add(NewOrder);

				// Sort List
				Orders.Sort();

				return true;
			}


		}

		public int NumberOfOrders()
		{

			return Orders.Count;

		}


		public bool DoesOrderExists(string OrderID)
		{

			if (LocateOrderByID(OrderID) == -1)
				return false;

			else
				return true;
		}


		public bool AddLineItemToOrder(string OrderID, ref LineItem NewLineItem, out string ErrorMessage)
		{

			// Locate the matching order, if it exists
			int Index = LocateOrderByID(OrderID);

			// No orders exists with the specified Order ID
			if (Index == -1)
			{
				ErrorMessage = NoOrderFoundErrMsg;
				return false;
			}

			// Order exists, try to add LineItem
			else
			{
				// If returns false, the line item was NOT added because of a duplicate line item number
				if (!Orders[Index].AddLineItem(ref NewLineItem))
				{
					ErrorMessage = string.Format("Order ID: {0}, Duplicate line item number - {1}", OrderID, NewLineItem.LineNumber);
					return false;
				}

				// Success.  No error message and return true
				else
				{
					ErrorMessage = "";
					return true;
				}

			}

		}

		// Gets an order if it exists.  If not, returns null.
		public Order GetExistingOrder(string OrderID)
		{

			// Check if order exists
			int Index;

			// No matching order exists.  return null.
			if ((Index = LocateOrderByID(OrderID)) == -1)
				return (Order)null;

			// order exists.  Return the order
			else
				return Orders[Index];
		}


		public bool GetExistingOrder(out Order OrderItem, int Index)
		{
			if (Index < Orders.Count)
			{
				OrderItem = Orders[Index];
				return true;
			}

			else
			{
				OrderItem = (Order)null;
				return false;
			}


		}

		// Gets a list of OrderIDs from the list of orders.
		public void GetOrdersIDList(ref List<string> OrderIDs)
		{

			for (int u = 0; u < Orders.Count; u++)
			{

				OrderIDs.Add(Orders[u].OrderNumber);

			}
		}


		public void RemoveOrder(Order OrderObj)
		{
			Orders.Remove(OrderObj);

		}


		public void RemoveAllOrders()
		{

			int Count = Orders.Count;

			Orders.RemoveRange(0, Count);

		}


		// Reverse the sorted order
		public void ReverseList()
		{

			Orders.Reverse();

		}


		// Serialization

		// Save parameters to disk in local directory
		public void SaveParameters()
		{

			BinaryFormatter Formatter = new BinaryFormatter();

			using (Stream fstream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				Formatter.Serialize(fstream, this);
			}

		}

		// Attempt to read parameter data file
		// Reads data if file exists and returns true
		// Returns false if no data file exists
		public bool ReadParameters(ref SystemOrders OrderList)
		{

			// Check if data file exists.  If so, open it and return true.  If not, false
			if (File.Exists(FileName))
			{

				try
				{
					BinaryFormatter Formatter = new BinaryFormatter();

					using (Stream fstream = File.OpenRead(FileName))
					{
						OrderList = (SystemOrders)Formatter.Deserialize(fstream);
					}

					return true;
				}

				catch (Exception e)
				{

					MessageBox.Show(string.Format("Error opening System Orders history file.\nError: {0}", e.Message), "System Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					return false;

				}

			}

			else
				return false;

		}

	}
}
