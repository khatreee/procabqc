﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace ProCABQC11
{

	// The enumeration that identifies the LineItem Template columns
	public enum SingleOrderLineItemColumnIDV1100
	{
		LineItemNumber,
		PartID,
		QuantityRequired,
		NominalWidth,
		NominalHeight,
		NominalThickness,
		Material,
		Style,

		Hinging,
		Finish,
		Type,
		Location,
		Machining,
		Assembly,
		UserMessage,


		Comment,
		UserDef1,
		UserDef2,
		UserDef3,

		MaxItems
	}
  



	public class SingleOrderLineItemTemplate : MasterLineItemTemplate
	{


		private const string TemplateRegistryKeyStr = "Single Order Line Item";
		


		private string[] ColumnNames = {		"Line Item Number",
												"Part ID*",
												"Quantity Required*",
												"Nominal Width*",
												"Nominal Height*",
												"Nominal Depth",
												"Material",
												"Style",

												"Hinging",
												"Finish",
												"Type",
												"Location",
												"Machining",
												"Assembly",
												"User Message",

												"Comment", 
												"User Defined 1",
												"User Defined 2",
												"User Defined 3"
										};


		private const string OutOfBoundsColumnErrorMsg = "Template defines a column that is outside the column range of the CSV file";





		public bool GetLineItemColumnTemplate(SingleOrderLineItemColumnIDV1100 Index, out TemplateLineItemColumn LineItemColumnTemplate)
		{

			if ((Index < SingleOrderLineItemColumnIDV1100.MaxItems) && (Index >= SingleOrderLineItemColumnIDV1100.LineItemNumber))
			{
				LineItemColumnTemplate = LineItemTemplateItems[(int)Index];
				return true;
			}

			else
			{
				LineItemColumnTemplate = (TemplateLineItemColumn)null;
				return false;
			}
		}



		// The default constructor
		public SingleOrderLineItemTemplate()
		{

		}

		// The overridden constructor
		public SingleOrderLineItemTemplate(ref PersistentParametersV1100 PParam, int TemplateIndex)
		{

			// Get the template data
			SingleOrderTemplateDataV1100 TemplateData;

			if (PParam.GetSOTemplateData(TemplateIndex, out TemplateData))
			{

				// Create the array of Template Line Item columns for a Single order template
				LineItemTemplateItems = new TemplateLineItemColumn[(int)SingleOrderLineItemColumnIDV1100.MaxItems];

				// Create each LineItemTemplateItem
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.LineItemNumber] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.LineItemNumber), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.LineItemNumber], ParameterDataType.Integer, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.PartID] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.PartID), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.PartID], ParameterDataType.String, true, true);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.QuantityRequired] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.QuantityRequired), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.QuantityRequired], ParameterDataType.Integer, true, true);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.NominalWidth] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.NominalWidth), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.NominalWidth], ParameterDataType.Float, true, true);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.NominalHeight] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.NominalHeight), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.NominalHeight], ParameterDataType.Float, true, true);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.NominalThickness] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.NominalThickness), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.NominalThickness], ParameterDataType.Float, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Material] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Material), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Material], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Style] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Style), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Style], ParameterDataType.String, true, false);

				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Hinging] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Hinging), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Hinging], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Finish] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Finish), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Finish], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Type] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Type), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Type], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Location] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Location), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Location], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Machining] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Machining), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Machining], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Assembly] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Assembly), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Assembly], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.UserMessage] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.UserMessage), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.UserMessage], ParameterDataType.String, true, false);

				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.Comment] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.Comment), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.Comment], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.UserDef1] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.UserDef1), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.UserDef1], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.UserDef2] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.UserDef2), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.UserDef2], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)SingleOrderLineItemColumnIDV1100.UserDef3] = new TemplateLineItemColumn(TemplateData.GetColumnSetting(SingleOrderLineItemColumnIDV1100.UserDef3), ColumnNames[(int)SingleOrderLineItemColumnIDV1100.UserDef3], ParameterDataType.String, true, false);


				// The line item start position
				LineItemStartPos = TemplateData.LineItemStart;

			}


		}




		public TemplateParsingResultCodes ParseSingleOrderLineItem(int CSVLineNumber, string CSVLineString, out LineItem LI, ref List<string> ErrorMsgLst)
		{

			// Create an array of strings to pass to the line parser
			string[] Columns;

			// Parse CSV line into columns of strings.  Returns true if one or more lines parsed.  False if all empty fields
			bool Result = ParseLineToColumns(CSVLineString, out Columns);

			// If true, one or more non-blank fields exist.
			if (Result)
			{
				// Instantiate the LineItem
				LI = new LineItem();

				// Cycle through all the template items
				for (int u = 0; u < LineItemTemplateItems.Length; u++)
				{
					// Check if a LineItemTemplate is defined.  If so, continue
					if (LineItemTemplateItems[u].IsDefined)
					{
						// Get the zero based column data stored in the template
						int ActiveColumn = LineItemTemplateItems[u].GetColumn;

						// Make sure the stored column is within range of the CSV file columns passed
						if (ActiveColumn < Columns.Length)
						{
							// Create a new data structure for the parameter parsing
							ParamStruct Data = new ParamStruct();
							string ErrorMsg = "";

							// Attempt to parse the cooresponding string into the correct data type based on the template
							DataConversionResult ConvertResult = LineItemTemplateItems[u].GetParameterValue(Columns[ActiveColumn], ref Data, ref ErrorMsg);

							// If an error occured, add the line number to the error message and add to the error list
							if (ConvertResult == DataConversionResult.Error)
							{
								string Msg = string.Format("Error - Line: {0}, {1}", CSVLineNumber + 1, ErrorMsg);
								ErrorMsgLst.Add(Msg);
							}

							// The conversion succeded.  Store the data to the line item
							else
							{
								// Conversion successful.  Update appropriate LineItem field
								UpdateLineItem(ref LI, ref Data, (SingleOrderLineItemColumnIDV1100)u);

							}
						}

						// The stored column in the template is outside the number of columns in the CSV file
						else
						{
							string Msg = string.Format("Error - Line: {0}, {1} at column {2}: {3}", CSVLineNumber + 1, LineItemTemplateItems[u].FieldName, LineItemTemplateItems[u].GetColumnString, OutOfBoundsColumnErrorMsg);
							ErrorMsgLst.Add(Msg);
						}
					}
				}

				// All template items itterated.  Determine the appropriate result code
				if (ErrorMsgLst.Count > 0)
				{
					// Set line item to null and indicate error
					LI = (LineItem)null;
					return TemplateParsingResultCodes.Error;
				}
					
				// Success!
				else
					return TemplateParsingResultCodes.Success;

			}

			// Blank line
			else
			{
				// Indicate blank line
				LI = (LineItem)null;
				return TemplateParsingResultCodes.BlankLine;
			}
		}


		private void UpdateLineItem(ref LineItem LI, ref ParamStruct Data, SingleOrderLineItemColumnIDV1100 ID)
		{
			switch (ID)
			{
				case SingleOrderLineItemColumnIDV1100.LineItemNumber :

					LI.LineNumber = Data.IntVal;
					break;
					
				case SingleOrderLineItemColumnIDV1100.PartID:

					LI.PartIdStr = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.QuantityRequired:

					LI.QtyRequired = Data.IntVal;
					break;

				case SingleOrderLineItemColumnIDV1100.NominalWidth:

					LI.Width = Data.FPVal;
					break;

				case SingleOrderLineItemColumnIDV1100.NominalHeight:

					LI.Height = Data.FPVal;
					break;

				case SingleOrderLineItemColumnIDV1100.NominalThickness:

					LI.Thickness = Data.FPVal;
					break;

				case SingleOrderLineItemColumnIDV1100.Style:

					LI.StyleText = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.Material:

					LI.MaterialText = Data.StringVal;
					break;



				case SingleOrderLineItemColumnIDV1100.Hinging:

					LI.HingingText = Data.StringVal;
					break;


				case SingleOrderLineItemColumnIDV1100.Finish:

					LI.FinishText = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.Type:

					LI.TypeText = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.Location:

					LI.LocationText = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.Machining:

					LI.MachiningText = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.Assembly:

					LI.AssemblyText = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.UserMessage:

					LI.UserMessageText = Data.StringVal;
					break;




				case SingleOrderLineItemColumnIDV1100.Comment:

					LI.CommentText = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.UserDef1:

					LI.UserDef1Text = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.UserDef2:

					LI.UserDef2Text = Data.StringVal;
					break;

				case SingleOrderLineItemColumnIDV1100.UserDef3:

					LI.UserDef3Text = Data.StringVal;
					break;

			}
		}

        // FUNCTIONS FOR IMPORTING AND EXPORTING TEMPLATE DATA BELOW....

        // Gets the Line Item template column field name text
        public bool GetLineItemFieldText(out string FieldName, int Index)
        {

            bool Result = false;

            if (Index < (int)SingleOrderLineItemColumnIDV1100.MaxItems)
            {
                Result = true;
                FieldName = ColumnNames[Index];
            }

            else
            {
                Result = false;
                FieldName = "";
            }

            return Result;
        }


        // Searches field names for a match with passed name.  If match found, return true and index to field
        public bool LocateLineItemFieldNameIndex(string TargetName, out  SingleOrderLineItemColumnIDV1100 Index)
        {

            bool Result = false;
            Index = SingleOrderLineItemColumnIDV1100.MaxItems;

            // Check each column name for comparison
			for (SingleOrderLineItemColumnIDV1100 u = 0; u < SingleOrderLineItemColumnIDV1100.MaxItems; u++)
            {
                // If text matches, save index and break
                if (ColumnNames[(int)u].Equals(TargetName))
                {
                    Result = true;
                    Index = u;
                    break;
                }
            }

            return Result;
        }

	}
}
