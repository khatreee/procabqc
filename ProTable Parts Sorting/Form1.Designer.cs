﻿namespace ProCABQC11
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			Ekc.Comp.InstantLocalKey instantLocalKey3 = new Ekc.Comp.InstantLocalKey();
			Ekc.Comp.KeyValue keyValue5 = new Ekc.Comp.KeyValue();
			Ekc.Comp.InstantNetworkKey instantNetworkKey3 = new Ekc.Comp.InstantNetworkKey();
			Ekc.Comp.KeyValue keyValue6 = new Ekc.Comp.KeyValue();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.ToolStrip = new System.Windows.Forms.ToolStrip();
			this.ImportOrderFileToolButton = new System.Windows.Forms.ToolStripButton();
			this.ManualMeasurementToolButton = new System.Windows.Forms.ToolStripButton();
			this.LineItemDetailToolButton = new System.Windows.Forms.ToolStripButton();
			this.DeleteMeasurementToolButton = new System.Windows.Forms.ToolStripButton();
			this.IncompleteOrderToolButton = new System.Windows.Forms.ToolStripButton();
			this.ProcessAsCompleteToolButton = new System.Windows.Forms.ToolStripButton();
			this.OutOfSquareToolButton = new System.Windows.Forms.ToolStripButton();
			this.FinishDefectToolButton = new System.Windows.Forms.ToolStripButton();
			this.OtherDefectLabelToolButton = new System.Windows.Forms.ToolStripButton();
			this.DatumWidthToolButton = new System.Windows.Forms.ToolStripButton();
			this.DatumVerticalToolButton = new System.Windows.Forms.ToolStripButton();
			this.DatumDepthToolButton = new System.Windows.Forms.ToolStripButton();
			this.ExitToolButton = new System.Windows.Forms.ToolStripButton();
			this.DatumAngleToolButton = new System.Windows.Forms.ToolStripButton();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.label6 = new System.Windows.Forms.Label();
			this.LineItemList = new System.Windows.Forms.ListView();
			this.LineItemHeader = new System.Windows.Forms.ColumnHeader();
			this.PartIDHeader = new System.Windows.Forms.ColumnHeader();
			this.widthHeader = new System.Windows.Forms.ColumnHeader();
			this.heightHeader = new System.Windows.Forms.ColumnHeader();
			this.depthHeader = new System.Windows.Forms.ColumnHeader();
			this.QtyReqHeader = new System.Windows.Forms.ColumnHeader();
			this.QtyMeasuredHeader = new System.Windows.Forms.ColumnHeader();
			this.TypeHeader = new System.Windows.Forms.ColumnHeader();
			this.MaterialHeader = new System.Windows.Forms.ColumnHeader();
			this.FinishHeader = new System.Windows.Forms.ColumnHeader();
			this.HingingHeader = new System.Windows.Forms.ColumnHeader();
			this.MachiningHeader = new System.Windows.Forms.ColumnHeader();
			this.AssemblyHeader = new System.Windows.Forms.ColumnHeader();
			this.CommentsHeader = new System.Windows.Forms.ColumnHeader();
			this.User1Header = new System.Windows.Forms.ColumnHeader();
			this.User2Header = new System.Windows.Forms.ColumnHeader();
			this.User3Header = new System.Windows.Forms.ColumnHeader();
			this.LocationHeader = new System.Windows.Forms.ColumnHeader();
			this.StyleHeader = new System.Windows.Forms.ColumnHeader();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.label3 = new System.Windows.Forms.Label();
			this.BillEmail = new System.Windows.Forms.TextBox();
			this.BillTelephone = new System.Windows.Forms.TextBox();
			this.BillContact = new System.Windows.Forms.TextBox();
			this.BillCountry = new System.Windows.Forms.TextBox();
			this.BillZip = new System.Windows.Forms.TextBox();
			this.BillState = new System.Windows.Forms.TextBox();
			this.BillCity = new System.Windows.Forms.TextBox();
			this.BillAddr2 = new System.Windows.Forms.TextBox();
			this.BillAddr1 = new System.Windows.Forms.TextBox();
			this.BillCustomer = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.label4 = new System.Windows.Forms.Label();
			this.ShipEmail = new System.Windows.Forms.TextBox();
			this.ShipTelephone = new System.Windows.Forms.TextBox();
			this.ShipContact = new System.Windows.Forms.TextBox();
			this.ShipCountry = new System.Windows.Forms.TextBox();
			this.ShipZip = new System.Windows.Forms.TextBox();
			this.ShipState = new System.Windows.Forms.TextBox();
			this.ShipCity = new System.Windows.Forms.TextBox();
			this.ShipAddr2 = new System.Windows.Forms.TextBox();
			this.ShipAddr1 = new System.Windows.Forms.TextBox();
			this.ShipCustomer = new System.Windows.Forms.TextBox();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this.label33 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.label35 = new System.Windows.Forms.Label();
			this.label36 = new System.Windows.Forms.Label();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openOrderFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openBatchOrderFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteCurrentOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteAllOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.incompleteOrderReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.processOrderAsCompleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.partErrorLabelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.outOfSquareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.finishDefectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.axesDatumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.setWidthDatumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.setHeightDatumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.setDepthDatumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.logInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.simulateMeasurementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.SortButton = new System.Windows.Forms.Button();
			this.JobsInstructionsLabel = new System.Windows.Forms.Label();
			this.JobsTitleLabel = new System.Windows.Forms.Label();
			this.OrderListing = new System.Windows.Forms.ListBox();
			this.PositionGroupBox = new System.Windows.Forms.GroupBox();
			this.AnglePosition = new System.Windows.Forms.Label();
			this.DepthPosition = new System.Windows.Forms.Label();
			this.AngleLabel = new System.Windows.Forms.Label();
			this.DepthLabel = new System.Windows.Forms.Label();
			this.WidthLabel = new System.Windows.Forms.Label();
			this.HeightLabel = new System.Windows.Forms.Label();
			this.HeightPosition = new System.Windows.Forms.Label();
			this.WidthPosition = new System.Windows.Forms.Label();
			this.UnitsGroupBox = new System.Windows.Forms.GroupBox();
			this.MillimeterSelect = new System.Windows.Forms.RadioButton();
			this.InchSelect = new System.Windows.Forms.RadioButton();
			this.MeasureButton = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.OrderUsesDepthLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.SystemOrderStateLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.LineItemContextOptions = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.EditLineItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ViewFullLineItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ViewMeasuredParts = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteLastLineItemMeasurement = new System.Windows.Forms.ToolStripMenuItem();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.textBox11 = new System.Windows.Forms.TextBox();
			this.textBox12 = new System.Windows.Forms.TextBox();
			this.textBox13 = new System.Windows.Forms.TextBox();
			this.textBox14 = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.OrdersContextOptions = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveCompletedOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.incompleteOrderReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.processOrderAsCompletePopupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpProvider1 = new System.Windows.Forms.HelpProvider();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.StatusBarTimer = new System.Windows.Forms.Timer(this.components);
			this.OptionLauncherTimer = new System.Windows.Forms.Timer(this.components);
			//this.instantProtection1 = new Ekc.Comp.InstantProtection(this.components);
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.ToolStrip.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.PositionGroupBox.SuspendLayout();
			this.UnitsGroupBox.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.LineItemContextOptions.SuspendLayout();
			this.OrdersContextOptions.SuspendLayout();
			//((System.ComponentModel.ISupportInitialize)(this.instantProtection1)).BeginInit();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.IsSplitterFixed = true;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ControlLight;
			this.splitContainer1.Panel2.Controls.Add(this.PositionGroupBox);
			this.splitContainer1.Panel2.Controls.Add(this.UnitsGroupBox);
			this.splitContainer1.Panel2.Controls.Add(this.MeasureButton);
			this.splitContainer1.Panel2.Controls.Add(this.statusStrip1);
			this.splitContainer1.Size = new System.Drawing.Size(1008, 730);
			this.splitContainer1.SplitterDistance = 498;
			this.splitContainer1.TabIndex = 0;
			// 
			// splitContainer2
			// 
			this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.IsSplitterFixed = true;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Margin = new System.Windows.Forms.Padding(0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.ControlLight;
			this.splitContainer2.Panel1.Controls.Add(this.ToolStrip);
			this.splitContainer2.Panel1.Controls.Add(this.tabControl1);
			this.splitContainer2.Panel1.Controls.Add(this.menuStrip1);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.ControlLight;
			this.splitContainer2.Panel2.Controls.Add(this.SortButton);
			this.splitContainer2.Panel2.Controls.Add(this.JobsInstructionsLabel);
			this.splitContainer2.Panel2.Controls.Add(this.JobsTitleLabel);
			this.splitContainer2.Panel2.Controls.Add(this.OrderListing);
			this.splitContainer2.Size = new System.Drawing.Size(1008, 498);
			this.splitContainer2.SplitterDistance = 811;
			this.splitContainer2.TabIndex = 0;
			// 
			// ToolStrip
			// 
			this.ToolStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.ToolStrip.AutoSize = false;
			this.ToolStrip.Dock = System.Windows.Forms.DockStyle.None;
			this.ToolStrip.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.ToolStrip.ImageScalingSize = new System.Drawing.Size(35, 35);
			this.ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportOrderFileToolButton,
            this.ManualMeasurementToolButton,
            this.LineItemDetailToolButton,
            this.DeleteMeasurementToolButton,
            this.IncompleteOrderToolButton,
            this.ProcessAsCompleteToolButton,
            this.OutOfSquareToolButton,
            this.FinishDefectToolButton,
            this.OtherDefectLabelToolButton,
            this.DatumWidthToolButton,
            this.DatumVerticalToolButton,
            this.DatumDepthToolButton,
            this.ExitToolButton,
            this.DatumAngleToolButton});
			this.ToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
			this.ToolStrip.Location = new System.Drawing.Point(0, 28);
			this.ToolStrip.Name = "ToolStrip";
			this.ToolStrip.Size = new System.Drawing.Size(807, 110);
			this.ToolStrip.TabIndex = 2;
			this.ToolStrip.Text = "toolStrip1";
			// 
			// ImportOrderFileToolButton
			// 
			this.ImportOrderFileToolButton.Image = ((System.Drawing.Image)(resources.GetObject("ImportOrderFileToolButton.Image")));
			this.ImportOrderFileToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ImportOrderFileToolButton.Name = "ImportOrderFileToolButton";
			this.ImportOrderFileToolButton.Size = new System.Drawing.Size(99, 52);
			this.ImportOrderFileToolButton.Text = "Import Order File";
			this.ImportOrderFileToolButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.ImportOrderFileToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ImportOrderFileToolButton.ToolTipText = "Import Order File";
			this.ImportOrderFileToolButton.Click += new System.EventHandler(this.openOrderFileToolStripMenuItem_Click);
			// 
			// ManualMeasurementToolButton
			// 
			this.ManualMeasurementToolButton.Image = ((System.Drawing.Image)(resources.GetObject("ManualMeasurementToolButton.Image")));
			this.ManualMeasurementToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ManualMeasurementToolButton.Name = "ManualMeasurementToolButton";
			this.ManualMeasurementToolButton.Size = new System.Drawing.Size(97, 52);
			this.ManualMeasurementToolButton.Text = "Manual Measure";
			this.ManualMeasurementToolButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.ManualMeasurementToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ManualMeasurementToolButton.Click += new System.EventHandler(this.simulateMeasurementToolStripMenuItem_Click);
			// 
			// LineItemDetailToolButton
			// 
			this.LineItemDetailToolButton.Image = ((System.Drawing.Image)(resources.GetObject("LineItemDetailToolButton.Image")));
			this.LineItemDetailToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.LineItemDetailToolButton.Name = "LineItemDetailToolButton";
			this.LineItemDetailToolButton.Size = new System.Drawing.Size(81, 52);
			this.LineItemDetailToolButton.Text = "Line Item Info";
			this.LineItemDetailToolButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.LineItemDetailToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.LineItemDetailToolButton.Click += new System.EventHandler(this.LineItemDetailToolButton_Click);
			// 
			// DeleteMeasurementToolButton
			// 
			this.DeleteMeasurementToolButton.Image = ((System.Drawing.Image)(resources.GetObject("DeleteMeasurementToolButton.Image")));
			this.DeleteMeasurementToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DeleteMeasurementToolButton.Name = "DeleteMeasurementToolButton";
			this.DeleteMeasurementToolButton.Size = new System.Drawing.Size(117, 52);
			this.DeleteMeasurementToolButton.Text = "Delete Measurement";
			this.DeleteMeasurementToolButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.DeleteMeasurementToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DeleteMeasurementToolButton.Click += new System.EventHandler(this.DeleteMeasurementToolButton_Click);
			// 
			// IncompleteOrderToolButton
			// 
			this.IncompleteOrderToolButton.Image = ((System.Drawing.Image)(resources.GetObject("IncompleteOrderToolButton.Image")));
			this.IncompleteOrderToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.IncompleteOrderToolButton.Name = "IncompleteOrderToolButton";
			this.IncompleteOrderToolButton.Size = new System.Drawing.Size(106, 52);
			this.IncompleteOrderToolButton.Text = "Incomplete Report";
			this.IncompleteOrderToolButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.IncompleteOrderToolButton.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
			this.IncompleteOrderToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.IncompleteOrderToolButton.Click += new System.EventHandler(this.IncompleteOrderToolButton_Click);
			// 
			// ProcessAsCompleteToolButton
			// 
			this.ProcessAsCompleteToolButton.Image = ((System.Drawing.Image)(resources.GetObject("ProcessAsCompleteToolButton.Image")));
			this.ProcessAsCompleteToolButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ProcessAsCompleteToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ProcessAsCompleteToolButton.Name = "ProcessAsCompleteToolButton";
			this.ProcessAsCompleteToolButton.Size = new System.Drawing.Size(115, 52);
			this.ProcessAsCompleteToolButton.Text = "Process as Complete";
			this.ProcessAsCompleteToolButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.ProcessAsCompleteToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ProcessAsCompleteToolButton.ToolTipText = "Process an incomplete order as completed.";
			this.ProcessAsCompleteToolButton.Click += new System.EventHandler(this.ProcessAsCompleteToolButton_Click);
			// 
			// OutOfSquareToolButton
			// 
			this.OutOfSquareToolButton.Image = ((System.Drawing.Image)(resources.GetObject("OutOfSquareToolButton.Image")));
			this.OutOfSquareToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.OutOfSquareToolButton.Name = "OutOfSquareToolButton";
			this.OutOfSquareToolButton.Size = new System.Drawing.Size(114, 52);
			this.OutOfSquareToolButton.Text = "Out of Square Label";
			this.OutOfSquareToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.OutOfSquareToolButton.ToolTipText = "Prints an Out of Square defect label";
			this.OutOfSquareToolButton.Click += new System.EventHandler(this.outOfSquareToolStripMenuItem_Click);
			// 
			// FinishDefectToolButton
			// 
			this.FinishDefectToolButton.Image = ((System.Drawing.Image)(resources.GetObject("FinishDefectToolButton.Image")));
			this.FinishDefectToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.FinishDefectToolButton.Name = "FinishDefectToolButton";
			this.FinishDefectToolButton.Size = new System.Drawing.Size(108, 52);
			this.FinishDefectToolButton.Text = "Finish Defect Label";
			this.FinishDefectToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.FinishDefectToolButton.ToolTipText = "Prints a Finish Defect label";
			this.FinishDefectToolButton.Click += new System.EventHandler(this.finishDefectToolStripMenuItem_Click);
			// 
			// OtherDefectLabelToolButton
			// 
			this.OtherDefectLabelToolButton.Image = ((System.Drawing.Image)(resources.GetObject("OtherDefectLabelToolButton.Image")));
			this.OtherDefectLabelToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.OtherDefectLabelToolButton.Name = "OtherDefectLabelToolButton";
			this.OtherDefectLabelToolButton.Size = new System.Drawing.Size(107, 52);
			this.OtherDefectLabelToolButton.Text = "Other Defect Label";
			this.OtherDefectLabelToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.OtherDefectLabelToolButton.ToolTipText = "Prints a Generic Defect label";
			this.OtherDefectLabelToolButton.Click += new System.EventHandler(this.otherToolStripMenuItem_Click);
			// 
			// DatumWidthToolButton
			// 
			this.DatumWidthToolButton.Image = ((System.Drawing.Image)(resources.GetObject("DatumWidthToolButton.Image")));
			this.DatumWidthToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DatumWidthToolButton.Name = "DatumWidthToolButton";
			this.DatumWidthToolButton.Size = new System.Drawing.Size(80, 52);
			this.DatumWidthToolButton.Text = "Datum Width";
			this.DatumWidthToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DatumWidthToolButton.ToolTipText = "Datum Width Axis";
			this.DatumWidthToolButton.Click += new System.EventHandler(this.setWidthDatumToolStripMenuItem_Click);
			// 
			// DatumVerticalToolButton
			// 
			this.DatumVerticalToolButton.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
			this.DatumVerticalToolButton.Image = ((System.Drawing.Image)(resources.GetObject("DatumVerticalToolButton.Image")));
			this.DatumVerticalToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DatumVerticalToolButton.Name = "DatumVerticalToolButton";
			this.DatumVerticalToolButton.Size = new System.Drawing.Size(83, 52);
			this.DatumVerticalToolButton.Text = "Datum Height";
			this.DatumVerticalToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DatumVerticalToolButton.ToolTipText = "Datum Height Axis";
			this.DatumVerticalToolButton.Click += new System.EventHandler(this.setHeightDatumToolStripMenuItem_Click);
			// 
			// DatumDepthToolButton
			// 
			this.DatumDepthToolButton.Image = ((System.Drawing.Image)(resources.GetObject("DatumDepthToolButton.Image")));
			this.DatumDepthToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DatumDepthToolButton.Name = "DatumDepthToolButton";
			this.DatumDepthToolButton.Size = new System.Drawing.Size(80, 52);
			this.DatumDepthToolButton.Text = "Datum Depth";
			this.DatumDepthToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DatumDepthToolButton.ToolTipText = "Datum Depth Axis";
			this.DatumDepthToolButton.Visible = false;
			this.DatumDepthToolButton.Click += new System.EventHandler(this.setDepthDatumToolStripMenuItem_Click);
			// 
			// ExitToolButton
			// 
			this.ExitToolButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.ExitToolButton.Image = ((System.Drawing.Image)(resources.GetObject("ExitToolButton.Image")));
			this.ExitToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ExitToolButton.Name = "ExitToolButton";
			this.ExitToolButton.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
			this.ExitToolButton.Size = new System.Drawing.Size(74, 52);
			this.ExitToolButton.Text = "Exit";
			this.ExitToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ExitToolButton.ToolTipText = "Exit Program";
			this.ExitToolButton.Click += new System.EventHandler(this.ExitToolButton_Click);
			// 
			// DatumAngleToolButton
			// 
			this.DatumAngleToolButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.DatumAngleToolButton.Image = ((System.Drawing.Image)(resources.GetObject("DatumAngleToolButton.Image")));
			this.DatumAngleToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DatumAngleToolButton.Name = "DatumAngleToolButton";
			this.DatumAngleToolButton.Size = new System.Drawing.Size(78, 52);
			this.DatumAngleToolButton.Text = "Datum Angle";
			this.DatumAngleToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DatumAngleToolButton.Click += new System.EventHandler(this.DatumAngleToolButton_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabControl1.Location = new System.Drawing.Point(1, 141);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(806, 357);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Controls.Add(this.LineItemList);
			this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabPage1.Location = new System.Drawing.Point(4, 27);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(798, 326);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Line Items";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(244, 8);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(310, 16);
			this.label6.TabIndex = 1;
			this.label6.Text = "Double click line item for additional options.";
			// 
			// LineItemList
			// 
			this.LineItemList.AllowColumnReorder = true;
			this.LineItemList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.LineItemList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LineItemHeader,
            this.PartIDHeader,
            this.widthHeader,
            this.heightHeader,
            this.depthHeader,
            this.QtyReqHeader,
            this.QtyMeasuredHeader,
            this.TypeHeader,
            this.MaterialHeader,
            this.FinishHeader,
            this.HingingHeader,
            this.MachiningHeader,
            this.AssemblyHeader,
            this.CommentsHeader,
            this.User1Header,
            this.User2Header,
            this.User3Header,
            this.LocationHeader,
            this.StyleHeader});
			this.LineItemList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LineItemList.FullRowSelect = true;
			this.LineItemList.GridLines = true;
			this.LineItemList.Location = new System.Drawing.Point(-2, 34);
			this.LineItemList.MultiSelect = false;
			this.LineItemList.Name = "LineItemList";
			this.LineItemList.Size = new System.Drawing.Size(800, 292);
			this.LineItemList.TabIndex = 0;
			this.LineItemList.UseCompatibleStateImageBehavior = false;
			this.LineItemList.View = System.Windows.Forms.View.Details;
			this.LineItemList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LineItemList_MouseDoubleClick);
			this.LineItemList.ColumnReordered += new System.Windows.Forms.ColumnReorderedEventHandler(this.LineItemList_ColumnReordered);
			// 
			// LineItemHeader
			// 
			this.LineItemHeader.Tag = "LineItemHeader";
			this.LineItemHeader.Text = "Line No.";
			this.LineItemHeader.Width = 70;
			// 
			// PartIDHeader
			// 
			this.PartIDHeader.Tag = "PartIDHeader";
			this.PartIDHeader.Text = "Part ID";
			this.PartIDHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.PartIDHeader.Width = 110;
			// 
			// widthHeader
			// 
			this.widthHeader.Tag = "widthHeader";
			this.widthHeader.Text = "Width";
			this.widthHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.widthHeader.Width = 70;
			// 
			// heightHeader
			// 
			this.heightHeader.Tag = "heightHeader";
			this.heightHeader.Text = "Height";
			this.heightHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.heightHeader.Width = 70;
			// 
			// depthHeader
			// 
			this.depthHeader.Tag = "depthHeader";
			this.depthHeader.Text = "Depth";
			this.depthHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.depthHeader.Width = 70;
			// 
			// QtyReqHeader
			// 
			this.QtyReqHeader.Tag = "QtyReqHeader";
			this.QtyReqHeader.Text = "Qty Required";
			this.QtyReqHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.QtyReqHeader.Width = 110;
			// 
			// QtyMeasuredHeader
			// 
			this.QtyMeasuredHeader.Tag = "QtyMeasuredHeader";
			this.QtyMeasuredHeader.Text = "Qty Measured";
			this.QtyMeasuredHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.QtyMeasuredHeader.Width = 110;
			// 
			// TypeHeader
			// 
			this.TypeHeader.Tag = "TypeHeader";
			this.TypeHeader.Text = "Type";
			this.TypeHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.TypeHeader.Width = 110;
			// 
			// MaterialHeader
			// 
			this.MaterialHeader.Tag = "MaterialHeader";
			this.MaterialHeader.Text = "Material";
			this.MaterialHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.MaterialHeader.Width = 110;
			// 
			// FinishHeader
			// 
			this.FinishHeader.Tag = "FinishHeader";
			this.FinishHeader.Text = "Finish";
			this.FinishHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.FinishHeader.Width = 110;
			// 
			// HingingHeader
			// 
			this.HingingHeader.Tag = "HingingHeader";
			this.HingingHeader.Text = "Hinging";
			this.HingingHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.HingingHeader.Width = 110;
			// 
			// MachiningHeader
			// 
			this.MachiningHeader.Tag = "MachiningHeader";
			this.MachiningHeader.Text = "Machining";
			this.MachiningHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.MachiningHeader.Width = 110;
			// 
			// AssemblyHeader
			// 
			this.AssemblyHeader.Tag = "AssemblyHeader";
			this.AssemblyHeader.Text = "Assembly";
			this.AssemblyHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.AssemblyHeader.Width = 110;
			// 
			// CommentsHeader
			// 
			this.CommentsHeader.Tag = "CommentsHeader";
			this.CommentsHeader.Text = "Comments";
			this.CommentsHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.CommentsHeader.Width = 110;
			// 
			// User1Header
			// 
			this.User1Header.Tag = "User1Header";
			this.User1Header.Text = "User Def 1";
			this.User1Header.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.User1Header.Width = 110;
			// 
			// User2Header
			// 
			this.User2Header.Tag = "User2Header";
			this.User2Header.Text = "User Def 2";
			this.User2Header.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.User2Header.Width = 110;
			// 
			// User3Header
			// 
			this.User3Header.Tag = "User3Header";
			this.User3Header.Text = "User Def 3";
			this.User3Header.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.User3Header.Width = 110;
			// 
			// LocationHeader
			// 
			this.LocationHeader.Tag = "LocationHeader";
			this.LocationHeader.Text = "Location";
			this.LocationHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.LocationHeader.Width = 110;
			// 
			// StyleHeader
			// 
			this.StyleHeader.Tag = "StyleHeader";
			this.StyleHeader.Text = "Style";
			this.StyleHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.StyleHeader.Width = 110;
			// 
			// tabPage2
			// 
			this.tabPage2.AutoScroll = true;
			this.tabPage2.BackColor = System.Drawing.SystemColors.ControlLight;
			this.tabPage2.Controls.Add(this.label3);
			this.tabPage2.Controls.Add(this.BillEmail);
			this.tabPage2.Controls.Add(this.BillTelephone);
			this.tabPage2.Controls.Add(this.BillContact);
			this.tabPage2.Controls.Add(this.BillCountry);
			this.tabPage2.Controls.Add(this.BillZip);
			this.tabPage2.Controls.Add(this.BillState);
			this.tabPage2.Controls.Add(this.BillCity);
			this.tabPage2.Controls.Add(this.BillAddr2);
			this.tabPage2.Controls.Add(this.BillAddr1);
			this.tabPage2.Controls.Add(this.BillCustomer);
			this.tabPage2.Controls.Add(this.label18);
			this.tabPage2.Controls.Add(this.label17);
			this.tabPage2.Controls.Add(this.label16);
			this.tabPage2.Controls.Add(this.label15);
			this.tabPage2.Controls.Add(this.label14);
			this.tabPage2.Controls.Add(this.label13);
			this.tabPage2.Controls.Add(this.label12);
			this.tabPage2.Controls.Add(this.label11);
			this.tabPage2.Controls.Add(this.label10);
			this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabPage2.Location = new System.Drawing.Point(4, 27);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(798, 326);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Bill To Info";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(20, 336);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(48, 20);
			this.label3.TabIndex = 19;
			this.label3.Text = "Email";
			// 
			// BillEmail
			// 
			this.BillEmail.BackColor = System.Drawing.SystemColors.Control;
			this.BillEmail.Location = new System.Drawing.Point(174, 333);
			this.BillEmail.Name = "BillEmail";
			this.BillEmail.ReadOnly = true;
			this.BillEmail.Size = new System.Drawing.Size(383, 26);
			this.BillEmail.TabIndex = 18;
			// 
			// BillTelephone
			// 
			this.BillTelephone.BackColor = System.Drawing.SystemColors.Control;
			this.BillTelephone.Location = new System.Drawing.Point(174, 299);
			this.BillTelephone.Name = "BillTelephone";
			this.BillTelephone.ReadOnly = true;
			this.BillTelephone.Size = new System.Drawing.Size(383, 26);
			this.BillTelephone.TabIndex = 17;
			// 
			// BillContact
			// 
			this.BillContact.BackColor = System.Drawing.SystemColors.Control;
			this.BillContact.Location = new System.Drawing.Point(174, 264);
			this.BillContact.Name = "BillContact";
			this.BillContact.ReadOnly = true;
			this.BillContact.Size = new System.Drawing.Size(383, 26);
			this.BillContact.TabIndex = 16;
			// 
			// BillCountry
			// 
			this.BillCountry.BackColor = System.Drawing.SystemColors.Control;
			this.BillCountry.Location = new System.Drawing.Point(174, 229);
			this.BillCountry.Name = "BillCountry";
			this.BillCountry.ReadOnly = true;
			this.BillCountry.Size = new System.Drawing.Size(383, 26);
			this.BillCountry.TabIndex = 15;
			// 
			// BillZip
			// 
			this.BillZip.BackColor = System.Drawing.SystemColors.Control;
			this.BillZip.Location = new System.Drawing.Point(174, 194);
			this.BillZip.Name = "BillZip";
			this.BillZip.ReadOnly = true;
			this.BillZip.Size = new System.Drawing.Size(383, 26);
			this.BillZip.TabIndex = 14;
			// 
			// BillState
			// 
			this.BillState.BackColor = System.Drawing.SystemColors.Control;
			this.BillState.Location = new System.Drawing.Point(174, 159);
			this.BillState.Name = "BillState";
			this.BillState.ReadOnly = true;
			this.BillState.Size = new System.Drawing.Size(383, 26);
			this.BillState.TabIndex = 13;
			// 
			// BillCity
			// 
			this.BillCity.BackColor = System.Drawing.SystemColors.Control;
			this.BillCity.Location = new System.Drawing.Point(174, 124);
			this.BillCity.Name = "BillCity";
			this.BillCity.ReadOnly = true;
			this.BillCity.Size = new System.Drawing.Size(383, 26);
			this.BillCity.TabIndex = 12;
			// 
			// BillAddr2
			// 
			this.BillAddr2.BackColor = System.Drawing.SystemColors.Control;
			this.BillAddr2.Location = new System.Drawing.Point(174, 89);
			this.BillAddr2.Name = "BillAddr2";
			this.BillAddr2.ReadOnly = true;
			this.BillAddr2.Size = new System.Drawing.Size(383, 26);
			this.BillAddr2.TabIndex = 11;
			// 
			// BillAddr1
			// 
			this.BillAddr1.BackColor = System.Drawing.SystemColors.Control;
			this.BillAddr1.Location = new System.Drawing.Point(174, 54);
			this.BillAddr1.Name = "BillAddr1";
			this.BillAddr1.ReadOnly = true;
			this.BillAddr1.Size = new System.Drawing.Size(383, 26);
			this.BillAddr1.TabIndex = 10;
			// 
			// BillCustomer
			// 
			this.BillCustomer.BackColor = System.Drawing.SystemColors.Control;
			this.BillCustomer.Location = new System.Drawing.Point(174, 19);
			this.BillCustomer.Name = "BillCustomer";
			this.BillCustomer.ReadOnly = true;
			this.BillCustomer.Size = new System.Drawing.Size(383, 26);
			this.BillCustomer.TabIndex = 9;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(20, 302);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(84, 20);
			this.label18.TabIndex = 8;
			this.label18.Text = "Telephone";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(20, 267);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(111, 20);
			this.label17.TabIndex = 7;
			this.label17.Text = "Contact Name";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(20, 232);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(64, 20);
			this.label16.TabIndex = 6;
			this.label16.Text = "Country";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(20, 197);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(121, 20);
			this.label15.TabIndex = 5;
			this.label15.Text = "Zip/Postal Code";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(20, 162);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(112, 20);
			this.label14.TabIndex = 4;
			this.label14.Text = "State/Province";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(20, 127);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(35, 20);
			this.label13.TabIndex = 3;
			this.label13.Text = "City";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(20, 92);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(81, 20);
			this.label12.TabIndex = 2;
			this.label12.Text = "Address 2";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(20, 57);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(81, 20);
			this.label11.TabIndex = 1;
			this.label11.Text = "Address 1";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(20, 22);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(102, 20);
			this.label10.TabIndex = 0;
			this.label10.Text = "Bill Customer";
			// 
			// tabPage3
			// 
			this.tabPage3.AutoScroll = true;
			this.tabPage3.BackColor = System.Drawing.SystemColors.ControlLight;
			this.tabPage3.Controls.Add(this.label4);
			this.tabPage3.Controls.Add(this.ShipEmail);
			this.tabPage3.Controls.Add(this.ShipTelephone);
			this.tabPage3.Controls.Add(this.ShipContact);
			this.tabPage3.Controls.Add(this.ShipCountry);
			this.tabPage3.Controls.Add(this.ShipZip);
			this.tabPage3.Controls.Add(this.ShipState);
			this.tabPage3.Controls.Add(this.ShipCity);
			this.tabPage3.Controls.Add(this.ShipAddr2);
			this.tabPage3.Controls.Add(this.ShipAddr1);
			this.tabPage3.Controls.Add(this.ShipCustomer);
			this.tabPage3.Controls.Add(this.label28);
			this.tabPage3.Controls.Add(this.label29);
			this.tabPage3.Controls.Add(this.label30);
			this.tabPage3.Controls.Add(this.label31);
			this.tabPage3.Controls.Add(this.label32);
			this.tabPage3.Controls.Add(this.label33);
			this.tabPage3.Controls.Add(this.label34);
			this.tabPage3.Controls.Add(this.label35);
			this.tabPage3.Controls.Add(this.label36);
			this.tabPage3.Location = new System.Drawing.Point(4, 27);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(798, 326);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Ship To Info";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(21, 336);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(48, 20);
			this.label4.TabIndex = 37;
			this.label4.Text = "Email";
			// 
			// ShipEmail
			// 
			this.ShipEmail.BackColor = System.Drawing.SystemColors.Control;
			this.ShipEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipEmail.Location = new System.Drawing.Point(175, 333);
			this.ShipEmail.Name = "ShipEmail";
			this.ShipEmail.ReadOnly = true;
			this.ShipEmail.Size = new System.Drawing.Size(383, 26);
			this.ShipEmail.TabIndex = 36;
			// 
			// ShipTelephone
			// 
			this.ShipTelephone.BackColor = System.Drawing.SystemColors.Control;
			this.ShipTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipTelephone.Location = new System.Drawing.Point(175, 299);
			this.ShipTelephone.Name = "ShipTelephone";
			this.ShipTelephone.ReadOnly = true;
			this.ShipTelephone.Size = new System.Drawing.Size(383, 26);
			this.ShipTelephone.TabIndex = 35;
			// 
			// ShipContact
			// 
			this.ShipContact.BackColor = System.Drawing.SystemColors.Control;
			this.ShipContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipContact.Location = new System.Drawing.Point(175, 264);
			this.ShipContact.Name = "ShipContact";
			this.ShipContact.ReadOnly = true;
			this.ShipContact.Size = new System.Drawing.Size(383, 26);
			this.ShipContact.TabIndex = 34;
			// 
			// ShipCountry
			// 
			this.ShipCountry.BackColor = System.Drawing.SystemColors.Control;
			this.ShipCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipCountry.Location = new System.Drawing.Point(175, 229);
			this.ShipCountry.Name = "ShipCountry";
			this.ShipCountry.ReadOnly = true;
			this.ShipCountry.Size = new System.Drawing.Size(383, 26);
			this.ShipCountry.TabIndex = 33;
			// 
			// ShipZip
			// 
			this.ShipZip.BackColor = System.Drawing.SystemColors.Control;
			this.ShipZip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipZip.Location = new System.Drawing.Point(175, 194);
			this.ShipZip.Name = "ShipZip";
			this.ShipZip.ReadOnly = true;
			this.ShipZip.Size = new System.Drawing.Size(383, 26);
			this.ShipZip.TabIndex = 32;
			// 
			// ShipState
			// 
			this.ShipState.BackColor = System.Drawing.SystemColors.Control;
			this.ShipState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipState.Location = new System.Drawing.Point(175, 159);
			this.ShipState.Name = "ShipState";
			this.ShipState.ReadOnly = true;
			this.ShipState.Size = new System.Drawing.Size(383, 26);
			this.ShipState.TabIndex = 31;
			// 
			// ShipCity
			// 
			this.ShipCity.BackColor = System.Drawing.SystemColors.Control;
			this.ShipCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipCity.Location = new System.Drawing.Point(175, 124);
			this.ShipCity.Name = "ShipCity";
			this.ShipCity.ReadOnly = true;
			this.ShipCity.Size = new System.Drawing.Size(383, 26);
			this.ShipCity.TabIndex = 30;
			// 
			// ShipAddr2
			// 
			this.ShipAddr2.BackColor = System.Drawing.SystemColors.Control;
			this.ShipAddr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipAddr2.Location = new System.Drawing.Point(175, 89);
			this.ShipAddr2.Name = "ShipAddr2";
			this.ShipAddr2.ReadOnly = true;
			this.ShipAddr2.Size = new System.Drawing.Size(383, 26);
			this.ShipAddr2.TabIndex = 29;
			// 
			// ShipAddr1
			// 
			this.ShipAddr1.BackColor = System.Drawing.SystemColors.Control;
			this.ShipAddr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipAddr1.Location = new System.Drawing.Point(175, 54);
			this.ShipAddr1.Name = "ShipAddr1";
			this.ShipAddr1.ReadOnly = true;
			this.ShipAddr1.Size = new System.Drawing.Size(383, 26);
			this.ShipAddr1.TabIndex = 28;
			// 
			// ShipCustomer
			// 
			this.ShipCustomer.BackColor = System.Drawing.SystemColors.Control;
			this.ShipCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ShipCustomer.Location = new System.Drawing.Point(175, 19);
			this.ShipCustomer.Name = "ShipCustomer";
			this.ShipCustomer.ReadOnly = true;
			this.ShipCustomer.Size = new System.Drawing.Size(383, 26);
			this.ShipCustomer.TabIndex = 27;
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label28.Location = new System.Drawing.Point(21, 302);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(84, 20);
			this.label28.TabIndex = 26;
			this.label28.Text = "Telephone";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label29.Location = new System.Drawing.Point(21, 267);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(111, 20);
			this.label29.TabIndex = 25;
			this.label29.Text = "Contact Name";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label30.Location = new System.Drawing.Point(21, 232);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(64, 20);
			this.label30.TabIndex = 24;
			this.label30.Text = "Country";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label31.Location = new System.Drawing.Point(21, 197);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(121, 20);
			this.label31.TabIndex = 23;
			this.label31.Text = "Zip/Postal Code";
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label32.Location = new System.Drawing.Point(21, 162);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(112, 20);
			this.label32.TabIndex = 22;
			this.label32.Text = "State/Province";
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label33.Location = new System.Drawing.Point(21, 127);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(35, 20);
			this.label33.TabIndex = 21;
			this.label33.Text = "City";
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label34.Location = new System.Drawing.Point(21, 92);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(81, 20);
			this.label34.TabIndex = 20;
			this.label34.Text = "Address 2";
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label35.Location = new System.Drawing.Point(21, 57);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(81, 20);
			this.label35.TabIndex = 19;
			this.label35.Text = "Address 1";
			// 
			// label36
			// 
			this.label36.AutoSize = true;
			this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label36.Location = new System.Drawing.Point(21, 22);
			this.label36.Name = "label36";
			this.label36.Size = new System.Drawing.Size(114, 20);
			this.label36.TabIndex = 18;
			this.label36.Text = "Ship Customer";
			// 
			// menuStrip1
			// 
			this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
			this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.partErrorLabelsToolStripMenuItem,
            this.axesDatumToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.simulateMeasurementToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(807, 28);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openOrderFileToolStripMenuItem,
            this.openBatchOrderFileToolStripMenuItem,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
			this.fileToolStripMenuItem.Text = "File";
			this.fileToolStripMenuItem.DropDownOpening += new System.EventHandler(this.fileToolStripMenuItem_DropDownOpening);
			// 
			// openOrderFileToolStripMenuItem
			// 
			this.openOrderFileToolStripMenuItem.Name = "openOrderFileToolStripMenuItem";
			this.openOrderFileToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
			this.openOrderFileToolStripMenuItem.Text = "Import Order File";
			this.openOrderFileToolStripMenuItem.Click += new System.EventHandler(this.openOrderFileToolStripMenuItem_Click);
			// 
			// openBatchOrderFileToolStripMenuItem
			// 
			this.openBatchOrderFileToolStripMenuItem.Name = "openBatchOrderFileToolStripMenuItem";
			this.openBatchOrderFileToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
			this.openBatchOrderFileToolStripMenuItem.Text = "Run Batch Order File";
			this.openBatchOrderFileToolStripMenuItem.Click += new System.EventHandler(this.openBatchOrderFileToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// ordersToolStripMenuItem
			// 
			this.ordersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteCurrentOrderToolStripMenuItem,
            this.deleteAllOrdersToolStripMenuItem,
            this.incompleteOrderReportToolStripMenuItem1,
            this.processOrderAsCompleteToolStripMenuItem});
			this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
			this.ordersToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
			this.ordersToolStripMenuItem.Text = "Orders";
			this.ordersToolStripMenuItem.DropDownOpening += new System.EventHandler(this.ordersToolStripMenuItem_DropDownOpening);
			// 
			// deleteCurrentOrderToolStripMenuItem
			// 
			this.deleteCurrentOrderToolStripMenuItem.Name = "deleteCurrentOrderToolStripMenuItem";
			this.deleteCurrentOrderToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
			this.deleteCurrentOrderToolStripMenuItem.Text = "Remove Current Order";
			this.deleteCurrentOrderToolStripMenuItem.Click += new System.EventHandler(this.deleteCurrentOrderToolStripMenuItem_Click);
			// 
			// deleteAllOrdersToolStripMenuItem
			// 
			this.deleteAllOrdersToolStripMenuItem.Name = "deleteAllOrdersToolStripMenuItem";
			this.deleteAllOrdersToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
			this.deleteAllOrdersToolStripMenuItem.Text = "Remove All Orders";
			this.deleteAllOrdersToolStripMenuItem.Click += new System.EventHandler(this.deleteAllOrdersToolStripMenuItem_Click);
			// 
			// incompleteOrderReportToolStripMenuItem1
			// 
			this.incompleteOrderReportToolStripMenuItem1.Name = "incompleteOrderReportToolStripMenuItem1";
			this.incompleteOrderReportToolStripMenuItem1.Size = new System.Drawing.Size(256, 24);
			this.incompleteOrderReportToolStripMenuItem1.Text = "Incomplete Order Report";
			this.incompleteOrderReportToolStripMenuItem1.Click += new System.EventHandler(this.incompleteOrderReportToolStripMenuItem1_Click);
			// 
			// processOrderAsCompleteToolStripMenuItem
			// 
			this.processOrderAsCompleteToolStripMenuItem.Name = "processOrderAsCompleteToolStripMenuItem";
			this.processOrderAsCompleteToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
			this.processOrderAsCompleteToolStripMenuItem.Text = "Process Order as Complete";
			this.processOrderAsCompleteToolStripMenuItem.Click += new System.EventHandler(this.processOrderAsCompleteToolStripMenuItem_Click);
			// 
			// partErrorLabelsToolStripMenuItem
			// 
			this.partErrorLabelsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outOfSquareToolStripMenuItem,
            this.finishDefectToolStripMenuItem,
            this.otherToolStripMenuItem});
			this.partErrorLabelsToolStripMenuItem.Name = "partErrorLabelsToolStripMenuItem";
			this.partErrorLabelsToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
			this.partErrorLabelsToolStripMenuItem.Text = "Part Error Labels";
			// 
			// outOfSquareToolStripMenuItem
			// 
			this.outOfSquareToolStripMenuItem.Name = "outOfSquareToolStripMenuItem";
			this.outOfSquareToolStripMenuItem.Size = new System.Drawing.Size(172, 24);
			this.outOfSquareToolStripMenuItem.Text = "Out Of Square";
			this.outOfSquareToolStripMenuItem.Click += new System.EventHandler(this.outOfSquareToolStripMenuItem_Click);
			// 
			// finishDefectToolStripMenuItem
			// 
			this.finishDefectToolStripMenuItem.Name = "finishDefectToolStripMenuItem";
			this.finishDefectToolStripMenuItem.Size = new System.Drawing.Size(172, 24);
			this.finishDefectToolStripMenuItem.Text = "Finish Defect";
			this.finishDefectToolStripMenuItem.Click += new System.EventHandler(this.finishDefectToolStripMenuItem_Click);
			// 
			// otherToolStripMenuItem
			// 
			this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
			this.otherToolStripMenuItem.Size = new System.Drawing.Size(172, 24);
			this.otherToolStripMenuItem.Text = "Other";
			this.otherToolStripMenuItem.Click += new System.EventHandler(this.otherToolStripMenuItem_Click);
			// 
			// axesDatumToolStripMenuItem
			// 
			this.axesDatumToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setWidthDatumToolStripMenuItem,
            this.setHeightDatumToolStripMenuItem,
            this.setDepthDatumToolStripMenuItem});
			this.axesDatumToolStripMenuItem.Name = "axesDatumToolStripMenuItem";
			this.axesDatumToolStripMenuItem.Size = new System.Drawing.Size(101, 24);
			this.axesDatumToolStripMenuItem.Text = "Axes Datum";
			this.axesDatumToolStripMenuItem.DropDownOpening += new System.EventHandler(this.axesDatumToolStripMenuItem_DropDownOpening);
			// 
			// setWidthDatumToolStripMenuItem
			// 
			this.setWidthDatumToolStripMenuItem.Name = "setWidthDatumToolStripMenuItem";
			this.setWidthDatumToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
			this.setWidthDatumToolStripMenuItem.Text = "Set Width Datum";
			this.setWidthDatumToolStripMenuItem.Click += new System.EventHandler(this.setWidthDatumToolStripMenuItem_Click);
			// 
			// setHeightDatumToolStripMenuItem
			// 
			this.setHeightDatumToolStripMenuItem.Name = "setHeightDatumToolStripMenuItem";
			this.setHeightDatumToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
			this.setHeightDatumToolStripMenuItem.Text = "Set Height Datum";
			this.setHeightDatumToolStripMenuItem.Click += new System.EventHandler(this.setHeightDatumToolStripMenuItem_Click);
			// 
			// setDepthDatumToolStripMenuItem
			// 
			this.setDepthDatumToolStripMenuItem.Name = "setDepthDatumToolStripMenuItem";
			this.setDepthDatumToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
			this.setDepthDatumToolStripMenuItem.Text = "Set Depth Datum";
			this.setDepthDatumToolStripMenuItem.Click += new System.EventHandler(this.setDepthDatumToolStripMenuItem_Click);
			// 
			// configurationToolStripMenuItem
			// 
			this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logInToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.optionsToolStripMenuItem});
			this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
			this.configurationToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
			this.configurationToolStripMenuItem.Text = "Configuration";
			// 
			// logInToolStripMenuItem
			// 
			this.logInToolStripMenuItem.Name = "logInToolStripMenuItem";
			this.logInToolStripMenuItem.Size = new System.Drawing.Size(131, 24);
			this.logInToolStripMenuItem.Text = "Log In";
			this.logInToolStripMenuItem.Click += new System.EventHandler(this.logInToolStripMenuItem_Click);
			// 
			// logOutToolStripMenuItem
			// 
			this.logOutToolStripMenuItem.Enabled = false;
			this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
			this.logOutToolStripMenuItem.Size = new System.Drawing.Size(131, 24);
			this.logOutToolStripMenuItem.Text = "Log Out";
			this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
			// 
			// optionsToolStripMenuItem
			// 
			this.optionsToolStripMenuItem.Enabled = false;
			this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
			this.optionsToolStripMenuItem.Size = new System.Drawing.Size(131, 24);
			this.optionsToolStripMenuItem.Text = "Options";
			this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
			// 
			// simulateMeasurementToolStripMenuItem
			// 
			this.simulateMeasurementToolStripMenuItem.Name = "simulateMeasurementToolStripMenuItem";
			this.simulateMeasurementToolStripMenuItem.Size = new System.Drawing.Size(164, 24);
			this.simulateMeasurementToolStripMenuItem.Text = "Manual Measurement";
			this.simulateMeasurementToolStripMenuItem.Visible = false;
			this.simulateMeasurementToolStripMenuItem.Click += new System.EventHandler(this.simulateMeasurementToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// helpToolStripMenuItem1
			// 
			this.helpToolStripMenuItem1.Image = global::ProCABQC11.Properties.Resources.Help;
			this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
			this.helpToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F1;
			this.helpToolStripMenuItem1.Size = new System.Drawing.Size(134, 24);
			this.helpToolStripMenuItem1.Text = "Help";
			this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(131, 6);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// SortButton
			// 
			this.SortButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.SortButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.SortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SortButton.Image = global::ProCABQC11.Properties.Resources._1474073350_Refresh;
			this.SortButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.SortButton.Location = new System.Drawing.Point(0, 445);
			this.SortButton.Name = "SortButton";
			this.SortButton.Size = new System.Drawing.Size(190, 51);
			this.SortButton.TabIndex = 3;
			this.SortButton.Text = "   Sort Direction";
			this.SortButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.SortButton.UseVisualStyleBackColor = true;
			this.SortButton.Click += new System.EventHandler(this.SortButton_Click);
			// 
			// JobsInstructionsLabel
			// 
			this.JobsInstructionsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.JobsInstructionsLabel.AutoSize = true;
			this.JobsInstructionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.JobsInstructionsLabel.Location = new System.Drawing.Point(19, 50);
			this.JobsInstructionsLabel.Name = "JobsInstructionsLabel";
			this.JobsInstructionsLabel.Size = new System.Drawing.Size(150, 32);
			this.JobsInstructionsLabel.TabIndex = 2;
			this.JobsInstructionsLabel.Text = "Single click to select.\r\nDouble click for options.";
			this.JobsInstructionsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// JobsTitleLabel
			// 
			this.JobsTitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.JobsTitleLabel.AutoSize = true;
			this.JobsTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.JobsTitleLabel.Location = new System.Drawing.Point(38, 17);
			this.JobsTitleLabel.Name = "JobsTitleLabel";
			this.JobsTitleLabel.Size = new System.Drawing.Size(113, 24);
			this.JobsTitleLabel.TabIndex = 1;
			this.JobsTitleLabel.Text = "Orders/Jobs";
			this.toolTip1.SetToolTip(this.JobsTitleLabel, "The list of Orders or Jobs\r\ncurrently loaded.  Click\r\non an order/job to begin\r\np" +
					"rocessing.");
			// 
			// OrderListing
			// 
			this.OrderListing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.OrderListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.OrderListing.HorizontalScrollbar = true;
			this.OrderListing.ItemHeight = 24;
			this.OrderListing.Location = new System.Drawing.Point(0, 105);
			this.OrderListing.Name = "OrderListing";
			this.OrderListing.Size = new System.Drawing.Size(190, 340);
			this.OrderListing.TabIndex = 0;
			this.OrderListing.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OrderListing_MouseDoubleClick);
			this.OrderListing.SelectedValueChanged += new System.EventHandler(this.OrderListing_SelectedValueChanged);
			// 
			// PositionGroupBox
			// 
			this.PositionGroupBox.Controls.Add(this.AnglePosition);
			this.PositionGroupBox.Controls.Add(this.DepthPosition);
			this.PositionGroupBox.Controls.Add(this.AngleLabel);
			this.PositionGroupBox.Controls.Add(this.DepthLabel);
			this.PositionGroupBox.Controls.Add(this.WidthLabel);
			this.PositionGroupBox.Controls.Add(this.HeightLabel);
			this.PositionGroupBox.Controls.Add(this.HeightPosition);
			this.PositionGroupBox.Controls.Add(this.WidthPosition);
			this.PositionGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.PositionGroupBox.Location = new System.Drawing.Point(8, -2);
			this.PositionGroupBox.Name = "PositionGroupBox";
			this.PositionGroupBox.Size = new System.Drawing.Size(542, 196);
			this.PositionGroupBox.TabIndex = 16;
			this.PositionGroupBox.TabStop = false;
			// 
			// AnglePosition
			// 
			this.AnglePosition.BackColor = System.Drawing.Color.White;
			this.AnglePosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.AnglePosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AnglePosition.Location = new System.Drawing.Point(334, 79);
			this.AnglePosition.Name = "AnglePosition";
			this.AnglePosition.Size = new System.Drawing.Size(168, 44);
			this.AnglePosition.TabIndex = 14;
			// 
			// DepthPosition
			// 
			this.DepthPosition.BackColor = System.Drawing.Color.White;
			this.DepthPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.DepthPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DepthPosition.Location = new System.Drawing.Point(121, 136);
			this.DepthPosition.Name = "DepthPosition";
			this.DepthPosition.Size = new System.Drawing.Size(168, 44);
			this.DepthPosition.TabIndex = 13;
			this.DepthPosition.Visible = false;
			// 
			// AngleLabel
			// 
			this.AngleLabel.AutoSize = true;
			this.AngleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AngleLabel.Location = new System.Drawing.Point(380, 43);
			this.AngleLabel.Name = "AngleLabel";
			this.AngleLabel.Size = new System.Drawing.Size(83, 31);
			this.AngleLabel.TabIndex = 15;
			this.AngleLabel.Text = "Angle";
			// 
			// DepthLabel
			// 
			this.DepthLabel.AutoSize = true;
			this.DepthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DepthLabel.Location = new System.Drawing.Point(22, 146);
			this.DepthLabel.Name = "DepthLabel";
			this.DepthLabel.Size = new System.Drawing.Size(87, 31);
			this.DepthLabel.TabIndex = 12;
			this.DepthLabel.Text = "Depth";
			this.DepthLabel.Visible = false;
			// 
			// WidthLabel
			// 
			this.WidthLabel.AutoSize = true;
			this.WidthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.WidthLabel.Location = new System.Drawing.Point(22, 28);
			this.WidthLabel.Name = "WidthLabel";
			this.WidthLabel.Size = new System.Drawing.Size(83, 31);
			this.WidthLabel.TabIndex = 1;
			this.WidthLabel.Text = "Width";
			// 
			// HeightLabel
			// 
			this.HeightLabel.AutoSize = true;
			this.HeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.HeightLabel.Location = new System.Drawing.Point(22, 87);
			this.HeightLabel.Name = "HeightLabel";
			this.HeightLabel.Size = new System.Drawing.Size(93, 31);
			this.HeightLabel.TabIndex = 2;
			this.HeightLabel.Text = "Height";
			// 
			// HeightPosition
			// 
			this.HeightPosition.BackColor = System.Drawing.Color.White;
			this.HeightPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.HeightPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.HeightPosition.Location = new System.Drawing.Point(121, 79);
			this.HeightPosition.Name = "HeightPosition";
			this.HeightPosition.Size = new System.Drawing.Size(168, 44);
			this.HeightPosition.TabIndex = 4;
			// 
			// WidthPosition
			// 
			this.WidthPosition.BackColor = System.Drawing.Color.White;
			this.WidthPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.WidthPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.WidthPosition.Location = new System.Drawing.Point(121, 23);
			this.WidthPosition.Name = "WidthPosition";
			this.WidthPosition.Size = new System.Drawing.Size(168, 43);
			this.WidthPosition.TabIndex = 3;
			// 
			// UnitsGroupBox
			// 
			this.UnitsGroupBox.BackColor = System.Drawing.SystemColors.ControlLight;
			this.UnitsGroupBox.Controls.Add(this.MillimeterSelect);
			this.UnitsGroupBox.Controls.Add(this.InchSelect);
			this.UnitsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.UnitsGroupBox.Location = new System.Drawing.Point(571, 21);
			this.UnitsGroupBox.Name = "UnitsGroupBox";
			this.UnitsGroupBox.Size = new System.Drawing.Size(162, 157);
			this.UnitsGroupBox.TabIndex = 11;
			this.UnitsGroupBox.TabStop = false;
			this.UnitsGroupBox.Text = "Measuring Units";
			// 
			// MillimeterSelect
			// 
			this.MillimeterSelect.Appearance = System.Windows.Forms.Appearance.Button;
			this.MillimeterSelect.AutoSize = true;
			this.MillimeterSelect.BackColor = System.Drawing.SystemColors.ControlLight;
			this.MillimeterSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MillimeterSelect.Location = new System.Drawing.Point(30, 101);
			this.MillimeterSelect.Name = "MillimeterSelect";
			this.MillimeterSelect.Size = new System.Drawing.Size(104, 30);
			this.MillimeterSelect.TabIndex = 1;
			this.MillimeterSelect.TabStop = true;
			this.MillimeterSelect.Text = "Millimeters";
			this.MillimeterSelect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.MillimeterSelect, "Display measurements in Millimeters");
			this.MillimeterSelect.UseVisualStyleBackColor = true;
			// 
			// InchSelect
			// 
			this.InchSelect.Appearance = System.Windows.Forms.Appearance.Button;
			this.InchSelect.AutoSize = true;
			this.InchSelect.BackColor = System.Drawing.SystemColors.ControlLight;
			this.InchSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.InchSelect.Location = new System.Drawing.Point(42, 47);
			this.InchSelect.Name = "InchSelect";
			this.InchSelect.Size = new System.Drawing.Size(73, 30);
			this.InchSelect.TabIndex = 0;
			this.InchSelect.TabStop = true;
			this.InchSelect.Text = "Inches";
			this.InchSelect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.InchSelect, "Display measurements in Inches");
			this.InchSelect.UseVisualStyleBackColor = true;
			this.InchSelect.CheckedChanged += new System.EventHandler(this.InchSelect_CheckedChanged);
			// 
			// MeasureButton
			// 
			this.MeasureButton.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.MeasureButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MeasureButton.Image = ((System.Drawing.Image)(resources.GetObject("MeasureButton.Image")));
			this.MeasureButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.MeasureButton.Location = new System.Drawing.Point(758, 46);
			this.MeasureButton.Name = "MeasureButton";
			this.MeasureButton.Size = new System.Drawing.Size(200, 112);
			this.MeasureButton.TabIndex = 9;
			this.MeasureButton.Text = "Measure   ";
			this.MeasureButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.toolTip1.SetToolTip(this.MeasureButton, "Position moving jaws against\r\npart and then press this button\r\nto create a measur" +
					"ement.");
			this.MeasureButton.UseVisualStyleBackColor = true;
			this.MeasureButton.Click += new System.EventHandler(this.MeasureButton_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OrderUsesDepthLabel,
            this.SystemOrderStateLabel});
			this.statusStrip1.Location = new System.Drawing.Point(0, 202);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1004, 22);
			this.statusStrip1.TabIndex = 0;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// OrderUsesDepthLabel
			// 
			this.OrderUsesDepthLabel.Name = "OrderUsesDepthLabel";
			this.OrderUsesDepthLabel.Size = new System.Drawing.Size(159, 17);
			this.OrderUsesDepthLabel.Text = "Order Uses Depth Dimension";
			this.OrderUsesDepthLabel.Visible = false;
			// 
			// SystemOrderStateLabel
			// 
			this.SystemOrderStateLabel.AutoSize = false;
			this.SystemOrderStateLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
			this.SystemOrderStateLabel.Name = "SystemOrderStateLabel";
			this.SystemOrderStateLabel.Size = new System.Drawing.Size(121, 17);
			this.SystemOrderStateLabel.Text = "System Orders Saved";
			this.SystemOrderStateLabel.Visible = false;
			// 
			// LineItemContextOptions
			// 
			this.LineItemContextOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EditLineItem,
            this.ViewFullLineItem,
            this.ViewMeasuredParts,
            this.deleteLastLineItemMeasurement});
			this.LineItemContextOptions.Name = "contextMenuStrip1";
			this.LineItemContextOptions.Size = new System.Drawing.Size(260, 92);
			this.LineItemContextOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
			// 
			// EditLineItem
			// 
			this.EditLineItem.Name = "EditLineItem";
			this.EditLineItem.Size = new System.Drawing.Size(259, 22);
			this.EditLineItem.Text = "Edit Line Item";
			this.EditLineItem.ToolTipText = "Edits Height, Width or Quantity";
			// 
			// ViewFullLineItem
			// 
			this.ViewFullLineItem.Name = "ViewFullLineItem";
			this.ViewFullLineItem.Size = new System.Drawing.Size(259, 22);
			this.ViewFullLineItem.Text = "View Full Line Item";
			// 
			// ViewMeasuredParts
			// 
			this.ViewMeasuredParts.Name = "ViewMeasuredParts";
			this.ViewMeasuredParts.Size = new System.Drawing.Size(259, 22);
			this.ViewMeasuredParts.Text = "View Measured Parts";
			// 
			// deleteLastLineItemMeasurement
			// 
			this.deleteLastLineItemMeasurement.Name = "deleteLastLineItemMeasurement";
			this.deleteLastLineItemMeasurement.Size = new System.Drawing.Size(259, 22);
			this.deleteLastLineItemMeasurement.Text = "Delete Last Line Item Measurement";
			// 
			// textBox8
			// 
			this.textBox8.BackColor = System.Drawing.SystemColors.Control;
			this.textBox8.Location = new System.Drawing.Point(174, 226);
			this.textBox8.Name = "textBox8";
			this.textBox8.ReadOnly = true;
			this.textBox8.Size = new System.Drawing.Size(383, 20);
			this.textBox8.TabIndex = 15;
			// 
			// textBox9
			// 
			this.textBox9.BackColor = System.Drawing.SystemColors.Control;
			this.textBox9.Location = new System.Drawing.Point(174, 191);
			this.textBox9.Name = "textBox9";
			this.textBox9.ReadOnly = true;
			this.textBox9.Size = new System.Drawing.Size(383, 20);
			this.textBox9.TabIndex = 14;
			// 
			// textBox10
			// 
			this.textBox10.BackColor = System.Drawing.SystemColors.Control;
			this.textBox10.Location = new System.Drawing.Point(174, 156);
			this.textBox10.Name = "textBox10";
			this.textBox10.ReadOnly = true;
			this.textBox10.Size = new System.Drawing.Size(383, 20);
			this.textBox10.TabIndex = 13;
			// 
			// textBox11
			// 
			this.textBox11.BackColor = System.Drawing.SystemColors.Control;
			this.textBox11.Location = new System.Drawing.Point(174, 121);
			this.textBox11.Name = "textBox11";
			this.textBox11.ReadOnly = true;
			this.textBox11.Size = new System.Drawing.Size(383, 20);
			this.textBox11.TabIndex = 12;
			// 
			// textBox12
			// 
			this.textBox12.BackColor = System.Drawing.SystemColors.Control;
			this.textBox12.Location = new System.Drawing.Point(174, 86);
			this.textBox12.Name = "textBox12";
			this.textBox12.ReadOnly = true;
			this.textBox12.Size = new System.Drawing.Size(383, 20);
			this.textBox12.TabIndex = 11;
			// 
			// textBox13
			// 
			this.textBox13.BackColor = System.Drawing.SystemColors.Control;
			this.textBox13.Location = new System.Drawing.Point(174, 51);
			this.textBox13.Name = "textBox13";
			this.textBox13.ReadOnly = true;
			this.textBox13.Size = new System.Drawing.Size(383, 20);
			this.textBox13.TabIndex = 10;
			// 
			// textBox14
			// 
			this.textBox14.BackColor = System.Drawing.SystemColors.Control;
			this.textBox14.Location = new System.Drawing.Point(174, 16);
			this.textBox14.Name = "textBox14";
			this.textBox14.ReadOnly = true;
			this.textBox14.Size = new System.Drawing.Size(383, 20);
			this.textBox14.TabIndex = 9;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(20, 299);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(84, 20);
			this.label19.TabIndex = 8;
			this.label19.Text = "Telephone";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(20, 264);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(111, 20);
			this.label20.TabIndex = 7;
			this.label20.Text = "Contact Name";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(20, 229);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(64, 20);
			this.label21.TabIndex = 6;
			this.label21.Text = "Country";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(20, 194);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(121, 20);
			this.label22.TabIndex = 5;
			this.label22.Text = "Zip/Postal Code";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(20, 159);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(112, 20);
			this.label23.TabIndex = 4;
			this.label23.Text = "State/Province";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(20, 124);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(35, 20);
			this.label24.TabIndex = 3;
			this.label24.Text = "City";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(20, 89);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(81, 20);
			this.label25.TabIndex = 2;
			this.label25.Text = "Address 2";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(20, 54);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(81, 20);
			this.label26.TabIndex = 1;
			this.label26.Text = "Address 1";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(20, 19);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(102, 20);
			this.label27.TabIndex = 0;
			this.label27.Text = "Bill Customer";
			// 
			// OrdersContextOptions
			// 
			this.OrdersContextOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteOrderToolStripMenuItem,
            this.saveCompletedOrderToolStripMenuItem,
            this.incompleteOrderReportToolStripMenuItem,
            this.processOrderAsCompletePopupToolStripMenuItem});
			this.OrdersContextOptions.Name = "OrdersContextOptions";
			this.OrdersContextOptions.Size = new System.Drawing.Size(217, 92);
			this.OrdersContextOptions.Opening += new System.ComponentModel.CancelEventHandler(this.OrdersContextOptions_Opening);
			// 
			// deleteOrderToolStripMenuItem
			// 
			this.deleteOrderToolStripMenuItem.Name = "deleteOrderToolStripMenuItem";
			this.deleteOrderToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
			this.deleteOrderToolStripMenuItem.Text = "Remove Order";
			this.deleteOrderToolStripMenuItem.Click += new System.EventHandler(this.deleteOrderToolStripMenuItem_Click);
			// 
			// saveCompletedOrderToolStripMenuItem
			// 
			this.saveCompletedOrderToolStripMenuItem.Name = "saveCompletedOrderToolStripMenuItem";
			this.saveCompletedOrderToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
			this.saveCompletedOrderToolStripMenuItem.Text = "Save Completed Order";
			this.saveCompletedOrderToolStripMenuItem.Click += new System.EventHandler(this.saveCompletedOrderToolStripMenuItem_Click);
			// 
			// incompleteOrderReportToolStripMenuItem
			// 
			this.incompleteOrderReportToolStripMenuItem.Name = "incompleteOrderReportToolStripMenuItem";
			this.incompleteOrderReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
			this.incompleteOrderReportToolStripMenuItem.Text = "Incomplete Order Report";
			this.incompleteOrderReportToolStripMenuItem.Click += new System.EventHandler(this.incompleteOrderReportToolStripMenuItem_Click);
			// 
			// processOrderAsCompletePopupToolStripMenuItem
			// 
			this.processOrderAsCompletePopupToolStripMenuItem.Name = "processOrderAsCompletePopupToolStripMenuItem";
			this.processOrderAsCompletePopupToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
			this.processOrderAsCompletePopupToolStripMenuItem.Text = "Process Order as Complete";
			this.processOrderAsCompletePopupToolStripMenuItem.Click += new System.EventHandler(this.processOrderAsCompletePopupToolStripMenuItem_Click);
			// 
			// helpProvider1
			// 
			this.helpProvider1.HelpNamespace = ".\\Accurate Technology, Inc.  ProCAB QC Measurement Software User Manual.chm";
			// 
			// StatusBarTimer
			// 
			this.StatusBarTimer.Interval = 2000;
			this.StatusBarTimer.Tick += new System.EventHandler(this.StatusBarTimer_Tick);
			// 
			// OptionLauncherTimer
			// 
			this.OptionLauncherTimer.Tick += new System.EventHandler(this.OptionLauncher_Tick);
			// 
			// instantProtection1
			// 
			//this.instantProtection1.Active = true;
			//this.instantProtection1.AppPath = "";
			//this.instantProtection1.DetectionMethod = Ekc.Comp.TDetectionMethod.LocalKey;
			//this.instantProtection1.InstantDetectionMode = Ekc.Comp.TInstantDetectionMode.Default;
			instantLocalKey3.KeyID = ((uint)(1115348243u));
			instantLocalKey3.KeyLocation = Ekc.Comp.TKeyLocation.Default;
			keyValue5.Key01 = ((ushort)(0));
			keyValue5.Key02 = ((ushort)(0));
			keyValue5.Key03 = ((ushort)(0));
			keyValue5.Key04 = ((ushort)(0));
			keyValue5.Key05 = ((ushort)(0));
			keyValue5.Key06 = ((ushort)(0));
			keyValue5.Key07 = ((ushort)(0));
			keyValue5.Key08 = ((ushort)(0));
			keyValue5.Key09 = ((ushort)(0));
			keyValue5.Key10 = ((ushort)(0));
			keyValue5.KeyValueCode = "";
			keyValue5.KeyValueIndex = ((long)(0));
			keyValue5.Value01 = ((ushort)(0));
			keyValue5.Value02 = ((ushort)(0));
			keyValue5.Value03 = ((ushort)(0));
			keyValue5.Value04 = ((ushort)(0));
			keyValue5.Value05 = ((ushort)(0));
			keyValue5.Value06 = ((ushort)(0));
			keyValue5.Value07 = ((ushort)(0));
			keyValue5.Value08 = ((ushort)(0));
			keyValue5.Value09 = ((ushort)(0));
			keyValue5.Value10 = ((ushort)(0));
			instantLocalKey3.KeyValue = keyValue5;
			instantLocalKey3.ModuleID = 0;
			instantLocalKey3.ProgramID = 0;
			instantLocalKey3.SystemImageFile = "ProCAB QC.sif";
			//this.instantProtection1.LocalKey = instantLocalKey3;
			instantNetworkKey3.KeyID = ((uint)(0u));
			instantNetworkKey3.KeyLocation = Ekc.Comp.TKeyLocation.Default;
			keyValue6.Key01 = ((ushort)(0));
			keyValue6.Key02 = ((ushort)(0));
			keyValue6.Key03 = ((ushort)(0));
			keyValue6.Key04 = ((ushort)(0));
			keyValue6.Key05 = ((ushort)(0));
			keyValue6.Key06 = ((ushort)(0));
			keyValue6.Key07 = ((ushort)(0));
			keyValue6.Key08 = ((ushort)(0));
			keyValue6.Key09 = ((ushort)(0));
			keyValue6.Key10 = ((ushort)(0));
			keyValue6.KeyValueCode = "";
			keyValue6.KeyValueIndex = ((long)(0));
			keyValue6.Value01 = ((ushort)(0));
			keyValue6.Value02 = ((ushort)(0));
			keyValue6.Value03 = ((ushort)(0));
			keyValue6.Value04 = ((ushort)(0));
			keyValue6.Value05 = ((ushort)(0));
			keyValue6.Value06 = ((ushort)(0));
			keyValue6.Value07 = ((ushort)(0));
			keyValue6.Value08 = ((ushort)(0));
			keyValue6.Value09 = ((ushort)(0));
			keyValue6.Value10 = ((ushort)(0));
			instantNetworkKey3.KeyValue = keyValue6;
			instantNetworkKey3.ModuleID = 0;
			instantNetworkKey3.ProgramID = 0;
			instantNetworkKey3.SystemImageFile = "";
			//this.instantProtection1.NetworkKey = instantNetworkKey3;
			//this.instantProtection1.OnDetectionError += new Ekc.Comp.DetectionErrorEventHandler(this.instantProtection1_OnDetectionError);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1008, 730);
			this.Controls.Add(this.splitContainer1);
			this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.MaximumSize = new System.Drawing.Size(1920, 1080);
			this.MinimumSize = new System.Drawing.Size(1024, 768);
			this.Name = "Form1";
			this.helpProvider1.SetShowHelp(this, true);
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ProCAB QC®";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.Shown += new System.EventHandler(this.Form1_Shown);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Resize += new System.EventHandler(this.Form1_Resize);
			this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel1.PerformLayout();
			this.splitContainer2.Panel2.ResumeLayout(false);
			this.splitContainer2.Panel2.PerformLayout();
			this.splitContainer2.ResumeLayout(false);
			this.ToolStrip.ResumeLayout(false);
			this.ToolStrip.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.PositionGroupBox.ResumeLayout(false);
			this.PositionGroupBox.PerformLayout();
			this.UnitsGroupBox.ResumeLayout(false);
			this.UnitsGroupBox.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.LineItemContextOptions.ResumeLayout(false);
			this.OrdersContextOptions.ResumeLayout(false);
			//((System.ComponentModel.ISupportInitialize)(this.instantProtection1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ListBox OrderListing;
		private System.Windows.Forms.Label HeightPosition;
		private System.Windows.Forms.Label WidthPosition;
		private System.Windows.Forms.Label HeightLabel;
		private System.Windows.Forms.Label WidthLabel;
		private System.Windows.Forms.Button MeasureButton;
		private System.Windows.Forms.Label JobsTitleLabel;
		private System.Windows.Forms.ListView LineItemList;
		private System.Windows.Forms.ColumnHeader LineItemHeader;
		private System.Windows.Forms.ColumnHeader PartIDHeader;
		private System.Windows.Forms.ColumnHeader QtyReqHeader;
		private System.Windows.Forms.ColumnHeader QtyMeasuredHeader;
		private System.Windows.Forms.ColumnHeader CommentsHeader;
		private System.Windows.Forms.ToolStripMenuItem openOrderFileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openBatchOrderFileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip LineItemContextOptions;
		private System.Windows.Forms.ToolStripMenuItem EditLineItem;
		private System.Windows.Forms.ToolStripMenuItem ViewFullLineItem;
		private System.Windows.Forms.ToolStripMenuItem ViewMeasuredParts;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox BillCustomer;
		private System.Windows.Forms.TextBox BillTelephone;
		private System.Windows.Forms.TextBox BillContact;
		private System.Windows.Forms.TextBox BillCountry;
		private System.Windows.Forms.TextBox BillZip;
		private System.Windows.Forms.TextBox BillState;
		private System.Windows.Forms.TextBox BillCity;
		private System.Windows.Forms.TextBox BillAddr2;
		private System.Windows.Forms.TextBox BillAddr1;
		private System.Windows.Forms.TextBox ShipTelephone;
		private System.Windows.Forms.TextBox ShipContact;
		private System.Windows.Forms.TextBox ShipCountry;
		private System.Windows.Forms.TextBox ShipZip;
		private System.Windows.Forms.TextBox ShipState;
		private System.Windows.Forms.TextBox ShipCity;
		private System.Windows.Forms.TextBox ShipAddr2;
		private System.Windows.Forms.TextBox ShipAddr1;
		private System.Windows.Forms.TextBox ShipCustomer;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.TextBox textBox8;
		private System.Windows.Forms.TextBox textBox9;
		private System.Windows.Forms.TextBox textBox10;
		private System.Windows.Forms.TextBox textBox11;
		private System.Windows.Forms.TextBox textBox12;
		private System.Windows.Forms.TextBox textBox13;
		private System.Windows.Forms.TextBox textBox14;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
		private System.Windows.Forms.GroupBox UnitsGroupBox;
		private System.Windows.Forms.RadioButton MillimeterSelect;
		private System.Windows.Forms.RadioButton InchSelect;
		private System.Windows.Forms.ColumnHeader widthHeader;
		private System.Windows.Forms.ColumnHeader heightHeader;
		private System.Windows.Forms.ContextMenuStrip OrdersContextOptions;
		private System.Windows.Forms.ToolStripMenuItem deleteOrderToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteCurrentOrderToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteAllOrdersToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveCompletedOrderToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem logInToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem axesDatumToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem setWidthDatumToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem setHeightDatumToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem simulateMeasurementToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem incompleteOrderReportToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem incompleteOrderReportToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem partErrorLabelsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem outOfSquareToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem finishDefectToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem otherToolStripMenuItem;
		private System.Windows.Forms.HelpProvider helpProvider1;
		private System.Windows.Forms.ToolStripMenuItem deleteLastLineItemMeasurement;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox BillEmail;
		private System.Windows.Forms.TextBox ShipEmail;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ToolStripMenuItem processOrderAsCompleteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem processOrderAsCompletePopupToolStripMenuItem;
		private System.Windows.Forms.ColumnHeader User1Header;
		private System.Windows.Forms.ColumnHeader User2Header;
		private System.Windows.Forms.Label DepthPosition;
		private System.Windows.Forms.Label DepthLabel;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ColumnHeader depthHeader;
		private System.Windows.Forms.ToolStripStatusLabel SystemOrderStateLabel;
		private System.Windows.Forms.Timer StatusBarTimer;
		private System.Windows.Forms.ToolStrip ToolStrip;
		private System.Windows.Forms.ToolStripButton ImportOrderFileToolButton;
		private System.Windows.Forms.ToolStripButton ManualMeasurementToolButton;
		private System.Windows.Forms.ToolStripButton LineItemDetailToolButton;
		private System.Windows.Forms.ToolStripButton DeleteMeasurementToolButton;
		private System.Windows.Forms.ToolStripButton IncompleteOrderToolButton;
		private System.Windows.Forms.ToolStripMenuItem setDepthDatumToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton ProcessAsCompleteToolButton;
		private System.Windows.Forms.ToolStripButton OutOfSquareToolButton;
		private System.Windows.Forms.ToolStripButton FinishDefectToolButton;
		private System.Windows.Forms.ToolStripButton OtherDefectLabelToolButton;
		private System.Windows.Forms.ToolStripButton DatumVerticalToolButton;
		private System.Windows.Forms.ToolStripButton DatumWidthToolButton;
		private System.Windows.Forms.ToolStripButton DatumDepthToolButton;
		private System.Windows.Forms.ToolStripButton ExitToolButton;
		private System.Windows.Forms.Label JobsInstructionsLabel;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ToolStripStatusLabel OrderUsesDepthLabel;
		private System.Windows.Forms.Timer OptionLauncherTimer;
		private System.Windows.Forms.Label AngleLabel;
		private System.Windows.Forms.Label AnglePosition;
		private System.Windows.Forms.GroupBox PositionGroupBox;
		private System.Windows.Forms.ToolStripButton DatumAngleToolButton;
		private System.Windows.Forms.ColumnHeader HingingHeader;
		private System.Windows.Forms.ColumnHeader FinishHeader;
		private System.Windows.Forms.ColumnHeader TypeHeader;
		private System.Windows.Forms.ColumnHeader LocationHeader;
		private System.Windows.Forms.ColumnHeader MachiningHeader;
		private System.Windows.Forms.ColumnHeader AssemblyHeader;
		private System.Windows.Forms.Button SortButton;
		//private Ekc.Comp.InstantProtection instantProtection1;
		private System.Windows.Forms.ColumnHeader MaterialHeader;
		private System.Windows.Forms.ColumnHeader User3Header;
		private System.Windows.Forms.ColumnHeader StyleHeader;

	}
}

