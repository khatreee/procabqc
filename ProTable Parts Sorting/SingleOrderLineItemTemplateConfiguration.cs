﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public class SingleOrderLineItemTemplateConfiguration
	{

		private const int TitleXPos = 50;
		private const int TitleYPos = 5;
		private const int NameLabelXPos = 5;
		private const int DataTextBoxXPos = 160;
		private const int RowIncrementYPos = 28;
		private const int StartYPos = TitleYPos + 60;
		private const int TextBoxXSize = 40;
		private const int TextBoxYSize = 20;
		private const int PanelXSize = 220;
		private const int PanelYSize = 650;
		private const int ColumnTitleXPos = DataTextBoxXPos - 12;
		private const int ColumnTitleYPos = StartYPos - 25;
		private const string TitleText = "Line Items";
		private const string LineItemStartText = "Line Item Start Row";




		Panel MainPanel;
		Label[] TemplateNames;
		TextBox[] TemplateColumnData;
		SingleOrderLineItemTemplate LineItemTemplate;
		Label Title;
		Label ColumnTitle;

		Label LineItemStartLabel;
		TextBox LineItemStartData;

		// A reference to the params
		PersistentParametersV1100 LocalParams;

		// A local copy of the template index
		int LocalTemplateIndex;


		public SingleOrderLineItemTemplateConfiguration()
		{

		}

		public SingleOrderLineItemTemplateConfiguration(Point PanelLocation, PersistentParametersV1100 PParams, int TemplateIndex)
		{

			// Create Panel
			MainPanel = new Panel();
			MainPanel.SuspendLayout();
			 
			// Create arrays of labels for template names
			TemplateNames = new Label[(int)SingleOrderLineItemColumnIDV1100.MaxItems];

			// Create array of text boxes for template data
			TemplateColumnData = new TextBox[(int)SingleOrderLineItemColumnIDV1100.MaxItems];

			// Create the template, thus reading the registry
			LineItemTemplate = new SingleOrderLineItemTemplate(ref PParams, TemplateIndex);

			// Create the labels and textboxes for each template
			for (SingleOrderLineItemColumnIDV1100 u = 0; u < SingleOrderLineItemColumnIDV1100.MaxItems; u++)
			{
				TemplateNames[(int)u] = new Label();
				TemplateColumnData[(int)u] = new TextBox();
			}

			// Store a copy of system params for use in VerifyTemplateEntries()
			LocalParams = PParams;

			// Copy the index
			LocalTemplateIndex = TemplateIndex;

			// A template item reference
			TemplateLineItemColumn Template;

			int YPos = StartYPos;
			

			// Get each template item and load the data into the labels and text boxes
			for (SingleOrderLineItemColumnIDV1100 u = 0; u < SingleOrderLineItemColumnIDV1100.MaxItems; u++)
			{
				// Get template item from template
				LineItemTemplate.GetLineItemColumnTemplate(u, out Template);

				// Update the text data
				TemplateNames[(int)u].Text = Template.FieldName;
				TemplateColumnData[(int)u].Text = Template.GetColumnString;

				// Set the parameters for the label
				TemplateNames[(int)u].Location = new Point(NameLabelXPos, YPos);
				TemplateNames[(int)u].AutoSize = true;

				// Set the parameters for the text box
				TemplateColumnData[(int)u].Location = new Point(DataTextBoxXPos, YPos);
				TemplateColumnData[(int)u].Size = new Size(TextBoxXSize, TextBoxYSize);

				// Increment to the next row in Y
				YPos += RowIncrementYPos;


				// Add the object to the panel
				MainPanel.Controls.Add(TemplateNames[(int)u]);
				MainPanel.Controls.Add(TemplateColumnData[(int)u]);

			}

			// Add extra space between line item start and other fields
			YPos += 15;

			// Create line item row start label
			LineItemStartLabel = new Label();
			LineItemStartLabel.Text = LineItemStartText;
			LineItemStartLabel.AutoSize = true;
			LineItemStartLabel.Location = new Point(NameLabelXPos, YPos);

			// Create text box for line item start data
			LineItemStartData = new TextBox();
			LineItemStartData.Text = LineItemTemplate.GetLineItemStartString;
			LineItemStartData.Location = new Point(DataTextBoxXPos, YPos);
			LineItemStartData.Size = new Size(TextBoxXSize, TextBoxYSize);
			
			// Create the title	and format it
			Title = new Label();
			Title.Location = new Point(TitleXPos, TitleYPos);
			Title.Text = TitleText;
			Title.AutoSize = true;
			Title.Font = new Font("Microsoft Sans Serif", (float)16);

			// Create the column title
			ColumnTitle = new Label();
			ColumnTitle.Location = new Point(ColumnTitleXPos, ColumnTitleYPos);
			ColumnTitle.Text = "Column";
			ColumnTitle.AutoSize = true;

			// Add the other components to the panel
			MainPanel.Controls.Add(Title);
			MainPanel.Controls.Add(ColumnTitle);
			MainPanel.Controls.Add(LineItemStartLabel);
			MainPanel.Controls.Add(LineItemStartData);
					

			// Configure the panel location, size and other parameters
			MainPanel.Location = PanelLocation;
			MainPanel.Size = new Size(PanelXSize, PanelYSize);
			MainPanel.Font = new Font("Microsoft Sans Serif", (float)12);
			MainPanel.BorderStyle = BorderStyle.FixedSingle;

			MainPanel.ResumeLayout(false);


		}

		public Panel GetConfigurationPanel()
		{

			return MainPanel;

		}


		public bool VerifyTemplateEntries(ref List<string> Errors)
		{

			bool NoErrors = true;
			string ErrorMsg = "";

			// Get another copy of the original template data
			SingleOrderTemplateDataV1100 TemplateData;
			LocalParams.GetSOTemplateData(LocalTemplateIndex, out TemplateData);

			// A template item reference
			TemplateLineItemColumn Template;

			// Let template items attempt to verify data. 
			for (SingleOrderLineItemColumnIDV1100 u = 0; u < SingleOrderLineItemColumnIDV1100.MaxItems; u++)
			{
				// Get template item from template
				LineItemTemplate.GetLineItemColumnTemplate(u, out Template);
 
				// Attempt to set the column string value of the template item.  If false, an error occured
				bool Result = Template.SetColumnString(TemplateColumnData[(int)u].Text, out ErrorMsg);

				// An error
				if (!Result)
				{
					// Add error message to list
					Errors.Add(ErrorMsg);

					// Re-display original data
					TemplateColumnData[(int)u].Text = Template.GetColumnString;

					// Indicate that at least one error has occured
					NoErrors = false;
				}

				// No error.  Save data to system parameters
				else
				{
					TemplateData.SetColumnSettings(u, Template.GetColumn);

				}
 			}

			// Save the template data back to the local copy   *** THIS MUST BE DONE BEFORE LINE ITEM START TEST ***
			LocalParams.SaveSOTemplateData(LocalTemplateIndex, TemplateData);
			
			// Check if user entered data for line item start row is valid.  If not, add error message
			if (!LineItemTemplate.SetLineItemStartString(ref LocalParams, CSVFileType.SingleOrderFile, LocalTemplateIndex, LineItemStartData.Text, out ErrorMsg))
			{
				// Add error to list
				Errors.Add(ErrorMsg);

				// Reset original data
				LineItemStartData.Text = LineItemTemplate.GetLineItemStartString;

				// Indicate error
				NoErrors = false;
			}



			return NoErrors;
		}




	}
}
