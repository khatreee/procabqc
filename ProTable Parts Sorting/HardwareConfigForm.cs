﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace ProCABQC11
{
	public partial class HardwareConfigForm : Form
	{

		private const String DeviceTypeProRFRegistryValue = "ProRF";
		private const String DeviceTypeProMUXRegistryValue = "ProMUX3";


		private String ReceivedMsg;
		private bool MsgActive = false;
		private int ConnectionAttemptCount = 0;
		private int NumAxes;

		MeasurementInterfaceHardware LocalHardware;
		PersistentParametersV1100 LocalParams;


		enum ProRFStates
		{
			PRORF_RECEIVER_INIT, PRORF_CONFIG_OUT, PRORF_UNLEARN_X, PRORF_UNLEARN_Y, PRORF_UNLEARN_Z, PRORF_UNLEARN_ANGLE, PRORF_ENABLE_LEARN, PRORF_LEARN_X,
			PRORF_LEARN_Y, PRORF_LEARN_Z, PRORF_LEARN_ANGLE, PRORF_DISABLE_LEARN, PRORF_CONFIG_X, PRORF_CONFIG_Y, PRORF_CONFIG_Z, PRORF_CONFIG_ANGLE, PRORF_CONFIG_DONE, 
			PRORF_CONFIG_DELAY, PRORF_CONFIG_EXIT
		}

		enum ProMUXStates
		{
			PROMUX_INIT, PROMUX_ENABLE_INPUTS, PROMUX_CONFIG_INPUTS, PROMUX_CONFIG_DONE, PROMUX_CONFIG_DELAY, PROMUX_CONFIG_EXIT

		}





		private ProRFStates ConfigState = ProRFStates.PRORF_RECEIVER_INIT;
		private ProMUXStates ConfigPromuxState = ProMUXStates.PROMUX_INIT;



		public HardwareConfigForm()
		{
			InitializeComponent();

			
		}

		public HardwareConfigForm(PersistentParametersV1100 PParams, MeasurementInterfaceHardware MeasurementHardware)
		{

			InitializeComponent();

			// Save a reference for later use
			LocalHardware = MeasurementHardware;
			LocalParams = PParams;

			// Add the event
			MeasurementHardware.ConfigMessage += new MeasurementInterfaceHardware.ConfigurationMessage(DeviceMessageReceived);

			// Enable the hardware interface config mode
			LocalHardware.ConfigurationState = true;

			// Set number of axes based on Z and Angle configuration  V1.2.0.6
			if (PParams.ZAxisInUse)
			{
				if (PParams.AngleAxisInUse)
					NumAxes = 4;

				else
					NumAxes = 3;
			}

			else if(PParams.AngleAxisInUse)
				NumAxes = 3;

				
			else
				NumAxes = 2;


			timer1.Interval = 100;
			timer1.Enabled = true;

		}


	


		public void DeviceMessageReceived(String Msg)
		{

			ReceivedMsg = Msg;
			MsgActive = true;

			
		}


	


		private void timer1_Tick(object sender, EventArgs e)
		{

			if (LocalParams.MeasurementInterfaceType == MeasurementInterfaceHardware.MeasureHardwareType.ProRF)
			{

				switch (ConfigState)
				{
					case ProRFStates.PRORF_RECEIVER_INIT:

						MsgLabel.Text = "Verifying Connection with ProRF";

						// Write the version command to the device
						LocalHardware.SerialPortTransmitMessage("v");

						// Set timer for 1 sec intervals
						timer1.Interval = 1000;

						// Move to next state
						ConfigState = ProRFStates.PRORF_CONFIG_OUT;

						break;



					case ProRFStates.PRORF_CONFIG_OUT:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 20) == "ProScale RF Receiver"))
							{

								// Update dialog label
								MsgLabel.Text = "Configuring Receiver Output";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("O 3");

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigState = ProRFStates.PRORF_UNLEARN_X;

							}

							else
							{
								ConnectionAttemptCount++;

								if (ConnectionAttemptCount > 5)
								{
									MsgLabel.Text = "Failed to connect to ProRF receiver.\nVerify connections and device settings.";
									timer1.Interval = 2000;
									ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
								}
							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;


					case ProRFStates.PRORF_UNLEARN_X:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 24) == "Output Mode now set to 3"))
							{

								// Update dialog label
								MsgLabel.Text = "Disassociating X transmitter.";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("I 1");

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigState = ProRFStates.PRORF_UNLEARN_Y;

							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}


						break;


					case ProRFStates.PRORF_UNLEARN_Y:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 6) == "Axis 1"))
							{

								// Update dialog label
								MsgLabel.Text = "Disassociating Y transmitter.";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("I 2");

								// Clear flag
								MsgActive = false;

								// Move to Z
								ConfigState = ProRFStates.PRORF_UNLEARN_Z;
	
							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}


						break;


					case ProRFStates.PRORF_UNLEARN_Z:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 6) == "Axis 2"))
							{

								// Update dialog label
								MsgLabel.Text = "Disassociating Z transmitter.";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("I 3");

								// Clear flag
								MsgActive = false;

								// Move to Angle
								ConfigState = ProRFStates.PRORF_UNLEARN_ANGLE;
							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}


						break;


					case ProRFStates.PRORF_UNLEARN_ANGLE:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 6) == "Axis 3"))
							{

								// Update dialog label
								MsgLabel.Text = "Disassociating Angle transmitter.";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("I 4");

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigState = ProRFStates.PRORF_ENABLE_LEARN;
							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}


						break;



					case ProRFStates.PRORF_ENABLE_LEARN:

						try
						{
							// Verify Message
							if ((MsgActive) && ((ReceivedMsg.Substring(0, 6) == "Axis 4")))
							{

								// Update dialog label
								MsgLabel.Text = "Enabling transmitter association.";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("A 1");

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigState = ProRFStates.PRORF_LEARN_X;

							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}


						break;


					case ProRFStates.PRORF_LEARN_X:


						try
						{

							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 22) == "Associate Transmitters"))
							{

								// Update dialog label
								MsgLabel.Text = "Press Transmitter X Learn Button";

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigState = ProRFStates.PRORF_LEARN_Y;

							}

						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;



					case ProRFStates.PRORF_LEARN_Y:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 19) == "Transmitter Learned"))
							{

								if ((NumAxes == 2) || (NumAxes == 3))
								{
									// Update dialog label
									MsgLabel.Text = "Press Transmitter Y Learn Button";

									// Clear flag
									MsgActive = false;

									// Not using Z axis	AND Angle, disable learn
									if ((!LocalParams.ZAxisInUse) && (!LocalParams.AngleAxisInUse))
									{
										// Move to next state
										ConfigState = ProRFStates.PRORF_DISABLE_LEARN;
									}

									// Using Z axis.  
									else
									{
										ConfigState = ProRFStates.PRORF_LEARN_Z;
									}
									
								}

								// Only X axis
								else
								{

									SendDisableLearn();

								}


							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;


					case ProRFStates.PRORF_LEARN_Z:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 19) == "Transmitter Learned"))
							{

								// Using Z
								if (LocalParams.ZAxisInUse)
								{

									// Update dialog label
									MsgLabel.Text = "Press Transmitter Z Learn Button";

									// Clear flag
									MsgActive = false;

									// No angle, move to disable learn
									if (!LocalParams.AngleAxisInUse)
									{

										// Move to disable learn
										ConfigState = ProRFStates.PRORF_DISABLE_LEARN;
									}

									// Using Z and using angle
									else
									{
										// Move to learn Angle
										ConfigState = ProRFStates.PRORF_LEARN_ANGLE;
									}


								}

								// Using Angle   only
								else if (LocalParams.AngleAxisInUse)
								{
									// Update dialog label
									MsgLabel.Text = "Press Transmitter Angle Learn Button";

									// Clear flag
									MsgActive = false;

									// Move to next state
									ConfigState = ProRFStates.PRORF_DISABLE_LEARN;
								}

							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;



					case ProRFStates.PRORF_LEARN_ANGLE:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 19) == "Transmitter Learned"))
							{

								// Update dialog label
								MsgLabel.Text = "Press Transmitter Angle Learn Button";

								// Clear flag
								MsgActive = false;

								// Move to disable learn
								ConfigState = ProRFStates.PRORF_DISABLE_LEARN;

							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;





					case ProRFStates.PRORF_DISABLE_LEARN:

						try
						{

							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 19) == "Transmitter Learned"))
							{

								SendDisableLearn();

							}

						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;




					case ProRFStates.PRORF_CONFIG_X:


						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 22) == "Associate Transmitters"))
							{

								// Update dialog label
								MsgLabel.Text = "Configuring transmitter X";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("U 1 0");

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigState = ProRFStates.PRORF_CONFIG_Y;

								
							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;





					case ProRFStates.PRORF_CONFIG_Y:

						try
						{

							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 30) == "Axis 1 reports position in MM."))
							{

								if ((NumAxes == 2) || (NumAxes == 3))
								{
									// Update dialog label
									MsgLabel.Text = "Configuring transmitter Y";

									// Write the output command to the device
									LocalHardware.SerialPortTransmitMessage("U 2 0");

									// Clear flag
									MsgActive = false;

									// Not using Z axis
									if ((!LocalParams.ZAxisInUse) && (!LocalParams.AngleAxisInUse))
									{
										// Move to next state
										ConfigState = ProRFStates.PRORF_CONFIG_DONE;
									}

									// Using Z axis.  
									else if (LocalParams.ZAxisInUse)
									{
										ConfigState = ProRFStates.PRORF_CONFIG_Z;
									}

									else
									{
										ConfigState = ProRFStates.PRORF_CONFIG_ANGLE;
									}

								}

								// Only X
								else
								{
									// Update dialog label
									MsgLabel.Text = "Auto Configuration Complete";

									// Clear flag
									MsgActive = false;

									// Move to end state
									ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
								}


							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}
						break;



					case ProRFStates.PRORF_CONFIG_Z:

						try
						{

							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 30) == "Axis 2 reports position in MM."))
							{

								// Update dialog label
								MsgLabel.Text = "Configuring transmitter Z";

								// Write the output command to the device
								LocalHardware.SerialPortTransmitMessage("U 3 0");

								// Clear flag
								MsgActive = false;

								// No using angle. Move to end
								if (!LocalParams.AngleAxisInUse)
								{
									// Move to Done
									ConfigState = ProRFStates.PRORF_CONFIG_DONE;
								}

								// Using angle, Move to angle config
								else
								{

									ConfigState = ProRFStates.PRORF_CONFIG_ANGLE;
								}
								
							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}
						break;



					case ProRFStates.PRORF_CONFIG_ANGLE:

						try
						{

							// Verify Message
							if ((MsgActive) && ((ReceivedMsg.Substring(0, 30) == "Axis 2 reports position in MM.") ||
								(ReceivedMsg.Substring(0, 30) == "Axis 3 reports position in MM.")))
							{

								// Update dialog label
								MsgLabel.Text = "Configuring transmitter for Angle";

								// If using Z, Angle is 4
								if (LocalParams.ZAxisInUse)
								{
									// Write the output command to the device
									LocalHardware.SerialPortTransmitMessage("U 4 0");
								}

								// Not using Z, Angle is 3
								else
								{
									// Write the output command to the device
									LocalHardware.SerialPortTransmitMessage("U 3 0");
								}


								// Clear flag
								MsgActive = false;

								// Move to Done
								ConfigState = ProRFStates.PRORF_CONFIG_DONE;
							}
						}

						catch
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}
						break;




					case ProRFStates.PRORF_CONFIG_DONE:

						try
						{
							// Verify Message
							if ((MsgActive) && ((ReceivedMsg.Substring(0, 30) == "Axis 2 reports position in MM.") || (ReceivedMsg.Substring(0, 30) == "Axis 3 reports position in MM.") ||
								(ReceivedMsg.Substring(0, 30) == "Axis 4 reports position in MM.")))
							{

								// Update dialog label
								MsgLabel.Text = "Auto Configuration Complete";

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigState = ProRFStates.PRORF_CONFIG_DELAY;

							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigState = ProRFStates.PRORF_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;



					case ProRFStates.PRORF_CONFIG_DELAY:


						ConfigState = ProRFStates.PRORF_CONFIG_EXIT;

						break;



					case ProRFStates.PRORF_CONFIG_EXIT:

						timer1.Enabled = false;

						// Disable the hardware interface config mode
						LocalHardware.ConfigurationState = false;

						Close();

						break;



				}
			}

			// ProMUX 3
			else
			{
				switch (ConfigPromuxState)
				{
					case ProMUXStates.PROMUX_INIT:

						MsgLabel.Text = "Verifying Connection with ProMUX-3";

						// Write the version command to the device
						LocalHardware.SerialPortTransmitMessage("v");

						// Set timer for 1 sec intervals
						timer1.Interval = 1000;

						// Move to next state
						ConfigPromuxState = ProMUXStates.PROMUX_ENABLE_INPUTS;

						break;



					case ProMUXStates.PROMUX_ENABLE_INPUTS:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 1) == "*"))
							{

								// Update dialog label
								MsgLabel.Text = "Enabling Encoder Inputs";

								if (NumAxes == 2)
								{

									// Write the output command to the device
									LocalHardware.SerialPortTransmitMessage("M5");
								}

								else
								{

									// Write the output command to the device
									LocalHardware.SerialPortTransmitMessage("M1");
								}


								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_INPUTS;

							}

							else
							{
								ConnectionAttemptCount++;

								if (ConnectionAttemptCount > 5)
								{
									MsgLabel.Text = "Failed to connect to ProMUX-3.\nVerify connections and device settings.";
									timer1.Interval = 2000;
									ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_DELAY;
								}
							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;


					case ProMUXStates.PROMUX_CONFIG_INPUTS:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 3) == "*OK"))
							{

								// Update dialog label
								MsgLabel.Text = "Configuring Encoder Inputs";

								if (NumAxes == 2)
								{
									// Write the output command to the device
									LocalHardware.SerialPortTransmitMessage("L5");
								}

								else
								{
									// Write the output command to the device
									LocalHardware.SerialPortTransmitMessage("L1");
								}

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_DONE;

							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_DELAY;
							timer1.Interval = 2000;
						}


						break;



					case ProMUXStates.PROMUX_CONFIG_DONE:

						try
						{
							// Verify Message
							if ((MsgActive) && (ReceivedMsg.Substring(0, 3) == "*OK"))
							{

								// Update dialog label
								MsgLabel.Text = "Auto Configuration Complete";

								// Clear flag
								MsgActive = false;

								// Move to next state
								ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_DELAY;

							}
						}

						catch 
						{
							MsgLabel.Text = "Unexpected Response:\n" + ReceivedMsg;

							ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_DELAY;
							timer1.Interval = 2000;
						}

						break;


					case ProMUXStates.PROMUX_CONFIG_DELAY:


						ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_EXIT;

						break;



					case ProMUXStates.PROMUX_CONFIG_EXIT:

						timer1.Enabled = false;

						// Disable the hardware interface config mode
						LocalHardware.ConfigurationState = false;

						Close();

						break;
				}

	
			}
		}


		private void SendDisableLearn()
		{

			// Update dialog label
			MsgLabel.Text = "Disable Transmitter Learning";

			// Write the output command to the device
			LocalHardware.SerialPortTransmitMessage("A 0");

			// Clear flag
			MsgActive = false;

			// Move to next state
			ConfigState = ProRFStates.PRORF_CONFIG_X;

		}



		private void CancelBut_Click(object sender, EventArgs e)
		{

			if (LocalParams.MeasurementInterfaceType == MeasurementInterfaceHardware.MeasureHardwareType.ProRF)
			{

				ConfigState = ProRFStates.PRORF_CONFIG_DELAY;

			}

			else
			{
				ConfigPromuxState = ProMUXStates.PROMUX_CONFIG_DELAY;

			}

			timer1.Interval = 2000;
			MsgLabel.Text = "Configuration Aborted by User.\nRestart auto-configuration again.";

			// Disable the hardware interface config mode
			LocalHardware.ConfigurationState = false;


		}




		


	}
}
