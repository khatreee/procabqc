﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class TemplateIndexSelectionDialog : Form
	{
		int SelectedTemplateIndex;

		public int TemplateIndex
		{
			get { return SelectedTemplateIndex; }
		}

		public TemplateIndexSelectionDialog(PersistentParametersV1100 PParams)
		{
			InitializeComponent();

			List<string> NameStr;

			// Get templates names based on type in use
			if (PParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
				PParams.GetMOTemplateNames(out NameStr);

			else
				PParams.GetSOTemplateNames(out NameStr);

			// Populate list box
			for (int u = 0; u < NameStr.Count; u++)
			{
				NameList.Items.Add(NameStr[u]);
			}


		}

		private void OKBut_Click(object sender, EventArgs e)
		{

			// Only process if a selection made
			if (NameList.SelectedIndex != -1)
			{
				SelectedTemplateIndex = NameList.SelectedIndex;
				this.DialogResult = DialogResult.OK;
				this.Close();

			}

			// No selection
			else
				MessageBox.Show("A template MUST be selected.", "Template Selection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);


		}

		private void NameList_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (NameList.SelectedIndex != -1)
			{
				SelectedTemplateIndex = NameList.SelectedIndex;
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}
	}
}
