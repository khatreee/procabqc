﻿namespace ProCABQC11
{
	partial class PasswordDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasswordDialog));
			this.PasswordField = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.OKBut = new System.Windows.Forms.Button();
			this.CancelBut = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// PasswordField
			// 
			this.PasswordField.AcceptsReturn = true;
			this.PasswordField.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PasswordField.Location = new System.Drawing.Point(112, 31);
			this.PasswordField.Name = "PasswordField";
			this.PasswordField.PasswordChar = '.';
			this.PasswordField.Size = new System.Drawing.Size(174, 26);
			this.PasswordField.TabIndex = 0;
			this.PasswordField.UseSystemPasswordChar = true;
			this.PasswordField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PasswordField_KeyPress);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(28, 34);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(78, 20);
			this.label1.TabIndex = 1;
			this.label1.Text = "Password";
			// 
			// OKBut
			// 
			this.OKBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OKBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OKBut.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OKBut.Location = new System.Drawing.Point(57, 93);
			this.OKBut.Name = "OKBut";
			this.OKBut.Size = new System.Drawing.Size(70, 36);
			this.OKBut.TabIndex = 1;
			this.OKBut.UseVisualStyleBackColor = true;
			// 
			// CancelBut
			// 
			this.CancelBut.BackgroundImage = global::ProCABQC11.Properties.Resources.remove_48x48;
			this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBut.Location = new System.Drawing.Point(198, 93);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(70, 36);
			this.CancelBut.TabIndex = 2;
			this.CancelBut.UseVisualStyleBackColor = true;
			// 
			// PasswordDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(324, 151);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.OKBut);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.PasswordField);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PasswordDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Enter Password";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox PasswordField;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button OKBut;
		private System.Windows.Forms.Button CancelBut;
	}
}