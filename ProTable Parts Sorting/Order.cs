﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace ProCABQC11
{

	// The result state returned by LocateAndStorePartMeasurement() 
	public enum LineItemMatches
	{

		None,
		One,
        OneRotated,
		Multiple,
        MultipleRotated,
		AlreadyCompleted

	}

	public struct Tolerances
	{
		public double WidthPlusTol;
		public double WidthMinusTol;
		public double HeightPlusTol;
		public double HeightMinusTol;
		public double ThicknessPlusTol;
		public double ThicknessMinusTol;
		public double AnglePlusTol;
		public double AngleMinusTol;

	}


	
	[Serializable]
	public class Order : IEquatable<Order>, IComparable<Order>
	{




		private string OrderID = "";
		private string PacklistTemplate = "";
		private bool PacklistTemplateDefined = false;
		private string PartLabelTemplate = "";
		private bool PartLabelTemplateDefined = false;
		private string UserDefined1 = "";
		private string TestThicknessDim = "";
		private bool TestThicknessDimDefined = false;


		private int NextLineItemNumber = 1;
		private List<LineItem> LineItems;
		private CustomerInfo[] Customers = new CustomerInfo[(int)CustomersTypes.MaxCustomers];
		private StreamWriter CSVWriteStream;

		// Added on V0003
		private MeasuringUnits CSVMeasureUnits = MeasuringUnits.Inches;

		// Measuring tolerances.  Updated when an order is created and coppied from
		// template data.  Tolerance values are stored in CSV measurement units.
		private double WidthPlusAccuracy = 0.0;		
		private double WidthMinusAccuracy = 0.0;		
		private double HeightPlusAccuracy = 0.0;		
		private double HeightMinusAccuracy = 0.0;	
		private double ThicknessPlusAccuracy = 0.0;
		private double ThicknessMinusAccuracy = 0.0;
		private double AnglePlusAccuracy = 0.0;
		private double AngleMinusAccuracy = 0.0;
		


		public string OrderNumber
		{
			get { return OrderID; }
			set { OrderID = value; }
		}

		public string PackListTemplateText
		{
			get { return PacklistTemplate; }
			set { PacklistTemplate = value; }
		}

		public bool PacklistTemplateFieldDefined
		{
			get { return PacklistTemplateDefined; }
			set { PacklistTemplateDefined = value; }
		}

		
		public string PartLabelTemplateText
		{
			get { return PartLabelTemplate; }
			set { PartLabelTemplate = value; }
		}

		public bool PartLabelTemplateFieldDefined
		{
			get { return PartLabelTemplateDefined; }
			set { PartLabelTemplateDefined = value; }
		}

		public string UserDefined1Text
		{
			get { return UserDefined1; }
			set { UserDefined1 = value; }
		}

		public string TestThicknessDimText
		{
			get { return TestThicknessDim; }
			set { TestThicknessDim = value; }
		}

		public bool TestThicknessDimFieldDefined
		{
			get { return TestThicknessDimDefined; }
			set { TestThicknessDimDefined = value; }
		}
			


		public MeasuringUnits CSVMeasurementUnits
		{
			get { return CSVMeasureUnits; }
			set { CSVMeasureUnits = value; }
		}

		public double WidthPlusAcc
		{
			get { return WidthPlusAccuracy; }
			set { WidthPlusAccuracy = value; }
		}

		public double WidthMinusAcc
		{
			get { return WidthMinusAccuracy; }
			set { WidthMinusAccuracy = value; }
		}

		public double HeightPlusAcc
		{
			get { return HeightPlusAccuracy; }
			set { HeightPlusAccuracy = value; }
		}

		public double HeightMinusAcc
		{
			get { return HeightMinusAccuracy; }
			set { HeightMinusAccuracy = value; }
		}

		public double ThicknessPlusAcc
		{
			get { return ThicknessPlusAccuracy; }
			set { ThicknessPlusAccuracy = value; }
		}

		public double ThicknessMinusAcc
		{
			get { return ThicknessMinusAccuracy; }
			set { ThicknessMinusAccuracy = value; }
		}

 		public double AnglePlusAcc
		{
			get { return AnglePlusAccuracy; }
			set { AnglePlusAccuracy = value; }
		}

		public double AngleMinusAcc
		{
			get { return AngleMinusAccuracy; }
			set { AngleMinusAccuracy = value; }
		}









		// Default Constructor
		public Order()
		{

			// Create list of LineItems
			LineItems = new List<LineItem>();

			Customers[(int)CustomersTypes.Billing] = new CustomerInfo();
			Customers[(int)CustomersTypes.Shipping] = new CustomerInfo();



		}


		// Returns the specified Customer Type info
		public bool GetCustomerInfoData(out CustomerInfo RequestedCustomer, CustomersTypes Type)
		{
			// Make sure that requested index within range
			if ((int)Type < Customers.Length)
			{
				RequestedCustomer = Customers[(int)Type];
				return true;
			}

			else
			{
				RequestedCustomer = (CustomerInfo)null;
				return false;
			}

		}


		// Adds a line item to the order.  
		public bool AddLineItem(ref LineItem NewLineItem)
		{

			// If line number is -1, a line item number was not defined in the template.
			// Update the line number ID with the Order's internal next line item number
			if (NewLineItem.LineNumber == -1)
			{
				// Assign next internal line number of order
				NewLineItem.LineNumber = NextLineItemNumber++;
 
				// Add the line item to the order
				LineItems.Add(NewLineItem);

				return true;

			}


			// LineNumber has a value.  Check for duplicates
			else
			{

				for (int u = 0; u < LineItems.Count; u++)
				{
					// A duplicate found.  Do not save
					if (LineItems[u].LineNumber == NewLineItem.LineNumber)
						return false;

				}

				// If this code reached, no duplicate line numbers found.  Store the new line item
				LineItems.Add(NewLineItem);

				return true;

			}

		}


		// Searches all of the LineItems in the order for a LineItem that has matching parameters to the measured part (within tolerance)
		public LineItemMatches CheckLineItemsForPartMeasurement(out List<LineItem> PassedLineItemList, ref MeasuredPartInstance TestPart, bool Defect, out Tolerances OrderTol, bool CheckCompletedLineItems, bool UseZAxis, bool UseAngleAxis)
		{

			// Get tolerances from order and save to structure
			// Tolerances are already in CVS based measuring units
			OrderTol.HeightMinusTol = this.HeightMinusAccuracy;
			OrderTol.HeightPlusTol = this.HeightPlusAccuracy;
			OrderTol.WidthMinusTol = this.WidthMinusAccuracy;
			OrderTol.WidthPlusTol = this.WidthPlusAccuracy;
			OrderTol.ThicknessMinusTol = this.ThicknessMinusAccuracy;
			OrderTol.ThicknessPlusTol = this.ThicknessPlusAccuracy;
			OrderTol.AngleMinusTol = this.AngleMinusAccuracy;
			OrderTol.AnglePlusTol = this.AnglePlusAccuracy;
			

			LineItemMatches Result = LineItemMatches.None;

			PassedLineItemList = new List<LineItem>();

            bool RotationDetected = false;


			// For each LineItem in the list, check if the passed measured parameters are within tolerance of the LineItem nominals
			// If so, add the order's line item to the passed list.
			for (int u = 0; u < LineItems.Count; u++)
			{

                LineItemMatchResults IterationResult = LineItemMatchResults.None;

                IterationResult = LineItems[u].DoesLineItemMatchTarget(ref TestPart, OrderTol, (this.TestThicknessDimFieldDefined && UseZAxis), UseAngleAxis);

				// The passed parameters match the line item parameters within tolerance.  Add the line item to the list
                if ((IterationResult == LineItemMatchResults.Normal) || (IterationResult == LineItemMatchResults.Rotated))
                {

					// Only compare if measured quantity not already met unless overriden 
					if ((LineItems[u].QtyMeasured < LineItems[u].QtyRequired) || (CheckCompletedLineItems))
					{

						PassedLineItemList.Add(LineItems[u]);

						// If a rotation detected, latch state
						if (IterationResult == LineItemMatchResults.Rotated)
							RotationDetected = true;

					}

					else if (LineItems[u].QtyMeasured == LineItems[u].QtyRequired)
					{
						Result = LineItemMatches.AlreadyCompleted;
					}
                }

			}

			// Now, we need to examine how many items are in the passed list.  The possibilities are:
			//		
			//		None
			//		One
			//		More than 1
			//
			// If only 1, we have a single match.  Save the part instance to the LineItem
			// If more than one, we have part ambiguity.  Return the list of matching parts for the user to determine the match
			// If none, the part is out of tolerance.  Copy the order's line item list to the passed list for the user to determine the match

			// An exact single match.  Save part instance to LineItem and set return status to exact match
			if (PassedLineItemList.Count == 1)
			{
				// If this is a normal check (not for defect), save the line item
				if (!Defect)
				{
					// Save part instance to line item
					StoreMeasuredPartToLineItem(PassedLineItemList[0].LineNumber, ref TestPart, OrderTol, UseAngleAxis);

				}

				// Set return status based on rotation detected, 1 instance found regardless
				if (RotationDetected)
					Result = LineItemMatches.OneRotated;

				else
					Result = LineItemMatches.One;
				


			}

			// Multiple matches (part ambiguity)
			else if (PassedLineItemList.Count > 1)
			{

                if (RotationDetected)
                    Result = LineItemMatches.MultipleRotated;

                else
    				Result = LineItemMatches.Multiple;

			}

			// No matches and line item NOT already completed
			else if(Result != LineItemMatches.AlreadyCompleted)
			{

				// Copy all line items in order to passed list for user to determine
				for (int u = 0; u < LineItems.Count; u++)
				{
					// Only add line items that are not yet completed
					if(!LineItems[u].IsQuantityComplete)
						PassedLineItemList.Add(LineItems[u]);
				}

				// Set result to no matches
				Result = LineItemMatches.None;

			}

			return Result;

		}


		// Searches for the matching Line Item number in the list of LineItems. 
		// If found, store MeasuredPart to LineItem.  Return true for success.
		// If no match found, return false.
		public bool StoreMeasuredPartToLineItem(int LineItemNumberID, ref MeasuredPartInstance TestPart, Tolerances OrderTol, Boolean UseAngleAxis)
		{

			bool Result = false;

			// Look through all line items to find a match
			for (int u = 0; u < LineItems.Count; u++)
			{
				if (LineItems[u].LineNumber == LineItemNumberID)
				{
					LineItems[u].AddMeasuredPart(ref TestPart, OrderTol, this.TestThicknessDimFieldDefined, UseAngleAxis);
					Result = true;
					break;
				}
			}

			return Result;

		}

		public int NumberOfLineItems()
		{

			return LineItems.Count;

		}


		// Returns a reference to a LineItem based on an index
		// If index out of range, null and false
		public bool GetLineItem(out LineItem Item, int Index)
		{

			bool Result = false;


			if (Index < LineItems.Count)
			{
				Item = LineItems[Index];
				Result = true;
			}

			else
			{
				Item = (LineItem)null;
			}

			return Result;

		}


		// Returns a LineItem based on the requested line number string
		// If none found, returns null and false;
		public bool GetLineItem(out LineItem Item, string LineItemString)
		{
			bool Result = false;
			Item = (LineItem)null;

			for (int u = 0; u < LineItems.Count; u++)
			{
				if (LineItems[u].LineNumber == Convert.ToInt32(LineItemString))
				{
					Item = LineItems[u];
					Result = true;
					break;
				}
			}


			return Result;
			
		}


		// Determines if order is complete by comparing each line items' qty require to qty measured
		// if quantities equal for all line items, then order is complete
		public bool IsOrderComplete()
		{

			bool Result = true;


			for (int Index = 0; Index < LineItems.Count; Index++)
			{

				if (!LineItems[Index].IsQuantityComplete)
				{
					Result = false;
					break;
				}
			}

				return Result;
			
		}


		// Default comparer for Order type.
		public int CompareTo(Order CompareOrder)
		{
			// A null value means that this object is greater.
			if (CompareOrder == null)
				return 1;

			else
				return this.OrderID.CompareTo(CompareOrder.OrderID);
		}



		public bool Equals(Order other)
		{
			if (other == null) return false;
			return (this.OrderID.Equals(other.OrderID));
		}




		// Writes the selected customer data to the CSV file
		private void WriteOrderCSVCustomerData(CustomerInfo Customer, CustomersTypes CType, PersistentParametersV1100 PParams)
		{


			// Write a header based on the customer type
			if (CType == CustomersTypes.Billing)
				CSVWriteStream.WriteLine("Bill To Customer");

			else
				CSVWriteStream.WriteLine("Ship To Customer");


			// Write each customer field
			CSVWriteStream.WriteLine(string.Format("Customer Name:, {0}", Customer.CustomerName));
			CSVWriteStream.WriteLine(string.Format("Address 1:, {0}", Customer.Address1));
			CSVWriteStream.WriteLine(string.Format("Address 2:, {0}", Customer.Address2));
			CSVWriteStream.WriteLine(string.Format("City:, {0}", Customer.City));
			CSVWriteStream.WriteLine(string.Format("State:, {0}", Customer.State));
			CSVWriteStream.WriteLine(string.Format("Zip:, {0}", Customer.Zip));
			CSVWriteStream.WriteLine(string.Format("Country:, {0}", Customer.Country));
			CSVWriteStream.WriteLine(string.Format("Contact Name:, {0}", Customer.ContactName));
			CSVWriteStream.WriteLine(string.Format("Telephone:, {0}", Customer.TelephoneNumber));

			// Write a blank line
			CSVWriteStream.WriteLine("");


		}


		// Creates the Order CSV output file
		private OrderCSVCreationResultCodes CreateOrderCSVFile(string Name, PersistentParametersV1100 PParams)
		{

			try
			{

				// Try to open the stream for the CSV write
				using (CSVWriteStream = new StreamWriter(Name))
				{

					// Start writing the data

					// Order Data
					CSVWriteStream.WriteLine(string.Format("Order ID:, {0}", OrderNumber));

					// CSV Units type.  Same as CSV unit import
					CSVWriteStream.WriteLine(string.Format("Units:, {0}", CSVMeasureUnits));

					// Write a blank line
					CSVWriteStream.WriteLine("");

					// Write Bill to customer data
					WriteOrderCSVCustomerData(Customers[(int)CustomersTypes.Billing], CustomersTypes.Billing, PParams);

					// Write Ship to customer data
					WriteOrderCSVCustomerData(Customers[(int)CustomersTypes.Shipping], CustomersTypes.Shipping, PParams);

					// Write each line item of the order along with the measurement instances
					for (int u = 0; u < LineItems.Count; u++)
						LineItems[u].WritePartDataToCSVFile(CSVWriteStream, PParams, CSVMeasureUnits, this.TestThicknessDimDefined);

					// Write end of file message
					CSVWriteStream.WriteLine(string.Format("End of Order: {0}", this.OrderID));

					// Close the stream
					CSVWriteStream.Close();

					return OrderCSVCreationResultCodes.Success;

				}

			}



			// A problem opening the file
			catch (Exception e)
			{

				MessageBox.Show(string.Format("Unable to open: {0}\nError: {1}", Name, e.Message), "Order CSV File Create Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				return OrderCSVCreationResultCodes.Failure;

			}
			


		}

		// Generate a CSV file showing order information, line items and part instance measurements
		public OrderCSVCreationResultCodes GenerateOrderCSVFile(PersistentParametersV1100 PParams)
		{

			// Create path and file name to proposed order based on system params Order path and Order ID

			// Check if path ends with \.  If so, strip
			string Path = PParams.CompletedOrderFilePathStr;
			char[] Delimit = { '\\' };
 			Path = PParams.CompletedOrderFilePathStr.TrimEnd(Delimit);

			string FileName = string.Format(@"{0}\{1}.csv", Path, this.OrderNumber);

			// Check if file exists.  Normally it should not since the CSV file is only written when the order is complete
			if (File.Exists(FileName))
			{

				if (MessageBox.Show(string.Format("Order: {0} Already Exists.\nOverwrite the data?", OrderNumber), "WARNING:  File Already Exists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					// File already exists but user wants to overwrite it.  Try to create it.
					return CreateOrderCSVFile(FileName, PParams);
				}

				// User does not want to overrite data
				else
				{
					return OrderCSVCreationResultCodes.Failure;
				}

			}

			// File does not exist, try to create it
			else
			{
				// Try to create the OrderCSV file
				return CreateOrderCSVFile(FileName, PParams);
			}

		}

	}
}
