﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{
	public class ParamStruct
	{

		private string PString = "";
		private int PInt = 0;
		private double PDouble = 0.0;

		public string StringVal
		{

			get { return PString; }
			set { PString = value; }
		}

		public int IntVal
		{
			get { return PInt; }
			set { PInt = value; }
		}

		public double FPVal
		{
			get { return PDouble; }
			set { PDouble = value; }
		}

	}

	public enum DataConversionResult
	{
		Error = -1,
		Undefined,
		String,
		Integer,
		Float
	}


	public enum ParameterDataType
	{

		String = DataConversionResult.String,
		Integer,
		Float
	}


	// The result codes for template line parsing
	public enum TemplateParsingResultCodes
	{
		Error = -1,
		BlankLine,
		Success
	}


	// The customer info enumerations
	public enum CustomersTypes
	{
		Billing,
		Shipping,
		MaxCustomers
	}

	// The result codes from a CSV reader
	public enum CSVReaderResultCodes
	{
		FileOpenError = -1,
		ParsingErrors,
		Success
	}

	// Result codes returned by an order when creating a completed order CSV file
	public enum OrderCSVCreationResultCodes
	{

		Failure = -1,
		Success
	}


	public static class PositionConversion
	{

		public static string ConvertUnits(double Position, MeasuringUnits SourceUnits, MeasuringUnits ConvertUnits, int MMPrecision, int InchPrecision)
		{

			double Value = Position;
			string PosStr;
			int Precision;

			// Translate only if units differ
			if (SourceUnits != ConvertUnits)
			{
				// Is source is mm, convert units must be inches
				if (SourceUnits == MeasuringUnits.Millimeters)
				{
					Value = Position / 25.4;
					Precision = InchPrecision;
				}

				// Source is in inches, convert units must be in mm
				else
				{
					Value = Position * 25.4;
					Precision = MMPrecision;

				}
			}

			// No conversion.  Units in inches
			else if (ConvertUnits == MeasuringUnits.Inches)
				Precision = InchPrecision;

			// No conversion.  Units in mm
			else
				Precision = MMPrecision;


			if (Precision == 4)
				PosStr = String.Format("{0:F4}", Value);

			else if (Precision == 3)
				PosStr = String.Format("{0:F3}", Value);

			else if (Precision == 2)
				PosStr = String.Format("{0:F2}", Value);

			else
				PosStr = String.Format("{0:F1}", Value);

			return PosStr;
		}
	}


}
