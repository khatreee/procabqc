﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class ViewMeasuredPartsDialog : Form
	{
		public ViewMeasuredPartsDialog()
		{
			InitializeComponent();
		}

		public ViewMeasuredPartsDialog(ref LineItem Item)
		{

			InitializeComponent();

			Text = string.Format("Line Item Measured Parts Viewer for Line Item {0}", Item.LineNumber);

			// Do this for each measured part in Line Item
			for (int u = 0; u < Item.MeasuredPartsCount; u++)
			{

				MeasuredPartInstance Part;

				if (Item.GetMeasuredPart(out Part, u))
				{

					// Create a list view item
					ListViewItem LVItem = new ListViewItem((u + 1).ToString());

					// Add time
					LVItem.SubItems.Add(Part.MeasuredTime);

					// Add measured height
					LVItem.SubItems.Add(string.Format("{0:F3}", Part.Height));

					// Add measured height spec
					if(Part.HeightInSpec)
						LVItem.SubItems.Add("YES");

					else
						LVItem.SubItems.Add("NO");

					// Add measured width
					LVItem.SubItems.Add(string.Format("{0:F3}", Part.Width));

					// Add measured width spec
					if (Part.WidthInSpec)
						LVItem.SubItems.Add("YES");

					else
						LVItem.SubItems.Add("NO");

					// Add measurement type string
					if (Part.IsManualMeasurement)
						LVItem.SubItems.Add("MANUAL");

					else
						LVItem.SubItems.Add("SYSTEM");

					// Add the list view item to the list
					PartListView.Items.Add(LVItem);


				}
			}
		}


	}
}
