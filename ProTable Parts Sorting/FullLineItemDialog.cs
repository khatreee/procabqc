﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class FullLineItemDialog : Form
	{

	
		public FullLineItemDialog()
		{
			InitializeComponent();
		}


		public FullLineItemDialog(ref LineItem Item, bool ShowYesNo, double HeightErrorVal, double WidthErrorVal, double RotHeightErrorVal, double RotWidthErrorVal, double ThicknessErrorVal, bool UseThickness)
		{

			InitializeComponent();

			// Load the fields
			LineNumberText.Text = Item.LineNumber.ToString();
			PartIDText.Text = Item.PartIdStr;
			HeightText.Text = string.Format("{0:F3}", Item.Height);
			WidthText.Text = string.Format("{0:F3}", Item.Width);
			ThicknessText.Text = string.Format("{0:F3}", Item.Thickness);
			MaterialText.Text = Item.MaterialText;
			PartStyleText.Text = Item.StyleText;
			CommentText.Text = Item.CommentText;
			QtyReqText.Text = Item.QtyRequired.ToString();
			UserDef1Text.Text = Item.UserDef1Text;
			UserDef2Text.Text = Item.UserDef2Text;
			UserDef3Text.Text = Item.UserDef3Text;
			TypeText.Text = Item.TypeText;
			FinishText.Text = Item.FinishText;
			HingingText.Text = Item.HingingText;
			MachiningText.Text = Item.MachiningText;
			AssemblyText.Text = Item.AssemblyText;
			LocationText.Text = Item.LocationText;
			UserMsgText.Text = Item.UserMessageText;


			// Show the appropriate buttons/positions
			// If yes and no required, re-label them
			if (ShowYesNo)
			{

				this.Text = "Manual Line Item Selection";
				QuestionText.Visible = true;

				NormalHeightError.Text = string.Format("{0:F3}", HeightErrorVal);
				NormalWidthError.Text = string.Format("{0:F3}", WidthErrorVal);
				RotatedHeightError.Text = string.Format("{0:F3}", RotHeightErrorVal);
				RotatedWidthError.Text = string.Format("{0:F3}", RotWidthErrorVal);
				ThicknessError.Text = string.Format("{0:F3}", ThicknessErrorVal);

				if (!UseThickness)
				{
					ThicknessError.Visible = false;
					ThicknessErrorLabel.Visible = false;
				}


			}

			// Show only OK.
			else
			{
				CancelBut.Visible = false;

				int XPos = this.Width / 2;
				int ButX = OkBut.Width / 2;



				Point Pt = new Point(XPos - ButX, 550);
				OkBut.Location = Pt;

				NormalHeightErrorLabel.Visible = false;
				NormalHeightError.Visible = false;
				NormalWidthErrorLabel.Visible = false;
				NormalWidthError.Visible = false;
				RotatedWidthError.Visible = false;
				RotatedWidthErrorLabel.Visible = false;
				RotatedHeightError.Visible = false;
				RotatedHeightErrorLabel.Visible = false;
				ThicknessErrorLabel.Visible = false;
				ThicknessError.Visible = false;


			}

		}



	}
}
