﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{


	// The enumeration that identifies the LineItem Template columns
	public enum SingleOrderHeaderCellIDV1100
	{

		OrderID,
		PacklistTemplate,
		PartLabelTemplate,
		OrderUserDefined1,
		TestThicknessDim,
		BillCustomerName,
		BillAddress1,
		BillAddress2,
		BillCity,
		BillState,
		BillZip,
		BillCountry,
		BillContact,
		BillTelephone,
		BillEmail,
		ShipCustomerName,
		ShipAddress1,
		ShipAddress2,
		ShipCity,
		ShipState,
		ShipZip,
		ShipCountry,
		ShipContact,
		ShipTelephone,
		ShipEmail,
		MaxItems
	}

	public enum SingleOrderHeaderTemplateResultCodes
	{
		Error = -1,
		BlankLine,
		Success
	}

	public class SingleOrderHeaderTemplate
	{

		TemplateCell[] HeaderTemplateItems;

		private const string TemplateRegistryKeyStr = "Single Order Header";

		private string[] CellNames = {			"Order ID",
												"Pack List Template",
												"Part Label Template",
												"Order User Def 1",
												"Test Depth Dim",
												"Bill Customer Name",
												"Bill Address 1",
												"Bill Address 2",
												"Bill City",
												"Bill State",
												"Bill Zip",
												"Bill Country",
												"Bill Contact",
												"Bill Telephone",
												"Bill Email",
												"Ship Customer Name",
												"Ship Address 1",
												"Ship Address 2",
												"Ship City",
												"Ship State",
												"Ship Zip",
												"Ship Country",
												"Ship Contact",
												"Ship Telephone",
												"Ship Email"

									   };


		private const string OutOfBoundsColumnErrorMsg = "Template defines a column that is outside the column range of the CSV file";
		private const string BlankOrderIDErrorMsg = "The Order ID template is defined but the CSV file data cell is empty";



		public bool GetHeaderCellTemplate(SingleOrderHeaderCellIDV1100 Index, out TemplateCell HeaderCellTemplate)
		{
			if ((Index < SingleOrderHeaderCellIDV1100.MaxItems) && (Index >= SingleOrderHeaderCellIDV1100.OrderID))
			{
				HeaderCellTemplate = HeaderTemplateItems[(int)Index];
				return true;
			}

			else
			{
				HeaderCellTemplate = (TemplateCell)null;
				return false;
			}
		}



		// The default constructor
		public SingleOrderHeaderTemplate()
		{

		}

		public SingleOrderHeaderTemplate(ref PersistentParametersV1100 PParams, int TemplateIndex)
		{

			// Get the template data
			SingleOrderTemplateDataV1100 TemplateData;

			if (PParams.GetSOTemplateData(TemplateIndex, out TemplateData))
			{

				// Create the array of cell templates
				HeaderTemplateItems = new TemplateCell[(int)SingleOrderHeaderCellIDV1100.MaxItems];

				// Create each template item
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.OrderID] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.OrderID), CellNames[(int)SingleOrderHeaderCellIDV1100.OrderID], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.PacklistTemplate), CellNames[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.PartLabelTemplate), CellNames[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.OrderUserDefined1), CellNames[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.TestThicknessDim), CellNames[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillCustomerName] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillCustomerName), CellNames[(int)SingleOrderHeaderCellIDV1100.BillCustomerName], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillAddress1] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillAddress1), CellNames[(int)SingleOrderHeaderCellIDV1100.BillAddress1], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillAddress2] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillAddress2), CellNames[(int)SingleOrderHeaderCellIDV1100.BillAddress2], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillCity] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillCity), CellNames[(int)SingleOrderHeaderCellIDV1100.BillCity], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillState] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillState), CellNames[(int)SingleOrderHeaderCellIDV1100.BillState], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillZip] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillZip), CellNames[(int)SingleOrderHeaderCellIDV1100.BillZip], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillCountry] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillCountry), CellNames[(int)SingleOrderHeaderCellIDV1100.BillCountry], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillContact] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillContact), CellNames[(int)SingleOrderHeaderCellIDV1100.BillContact], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillTelephone] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillTelephone), CellNames[(int)SingleOrderHeaderCellIDV1100.BillTelephone], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.BillEmail] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.BillEmail), CellNames[(int)SingleOrderHeaderCellIDV1100.BillEmail], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipCustomerName] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipCustomerName), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipCustomerName], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipAddress1] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipAddress1), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipAddress1], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipAddress2] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipAddress2), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipAddress2], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipCity] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipCity), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipCity], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipState] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipState), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipState], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipZip] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipZip), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipZip], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipCountry] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipCountry), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipCountry], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipContact] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipContact), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipContact], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipTelephone] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipTelephone), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipTelephone], ParameterDataType.String, false);
				HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.ShipEmail] = new TemplateCell(TemplateData.GetCellSettings(SingleOrderHeaderCellIDV1100.ShipEmail), CellNames[(int)SingleOrderHeaderCellIDV1100.ShipEmail], ParameterDataType.String, false);

			}
			
		}


		// Converts a comma delimited string to an array of strings with each string representing a column entry
		// if one or more columns have data, return true
		private bool ParseLineToColumns(string InputStr, out string[] ColumnStrings)
		{

			Char Delimit = ',';

			ColumnStrings = InputStr.Split(Delimit);

			bool Blank = false;

			// Check each string in array.  If any string (column) has data, return true
			for (int u = 0; u < ColumnStrings.Count(); u++)
			{

				if (ColumnStrings[u].Length > 0)
				{
					Blank = true;
					break;
				}

			}

			return Blank;
		}

		public TemplateParsingResultCodes ParseSingleOrderHeaderLine(int CSVLineNumber, string CSVLineString, ref Order CurOrder, ref List<string> ErrorMsgLst)
		{

			// Create an array of strings to pass to the line parser
			string[] Columns;

			// Parse CSV line into columns of strings.  Returns true if one or more lines parsed.  False if all empty fields
			bool Result = ParseLineToColumns(CSVLineString, out Columns);

			// If true, one or more non-blank fields exist.
			if (Result)
			{
				// Cycle through all the template items
				for (int u = 0; u < HeaderTemplateItems.Length; u++)
				{
					// Check if a LineItemTemplate is defined.  If so, continue
					if (HeaderTemplateItems[u].IsDefined(CSVLineNumber))
					{
						// Get the zero based column data stored in the template
						int ActiveColumn = HeaderTemplateItems[u].GetColumn;

						// Make sure the stored column is within range of the CSV file columns passed
						if (ActiveColumn < Columns.Length)
						{
							// Create a new data structure for the parameter parsing
							ParamStruct Data = new ParamStruct();
							string ErrorMsg = "";

							// Attempt to parse the cooresponding string into the correct data type based on the template
							DataConversionResult ConvertResult = HeaderTemplateItems[u].GetParameterValue(Columns[ActiveColumn], ref Data, ref ErrorMsg);

							// If an error occured, add the line number to the error message and add to the error list
							if (ConvertResult == DataConversionResult.Error)
							{
								string Msg = string.Format("Error - Line: {0}, {1}", CSVLineNumber + 1, ErrorMsg);
								ErrorMsgLst.Add(Msg);
							}

							// The conversion succeded. Continue to process
							else
							{
								// Check if this cell is the OrderID.  If so and data string is empty, write error for defined but blank order ID field
								if (u == (int)SingleOrderHeaderCellIDV1100.OrderID)
								{
									// Check for blank string
									if (Data.StringVal.Length == 0)
									{
										string Msg = string.Format("Error - Line: {0}, {1}", CSVLineNumber + 1, BlankOrderIDErrorMsg);
										ErrorMsgLst.Add(Msg);
									}

									// The data string is not empty.  Copy the string to the OrderID of the order
									else
									{
										CurOrder.OrderNumber = Data.StringVal;
									}
								}

								// Not the order ID cell.  Store the data to the appropriate order field
								else
								{
									UpdateOrderData(ref CurOrder, ref Data, (SingleOrderHeaderCellIDV1100)u);
								}
							}
						}

						// Define column outside CSV file column count
						else
						{
							string Msg = string.Format("Error - Line: {0}, {1} at column {2}: {3}", CSVLineNumber + 1, HeaderTemplateItems[u].FieldName, HeaderTemplateItems[u].GetColumnString, OutOfBoundsColumnErrorMsg);
							ErrorMsgLst.Add(Msg);
						}
					}
				}

				// Check if Error list has one or more errors.  If so, return error
				if (ErrorMsgLst.Count > 0)
				{
					return TemplateParsingResultCodes.Error;
				}

				// No errors, return success
				else
				{
					return TemplateParsingResultCodes.Success;
				}
			}

			// A blank line detected
			else
			{
				return TemplateParsingResultCodes.BlankLine;
			}
		}


		private void UpdateOrderData(ref Order CurOrder, ref ParamStruct Data, SingleOrderHeaderCellIDV1100 ID)
		{

			// Get the customer info data
			CustomerInfo BillTo;
			CustomerInfo ShipTo;

			CurOrder.GetCustomerInfoData(out BillTo, CustomersTypes.Billing);
			CurOrder.GetCustomerInfoData(out ShipTo, CustomersTypes.Shipping);

			switch (ID)
			{

				case SingleOrderHeaderCellIDV1100.BillCustomerName :

					BillTo.CustomerName = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillAddress1 :

					BillTo.Address1 = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillAddress2 :

					BillTo.Address2 = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillCity :

					BillTo.City = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillState :

					BillTo.State = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillZip :

					BillTo.Zip = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillCountry :

					BillTo.Country = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillContact :

					BillTo.ContactName = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillTelephone :

					BillTo.TelephoneNumber = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.BillEmail:

					BillTo.Email = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipCustomerName :

					ShipTo.CustomerName = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipAddress1 :

					ShipTo.Address1 = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipAddress2 :

					ShipTo.Address2 = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipCity :

					ShipTo.City = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipState :

					ShipTo.State = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipZip :

					ShipTo.Zip = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipCountry :

					ShipTo.Country = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipContact :

					ShipTo.ContactName = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipTelephone :

					ShipTo.TelephoneNumber = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.ShipEmail:

					ShipTo.Email = Data.StringVal;
					break;

				case SingleOrderHeaderCellIDV1100.PacklistTemplate:

					CurOrder.PackListTemplateText = Data.StringVal;

					// Because the field is being set, it must be defined within the template
					CurOrder.PacklistTemplateFieldDefined = true;

					break;

				case SingleOrderHeaderCellIDV1100.PartLabelTemplate:

					CurOrder.PartLabelTemplateText = Data.StringVal;

					// Because the field is being set, it must be defined within the template
					CurOrder.PartLabelTemplateFieldDefined = true;

					break;

				case SingleOrderHeaderCellIDV1100.OrderUserDefined1:

					CurOrder.UserDefined1Text = Data.StringVal;
					break;


				case SingleOrderHeaderCellIDV1100.TestThicknessDim:

					CurOrder.TestThicknessDimText = Data.StringVal;

					// Only set thickness defined to true IF string == "true" or "TRUE"
					if ((CurOrder.TestThicknessDimText == "yes") || (CurOrder.TestThicknessDimText == "YES"))
						CurOrder.TestThicknessDimFieldDefined = true;

					break;



			}
		}



		public bool IsOrderIDDefined()
		{
			return HeaderTemplateItems[(int)SingleOrderHeaderCellIDV1100.OrderID].IsDefined();

		}


        // FUNCTIONS FOR IMPORTING AND EXPORTING TEMPLATE DATA BELOW....

        // Gets the Header cell field name text
        public bool GetHeaderFieldText(out string FieldName, int Index)
        {
           
            bool Result = false;

            if(Index < (int)SingleOrderHeaderCellIDV1100.MaxItems)
            {
                Result = true;
                FieldName = CellNames[Index];
            }

            else
            {
                Result = false;
                FieldName = "";
            }

            return Result;
        }


        // Searches field names for a match with passed name.  If match found, return true and index to field
		public bool LocateHeaderFieldNameIndex(string TargetName, out SingleOrderHeaderCellIDV1100 Index)
        {

            bool Result = false;
			Index = SingleOrderHeaderCellIDV1100.MaxItems;

            // Check each cell name for comparison
			for (SingleOrderHeaderCellIDV1100 u = SingleOrderHeaderCellIDV1100.OrderID; u < SingleOrderHeaderCellIDV1100.MaxItems; u++)
            {
                // If text matches, save index and break
                if (CellNames[(int)u].Equals(TargetName))
                {
                    Result = true;
                    Index = u;
                    break;
                }
            }

            return Result;
        }


	}
}
