﻿namespace ProCABQC11
{
	partial class SingleOrderCSVConfigurationForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SingleOrderCSVConfigurationForm));
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.textBox11 = new System.Windows.Forms.TextBox();
			this.textBox12 = new System.Windows.Forms.TextBox();
			this.textBox13 = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.textBox14 = new System.Windows.Forms.TextBox();
			this.textBox15 = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.textBox16 = new System.Windows.Forms.TextBox();
			this.textBox17 = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.textBox18 = new System.Windows.Forms.TextBox();
			this.textBox19 = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.textBox20 = new System.Windows.Forms.TextBox();
			this.textBox21 = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.textBox22 = new System.Windows.Forms.TextBox();
			this.textBox23 = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.textBox24 = new System.Windows.Forms.TextBox();
			this.textBox25 = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.textBox26 = new System.Windows.Forms.TextBox();
			this.textBox27 = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label32 = new System.Windows.Forms.Label();
			this.textBox28 = new System.Windows.Forms.TextBox();
			this.textBox29 = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.textBox30 = new System.Windows.Forms.TextBox();
			this.textBox31 = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.textBox32 = new System.Windows.Forms.TextBox();
			this.textBox33 = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.textBox34 = new System.Windows.Forms.TextBox();
			this.textBox35 = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this.textBox36 = new System.Windows.Forms.TextBox();
			this.textBox37 = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.textBox38 = new System.Windows.Forms.TextBox();
			this.textBox39 = new System.Windows.Forms.TextBox();
			this.label26 = new System.Windows.Forms.Label();
			this.textBox40 = new System.Windows.Forms.TextBox();
			this.textBox41 = new System.Windows.Forms.TextBox();
			this.label27 = new System.Windows.Forms.Label();
			this.textBox42 = new System.Windows.Forms.TextBox();
			this.textBox43 = new System.Windows.Forms.TextBox();
			this.label28 = new System.Windows.Forms.Label();
			this.textBox44 = new System.Windows.Forms.TextBox();
			this.textBox45 = new System.Windows.Forms.TextBox();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label33 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(160, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Line Item Number Column";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(155, 25);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(47, 22);
			this.textBox1.TabIndex = 1;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(155, 62);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(47, 22);
			this.textBox2.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(14, 65);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Part ID Column";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(155, 99);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(47, 22);
			this.textBox3.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(14, 102);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(133, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Quantity Req Column";
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(155, 136);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(47, 22);
			this.textBox4.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(14, 139);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(143, 16);
			this.label4.TabIndex = 6;
			this.label4.Text = "Nominal Width Column";
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(155, 173);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(47, 22);
			this.textBox5.TabIndex = 9;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(14, 176);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(148, 16);
			this.label5.TabIndex = 8;
			this.label5.Text = "Nominal Height Column";
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(155, 210);
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(47, 22);
			this.textBox6.TabIndex = 11;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(14, 213);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(171, 16);
			this.label6.TabIndex = 10;
			this.label6.Text = "Nominal Thickness Column";
			// 
			// textBox7
			// 
			this.textBox7.Location = new System.Drawing.Point(155, 247);
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new System.Drawing.Size(47, 22);
			this.textBox7.TabIndex = 13;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(14, 250);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(104, 16);
			this.label7.TabIndex = 12;
			this.label7.Text = "Material Column";
			// 
			// textBox8
			// 
			this.textBox8.Location = new System.Drawing.Point(155, 284);
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new System.Drawing.Size(47, 22);
			this.textBox8.TabIndex = 15;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(14, 287);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(86, 16);
			this.label8.TabIndex = 14;
			this.label8.Text = "Style Column";
			// 
			// textBox9
			// 
			this.textBox9.Location = new System.Drawing.Point(155, 321);
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new System.Drawing.Size(47, 22);
			this.textBox9.TabIndex = 17;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(14, 324);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(113, 16);
			this.label9.TabIndex = 16;
			this.label9.Text = "Comment Column";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(15, 84);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(51, 13);
			this.label10.TabIndex = 18;
			this.label10.Text = "Customer";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(102, 45);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(42, 13);
			this.label11.TabIndex = 19;
			this.label11.Text = "Column";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(173, 45);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(29, 13);
			this.label12.TabIndex = 20;
			this.label12.Text = "Row";
			// 
			// textBox10
			// 
			this.textBox10.Location = new System.Drawing.Point(97, 80);
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new System.Drawing.Size(47, 20);
			this.textBox10.TabIndex = 21;
			// 
			// textBox11
			// 
			this.textBox11.Location = new System.Drawing.Point(165, 80);
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new System.Drawing.Size(47, 20);
			this.textBox11.TabIndex = 22;
			// 
			// textBox12
			// 
			this.textBox12.Location = new System.Drawing.Point(165, 117);
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new System.Drawing.Size(47, 20);
			this.textBox12.TabIndex = 25;
			// 
			// textBox13
			// 
			this.textBox13.Location = new System.Drawing.Point(97, 117);
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new System.Drawing.Size(47, 20);
			this.textBox13.TabIndex = 24;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(15, 121);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(54, 13);
			this.label13.TabIndex = 23;
			this.label13.Text = "Address 1";
			// 
			// textBox14
			// 
			this.textBox14.Location = new System.Drawing.Point(165, 191);
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new System.Drawing.Size(47, 20);
			this.textBox14.TabIndex = 31;
			// 
			// textBox15
			// 
			this.textBox15.Location = new System.Drawing.Point(97, 191);
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new System.Drawing.Size(47, 20);
			this.textBox15.TabIndex = 30;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(15, 195);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(24, 13);
			this.label14.TabIndex = 29;
			this.label14.Text = "City";
			// 
			// textBox16
			// 
			this.textBox16.Location = new System.Drawing.Point(165, 154);
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new System.Drawing.Size(47, 20);
			this.textBox16.TabIndex = 28;
			// 
			// textBox17
			// 
			this.textBox17.Location = new System.Drawing.Point(97, 154);
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new System.Drawing.Size(47, 20);
			this.textBox17.TabIndex = 27;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(15, 158);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(54, 13);
			this.label15.TabIndex = 26;
			this.label15.Text = "Address 2";
			// 
			// textBox18
			// 
			this.textBox18.Location = new System.Drawing.Point(165, 339);
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new System.Drawing.Size(47, 20);
			this.textBox18.TabIndex = 43;
			// 
			// textBox19
			// 
			this.textBox19.Location = new System.Drawing.Point(97, 339);
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new System.Drawing.Size(47, 20);
			this.textBox19.TabIndex = 42;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(15, 343);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(75, 13);
			this.label16.TabIndex = 41;
			this.label16.Text = "Contact Name";
			// 
			// textBox20
			// 
			this.textBox20.Location = new System.Drawing.Point(165, 302);
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new System.Drawing.Size(47, 20);
			this.textBox20.TabIndex = 40;
			// 
			// textBox21
			// 
			this.textBox21.Location = new System.Drawing.Point(97, 302);
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new System.Drawing.Size(47, 20);
			this.textBox21.TabIndex = 39;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(15, 306);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(43, 13);
			this.label17.TabIndex = 38;
			this.label17.Text = "Country";
			// 
			// textBox22
			// 
			this.textBox22.Location = new System.Drawing.Point(165, 265);
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new System.Drawing.Size(47, 20);
			this.textBox22.TabIndex = 37;
			// 
			// textBox23
			// 
			this.textBox23.Location = new System.Drawing.Point(97, 265);
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new System.Drawing.Size(47, 20);
			this.textBox23.TabIndex = 36;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(15, 269);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(22, 13);
			this.label18.TabIndex = 35;
			this.label18.Text = "Zip";
			// 
			// textBox24
			// 
			this.textBox24.Location = new System.Drawing.Point(165, 228);
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new System.Drawing.Size(47, 20);
			this.textBox24.TabIndex = 34;
			// 
			// textBox25
			// 
			this.textBox25.Location = new System.Drawing.Point(97, 228);
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new System.Drawing.Size(47, 20);
			this.textBox25.TabIndex = 33;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(15, 232);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(32, 13);
			this.label19.TabIndex = 32;
			this.label19.Text = "State";
			// 
			// textBox26
			// 
			this.textBox26.Location = new System.Drawing.Point(165, 376);
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new System.Drawing.Size(47, 20);
			this.textBox26.TabIndex = 46;
			// 
			// textBox27
			// 
			this.textBox27.Location = new System.Drawing.Point(97, 376);
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new System.Drawing.Size(47, 20);
			this.textBox27.TabIndex = 45;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(15, 380);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(58, 13);
			this.label20.TabIndex = 44;
			this.label20.Text = "Telephone";
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.label32);
			this.panel1.Controls.Add(this.textBox28);
			this.panel1.Controls.Add(this.textBox29);
			this.panel1.Controls.Add(this.label21);
			this.panel1.Controls.Add(this.textBox30);
			this.panel1.Controls.Add(this.textBox31);
			this.panel1.Controls.Add(this.label22);
			this.panel1.Controls.Add(this.textBox32);
			this.panel1.Controls.Add(this.textBox33);
			this.panel1.Controls.Add(this.label23);
			this.panel1.Controls.Add(this.textBox34);
			this.panel1.Controls.Add(this.textBox35);
			this.panel1.Controls.Add(this.label24);
			this.panel1.Controls.Add(this.textBox36);
			this.panel1.Controls.Add(this.textBox37);
			this.panel1.Controls.Add(this.label25);
			this.panel1.Controls.Add(this.textBox38);
			this.panel1.Controls.Add(this.textBox39);
			this.panel1.Controls.Add(this.label26);
			this.panel1.Controls.Add(this.textBox40);
			this.panel1.Controls.Add(this.textBox41);
			this.panel1.Controls.Add(this.label27);
			this.panel1.Controls.Add(this.textBox42);
			this.panel1.Controls.Add(this.textBox43);
			this.panel1.Controls.Add(this.label28);
			this.panel1.Controls.Add(this.textBox44);
			this.panel1.Controls.Add(this.textBox45);
			this.panel1.Controls.Add(this.label29);
			this.panel1.Controls.Add(this.label30);
			this.panel1.Controls.Add(this.label31);
			this.panel1.Location = new System.Drawing.Point(510, 27);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(247, 440);
			this.panel1.TabIndex = 47;
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Location = new System.Drawing.Point(14, 32);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(36, 13);
			this.label32.TabIndex = 76;
			this.label32.Text = "Bill To";
			// 
			// textBox28
			// 
			this.textBox28.Location = new System.Drawing.Point(173, 377);
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new System.Drawing.Size(47, 20);
			this.textBox28.TabIndex = 75;
			// 
			// textBox29
			// 
			this.textBox29.Location = new System.Drawing.Point(105, 377);
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new System.Drawing.Size(47, 20);
			this.textBox29.TabIndex = 74;
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(22, 381);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(58, 13);
			this.label21.TabIndex = 73;
			this.label21.Text = "Telephone";
			// 
			// textBox30
			// 
			this.textBox30.Location = new System.Drawing.Point(173, 340);
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new System.Drawing.Size(47, 20);
			this.textBox30.TabIndex = 72;
			// 
			// textBox31
			// 
			this.textBox31.Location = new System.Drawing.Point(105, 340);
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new System.Drawing.Size(47, 20);
			this.textBox31.TabIndex = 71;
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(22, 344);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(75, 13);
			this.label22.TabIndex = 70;
			this.label22.Text = "Contact Name";
			// 
			// textBox32
			// 
			this.textBox32.Location = new System.Drawing.Point(173, 303);
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new System.Drawing.Size(47, 20);
			this.textBox32.TabIndex = 69;
			// 
			// textBox33
			// 
			this.textBox33.Location = new System.Drawing.Point(105, 303);
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new System.Drawing.Size(47, 20);
			this.textBox33.TabIndex = 68;
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(22, 307);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(43, 13);
			this.label23.TabIndex = 67;
			this.label23.Text = "Country";
			// 
			// textBox34
			// 
			this.textBox34.Location = new System.Drawing.Point(173, 266);
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new System.Drawing.Size(47, 20);
			this.textBox34.TabIndex = 66;
			// 
			// textBox35
			// 
			this.textBox35.Location = new System.Drawing.Point(105, 266);
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new System.Drawing.Size(47, 20);
			this.textBox35.TabIndex = 65;
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(22, 270);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(22, 13);
			this.label24.TabIndex = 64;
			this.label24.Text = "Zip";
			// 
			// textBox36
			// 
			this.textBox36.Location = new System.Drawing.Point(173, 229);
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new System.Drawing.Size(47, 20);
			this.textBox36.TabIndex = 63;
			// 
			// textBox37
			// 
			this.textBox37.Location = new System.Drawing.Point(105, 229);
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new System.Drawing.Size(47, 20);
			this.textBox37.TabIndex = 62;
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(22, 233);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(32, 13);
			this.label25.TabIndex = 61;
			this.label25.Text = "State";
			// 
			// textBox38
			// 
			this.textBox38.Location = new System.Drawing.Point(173, 192);
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new System.Drawing.Size(47, 20);
			this.textBox38.TabIndex = 60;
			// 
			// textBox39
			// 
			this.textBox39.Location = new System.Drawing.Point(105, 192);
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new System.Drawing.Size(47, 20);
			this.textBox39.TabIndex = 59;
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(22, 196);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(24, 13);
			this.label26.TabIndex = 58;
			this.label26.Text = "City";
			// 
			// textBox40
			// 
			this.textBox40.Location = new System.Drawing.Point(173, 155);
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new System.Drawing.Size(47, 20);
			this.textBox40.TabIndex = 57;
			// 
			// textBox41
			// 
			this.textBox41.Location = new System.Drawing.Point(105, 155);
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new System.Drawing.Size(47, 20);
			this.textBox41.TabIndex = 56;
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(22, 159);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(54, 13);
			this.label27.TabIndex = 55;
			this.label27.Text = "Address 2";
			// 
			// textBox42
			// 
			this.textBox42.Location = new System.Drawing.Point(173, 118);
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new System.Drawing.Size(47, 20);
			this.textBox42.TabIndex = 54;
			// 
			// textBox43
			// 
			this.textBox43.Location = new System.Drawing.Point(105, 118);
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new System.Drawing.Size(47, 20);
			this.textBox43.TabIndex = 53;
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(22, 122);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(54, 13);
			this.label28.TabIndex = 52;
			this.label28.Text = "Address 1";
			// 
			// textBox44
			// 
			this.textBox44.Location = new System.Drawing.Point(173, 81);
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new System.Drawing.Size(47, 20);
			this.textBox44.TabIndex = 51;
			// 
			// textBox45
			// 
			this.textBox45.Location = new System.Drawing.Point(105, 81);
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new System.Drawing.Size(47, 20);
			this.textBox45.TabIndex = 50;
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Location = new System.Drawing.Point(181, 46);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(29, 13);
			this.label29.TabIndex = 49;
			this.label29.Text = "Row";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Location = new System.Drawing.Point(110, 46);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(42, 13);
			this.label30.TabIndex = 48;
			this.label30.Text = "Column";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Location = new System.Drawing.Point(22, 85);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(51, 13);
			this.label31.TabIndex = 47;
			this.label31.Text = "Customer";
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Controls.Add(this.label33);
			this.panel2.Controls.Add(this.textBox26);
			this.panel2.Controls.Add(this.textBox27);
			this.panel2.Controls.Add(this.label20);
			this.panel2.Controls.Add(this.textBox18);
			this.panel2.Controls.Add(this.textBox19);
			this.panel2.Controls.Add(this.label16);
			this.panel2.Controls.Add(this.textBox20);
			this.panel2.Controls.Add(this.textBox21);
			this.panel2.Controls.Add(this.label17);
			this.panel2.Controls.Add(this.textBox22);
			this.panel2.Controls.Add(this.textBox23);
			this.panel2.Controls.Add(this.label18);
			this.panel2.Controls.Add(this.textBox24);
			this.panel2.Controls.Add(this.textBox25);
			this.panel2.Controls.Add(this.label19);
			this.panel2.Controls.Add(this.textBox14);
			this.panel2.Controls.Add(this.textBox15);
			this.panel2.Controls.Add(this.label14);
			this.panel2.Controls.Add(this.textBox16);
			this.panel2.Controls.Add(this.textBox17);
			this.panel2.Controls.Add(this.label15);
			this.panel2.Controls.Add(this.textBox12);
			this.panel2.Controls.Add(this.textBox13);
			this.panel2.Controls.Add(this.label13);
			this.panel2.Controls.Add(this.textBox11);
			this.panel2.Controls.Add(this.textBox10);
			this.panel2.Controls.Add(this.label12);
			this.panel2.Controls.Add(this.label11);
			this.panel2.Controls.Add(this.label10);
			this.panel2.Location = new System.Drawing.Point(258, 27);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(237, 440);
			this.panel2.TabIndex = 48;
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Location = new System.Drawing.Point(24, 35);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(44, 13);
			this.label33.TabIndex = 47;
			this.label33.Text = "Ship To";
			// 
			// panel3
			// 
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel3.Controls.Add(this.textBox9);
			this.panel3.Controls.Add(this.label9);
			this.panel3.Controls.Add(this.textBox8);
			this.panel3.Controls.Add(this.label8);
			this.panel3.Controls.Add(this.textBox7);
			this.panel3.Controls.Add(this.label7);
			this.panel3.Controls.Add(this.textBox6);
			this.panel3.Controls.Add(this.label6);
			this.panel3.Controls.Add(this.textBox5);
			this.panel3.Controls.Add(this.label5);
			this.panel3.Controls.Add(this.textBox4);
			this.panel3.Controls.Add(this.label4);
			this.panel3.Controls.Add(this.textBox3);
			this.panel3.Controls.Add(this.label3);
			this.panel3.Controls.Add(this.textBox2);
			this.panel3.Controls.Add(this.label2);
			this.panel3.Controls.Add(this.textBox1);
			this.panel3.Controls.Add(this.label1);
			this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panel3.Location = new System.Drawing.Point(23, 27);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(215, 440);
			this.panel3.TabIndex = 49;
			// 
			// SingleOrderCSVConfigurationForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 564);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel3);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SingleOrderCSVConfigurationForm";
			this.ShowInTaskbar = false;
			this.Text = "SingleOrderCSVConfigurationForm";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBox8;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textBox9;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox textBox10;
		private System.Windows.Forms.TextBox textBox11;
		private System.Windows.Forms.TextBox textBox12;
		private System.Windows.Forms.TextBox textBox13;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textBox14;
		private System.Windows.Forms.TextBox textBox15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox textBox16;
		private System.Windows.Forms.TextBox textBox17;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textBox18;
		private System.Windows.Forms.TextBox textBox19;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox textBox20;
		private System.Windows.Forms.TextBox textBox21;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox textBox22;
		private System.Windows.Forms.TextBox textBox23;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox textBox24;
		private System.Windows.Forms.TextBox textBox25;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox textBox26;
		private System.Windows.Forms.TextBox textBox27;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TextBox textBox28;
		private System.Windows.Forms.TextBox textBox29;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox textBox30;
		private System.Windows.Forms.TextBox textBox31;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox textBox32;
		private System.Windows.Forms.TextBox textBox33;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox textBox34;
		private System.Windows.Forms.TextBox textBox35;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox textBox36;
		private System.Windows.Forms.TextBox textBox37;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox textBox38;
		private System.Windows.Forms.TextBox textBox39;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.TextBox textBox40;
		private System.Windows.Forms.TextBox textBox41;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox textBox42;
		private System.Windows.Forms.TextBox textBox43;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.TextBox textBox44;
		private System.Windows.Forms.TextBox textBox45;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Panel panel3;
	}
}