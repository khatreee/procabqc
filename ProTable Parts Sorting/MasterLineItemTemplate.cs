﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ProCABQC11
{


	public class MasterLineItemTemplate
	{

		// An array of line item columns (not yet defined)
		protected TemplateLineItemColumn[] LineItemTemplateItems;
		protected int LineItemStartPos = 0;

		// Error messages
		protected const string ColumnIDOutOfBounds = "Error: The specified column template ID is out of range.";

	
		public string GetLineItemStartString
		{
			get { return (LineItemStartPos + 1).ToString(); }
		}


		public bool SetLineItemStartString(ref PersistentParametersV1100 PParam, CSVFileType Type, int TemplateIndex, string Str, out string ErrorMsg)
		{

			// Test for empty string, not allowed
			if (Str.Length == 0)
			{
				ErrorMsg = string.Format("Error: Line item starting row - Row cannot be undefined");
				return false;
			}


			else
			{
				// For each character in string, make sure they are numbers
				for (int u = 0; u < Str.Length; u++)
				{
					if (!Char.IsDigit(Str[u]))
					{
						ErrorMsg = string.Format("Error: Line item starting row - Invalid characters in input");
						return false;
					}

				}

				// If this code reached, string is numeric.  Convert it	.
				try
				{
					int Temp = Convert.ToInt32(Str) - 1;

					// Make sure row is 0 or higher
					if (Temp >= 0)
					{
						if (Type == CSVFileType.SingleOrderFile)
						{
							// Get the template data
							SingleOrderTemplateDataV1100 TemplateData;
							PParam.GetSOTemplateData(TemplateIndex, out TemplateData);

							// Save the line item start
							TemplateData.LineItemStart = Temp;

							// Save template back to local parameters
							PParam.SaveSOTemplateData(TemplateIndex, TemplateData);
						}


						else if (Type == CSVFileType.CutlistFile)
						{
							// Get template data
							CutlistTemplateDataV1100 TemplateData;
							PParam.GetMOTemplateData(TemplateIndex, out TemplateData);

							// Save the line item start
							TemplateData.LineItemStart = Temp;

							// Save template data back to local parameters
							PParam.SaveMOTemplateData(TemplateIndex, TemplateData);
						}


						ErrorMsg = "";

						return true;
					}

					// A negative number (zero based), error
					else
					{
						ErrorMsg = string.Format("Error: Line item starting row - Value cannot be less that 1");
						return false;
					}

				}

				// Just in case..
				catch
				{
					ErrorMsg = string.Format("Error: Line item starting row - Invalid characters in input");
					return false;

				}
			}
		}

		public int LineItemStart
		{
			get { return LineItemStartPos; }
		}



		// Default Constructor
		public MasterLineItemTemplate()
		{

		}

		// Converts a comma delimited string to an array of strings with each string representing a column entry
		// if one or more columns have data, return true
		protected bool ParseLineToColumns(string InputStr, out string[] ColumnStrings)
		{

			Char Delimit = ',';

			ColumnStrings = InputStr.Split(Delimit);

			bool Blank = false;

			// Check each string in array.  If any string (column) has data, return true
			for (int u = 0; u < ColumnStrings.Count(); u++)
			{

				if (ColumnStrings[u].Length > 0)
				{
					Blank = true;
					break;
				}

			}

			return Blank;
		}


		

	}
}
