namespace ProCABQC11
{
	partial class DataBoundReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.CustomerShipInfo = new System.Data.DataTable();
			this.dataColumn9 = new System.Data.DataColumn();
			this.dataColumn10 = new System.Data.DataColumn();
			this.dataColumn11 = new System.Data.DataColumn();
			this.dataColumn12 = new System.Data.DataColumn();
			this.dataColumn13 = new System.Data.DataColumn();
			this.dataColumn14 = new System.Data.DataColumn();
			this.dataColumn15 = new System.Data.DataColumn();
			this.dataColumn16 = new System.Data.DataColumn();
			this.dataColumn17 = new System.Data.DataColumn();
			this.dataColumn18 = new System.Data.DataColumn();
			this.CustomerBillInfo = new System.Data.DataTable();
			this.id = new System.Data.DataColumn();
			this.Description = new System.Data.DataColumn();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.SystemData = new System.Data.DataSet();
			this.LineItems = new System.Data.DataTable();
			this.dataColumn19 = new System.Data.DataColumn();
			this.dataColumn20 = new System.Data.DataColumn();
			this.dataColumn21 = new System.Data.DataColumn();
			this.dataColumn22 = new System.Data.DataColumn();
			this.dataColumn23 = new System.Data.DataColumn();
			this.dataColumn24 = new System.Data.DataColumn();
			this.dataColumn25 = new System.Data.DataColumn();
			this.dataColumn26 = new System.Data.DataColumn();
			this.dataColumn27 = new System.Data.DataColumn();
			this.dataColumn28 = new System.Data.DataColumn();
			this.dataColumn30 = new System.Data.DataColumn();
			this.dataColumn31 = new System.Data.DataColumn();
			this.dataColumn59 = new System.Data.DataColumn();
			this.dataColumn60 = new System.Data.DataColumn();
			this.dataColumn61 = new System.Data.DataColumn();
			this.dataColumn62 = new System.Data.DataColumn();
			this.dataColumn63 = new System.Data.DataColumn();
			this.dataColumn64 = new System.Data.DataColumn();
			this.dataColumn65 = new System.Data.DataColumn();
			this.MeasuredInstance = new System.Data.DataTable();
			this.dataColumn33 = new System.Data.DataColumn();
			this.dataColumn34 = new System.Data.DataColumn();
			this.dataColumn35 = new System.Data.DataColumn();
			this.dataColumn36 = new System.Data.DataColumn();
			this.dataColumn37 = new System.Data.DataColumn();
			this.dataColumn38 = new System.Data.DataColumn();
			this.dataColumn39 = new System.Data.DataColumn();
			this.dataColumn40 = new System.Data.DataColumn();
			this.dataColumn41 = new System.Data.DataColumn();
			this.dataColumn42 = new System.Data.DataColumn();
			this.dataColumn55 = new System.Data.DataColumn();
			this.dataColumn56 = new System.Data.DataColumn();
			this.dataColumn57 = new System.Data.DataColumn();
			this.dataColumn58 = new System.Data.DataColumn();
			this.CompanyData = new System.Data.DataTable();
			this.dataColumn32 = new System.Data.DataColumn();
			this.dataColumn43 = new System.Data.DataColumn();
			this.dataColumn44 = new System.Data.DataColumn();
			this.dataColumn45 = new System.Data.DataColumn();
			this.dataColumn46 = new System.Data.DataColumn();
			this.dataColumn47 = new System.Data.DataColumn();
			this.dataColumn48 = new System.Data.DataColumn();
			this.dataColumn49 = new System.Data.DataColumn();
			this.dataColumn50 = new System.Data.DataColumn();
			this.dataColumn29 = new System.Data.DataColumn();
			this.OrderData = new System.Data.DataTable();
			this.dataColumn51 = new System.Data.DataColumn();
			this.dataColumn52 = new System.Data.DataColumn();
			this.dataColumn53 = new System.Data.DataColumn();
			this.dataColumn54 = new System.Data.DataColumn();
			this.dataColumn66 = new System.Data.DataColumn();
			((System.ComponentModel.ISupportInitialize)(this.CustomerShipInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CustomerBillInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SystemData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LineItems)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MeasuredInstance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CompanyData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.OrderData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 133;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// CustomerShipInfo
			// 
			this.CustomerShipInfo.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18});
			this.CustomerShipInfo.TableName = "CustomerShipInfo";
			// 
			// dataColumn9
			// 
			this.dataColumn9.ColumnName = "ShipName";
			// 
			// dataColumn10
			// 
			this.dataColumn10.ColumnName = "ShipAddress1";
			// 
			// dataColumn11
			// 
			this.dataColumn11.ColumnName = "ShipAddress2";
			// 
			// dataColumn12
			// 
			this.dataColumn12.ColumnName = "ShipCity";
			// 
			// dataColumn13
			// 
			this.dataColumn13.ColumnName = "ShipState";
			// 
			// dataColumn14
			// 
			this.dataColumn14.ColumnName = "ShipZip";
			// 
			// dataColumn15
			// 
			this.dataColumn15.ColumnName = "ShipCountry";
			// 
			// dataColumn16
			// 
			this.dataColumn16.ColumnName = "ShipContact";
			// 
			// dataColumn17
			// 
			this.dataColumn17.ColumnName = "ShipTelephone";
			// 
			// dataColumn18
			// 
			this.dataColumn18.ColumnName = "ShipEmail";
			// 
			// CustomerBillInfo
			// 
			this.CustomerBillInfo.Columns.AddRange(new System.Data.DataColumn[] {
            this.id,
            this.Description,
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8});
			this.CustomerBillInfo.TableName = "CustomerBillInfo";
			// 
			// id
			// 
			this.id.ColumnName = "BillName";
			// 
			// Description
			// 
			this.Description.ColumnName = "BillAddress1";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "BillAddress2";
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "BillCity";
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "BillState";
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "BillZip";
			// 
			// dataColumn5
			// 
			this.dataColumn5.ColumnName = "BillCountry";
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "BillContact";
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "BillTelephone";
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "BillEmail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 42;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 30;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// SystemData
			// 
			this.SystemData.DataSetName = "SystemData";
			this.SystemData.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("LineItemsToMeasuredParts", "LineItems", "Measurement", new string[] {
                        "LineItemNumber"}, new string[] {
                        "LineItemNumber"}, false)});
			this.SystemData.Tables.AddRange(new System.Data.DataTable[] {
            this.CustomerBillInfo,
            this.CustomerShipInfo,
            this.LineItems,
            this.MeasuredInstance,
            this.CompanyData,
            this.OrderData});
			// 
			// LineItems
			// 
			this.LineItems.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25,
            this.dataColumn26,
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn30,
            this.dataColumn31,
            this.dataColumn59,
            this.dataColumn60,
            this.dataColumn61,
            this.dataColumn62,
            this.dataColumn63,
            this.dataColumn64,
            this.dataColumn65,
            this.dataColumn66});
			this.LineItems.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "LineItemNumber"}, false)});
			this.LineItems.TableName = "LineItems";
			// 
			// dataColumn19
			// 
			this.dataColumn19.ColumnName = "LineItemNumber";
			// 
			// dataColumn20
			// 
			this.dataColumn20.ColumnName = "PartID";
			// 
			// dataColumn21
			// 
			this.dataColumn21.ColumnName = "QuantityRequired";
			this.dataColumn21.DataType = typeof(int);
			// 
			// dataColumn22
			// 
			this.dataColumn22.ColumnName = "QuantityMeasured";
			this.dataColumn22.DataType = typeof(int);
			// 
			// dataColumn23
			// 
			this.dataColumn23.ColumnName = "NominalWidth";
			this.dataColumn23.DataType = typeof(double);
			// 
			// dataColumn24
			// 
			this.dataColumn24.ColumnName = "NominalHeight";
			this.dataColumn24.DataType = typeof(double);
			// 
			// dataColumn25
			// 
			this.dataColumn25.ColumnName = "NominalThickness";
			this.dataColumn25.DataType = typeof(double);
			// 
			// dataColumn26
			// 
			this.dataColumn26.ColumnName = "Material";
			// 
			// dataColumn27
			// 
			this.dataColumn27.ColumnName = "Style";
			// 
			// dataColumn28
			// 
			this.dataColumn28.ColumnName = "Comment";
			// 
			// dataColumn30
			// 
			this.dataColumn30.ColumnName = "User1";
			// 
			// dataColumn31
			// 
			this.dataColumn31.ColumnName = "User2";
			// 
			// dataColumn59
			// 
			this.dataColumn59.ColumnName = "Hinging";
			// 
			// dataColumn60
			// 
			this.dataColumn60.ColumnName = "Finish";
			// 
			// dataColumn61
			// 
			this.dataColumn61.ColumnName = "Type";
			// 
			// dataColumn62
			// 
			this.dataColumn62.ColumnName = "Location";
			// 
			// dataColumn63
			// 
			this.dataColumn63.ColumnName = "Machining";
			// 
			// dataColumn64
			// 
			this.dataColumn64.ColumnName = "Assembly";
			// 
			// dataColumn65
			// 
			this.dataColumn65.ColumnName = "UserMsg";
			// 
			// MeasuredInstance
			// 
			this.MeasuredInstance.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn33,
            this.dataColumn34,
            this.dataColumn35,
            this.dataColumn36,
            this.dataColumn37,
            this.dataColumn38,
            this.dataColumn39,
            this.dataColumn40,
            this.dataColumn41,
            this.dataColumn42,
            this.dataColumn55,
            this.dataColumn56,
            this.dataColumn57,
            this.dataColumn58});
			this.MeasuredInstance.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("LineItemsToMeasuredParts", "LineItems", new string[] {
                        "LineItemNumber"}, new string[] {
                        "LineItemNumber"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
			this.MeasuredInstance.TableName = "Measurement";
			// 
			// dataColumn33
			// 
			this.dataColumn33.ColumnName = "MeasuredWidth";
			this.dataColumn33.DataType = typeof(double);
			// 
			// dataColumn34
			// 
			this.dataColumn34.ColumnName = "MeasuredHeight";
			this.dataColumn34.DataType = typeof(double);
			// 
			// dataColumn35
			// 
			this.dataColumn35.ColumnName = "MeasuredDepth";
			this.dataColumn35.DataType = typeof(double);
			// 
			// dataColumn36
			// 
			this.dataColumn36.ColumnName = "WidthInSpec";
			// 
			// dataColumn37
			// 
			this.dataColumn37.ColumnName = "HeightInSpec";
			// 
			// dataColumn38
			// 
			this.dataColumn38.ColumnName = "DepthInSpec";
			// 
			// dataColumn39
			// 
			this.dataColumn39.ColumnName = "TimeDate";
			// 
			// dataColumn40
			// 
			this.dataColumn40.ColumnName = "PartInstanceNumber";
			this.dataColumn40.DataType = typeof(int);
			// 
			// dataColumn41
			// 
			this.dataColumn41.ColumnName = "ManuallyMeasured";
			// 
			// dataColumn42
			// 
			this.dataColumn42.ColumnName = "LineItemNumber";
			// 
			// dataColumn55
			// 
			this.dataColumn55.ColumnName = "WidthError";
			this.dataColumn55.DataType = typeof(double);
			// 
			// dataColumn56
			// 
			this.dataColumn56.ColumnName = "HeightError";
			this.dataColumn56.DataType = typeof(double);
			// 
			// dataColumn57
			// 
			this.dataColumn57.ColumnName = "DepthError";
			this.dataColumn57.DataType = typeof(double);
			// 
			// dataColumn58
			// 
			this.dataColumn58.ColumnName = "PartMeasuredRotated";
			// 
			// CompanyData
			// 
			this.CompanyData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn32,
            this.dataColumn43,
            this.dataColumn44,
            this.dataColumn45,
            this.dataColumn46,
            this.dataColumn47,
            this.dataColumn48,
            this.dataColumn49,
            this.dataColumn50,
            this.dataColumn29});
			this.CompanyData.TableName = "CompanyData";
			// 
			// dataColumn32
			// 
			this.dataColumn32.ColumnName = "CompanyName";
			// 
			// dataColumn43
			// 
			this.dataColumn43.ColumnName = "CompanyAddress1";
			// 
			// dataColumn44
			// 
			this.dataColumn44.ColumnName = "CompanyAddress2";
			// 
			// dataColumn45
			// 
			this.dataColumn45.ColumnName = "CompanyCity";
			// 
			// dataColumn46
			// 
			this.dataColumn46.ColumnName = "CompanyState";
			// 
			// dataColumn47
			// 
			this.dataColumn47.ColumnName = "CompanyZip";
			// 
			// dataColumn48
			// 
			this.dataColumn48.ColumnName = "CompanyTelephone";
			// 
			// dataColumn49
			// 
			this.dataColumn49.ColumnName = "CompanyCountry";
			// 
			// dataColumn50
			// 
			this.dataColumn50.ColumnName = "CompanyLogoFileName";
			// 
			// dataColumn29
			// 
			this.dataColumn29.ColumnName = "CompanyURL";
			// 
			// OrderData
			// 
			this.OrderData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn51,
            this.dataColumn52,
            this.dataColumn53,
            this.dataColumn54});
			this.OrderData.TableName = "OrderData";
			// 
			// dataColumn51
			// 
			this.dataColumn51.ColumnName = "OrderID";
			// 
			// dataColumn52
			// 
			this.dataColumn52.ColumnName = "MeasurementUnits";
			// 
			// dataColumn53
			// 
			this.dataColumn53.ColumnName = "MeasurementCompletionDate";
			// 
			// dataColumn54
			// 
			this.dataColumn54.ColumnName = "OrderUserDef1";
			// 
			// dataColumn66
			// 
			this.dataColumn66.ColumnName = "User3";
			// 
			// DataBoundReport
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter});
			this.DataSource = this.SystemData;
			this.PrinterName = "HP Photosmart C8100 series via LogMeIn";
			this.Version = "9.1";
			this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DataBoundReport_BeforePrint);
			((System.ComponentModel.ISupportInitialize)(this.CustomerShipInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CustomerBillInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SystemData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LineItems)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MeasuredInstance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CompanyData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.OrderData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private System.Data.DataSet SystemData;
		private System.Data.DataTable CustomerBillInfo;
		private System.Data.DataColumn id;
		private System.Data.DataColumn Description;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataColumn dataColumn3;
		private System.Data.DataColumn dataColumn4;
		private System.Data.DataColumn dataColumn5;
		private System.Data.DataColumn dataColumn6;
		private System.Data.DataColumn dataColumn7;
		private System.Data.DataColumn dataColumn8;
		private System.Data.DataTable CustomerShipInfo;
		private System.Data.DataColumn dataColumn9;
		private System.Data.DataColumn dataColumn10;
		private System.Data.DataColumn dataColumn11;
		private System.Data.DataColumn dataColumn12;
		private System.Data.DataColumn dataColumn13;
		private System.Data.DataColumn dataColumn14;
		private System.Data.DataColumn dataColumn15;
		private System.Data.DataColumn dataColumn16;
		private System.Data.DataColumn dataColumn17;
		private System.Data.DataColumn dataColumn18;
		private System.Data.DataTable LineItems;
		private System.Data.DataColumn dataColumn19;
		private System.Data.DataColumn dataColumn20;
		private System.Data.DataColumn dataColumn21;
		private System.Data.DataColumn dataColumn22;
		private System.Data.DataColumn dataColumn23;
		private System.Data.DataColumn dataColumn24;
		private System.Data.DataColumn dataColumn25;
		private System.Data.DataColumn dataColumn26;
		private System.Data.DataColumn dataColumn27;
		private System.Data.DataColumn dataColumn28;
		private System.Data.DataColumn dataColumn30;
		private System.Data.DataColumn dataColumn31;
		private System.Data.DataTable MeasuredInstance;
		private System.Data.DataColumn dataColumn33;
		private System.Data.DataColumn dataColumn34;
		private System.Data.DataColumn dataColumn35;
		private System.Data.DataColumn dataColumn36;
		private System.Data.DataColumn dataColumn37;
		private System.Data.DataColumn dataColumn38;
		private System.Data.DataColumn dataColumn39;
		private System.Data.DataColumn dataColumn40;
		private System.Data.DataColumn dataColumn41;
		private System.Data.DataColumn dataColumn42;
		private System.Data.DataColumn dataColumn55;
		private System.Data.DataColumn dataColumn56;
		private System.Data.DataColumn dataColumn57;
		private System.Data.DataColumn dataColumn58;
		private System.Data.DataTable CompanyData;
		private System.Data.DataColumn dataColumn32;
		private System.Data.DataColumn dataColumn43;
		private System.Data.DataColumn dataColumn44;
		private System.Data.DataColumn dataColumn45;
		private System.Data.DataColumn dataColumn46;
		private System.Data.DataColumn dataColumn47;
		private System.Data.DataColumn dataColumn48;
		private System.Data.DataColumn dataColumn49;
		private System.Data.DataColumn dataColumn50;
		private System.Data.DataTable OrderData;
		private System.Data.DataColumn dataColumn51;
		private System.Data.DataColumn dataColumn52;
		private System.Data.DataColumn dataColumn53;
		private System.Data.DataColumn dataColumn29;
		private System.Data.DataColumn dataColumn54;
		private System.Data.DataColumn dataColumn59;
		private System.Data.DataColumn dataColumn60;
		private System.Data.DataColumn dataColumn61;
		private System.Data.DataColumn dataColumn62;
		private System.Data.DataColumn dataColumn63;
		private System.Data.DataColumn dataColumn64;
		private System.Data.DataColumn dataColumn65;
		private System.Data.DataColumn dataColumn66;
	}
}
