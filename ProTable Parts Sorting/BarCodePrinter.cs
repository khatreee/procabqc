﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;

namespace ProCABQC11
{
	[Serializable]
	public class BarCodePrinter : ICloneable
	{

		public object Clone()
		{

			MemoryStream ms = new MemoryStream();

			BinaryFormatter bf = new BinaryFormatter();

			bf.Serialize(ms, this);

			ms.Position = 0;

			object obj = bf.Deserialize(ms);

			ms.Close();

			return obj;


		}

		[NonSerialized]
		private SerialPort PrinterPort;

		private string PortNameStr = "";
		private int XDimXPos = 15;
		private int XDimYPos = 10;
		private int YDimXPos = 15;
		private int YDimYPos = 40;
		private int IDXPos = 15;
		private int IDYPos = 130;
		private int TimeStampXPos = 15;
		private int TimeStampYPos = 130;
		private int BarcodeXPos = 15;
		private int BarcodeYPos = 190;
		private int LiNoXPos = 15;
		private int LiNoYPos = 100;
		private int PrtNoXPos = 240;
		private int PrtNoYPos = 100;
		private int OrderXPos = 15;
		private int OrderYPos = 70;
		private const string BarcodeType = "1";
		private const int BarcodeNarrowWidth = 2;
		private const int BarcodeWideWidth = 1;
		private string Readable = "N";
		private int BarcodeHeight = 30;
		private bool Enabled = false;
		private bool PrintBarCode = false;

		private const string ConfigFileName = "Label Printer.dat";




		// Accessor for port name
		public string PortName
		{

			get 
			{
				if (PortNameStr == "")
					return "";

				else
					return PrinterPort.PortName;
			}
			set 
			{
				if (value != "")
				{
					PortNameStr = value;
				}

			}

		}

		public int WidthXPos
		{
			get { return XDimXPos; }
			set 
			{ 
				XDimXPos = value;
				
			}
		}

		public int WidthYPos
		{

			get { return XDimYPos; }
			set 
			{ 
				XDimYPos = value;

			}
		}

		public int HeightXPos
		{
			get { return YDimXPos; }
			set 
			{ 
				YDimXPos = value;

			}
		}

		public int HeightYPos
		{

			get { return YDimYPos; }
			set
			{ 
				YDimYPos = value;

			}
		}

		public int PartIDXPos
		{
			get { return IDXPos; }
			set 
			{ 
				IDXPos = value;

			}
		}

		public int PartIDYPos
		{

			get { return IDYPos; }
			set 
			{ 
				IDYPos = value;

			}
		}


		public int TimeXPos
		{
			get { return TimeStampXPos; }
			set
			{ 
				TimeStampXPos = value;

			}
		}

		public int TimeYPos
		{

			get { return TimeStampYPos; }
			set
			{ 
				TimeStampYPos = value;

			}
		}


		public int OrderIDXPos
		{
			get { return OrderXPos; }
			set
			{
				OrderXPos = value;
			}
		}

		public int OrderIDYPos
		{
			get { return OrderYPos; }
			set { OrderYPos = value; }
		}



		public int BarCodeXPos
		{
			get { return BarcodeXPos; }
			set
			{ 
				BarcodeXPos = value;

			}
		}

		public int BarCodeYPos
		{

			get { return BarcodeYPos; }
			set
			{ 
				BarcodeYPos = value;

			}
		}


		public int BarCodeHeight
		{

			get {return BarcodeHeight;}
			set 
			{
					if(value >= 10 && value <= 80)
						BarcodeHeight = value;

					else
						BarcodeHeight = 10;



			}
		}

		public bool BarcodeHumanReadable
		{

			get 
			{
				if(Readable == "B")
					return true;
				else
					return false;

			}

			set
			{
				if(value)
					Readable = "B";

				else
					Readable = "N";


			}
		}

		public bool PrinterEnabled
		{

			get { return Enabled; }
			set 
			{ 
				Enabled = value;

			}

		}


		public bool UseBarCode
		{

			get { return PrintBarCode; }
			set 
			{ 
				PrintBarCode = value;

			}
		}


		public int LineNoXPos
		{
			get { return LiNoXPos; }
			set { LiNoXPos = value; }
		}

		public int LineNoYPos
		{
			get { return LiNoYPos; }
			set { LiNoYPos = value; }
		}

		public int PartNoXPos
		{
			get { return PrtNoXPos; }
			set { PrtNoXPos = value; }
		}

		public int PartNoYPos
		{
			get { return PrtNoYPos; }
			set { PrtNoYPos = value; }
		}




		// Create the printer constructor
		public BarCodePrinter()
		{

			// Initially create and configure serial port
			ConfigureSerialPort();

		}

		private void ConfigureSerialPort()
		{

			PrinterPort = new SerialPort();

			// Set some non-configurable defaults
			PrinterPort.BaudRate = 9600;
			PrinterPort.DataBits = 8;
			PrinterPort.StopBits = StopBits.One;

			// Set port name if not null
			if (PortNameStr != "")
				PrinterPort.PortName = PortNameStr;

		}



		// Save parameters to disk in local directory
		public void SaveParameters()
		{

			BinaryFormatter Formatter = new BinaryFormatter();

			using (Stream fstream = new FileStream(ConfigFileName, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				Formatter.Serialize(fstream, this);
			}

		}

		// Attempt to read parameter data file
		// Reads data if file exists and returns true
		// Returns false if no data file exists
		public bool ReadParameters(ref BarCodePrinter Reference)
		{

			// Check if data file exists.  If so, open it and return true.  If not, false
			if (File.Exists(ConfigFileName))
			{

				BinaryFormatter Formatter = new BinaryFormatter();

				try
				{
					using (Stream fstream = File.OpenRead(ConfigFileName))
					{
						Reference = (BarCodePrinter)Formatter.Deserialize(fstream);
					}

					// Create and configure serial port	after deserialization
					Reference.ConfigureSerialPort();

					
					return true;
				}

				catch (Exception e)
				{
					MessageBox.Show(string.Format("Unable to read Lable Printer Config file\nError: {0}", e.Message), "System Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					return false;
				}
				 
			}

			else
				return false;

		}



		public bool OpenPort(out string Msg)
		{

			// Only open if enabled
			if (Enabled)
			{

				// Be sure port name string is defined
				if (PortNameStr != "")
				{

					// Try to open the port
					try
					{
						// Close port first if open
						if (PrinterPort.IsOpen)
							PrinterPort.Close();

						// Set port name
						PrinterPort.PortName = PortNameStr;

						// try to open port
						PrinterPort.Open();
						Msg = "";
						return true;

					}

					catch (UnauthorizedAccessException)
					{

						Msg = "The Printer Communications Port could not be opened.\nThe port is in use by another device!";
						return false;


					}

					catch (Exception oEx)
					{

						Msg = "The Printer Communications Port could not be opened.\n" + oEx.Message;
						return false;

					}
				}

				// Port name not defined
				else
				{
					Msg = "The Printer Communications Port has not yet been defined!";
					return false;
				}
			}

			// Not enabled, return true as if opened.
			else
			{
				Msg = "";
				return true;
			}
		}


		// Attempt to close the port
		public void ClosePort()
		{
			try
			{
				if (PrinterPort.IsOpen)
					PrinterPort.Close();

			}


			catch
			{

			}
		}



		private void Port_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
		{

			


		}


		private String FormatTextField(int XPos, int YPos, int Font, String Reversed, String Caption, String Data)
		{

			String Line;

			if ((XPos != 0) && (YPos != 0))
				Line = String.Format(@"A{0},{1},0,{2},1,1,{3},""{4}{5}""", XPos, YPos, Font, Reversed, Caption, Data);

			else
				Line = "";

			return Line;


		}

		
		private string FormatBarcode(int XPos, int YPos, string Data)
		{
			
			String Line;

			Line = String.Format(@"B{0},{1},0,{2},{3},{4},{5},{6},""{7}""", XPos, YPos, BarcodeType, BarcodeNarrowWidth, BarcodeWideWidth, BarcodeHeight, Readable, Data);

			return Line;

		}


		private void TransmitMessage(String Msg)
		{

			try
			{
				if(Msg != "")
					PrinterPort.Write(Msg + Convert.ToString(Convert.ToChar(0x0a)));
			}

			catch (Exception e)
			{
				MessageBox.Show(string.Format("Error sending label printer data:\n{0}", e.Message), "Label Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}


		}



		public void PrintLabel(string OrderID, string PartID, int LineNumber, MeasuredPartInstance Measurement, MeasuringUnits Units)
		{

			// Do only if printer enabled
			if (Enabled && (PortNameStr != "") && (PrinterPort.IsOpen))
			{
				string StatusStr;
				string UnitsStr;
				string WidthStr;
				string HeightStr;

				// Format measurement strings based on units.  Inches fixed at 3, mm at 2
				if (Units == MeasuringUnits.Inches)
				{
					WidthStr = string.Format("{0:F3}", Measurement.Width);
					HeightStr = string.Format("{0:F3}", Measurement.Height);
				}

				else
				{
					WidthStr = string.Format("{0:F2}", Measurement.Width);
					HeightStr = string.Format("{0:F2}", Measurement.Height);
				}

				// Send the new message command
				TransmitMessage("N");

				// Adjust spec string based on type of measurement (manual or auto)
				if (Measurement.IsManualMeasurement)
				{
					// Check width status
					if (Measurement.WidthInSpec == false)
						StatusStr = "[FAIL M-M]";

					else if (Measurement.WidthInSpec == true)
						StatusStr = "[OK M-M]";

					else
						StatusStr = "";
				}

				// Auto measurement
				else
				{
					// Check width status
					if (Measurement.WidthInSpec == false)
						StatusStr = "[FAIL]";

					else if (Measurement.WidthInSpec == true)
						StatusStr = "[OK]";

					else
						StatusStr = "";
				}


				// Shorten inches string if in inches
				if (Units == MeasuringUnits.Inches)
					UnitsStr = "In";

				else
					UnitsStr = "MM";

				// Transmit the width field
				TransmitMessage(FormatTextField(XDimXPos, XDimYPos, 4, "N",  "Width: ", string.Format("{0} {1} {2}", WidthStr, UnitsStr, StatusStr)));


				// Adjust spec string based on type of measurement (manual or system)
				if (Measurement.IsManualMeasurement)
				{
					// Check width status
					if (Measurement.HeightInSpec == false)
						StatusStr = "[FAIL M-M]";

					else if (Measurement.HeightInSpec == true)
						StatusStr = "[OK M-M]";

					else
						StatusStr = "";
				}

				// System measurement
				else
				{
					// Check width status
					if (Measurement.HeightInSpec == false)
						StatusStr = "[FAIL]";

					else if (Measurement.HeightInSpec == true)
						StatusStr = "[OK]";

					else
						StatusStr = "";
				}


				// Transmit the height field
				TransmitMessage(FormatTextField(YDimXPos, YDimYPos, 4, "N", "Height: ", string.Format("{0} {1} {2}", HeightStr, UnitsStr, StatusStr)));

				// Transmit the Order ID field
				TransmitMessage(FormatTextField(OrderXPos, OrderYPos, 4, "N", "Order: ", OrderID));

				// Transmit the Line number field
				TransmitMessage(FormatTextField(LiNoXPos, LiNoYPos, 4, "N", "Line Item: ", LineNumber.ToString()));

				// Transmit the Part Number field
				TransmitMessage(FormatTextField(PrtNoXPos, PrtNoYPos, 4, "N", "Part #: ", Measurement.PartInstanceNumber.ToString()));

				// Transmit the ID field
				TransmitMessage(FormatTextField(IDXPos, IDYPos, 4, "N", "ID: ", PartID));

				// Transmit the Timestamp field
				TransmitMessage(FormatTextField(TimeStampXPos, TimeStampYPos, 4, "N", "", Measurement.MeasuredTime));

				// Transmit the bar code, if necessary
				if ((PrintBarCode) && (PartID != ""))
					TransmitMessage(FormatBarcode(BarcodeXPos, BarcodeYPos, PartID));

				// Send print command
				TransmitMessage("P1");
			}

		}


		// Prints a dummy test label, if enabled and port defined
		public void PrintTestLabel()
		{

			// Do only if printer enabled
			if (Enabled && (PortNameStr != "") && (PrinterPort.IsOpen))
			{
				// Create a dummy part measurement
				MeasuredPartInstance Part = new MeasuredPartInstance(298.45, 158.75, MeasuringUnits.Inches, true, false);

				// Mark the part as in-spec
				Part.WidthInSpec = true;
				Part.HeightInSpec = true;
				Part.PartInstanceNumber = 1;

				// Send the data to the printer
				PrintLabel("Test Order", "Test Part", 1, Part, MeasuringUnits.Inches);

			}

		}


		public void PrintRejectedLabel(string Msg, string OrderID)
		{

			// Do only if printer enabled
			if (Enabled && (PortNameStr != "") && (PrinterPort.IsOpen))
			{

				// Send the new message command
				TransmitMessage("N");

				// Transmit the REJECTED title
				TransmitMessage(FormatTextField(10, 10, 5, "N", "REJECTED", ""));

				// Transmit the Cause message
				TransmitMessage(FormatTextField(10, 100, 4, "N", "Reason: ", Msg));

				// Transmit the Order ID field
				TransmitMessage(FormatTextField(10, 130, 4, "N", "Order: ", OrderID));

				// Send print command
				TransmitMessage("P1");
			}

		}


		public void PrintRejectedLabel(string Msg, string OrderID, MeasuredPartInstance Measurement, MeasuringUnits Units)
		{

			// Do only if printer enabled
			if (Enabled && (PortNameStr != "") && (PrinterPort.IsOpen))
			{

				// Send the new message command
				TransmitMessage("N");

				// Transmit the REJECTED title
				TransmitMessage(FormatTextField(10, 10, 5, "N", "REJECTED", ""));

				// Transmit the Cause message
				TransmitMessage(FormatTextField(10, 100, 4, "N", "Reason: ", Msg));

				// Transmit the Order ID field
				TransmitMessage(FormatTextField(10, 130, 4, "N", "Order: ", OrderID));

				string UnitsStr;
				string WidthStr;
				string HeightStr;

				// Format measurement strings based on units.  Inches fixed at 3, mm at 2
				if (Units == MeasuringUnits.Inches)
				{
					WidthStr = string.Format("{0:F3}", Measurement.Width);
					HeightStr = string.Format("{0:F3}", Measurement.Height);
				}

				else
				{
					WidthStr = string.Format("{0:F2}", Measurement.Width);
					HeightStr = string.Format("{0:F2}", Measurement.Height);
				}



				// Shorten inches string if in inches
				if (Units == MeasuringUnits.Inches)
					UnitsStr = "In";

				else
					UnitsStr = "MM";

				// Transmit the width field
				TransmitMessage(FormatTextField(10, 160, 4, "N", "Width: ", string.Format("{0} {1}", WidthStr, UnitsStr)));
				
				// Transmit the height field
				TransmitMessage(FormatTextField(10, 190, 4, "N", "Height: ", string.Format("{0} {1}", HeightStr, UnitsStr)));

				// Send print command
				TransmitMessage("P1");

			}

		}




			

	





	






	}
}
