﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProCABQC11
{

    // Results returned by DoesLineItemMatchTarget().  New to V1.1.0.1  Accomadates auto rotation parts

    public enum LineItemMatchResults
    {

        None,
        Normal,
        Rotated
    }


	[Serializable]
	public class LineItem
	{

		private int LineNumberID = -1;
		private string PartID = "";
		private int QuantityRequired = 0;
		private int QuantityMeasured = 0;
		private double NominalWidth = 0.0;
		private double NominalHeight = 0.0;
		private double NominalThickness = 0.0;
		private string PartStyle = "";
		private string PartMaterial = "";

		private string Hinging = "";
		private string Finish = "";
		private string Type = "";
		private string Location = "";
		private string Machining = "";
		private string Assembly = "";
		private string UserMessage = "";

		private string PartComment = "";
		private string UserDefined1 = "";
		private string UserDefined2 = "";
		private string UserDefined3 = "";

		private List<MeasuredPartInstance> MeasuredParts;



		public int LineNumber
		{

			get { return LineNumberID; }
			set
			{
				if (value > 0)
					LineNumberID = value;
			}
		}

		public string PartIdStr
		{

			get { return PartID; }
			set { PartID = value; }

		}

		public int QtyRequired
		{

			get { return QuantityRequired; }
			set
			{
				if (value >= 0)
					QuantityRequired = value;
			}
		}

		public int QtyMeasured
		{

			get { return QuantityMeasured; }
			set
			{
				if (value >= 0)
					QuantityMeasured = value;
			}
		}




		public double Width
		{
			get { return NominalWidth; }
			set
			{
				if (value > 0.0)
					NominalWidth = value;
			}
		}

		public double Height
		{

			get { return NominalHeight; }
			set
			{
				if (value > 0.0)
					NominalHeight = value;
			}
		}

		public double Thickness
		{

			get { return NominalThickness; }
			set
			{
				if (value > 0.0)
					NominalThickness = value;
			}
		}

		public string StyleText
		{

			get { return PartStyle; }
			set { PartStyle = value; }
		}

		public string MaterialText
		{

			get { return PartMaterial; }
			set { PartMaterial = value; }
		}



		public string HingingText
		{
			get { return Hinging; }
			set { Hinging = value; }
		}

		public string FinishText
		{
			get { return Finish; }
			set { Finish = value; }
		}

		public string TypeText
		{
			get { return Type; }
			set { Type = value; }
		}

		public string LocationText
		{
			get { return Location; }
			set { Location = value; }
		}

		public string MachiningText
		{
			get { return Machining; }
			set { Machining = value; }
		}

		public string AssemblyText
		{
			get { return Assembly; }
			set { Assembly = value; }
		}

		public string UserMessageText
		{
			get { return UserMessage; }
			set { UserMessage = value; }
		}




		public string CommentText
		{

			get { return PartComment; }
			set { PartComment = value; }
		}

		public string UserDef1Text
		{

			get { return UserDefined1; }
			set { UserDefined1 = value; }
		}

		public string UserDef2Text
		{

			get { return UserDefined2; }
			set { UserDefined2 = value; }
		}

		public string UserDef3Text
		{

			get { return UserDefined3; }
			set { UserDefined3 = value; }
		}

		public int MeasuredPartsCount
		{
			get { return MeasuredParts.Count; }

		}

		public bool IsQuantityComplete
		{
			get
			{
				if (QuantityMeasured >= QuantityRequired)
					return true;
				else
					return false;
			}
		}




		public LineItem()
		{

			// Create list of measured parts
			MeasuredParts = new List<MeasuredPartInstance>();


		}


		// Adds a measured part to the line item
		public void AddMeasuredPart(ref MeasuredPartInstance TestPart, Tolerances OrderTol, bool TestThickness, bool UseAngleAxis)
		{

			if ((TestPart.Width >= (NominalWidth - OrderTol.WidthMinusTol)) && (TestPart.Width <= (NominalWidth + OrderTol.WidthPlusTol))) 
				TestPart.WidthInSpec = true;

			if((TestPart.Height >= (NominalHeight - OrderTol.HeightMinusTol)) && (TestPart.Height <= (NominalHeight + OrderTol.HeightPlusTol)))
				TestPart.HeightInSpec = true;

			// Only check thickness if enabled
			if (TestThickness)
			{
				if ((TestPart.Thickness >= (NominalThickness - OrderTol.ThicknessMinusTol)) && (TestPart.Thickness <= (NominalThickness + OrderTol.ThicknessPlusTol)))
					TestPart.ThicknessInSpec = true;
			}

			// Update the measured part error values
			TestPart.HeightError = TestPart.Height - this.NominalHeight;
			TestPart.WidthError = TestPart.Width - this.NominalWidth;

			if (TestThickness)
				TestPart.ThicknessError = TestPart.Thickness - this.NominalThickness;

			// Using angle 
			if (UseAngleAxis)
			{
				// Calculate error
				TestPart.AngleError = TestPart.Angle - 90.0;

				// Check if in spec
				if ((Math.Abs(TestPart.AngleError) <= OrderTol.AnglePlusTol) && (Math.Abs(TestPart.AngleError) <= OrderTol.AnglePlusTol))
					TestPart.AngleInSpec = true;
			}



			// Update the quantity measured
			QuantityMeasured++;

			// Update the measured part instance ID
			TestPart.PartInstanceNumber = QuantityMeasured;

			// Add the measured part to the parts list for this line item
			MeasuredParts.Add(TestPart);

		}


		// Removes the last measured part from the line item
		public void RemoveLastMeasuredPart()
		{
			
			// Make sure at least one part exists
			if (QuantityMeasured > 0)
			{
				// decrement quantity measured
				QuantityMeasured--;

				// remove the last part from the measured parts list
				MeasuredParts.RemoveAt(MeasuredParts.Count - 1);
			}



		}

		// Get the number of measured parts
		public int GetMeasuredPartsCount()
		{

			return MeasuredParts.Count;

		}


		// Returns the measured parts list
		public void GetMeasuredPartsList(ref List<MeasuredPartInstance> PartsList)
		{

			for (int u = 0; u < MeasuredParts.Count; u++)
				PartsList.Add(MeasuredParts[u]);

		}


		// Returns a measured part based on the index
		public bool GetMeasuredPart(out MeasuredPartInstance Part, int Index)
		{

			// Valid index.  Return part and true
			if (Index < MeasuredParts.Count)
			{
				Part = MeasuredParts[Index];
				return true;
			}

			// Invalid part.  return false and null
			else
			{
				Part = (MeasuredPartInstance)null;
				return false;
			}

		}


		// Gets last measured part
		public bool GetLastMeasuredPart(out MeasuredPartInstance Part)
		{

			if (MeasuredParts.Count > 0)
			{
				Part = MeasuredParts.Last();
				return true;
			}

			else
			{
				Part = (MeasuredPartInstance)null;
				return false;
			}

		}

		// Test angle for tollerances
		private bool TestTargetAngle(ref MeasuredPartInstance TestPart, Tolerances OrderTol)
		{

			if((Math.Abs(90.0 - TestPart.Angle) <= OrderTol.AnglePlusTol) && (Math.Abs(90.0 - TestPart.Angle) <= OrderTol.AngleMinusTol))
				return true;

			else
				return false;
			
		}

		// Test if passed measurements match, within tolerance, the Line Item dimension parameters
        public LineItemMatchResults DoesLineItemMatchTarget(ref MeasuredPartInstance TestPart, Tolerances OrderTol, bool TestThickness, bool TestAngle)
		{
			
			// Not testing for thickness (depth)
			if ((!TestThickness) || (NominalThickness <= 0.0))
			{
				// If test part height and width are within the nominal (Not rotated), set InSpec to true and return normal
				if (((TestPart.Width >= (NominalWidth - OrderTol.WidthMinusTol)) && (TestPart.Width <= (NominalWidth + OrderTol.WidthPlusTol))) &&
					((TestPart.Height >= (NominalHeight - OrderTol.HeightMinusTol)) && (TestPart.Height <= (NominalHeight + OrderTol.HeightPlusTol))))
				{
					// Test angle if enabled
					if(TestAngle)
					{
						// Angle test good
						if(TestTargetAngle(ref TestPart, OrderTol))
							return LineItemMatchResults.Normal;

						else
							return LineItemMatchResults.None;
					}

					// Angle test not enabled but width and height match
					else
							return LineItemMatchResults.Normal;


				}

				// If test part height and width are within the nominal (Rotated), set InSpec to true and return rotated
				else if (((TestPart.Height >= (NominalWidth - OrderTol.WidthMinusTol)) && (TestPart.Height <= (NominalWidth + OrderTol.WidthPlusTol))) &&
					((TestPart.Width >= (NominalHeight - OrderTol.HeightMinusTol)) && (TestPart.Width <= (NominalHeight + OrderTol.HeightPlusTol))))
				{

					// Test angle if enabled
					if (TestAngle)
					{
						// Angle test good
						if (TestTargetAngle(ref TestPart, OrderTol))
						{
							// The part was rotated.  Swap height and width
							TestPart.SwapHeightAndWidth();

							// Mark part as rotated
							TestPart.PartWasRotated = true;

							// Indicated rotated
							return LineItemMatchResults.Rotated;
						}

						// Angle failed
						else
							return LineItemMatchResults.None;
					}

					// Angle test not enabled but width and height match, but rotated
					else
					{
						// The part was rotated.  Swap height and width
						TestPart.SwapHeightAndWidth();

						// Mark part as rotated
						TestPart.PartWasRotated = true;

						// Indicated rotated
						return LineItemMatchResults.Rotated;
					}

				}

				  
				// Outside tolerance both normal and rotated
				else
					return LineItemMatchResults.None;
			}

			// Testing thickness
			else
			{
				// If test part height, width and thickness (depth) are within the nominal (Not rotated), set InSpec to true and return normal
				if (((TestPart.Width >= (NominalWidth - OrderTol.WidthMinusTol)) && (TestPart.Width <= (NominalWidth + OrderTol.WidthPlusTol))) &&
					((TestPart.Height >= (NominalHeight - OrderTol.HeightMinusTol)) && (TestPart.Height <= (NominalHeight + OrderTol.HeightPlusTol))) &&
					((TestPart.Thickness >= (NominalThickness - OrderTol.ThicknessMinusTol)) && (TestPart.Thickness <= (NominalThickness + OrderTol.ThicknessPlusTol))))
				{

					// Test angle if enabled
					if (TestAngle)
					{
						// Angle test good
						if (TestTargetAngle(ref TestPart, OrderTol))
							return LineItemMatchResults.Normal;

						else
							return LineItemMatchResults.None;
					}

					// Angle test not enabled but width and height match
					else
						return LineItemMatchResults.Normal;
					
				}

				// If test part height, width and thickness (depth) are within the nominal (Rotated), set InSpec to true and return rotated
				else if (((TestPart.Height >= (NominalWidth - OrderTol.WidthMinusTol)) && (TestPart.Height <= (NominalWidth + OrderTol.WidthPlusTol))) &&
					((TestPart.Width >= (NominalHeight - OrderTol.HeightMinusTol)) && (TestPart.Width <= (NominalHeight + OrderTol.HeightPlusTol))) &&
					((TestPart.Thickness >= (NominalThickness - OrderTol.ThicknessMinusTol)) && (TestPart.Thickness <= (NominalThickness + OrderTol.ThicknessPlusTol))))
				{

					// Test angle if enabled
					if (TestAngle)
					{
						// Angle test good
						if (TestTargetAngle(ref TestPart, OrderTol))
						{
							// The part was rotated.  Swap height and width
							TestPart.SwapHeightAndWidth();

							// Mark part as rotated
							TestPart.PartWasRotated = true;

							// Indicated rotated
							return LineItemMatchResults.Rotated;
						}

						// Angle failed
						else
							return LineItemMatchResults.None;
					}

					// Angle test not enabled but width and height match, but rotated
					else
					{
						// The part was rotated.  Swap height and width
						TestPart.SwapHeightAndWidth();

						// Mark part as rotated
						TestPart.PartWasRotated = true;

						// Indicated rotated
						return LineItemMatchResults.Rotated;
					}

				}


				// Outside tolerance both normal and rotated
				else
					return LineItemMatchResults.None;
			}




		}



		// Get the measurement error of the passed measurement
		public void GetMeasurementError(ref MeasuredPartInstance TestPart, out double WidthError, out double HeightError, out double RotWidthError, out double RotHeightError, out double ThicknessError)
		{

			WidthError = this.Width - TestPart.Width;
			HeightError = this.Height - TestPart.Height;
			RotWidthError = this.Width - TestPart.Height;
			RotHeightError = this.Height - TestPart.Width;
			ThicknessError = this.Thickness - TestPart.Thickness;
			
			
 
		}


		private void WriteMeasurementsToCSVFile(StreamWriter Sw, PersistentParametersV1100 PParams, MeasuringUnits CSVMeasureUnits, bool UseThickness)
		{

			// Write header
			Sw.WriteLine("Part #, Time/Date, Measured Width, Width Measure Error, Width In-Spec, Measured Height, Height Measure Error, Height In-Spec, Measured Depth, Depth Measure Error, Depth In-Spec, Measured Angle, Angle Measure Error, Angle In-Spec, Measurement Type, Part Rotated State");

			
			// Write each instance
			for (int u = 0; u < MeasuredParts.Count; u++)
			{

				// Format the numeric data based on the CSV file units
				string WidthStr;
				string HeightStr;
				string ThicknessStr;
				string AngleStr;
				string WidthErrStr;
				string HeightErrStr;
				string ThicknessErrStr;
				string AngleErrStr;

				// Inches.  Fixed at 3 dp.
				if (CSVMeasureUnits == MeasuringUnits.Inches)
				{
					WidthStr = string.Format("{0:F3}", MeasuredParts[u].Width);
					WidthErrStr = string.Format("{0:F3}", MeasuredParts[u].WidthError);

					HeightStr = string.Format("{0:F3}", MeasuredParts[u].Height);
					HeightErrStr = string.Format("{0:F3}", MeasuredParts[u].HeightError);

					if (UseThickness)
					{
						ThicknessStr = string.Format("{0:F3}", MeasuredParts[u].Thickness);
						ThicknessErrStr = string.Format("{0:F3}", MeasuredParts[u].ThicknessError);
					}
					else
					{
						ThicknessStr = "0.000";
						ThicknessErrStr = "0.000";
					}


					// Angle in use
					if (PParams.AngleAxisInUse)
					{
						AngleStr = string.Format("{0:F1}", MeasuredParts[u].Angle);
						AngleErrStr = string.Format("{0:F1}", MeasuredParts[u].AngleError);
					}

					else
					{
						AngleStr = "90.0";
						AngleErrStr = "0.0";

					}


					

				}

				// Millimeters.  Fixed at 2 dp.
				else
				{

					WidthStr = string.Format("{0:F2}", MeasuredParts[u].Width);
					WidthErrStr = string.Format("{0:F2}", MeasuredParts[u].WidthError);

					HeightStr = string.Format("{0:F2}", MeasuredParts[u].Height);
					HeightErrStr = string.Format("{0:F2}", MeasuredParts[u].HeightError);

					if (UseThickness)
					{
						ThicknessStr = string.Format("{0:F2}", MeasuredParts[u].Thickness);
						ThicknessErrStr = string.Format("{0:F2}", MeasuredParts[u].ThicknessError);
					}
					else
					{
						ThicknessStr = "0.00";
						ThicknessErrStr = "0.00";

					}

					// Angle in use
					if (PParams.AngleAxisInUse)
					{
						AngleStr = string.Format("{0:F1}", MeasuredParts[u].Angle);
						AngleErrStr = string.Format("{0:F1}", MeasuredParts[u].AngleError);
					}

					else
					{
						AngleStr = "90.0";
						AngleErrStr = "0.0";

					}


				}

				Sw.Write(MeasuredParts[u].PartInstanceNumber + ",");
				Sw.Write(MeasuredParts[u].MeasuredTime + ",");
				Sw.Write(WidthStr + ",");
				Sw.Write(WidthErrStr + ",");
				Sw.Write(MeasuredParts[u].WidthInSpec + ",");
				Sw.Write(HeightStr + ",");
				Sw.Write(HeightErrStr + ",");
				Sw.Write(MeasuredParts[u].HeightInSpec + ",");
				Sw.Write(ThicknessStr + ",");
				Sw.Write(ThicknessErrStr + ",");
				Sw.Write(MeasuredParts[u].ThicknessInSpec + ",");
				Sw.Write(AngleStr + ",");
				Sw.Write(AngleErrStr + ",");
				Sw.Write(MeasuredParts[u].AngleInSpec + ",");


				// Save the type of measurement (manual or auto - measured by system)
				if (MeasuredParts[u].IsManualMeasurement)
					Sw.Write("Manual Entry,");

				else
					Sw.Write("System Entry,");

				// V2 Part measured rotated
				if (MeasuredParts[u].PartWasRotated)
					Sw.WriteLine("Rotated");

				else
					Sw.WriteLine("Not Rotated");


			}

			// Add a blank line
			Sw.WriteLine("");
			Sw.WriteLine("");
			
			

		}

		public void WritePartDataToCSVFile(StreamWriter Sw, PersistentParametersV1100 PParams, MeasuringUnits CSVMeasureUnits, bool UseThickness)
		{

			// Print the part data header
			Sw.WriteLine("Line Number,Part ID,Qty Required,Qty Missing,Nominal Width,Nominal Height,Nominal Depth,Material,Style,Comment,User Defined 1,User Defined 2");


			// Format the numeric data based on the CSV file units
			string WidthStr;
			string HeightStr;
			string ThicknessStr;

			// Inches.  Fixed at 3 dp.
			if (CSVMeasureUnits == MeasuringUnits.Inches)
			{
				WidthStr = string.Format("{0:F3}", Width);
				HeightStr = string.Format("{0:F3}", Height);

				if (UseThickness)
					ThicknessStr = string.Format("{0:F3}", Thickness);
				else
					ThicknessStr = "N/A";
			}

			// Millimeters.  Fixed at 2 dp.
			else
			{
				WidthStr = string.Format("{0:F2}", Width);
				HeightStr = string.Format("{0:F2}", Height);

				if (UseThickness)
					ThicknessStr = string.Format("{0:F2}", Thickness);
				else
					ThicknessStr = "N/A";

			}

			// Write the line item data (uses Write followed by WriteLine)
			Sw.Write(LineNumber + ",");
			Sw.Write(PartIdStr + ",");
			Sw.Write(QtyRequired + ",");
			Sw.Write((QtyRequired - QtyMeasured) + ",");	// Shortage
			Sw.Write(WidthStr + ",");
			Sw.Write(HeightStr + ",");
			Sw.Write(ThicknessStr + ",");
			Sw.Write(MaterialText + ",");
			Sw.Write(StyleText + ",");


			Sw.Write(HingingText + ",");
			Sw.Write(FinishText + ",");
			Sw.Write(TypeText + ",");
			Sw.Write(LocationText + ",");
			Sw.Write(MachiningText + ",");
			Sw.Write(AssemblyText + ",");
			Sw.Write(UserMessageText + ",");


			Sw.Write(CommentText + ",");
			Sw.Write(UserDefined1 + ",");
			Sw.WriteLine(UserDefined2);

			// Add a blank line
			Sw.WriteLine("");
			Sw.WriteLine(string.Format("Measurement Data for Part Line Item {0}", LineNumber));

			// Write out the measured part instances for this line item
			WriteMeasurementsToCSVFile(Sw, PParams, CSVMeasureUnits, UseThickness);

		}





	}
}
