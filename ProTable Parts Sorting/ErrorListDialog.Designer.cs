﻿namespace ProCABQC11
{
	partial class ErrorListDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorListDialog));
			this.ErrorList = new System.Windows.Forms.ListBox();
			this.OkButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// ErrorList
			// 
			this.ErrorList.FormattingEnabled = true;
			this.ErrorList.HorizontalScrollbar = true;
			this.ErrorList.ItemHeight = 20;
			this.ErrorList.Location = new System.Drawing.Point(13, 23);
			this.ErrorList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.ErrorList.Name = "ErrorList";
			this.ErrorList.Size = new System.Drawing.Size(541, 204);
			this.ErrorList.TabIndex = 0;
			// 
			// OkButton
			// 
			this.OkButton.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OkButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OkButton.Location = new System.Drawing.Point(245, 252);
			this.OkButton.Name = "OkButton";
			this.OkButton.Size = new System.Drawing.Size(77, 44);
			this.OkButton.TabIndex = 1;
			this.OkButton.UseVisualStyleBackColor = true;
			// 
			// ErrorListDialog
			// 
			this.AcceptButton = this.OkButton;
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(567, 308);
			this.Controls.Add(this.OkButton);
			this.Controls.Add(this.ErrorList);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ErrorListDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Error Listing";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox ErrorList;
		private System.Windows.Forms.Button OkButton;
	}
}