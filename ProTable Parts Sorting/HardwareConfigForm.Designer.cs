﻿namespace ProCABQC11
{
	partial class HardwareConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HardwareConfigForm));
			this.MsgLabel = new System.Windows.Forms.Label();
			this.CancelBut = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// MsgLabel
			// 
			this.MsgLabel.AutoSize = true;
			this.MsgLabel.Location = new System.Drawing.Point(22, 50);
			this.MsgLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.MsgLabel.Name = "MsgLabel";
			this.MsgLabel.Size = new System.Drawing.Size(155, 20);
			this.MsgLabel.TabIndex = 0;
			this.MsgLabel.Text = "Verifying Connection";
			this.MsgLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// CancelBut
			// 
			this.CancelBut.Location = new System.Drawing.Point(105, 124);
			this.CancelBut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(112, 35);
			this.CancelBut.TabIndex = 2;
			this.CancelBut.Text = "Cancel";
			this.CancelBut.UseVisualStyleBackColor = true;
			this.CancelBut.Click += new System.EventHandler(this.CancelBut_Click);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// HardwareConfigForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(318, 183);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.MsgLabel);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "HardwareConfigForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Auto Configure Device";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label MsgLabel;
		private System.Windows.Forms.Button CancelBut;
		private System.Windows.Forms.Timer timer1;
	}
}