﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class TemplateNameDialog : Form
	{

		public string TemplateName
		{
			get { return NameStr.Text; }
		}

		public TemplateNameDialog()
		{
			InitializeComponent();
		}

		private void OKBut_Click(object sender, EventArgs e)
		{

			if (NameStr.Text.Length > 0)
			{
				this.DialogResult = DialogResult.OK;
				this.Close();
			}

			else
			{
				MessageBox.Show("Template Name CANNOT be blank", "Template Name Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}
	}
}
