﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{


	public class TemplateItem
	{

		
		protected string TextName = "";
		protected bool RequiredStatus = false;
		protected ParameterDataType DataType = ParameterDataType.String;

		


		public string FieldName
		{
			get { return TextName; }
		}

		public bool FieldRequired
		{
			get { return RequiredStatus; }
		}


		public ParameterDataType FieldDataType
		{
			get { return DataType; }
		}

		protected DataConversionResult ConvertStringToValue(string DataString, ref ParamStruct ParamData, string ColumnString, ref string ErrorMsg)
		{

			if (RequiredStatus)
			{
				if (DataString.Length == 0)
				{
					ErrorMsg = string.Format("{0} at column {1}: Data field is required but empty!", TextName, ColumnString);
					return DataConversionResult.Error;
				}
			}

			DataConversionResult Result = DataConversionResult.Error;
			char[] TrimChars = { '"' };

			// V1.2.1.8  Add test to strip surrounding quotations (Open Office and others) from any string prior to conversion
			DataString = DataString.Trim(TrimChars);

			switch (DataType)
			{

				case ParameterDataType.Integer:

					try
					{

						ParamData.IntVal = Convert.ToInt32(DataString);
						Result = DataConversionResult.Integer;

					}

					catch
					{

						// V1.2.1.0  Allows fp quantities imported as integers
						try
						{
							if (DataString.Contains("."))
							{

								// Split into whole and deximal parts
								string[] NumberParts;
								NumberParts = DataString.Split('.');

								// Save only the integer part
								ParamData.IntVal = Convert.ToInt32(NumberParts[0]);
											  
								Result = DataConversionResult.Integer;

							}

						}

						catch
						{


							ErrorMsg = string.Format("{0} at column {1}: Data field requires an integer value.  Data not integer", TextName, ColumnString);
							Result = DataConversionResult.Error;
						}

						

					}

					break;


				case ParameterDataType.Float:

					try
					{

						// First, test for raw fractional data
						if(DataString.Contains("/"))
						{
							bool ConversionResult;

							// Attempt to convert fractional string to FP
							double temp = ConvertFractionToFP(DataString, ref ColumnString, ref ErrorMsg, out ConversionResult);

							// Success
							if (ConversionResult)
							{
								ParamData.FPVal = temp;
								Result = DataConversionResult.Float;
							}

							// Failure
							else
							{
								Result = DataConversionResult.Error;
							}
						}

						// Floating point data
						else
						{
							ParamData.FPVal = Convert.ToDouble(DataString);

							Result = DataConversionResult.Float;
						}

					}

					catch
					{
						ErrorMsg = string.Format("{0} at column {1}: Data field requires a floating point value.  Data is not floating point", TextName, ColumnString);
						Result = DataConversionResult.Error;
					}

					break;


				default:

					ParamData.StringVal = DataString;
					Result = DataConversionResult.String;
					break;

			}

			return Result;

			
		}

		 public virtual DataConversionResult GetParameterValue(string DataString, ref ParamStruct ParamData, ref string ErrorMsg)
		{


			return DataConversionResult.Undefined;
			

		}

		 protected double ConvertFractionToFP(string DataStr, ref string ColumnStr, ref string ErrorMsg, out bool Result)
		 {

			 // Trim all leading spaces from data string
			 DataStr = DataStr.TrimStart(' ');

			 // Split into whole and fractional strings
			 string[] NumberParts;
			 NumberParts = DataStr.Split(' ');

			 int WholeIndex = -1;
			 int FractionIndex = -1;

			 // Find strings with whole and fractional parts
			 for(int u = 0; u < NumberParts.Length; u++)
			 {
				 if(NumberParts[u].Length != 0)
				 {
					 if(WholeIndex == -1)
						 WholeIndex = u;

					 else if (FractionIndex == -1)
						 FractionIndex = u;
				 }
			 }



			// Split fraction into numerator/denominator
			string[] FractionalParts;

			FractionalParts = NumberParts[FractionIndex].Split('/');

			 // Make sure that fractional parts were really found
			if (FractionalParts.Length != 2)
			{
				ErrorMsg = string.Format("{0} at column {1}: Fractional parse error.  Must be 'w n/d'", TextName, ColumnStr);
				Result = false;
				return 0.0;
			}


			// Convert fractional parts to fp
			try
			{
				double numerator = Convert.ToDouble(FractionalParts[0]);
				double denominator = Convert.ToDouble(FractionalParts[1]);

				double Fraction = Convert.ToDouble(numerator / denominator);
				double Whole = Convert.ToDouble(NumberParts[WholeIndex]);

				Result = true;
				return Fraction + Whole;
			 }

			catch
			{
				ErrorMsg = string.Format("{0} at column {1}: Fractional data conversion error.  Must be 'w n/d'", TextName, ColumnStr);
				Result = false;
				return 0.0;
			}
			 
		 }

	}
}
