﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{
	public class TemplateLineItemColumn	: TemplateItem
	{

		protected bool IsLineItem = false;
		protected TemplateColumn Column;


		public bool FieldLineItem
		{
			get { return IsLineItem; }
		}

		public int GetColumn
		{
			get { return Column.GetColumnNumber; }
		}

		// String accessors for column.  Used during configuration
		public string GetColumnString
		{
			get { return Column.GetStringValue; }
		}

		public bool SetColumnString(string Data, out string ErrorMsg)
		{
			if (RequiredStatus && (Data.Length == 0))
			{
				ErrorMsg = string.Format("Error:  {0} - Field is required and cannot be undefined", FieldName);
				return false;
			}

			else
				return Column.SetStringValue(Data, FieldName, out ErrorMsg);
		}


		public TemplateLineItemColumn()
		{

		}

		public bool IsDefined
		{
			get
			{
				if (Column.GetColumnNumber == -1)
					return false;

				else
					return true;
			}
		}


		public TemplateLineItemColumn(int ColumnReference, string FName, ParameterDataType FDataType, bool LineItem, bool Required)
		{

			// Set up the internal data
			TextName = FName;
			DataType = FDataType;
			RequiredStatus = Required;
			IsLineItem = LineItem;
			

			// Create a column instance
			Column = new TemplateColumn(ColumnReference);


		}


		public override DataConversionResult GetParameterValue(string DataString, ref ParamStruct ParamData, ref string ErrorMsg)
		{

			if (Column.GetColumnNumber != -1)
			{
				return ConvertStringToValue(DataString, ref ParamData, Column.GetStringValue, ref ErrorMsg);

			}

			else
			{

				return DataConversionResult.Undefined;
			}
		}

	}
}
