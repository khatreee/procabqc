﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{
	public class CutlistCSVReader : CSVReader
	{

		public CutlistCSVReader()
		{

		}
																										  
		protected override CSVReaderResultCodes ProcessCSVFile(ref PersistentParametersV1100 PParams, ref List<string> ErrorLog, ref SystemOrders OrdersList, int TemplateIndex)
		{

			// Create Cutlist Template
			CutlistTemplate LineItemTemplate = new CutlistTemplate(ref PParams, TemplateIndex);

			CutlistTemplateDataV1100 TemplateData;
			
			// Get the template data
			PParams.GetMOTemplateData(TemplateIndex, out TemplateData);

			// Start reading lines from CSV file
			string CSVLine = "";
			while ((CSVLine = CSVFileStreamIn.ReadLine()) != null)
			{
				if (CSVLineNumber >= LineItemTemplate.LineItemStart)
				{
					TemplateParsingResultCodes ParseResult;
					List<string> ErrorList = new List<string>();

					// Parse a single LineItem line from the CSV file
					ParseResult = LineItemTemplate.ParseCutlistLineItem(CSVLineNumber, CSVLine, ref OrdersList, ref ErrorList, TemplateData);

					// If an error resulted, copy the errors from this parse to the error log
					if (ParseResult == TemplateParsingResultCodes.Error)
					{
						// Set flag to later indicate that parse errors occured
						ParseErrors = true;

						for (int u = 0; u < ErrorList.Count; u++)
						{
							ErrorLog.Add(ErrorList[u]);
						}
					}
				}

				// Increment the line number being read
				CSVLineNumber++;
			}

			// Close the CSV file
			CloseCSVFile();

			// return result code based on parsing errors
			if (ParseErrors)
				return CSVReaderResultCodes.ParsingErrors;

			else
				return CSVReaderResultCodes.Success;

		}

	}
}
