﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public class SingleOrderHeaderConfiguration
	{



		private const int BillToTitleXPos = 50;
		private const int ShipToTitleXPos = 315;
		private const int TitleYPos = 5;

		private const int BillNameLabelXPos = 10;
		private const int BillColumnDataTextBoxXPos = BillNameLabelXPos + 155;
		private const int BillRowDataTextBoxXPos = BillColumnDataTextBoxXPos + TextBoxXSize + 15;

		private const int ShipNameLabelXPos = 270;
		private const int ShipColumnDataTextBoxXPos = ShipNameLabelXPos + 165;
		private const int ShipRowDataTextBoxXPos = ShipColumnDataTextBoxXPos + TextBoxXSize + 15;

		private const int RowIncrementYPos = 28;
		private const int OrderMiscFieldsYInc = 35;

		private const int StartYPos = TitleYPos + 60;

		private const int TextBoxXSize = 40;
		private const int TextBoxYSize = 20;

		private const int PanelXSize = 550;
		private const int PanelYSize = 650;

		private const int BillColumnTitleXPos = BillColumnDataTextBoxXPos - 12;
		private const int BillRowTitleXPos = BillRowDataTextBoxXPos;
		private const int ColumnTitleYPos = StartYPos - 25;

		private const int ShipColumnTitleXPos = ShipColumnDataTextBoxXPos - 12;
		private const int ShipRowTitleXPos = ShipRowDataTextBoxXPos;
		private const int RowTitleYPos = ColumnTitleYPos;

		private const string OrderIDText = "Order ID";
		private const string BillCustomerTitleText = "Bill To Customer";
		private const string ShipToCustomerTitleText = "Ship To Customer";




		Panel MainPanel;
		Label[] TemplateNames;
		TextBox[] TemplateColumnData;
		TextBox[] TemplateRowData;
		SingleOrderHeaderTemplate HeaderTemplate;
		Label BillToTitle;
		Label ShipToTitle;
		Label ColumnTitle;
		Label RowTitle;


		// A reference to the params
		PersistentParametersV1100 LocalParams;

		// Reference to original template index
		int LocalTemplateIndex;

		int YPos;



		public SingleOrderHeaderConfiguration()
		{

		}

		public SingleOrderHeaderConfiguration(Point PanelLocation, PersistentParametersV1100 PParams, int TemplateIndex)
		{

			// Create Panel
			MainPanel = new Panel();
			MainPanel.SuspendLayout();
			 
			// Create arrays of labels for template names
			TemplateNames = new Label[(int)SingleOrderHeaderCellIDV1100.MaxItems];

			// Create array of text boxes for template column data
			TemplateColumnData = new TextBox[(int)SingleOrderHeaderCellIDV1100.MaxItems];

			// Create array of text boxes for template row data
			TemplateRowData = new TextBox[(int)SingleOrderHeaderCellIDV1100.MaxItems];

			// Create the template, thus reading the registry
			HeaderTemplate = new SingleOrderHeaderTemplate(ref PParams, TemplateIndex);

			// Create the labels and textboxes for each template
			for (SingleOrderHeaderCellIDV1100 u = 0; u < SingleOrderHeaderCellIDV1100.MaxItems; u++)
			{
				TemplateNames[(int)u] = new Label();
				TemplateColumnData[(int)u] = new TextBox();
				TemplateRowData[(int)u] = new TextBox();
			}

			// Store a copy of the system params for use in VerifyTemplateEntries()
			LocalParams = PParams;

			// Save the original template index
			LocalTemplateIndex = TemplateIndex;

			// A template item reference
			TemplateCell Template;

			YPos = StartYPos;
			SetCustomerGroup(CustomersTypes.Billing);

			YPos = StartYPos;
			SetCustomerGroup(CustomersTypes.Shipping);

			// move down some
			YPos += OrderMiscFieldsYInc;



			// Get the template item for the OrderID
			HeaderTemplate.GetHeaderCellTemplate(SingleOrderHeaderCellIDV1100.OrderID, out Template);

			// Create OrderID label
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderID].Text = Template.FieldName;
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderID].Location = new Point(BillNameLabelXPos, YPos);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderID].Size = new Size(TextBoxXSize, TextBoxYSize);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderID].AutoSize = true;

			// Order ID Column
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderID].Text = Template.GetColumnString;
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderID].Location = new Point(BillColumnDataTextBoxXPos, YPos);
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderID].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Order ID Row
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderID].Text = Template.GetRowString;
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderID].Location = new Point(BillRowDataTextBoxXPos, YPos);
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderID].Size = new Size(TextBoxXSize, TextBoxYSize);

			YPos += RowIncrementYPos;


			// Get the template item for the Packlist Template
			HeaderTemplate.GetHeaderCellTemplate(SingleOrderHeaderCellIDV1100.PacklistTemplate, out Template);

			// Create Packlist template label
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Text = Template.FieldName;
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Location = new Point(BillNameLabelXPos, YPos);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].AutoSize = true;

			// Packlist template Column
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Text = Template.GetColumnString;
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Location = new Point(BillColumnDataTextBoxXPos, YPos);
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Packlist template Row
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Text = Template.GetRowString;
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Location = new Point(BillRowDataTextBoxXPos, YPos);
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);

			YPos += RowIncrementYPos;


			// Get the template item for the Part Label Template
			HeaderTemplate.GetHeaderCellTemplate(SingleOrderHeaderCellIDV1100.PartLabelTemplate, out Template);

			// Create Part Label template label
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Text = Template.FieldName;
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Location = new Point(BillNameLabelXPos, YPos);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].AutoSize = true;

			// Part Label template Column
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Text = Template.GetColumnString;
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Location = new Point(BillColumnDataTextBoxXPos, YPos);
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Part Label template Row
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Text = Template.GetRowString;
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Location = new Point(BillRowDataTextBoxXPos, YPos);
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);

			YPos += RowIncrementYPos;

			// Get the template item for the Order user defined 1
			HeaderTemplate.GetHeaderCellTemplate(SingleOrderHeaderCellIDV1100.OrderUserDefined1, out Template);

			// Create Part Label template label
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Text = Template.FieldName;
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Location = new Point(BillNameLabelXPos, YPos);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Size = new Size(TextBoxXSize, TextBoxYSize);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].AutoSize = true;

			// Part Label template Column
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Text = Template.GetColumnString;
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Location = new Point(BillColumnDataTextBoxXPos, YPos);
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Part Label template Row
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Text = Template.GetRowString;
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Location = new Point(BillRowDataTextBoxXPos, YPos);
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1].Size = new Size(TextBoxXSize, TextBoxYSize);

			YPos += RowIncrementYPos;


			// Get the template item for the Order Test Thickness 
			HeaderTemplate.GetHeaderCellTemplate(SingleOrderHeaderCellIDV1100.TestThicknessDim, out Template);

			// Create Part Label template label
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Text = Template.FieldName;
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Location = new Point(BillNameLabelXPos, YPos);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Size = new Size(TextBoxXSize, TextBoxYSize);
			TemplateNames[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].AutoSize = true;

			// Part Label template Column
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Text = Template.GetColumnString;
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Location = new Point(BillColumnDataTextBoxXPos, YPos);
			TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Part Label template Row
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Text = Template.GetRowString;
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Location = new Point(BillRowDataTextBoxXPos, YPos);
			TemplateRowData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim].Size = new Size(TextBoxXSize, TextBoxYSize);



			// Create the "bill to" title and format it
			BillToTitle = new Label();
			BillToTitle.Location = new Point(BillToTitleXPos, TitleYPos);
			BillToTitle.Text = BillCustomerTitleText;
			BillToTitle.AutoSize = true;
			BillToTitle.Font = new Font("Microsoft Sans Serif", (float)16);

			// Create the "ship to" title and format it
			ShipToTitle = new Label();
			ShipToTitle.Location = new Point(ShipToTitleXPos, TitleYPos);
			ShipToTitle.Text = ShipToCustomerTitleText;
			ShipToTitle.AutoSize = true;
			ShipToTitle.Font = new Font("Microsoft Sans Serif", (float)16);


			// Add the other components to the panel
			MainPanel.Controls.Add(BillToTitle);
			MainPanel.Controls.Add(ShipToTitle);

			// Add Order data to form
			MainPanel.Controls.Add(TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderID]);
			MainPanel.Controls.Add(TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderID]);
			MainPanel.Controls.Add(TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderID]);


			// Add Packlist template data to form
			MainPanel.Controls.Add(TemplateNames[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate]);
			MainPanel.Controls.Add(TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate]);
			MainPanel.Controls.Add(TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PacklistTemplate]);


			// Add Part Label Template data to form
			MainPanel.Controls.Add(TemplateNames[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate]);
			MainPanel.Controls.Add(TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate]);
			MainPanel.Controls.Add(TemplateRowData[(int)SingleOrderHeaderCellIDV1100.PartLabelTemplate]);

			// Add Order user def 1 data to form
			MainPanel.Controls.Add(TemplateNames[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1]);
			MainPanel.Controls.Add(TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1]);
			MainPanel.Controls.Add(TemplateRowData[(int)SingleOrderHeaderCellIDV1100.OrderUserDefined1]);

			// Add Order Test Thickness data to form
			MainPanel.Controls.Add(TemplateNames[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim]);
			MainPanel.Controls.Add(TemplateColumnData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim]);
			MainPanel.Controls.Add(TemplateRowData[(int)SingleOrderHeaderCellIDV1100.TestThicknessDim]);




			// Configure the panel location, size and other parameters
			MainPanel.Location = PanelLocation;
			MainPanel.Size = new Size(PanelXSize, PanelYSize);
			MainPanel.Font = new Font("Microsoft Sans Serif", (float)12);
			MainPanel.BorderStyle = BorderStyle.FixedSingle;

			MainPanel.ResumeLayout(false);


		}

		public Panel GetConfigurationPanel()
		{

			return MainPanel;

		}


		public bool VerifyTemplateEntries(ref List<string> Errors)
		{

			bool NoErrors = true;
			string ErrorMsg = "";

			// Get the line item start value
			SingleOrderLineItemTemplate LineItemTemplate = new SingleOrderLineItemTemplate(ref LocalParams, LocalTemplateIndex);
			string LineItemStartStr = LineItemTemplate.GetLineItemStartString;

			// A template item reference
			TemplateCell Template;

			// Get the original template data again
			SingleOrderTemplateDataV1100 TemplateData;
			LocalParams.GetSOTemplateData(LocalTemplateIndex, out TemplateData);

			// Let template items attempt to save data. 
			for (SingleOrderHeaderCellIDV1100 u = 0; u < SingleOrderHeaderCellIDV1100.MaxItems; u++)
			{
				// Get template item from template
				HeaderTemplate.GetHeaderCellTemplate(u, out Template);
 
				// Attempt to set the column string value of the template item.  If false, an error occured
				bool Result = Template.SetColumnString(TemplateColumnData[(int)u].Text, out ErrorMsg);

				// An error
				if (!Result)
				{
					// Add error message to list
					Errors.Add(ErrorMsg);

					// Re-display original data
					TemplateColumnData[(int)u].Text = Template.GetColumnString;

					// Indicate that at least one error has occured
					NoErrors = false;
				}

				// No error on column string
				else
				{
  					// Attempt to set the row string value of the template item.  If false, an error occured
					Result = Template.SetRowString(TemplateRowData[(int)u].Text, out ErrorMsg);

					// An error
					if (!Result)
					{
						// Add error message to list
						Errors.Add(ErrorMsg);

						// Re-display original data
						TemplateRowData[(int)u].Text = Template.GetRowString;

						// Indicate that at least one error has occured
						NoErrors = false;
					}

					// No error. Test for Line Item Start infringment
					else
					{
						// Prior to setting row value, test if any header row beyond line item start
						if ((TemplateRowData[(int)u].Text.Length != 0) && (Convert.ToInt32(TemplateRowData[(int)u].Text) >= Convert.ToInt32(LineItemStartStr)))
						{
							// Add error message to list
							Errors.Add(string.Format("Warning:  {0} is defined at a row used for Line Items!", Template.FieldName));

							// Indicate that at least one error has occured
							NoErrors = false;

						}

						// All parameters ok.  Save row and column
						else
						{
							// Store row and column
							TemplateData.SetCellSettings(u, Template.GetColumn, Template.GetRow);
														
						}

					}
				}

 			}

			// Write the template data back to the local copy of parameters
			LocalParams.SaveSOTemplateData(LocalTemplateIndex, TemplateData);

			return NoErrors;
		}


		public void SetCustomerGroup(CustomersTypes Customer)
		{

			SingleOrderHeaderCellIDV1100 Start;
			SingleOrderHeaderCellIDV1100 InclusiveEnd;

			// A template item reference
			TemplateCell Template;

			int LabelXPos;
			int ColumnXPos;
			int RowXPos;
			int ColumnTitleXPos;
			int RowTitleXPos;

			if (Customer == CustomersTypes.Billing)
			{
				Start = SingleOrderHeaderCellIDV1100.BillCustomerName;
				InclusiveEnd = SingleOrderHeaderCellIDV1100.BillEmail;

				LabelXPos = BillNameLabelXPos;
				ColumnXPos = BillColumnDataTextBoxXPos;
				RowXPos = BillRowDataTextBoxXPos;
				ColumnTitleXPos = BillColumnTitleXPos;
				RowTitleXPos = BillRowTitleXPos;


			}

			else
			{
				Start = SingleOrderHeaderCellIDV1100.ShipCustomerName;
				InclusiveEnd = SingleOrderHeaderCellIDV1100.ShipEmail;

				LabelXPos = ShipNameLabelXPos;
				ColumnXPos = ShipColumnDataTextBoxXPos;
				RowXPos = ShipRowDataTextBoxXPos;
				ColumnTitleXPos = ShipColumnTitleXPos;
				RowTitleXPos = ShipRowTitleXPos;

			}

		

			// Get each template item and load the data into the labels and text boxes
			for (SingleOrderHeaderCellIDV1100 u = Start; u <= InclusiveEnd; u++)
			{
				// Get template item from template
				HeaderTemplate.GetHeaderCellTemplate(u, out Template);

				// Update the text data
				TemplateNames[(int)u].Text = Template.FieldName;
				TemplateColumnData[(int)u].Text = Template.GetColumnString;
				TemplateRowData[(int)u].Text = Template.GetRowString;

				// Set the parameters for the label
				TemplateNames[(int)u].Location = new Point(LabelXPos, YPos);
				TemplateNames[(int)u].AutoSize = true;

				// Set the parameters for the column text box
				TemplateColumnData[(int)u].Location = new Point(ColumnXPos, YPos);
				TemplateColumnData[(int)u].Size = new Size(TextBoxXSize, TextBoxYSize);

				// Set the parameters for the row text box
				TemplateRowData[(int)u].Location = new Point(RowXPos, YPos);
				TemplateRowData[(int)u].Size = new Size(TextBoxXSize, TextBoxYSize);

				// Increment to the next row in Y
				YPos += RowIncrementYPos;


				// Add the object to the panel
				MainPanel.Controls.Add(TemplateNames[(int)u]);
				MainPanel.Controls.Add(TemplateColumnData[(int)u]);
				MainPanel.Controls.Add(TemplateRowData[(int)u]);

			}


			// Create the column title
			ColumnTitle = new Label();
			ColumnTitle.Location = new Point(ColumnTitleXPos, ColumnTitleYPos);
			ColumnTitle.Text = "Column";
			ColumnTitle.AutoSize = true;

			// Create the row title
			RowTitle = new Label();
			RowTitle.Location = new Point(RowTitleXPos, RowTitleYPos);
			RowTitle.Text = "Row";
			RowTitle.AutoSize = true;

			// Add the column and row titles the panel
			MainPanel.Controls.Add(ColumnTitle);
			MainPanel.Controls.Add(RowTitle);

		}




	}
}
