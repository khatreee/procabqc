﻿namespace ProCABQC11
{
	partial class SimulatedMeasurementDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SimulatedMeasurementDialog));
			this.label1 = new System.Windows.Forms.Label();
			this.SimulatedWidthText = new System.Windows.Forms.TextBox();
			this.SimulatedHeightText = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.OKBut = new System.Windows.Forms.Button();
			this.CancelBut = new System.Windows.Forms.Button();
			this.SimulatedThicknessText = new System.Windows.Forms.TextBox();
			this.ThicknessLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(20, 38);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(125, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Measured Width";
			// 
			// SimulatedWidthText
			// 
			this.SimulatedWidthText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SimulatedWidthText.Location = new System.Drawing.Point(160, 35);
			this.SimulatedWidthText.Name = "SimulatedWidthText";
			this.SimulatedWidthText.Size = new System.Drawing.Size(183, 26);
			this.SimulatedWidthText.TabIndex = 1;
			// 
			// SimulatedHeightText
			// 
			this.SimulatedHeightText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SimulatedHeightText.Location = new System.Drawing.Point(160, 84);
			this.SimulatedHeightText.Name = "SimulatedHeightText";
			this.SimulatedHeightText.Size = new System.Drawing.Size(183, 26);
			this.SimulatedHeightText.TabIndex = 2;
			this.SimulatedHeightText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SimulatedHeightText_KeyPress);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(20, 87);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(131, 20);
			this.label2.TabIndex = 2;
			this.label2.Text = "Measured Height";
			// 
			// OKBut
			// 
			this.OKBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OKBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OKBut.Location = new System.Drawing.Point(66, 202);
			this.OKBut.Name = "OKBut";
			this.OKBut.Size = new System.Drawing.Size(75, 37);
			this.OKBut.TabIndex = 4;
			this.OKBut.UseVisualStyleBackColor = true;
			this.OKBut.Click += new System.EventHandler(this.OKBut_Click);
			// 
			// CancelBut
			// 
			this.CancelBut.BackgroundImage = global::ProCABQC11.Properties.Resources.remove_48x48;
			this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBut.Location = new System.Drawing.Point(235, 202);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(75, 37);
			this.CancelBut.TabIndex = 5;
			this.CancelBut.UseVisualStyleBackColor = true;
			// 
			// SimulatedThicknessText
			// 
			this.SimulatedThicknessText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SimulatedThicknessText.Location = new System.Drawing.Point(160, 135);
			this.SimulatedThicknessText.Name = "SimulatedThicknessText";
			this.SimulatedThicknessText.Size = new System.Drawing.Size(183, 26);
			this.SimulatedThicknessText.TabIndex = 3;
			// 
			// ThicknessLabel
			// 
			this.ThicknessLabel.AutoSize = true;
			this.ThicknessLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ThicknessLabel.Location = new System.Drawing.Point(20, 138);
			this.ThicknessLabel.Name = "ThicknessLabel";
			this.ThicknessLabel.Size = new System.Drawing.Size(128, 20);
			this.ThicknessLabel.TabIndex = 6;
			this.ThicknessLabel.Text = "Measured Depth";
			// 
			// SimulatedMeasurementDialog
			// 
			this.AcceptButton = this.OKBut;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.CancelButton = this.CancelBut;
			this.ClientSize = new System.Drawing.Size(377, 269);
			this.Controls.Add(this.SimulatedThicknessText);
			this.Controls.Add(this.ThicknessLabel);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.OKBut);
			this.Controls.Add(this.SimulatedHeightText);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.SimulatedWidthText);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SimulatedMeasurementDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Manual Measurement Input";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox SimulatedWidthText;
		private System.Windows.Forms.TextBox SimulatedHeightText;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button OKBut;
		private System.Windows.Forms.Button CancelBut;
		private System.Windows.Forms.TextBox SimulatedThicknessText;
		private System.Windows.Forms.Label ThicknessLabel;
	}
}