﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class SimulatedMeasurementDialog : Form
	{

		private PersistentParametersV1100 LocalParams;
		private double SWidth;
		private double SHeight;
		private double SThickness;

		public double SimWidth
		{
			get { return SWidth; }
		}

		public double SimHeight
		{
			get { return SHeight; }
		}

		public double SimThickness
		{
			get { return SThickness; }
		}


		public SimulatedMeasurementDialog(PersistentParametersV1100 PParams, bool UseThickness)
		{
			InitializeComponent();

			// assign a local reference
			LocalParams = PParams;

			if (!UseThickness)
			{
				SimulatedThicknessText.Visible = false;
				ThicknessLabel.Visible = false;
			}


		}

		private bool TestMeasurementData()
		{


			bool Result = true;

			try
			{

				SWidth = Convert.ToDouble(SimulatedWidthText.Text);

				// Modify for native units (mm)
				if (LocalParams.SysUnits == MeasuringUnits.Inches)
					SWidth = SWidth * 25.4;



			}
			catch (Exception Ex)
			{

				MessageBox.Show(string.Format("Width Conversion Error: {0}", Ex.Message), "Conversion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				SimulatedWidthText.Text = "";
				Result = false;

			}


			try
			{
				SHeight = Convert.ToDouble(SimulatedHeightText.Text);

				// Modify for native units (mm)
				if (LocalParams.SysUnits == MeasuringUnits.Inches)
					SHeight = SHeight * 25.4;
			}

			catch (Exception Ex)
			{

				MessageBox.Show(string.Format("Height Conversion Error: {0}", Ex.Message), "Conversion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				SimulatedHeightText.Text = "";
				Result = false;

			}


			try
			{
				if (SimulatedThicknessText.Text != "")
				{
					SThickness = Convert.ToDouble(SimulatedThicknessText.Text);

					// Modify for native units (mm)
					if (LocalParams.SysUnits == MeasuringUnits.Inches)
						SThickness = SThickness * 25.4;
				}

				else
					SThickness = 0.0;


			}

			catch (Exception Ex)
			{

				MessageBox.Show(string.Format("Thickness Conversion Error: {0}", Ex.Message), "Conversion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				SimulatedThicknessText.Text = "";
				Result = false;

			}

			return Result;

		}
		

		private void OKBut_Click(object sender, EventArgs e)
		{

			// Only close if no errors
			if (TestMeasurementData())
				this.DialogResult = DialogResult.OK;


		}

		private void SimulatedHeightText_KeyPress(object sender, KeyPressEventArgs e)
		{

			if (e.KeyChar == 13)
			{
				// Only close if no errors
				if (TestMeasurementData())
				{
					this.DialogResult = DialogResult.OK;
				 	this.Close();
				}
			}

		}
	}
}
