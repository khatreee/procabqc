﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class CutlistTemplateConfigDialog : Form
	{

		private CutlistTemplateConfiguration TemplateConfig;
		private PersistentParametersV1100 LocalParams;
		private int TIndex;

		public CutlistTemplateConfigDialog()
		{
			InitializeComponent();
 		}


		public CutlistTemplateConfigDialog(PersistentParametersV1100 PParams, int TemplateIndex)
		{

			InitializeComponent();

			// Create a location to place the config panel
			Point LineItemPanelLocation = new Point(5, 5);

			TemplateConfig = new CutlistTemplateConfiguration(LineItemPanelLocation, ref PParams, TemplateIndex);
				 
			// Save a reference to params
			LocalParams = PParams;
			TIndex = TemplateIndex;

			// Get the template data
			CutlistTemplateDataV1100 TemplateData;

			LocalParams.GetMOTemplateData(TIndex, out TemplateData);


			if (TemplateData.CSVMeasureUnits == MeasuringUnits.Inches)
				InchSelect.Checked = true;

			else
				MillimetersSelect.Checked = true;


			// Measuring Accuracy for width and height
			WidthPlusAccuracyText.Text = TemplateData.WidthPlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			HeightPlusAccuracyText.Text = TemplateData.HeightPlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			WidthMinusAccuracyText.Text = TemplateData.WidthMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			HeightMinusAccuracyText.Text = TemplateData.HeightMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			ThicknessPlusAccuracyText.Text = TemplateData.ThicknessPlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			ThicknessMinusAccuracyText.Text = TemplateData.ThicknessMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			AnglePlusAccuracyText.Text = TemplateData.AnglePlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			AngleMinusAccuracyText.Text = TemplateData.AngleMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);





			this.Controls.Add(TemplateConfig.GetConfigurationPanel());
			this.ResumeLayout(false);


		}

		private void button1_Click(object sender, EventArgs e)
		{

			bool AccuracyResult = true;

			List<string> ErrorLog = new List<string>();

			// Get the saved template data for editing units/tolerances
			CutlistTemplateDataV1100 TemplateData;

			LocalParams.GetMOTemplateData(TIndex, out TemplateData);


			// The CSV measuring units
			if (InchSelect.Checked)
				TemplateData.CSVMeasureUnits = MeasuringUnits.Inches;

			else
				TemplateData.CSVMeasureUnits = MeasuringUnits.Millimeters;


			// Measuring Accuracy for height
			if (!TemplateData.HeightPlusTolerance.SetToleranceStr(HeightPlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring accuracy for width
			if (!TemplateData.WidthPlusTolerance.SetToleranceStr(WidthPlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring Accuracy for height
			if (!TemplateData.HeightMinusTolerance.SetToleranceStr(HeightMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring accuracy for width
			if (!TemplateData.WidthMinusTolerance.SetToleranceStr(WidthMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring accuracy for depth (thickness)
			if (!TemplateData.ThicknessPlusTolerance.SetToleranceStr(ThicknessPlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			if (!TemplateData.ThicknessMinusTolerance.SetToleranceStr(ThicknessMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;


			// Measuring accuracy for Angle
			if (!TemplateData.AnglePlusTolerance.SetToleranceStr(AnglePlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			if (!TemplateData.AngleMinusTolerance.SetToleranceStr(AngleMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;



			if(!TemplateConfig.VerifyTemplateEntries(ref ErrorLog))
			{
				string ErrorStr = "";

				for (int u = 0; u < ErrorLog.Count; u++)
					ErrorStr += string.Format("{0}\n", ErrorLog[u]);


				MessageBox.Show(ErrorStr, "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}

			// An error in one or more of the accuracy settings
			else if (!AccuracyResult)
				MessageBox.Show("Correct Accuracy Settings!", "Accuracy Error", MessageBoxButtons.OK, MessageBoxIcon.Error);


			// If no errors, everything ok. Close
			else
			{
				// Save data back to template
				LocalParams.SaveMOTemplateData(TIndex, TemplateData);

				// Set result as ok
				this.DialogResult = DialogResult.OK;
			}
 
		}
	}
}
