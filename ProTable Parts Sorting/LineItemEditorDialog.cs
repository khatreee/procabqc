﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace ProCABQC11
{
	public partial class LineItemEditorDialog : Form
	{


		private const string WidthRegistryName = "Edit LineItem Width";
		private const string HeightRegistryName = "Edit LineItem Height";
		private const string QtyReqRegistryName = "Edit LineItem Quantity Required";

		LineItem LocalItem;


		public LineItemEditorDialog()
		{
			InitializeComponent();
		}

		public LineItemEditorDialog(ref LineItem Item, PersistentParametersV1100 PParams)
		{

			InitializeComponent();

			// Save the reference for later use when OK pressed
			LocalItem = Item;

			// Copy data over
			LineNumberText.Text = Item.LineNumber.ToString();
			PartIDText.Text = Item.PartIdStr;
			WidthText.Text = Item.Width.ToString();
			HeightText.Text = Item.Height.ToString();
			QtyReqText.Text = Item.QtyRequired.ToString();

			// If a line item has at least one measured part, do NOT allow dimension editing EVEN IF ENABLED
			if (Item.MeasuredPartsCount > 0)
			{
				WidthText.ReadOnly = true;
				WidthText.TabStop = false;

				HeightText.ReadOnly = true;
				HeightText.TabStop = false;

				QtyReqText.ReadOnly = !PParams.CanEditLineItemQuantityReq;

				if (QtyReqText.ReadOnly)
					QtyReqText.TabStop = false;
				else
					QtyReqText.TabStop = true;

			}

			// No measured parts yet.  Allow dimensional overrides if enabled
			else
			{

				// Set items for read only if not editable (Note negation)
				if (PParams.CanEditLineItemWidth)
				{
					WidthText.ReadOnly = false;
					WidthText.TabStop = true;
				}

				else
				{
					WidthText.ReadOnly = true;
					WidthText.TabStop = false;
				}


				if (PParams.CanEditLineItemHeight)
				{
					HeightText.ReadOnly = false;
					HeightText.TabStop = true;
				}

				else
				{
					HeightText.ReadOnly = true;
					HeightText.TabStop = false;
				}

				if (PParams.CanEditLineItemQuantityReq)
				{
					QtyReqText.ReadOnly = false;
					QtyReqText.TabStop = true;
				}

				else
				{
					QtyReqText.ReadOnly = true;
					QtyReqText.TabStop = false;
				}
				
			}




		}


		private bool TestDataEntry()
		{

			bool Result = true;

			// Save data back

			// Height
			try
			{
				double Value = Convert.ToDouble(HeightText.Text);

				if (Value > 0.0)
					LocalItem.Height = Value;

				else
				{
					MessageBox.Show("Height must be greater than 0.0", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					Result = false;
				}

			}

			catch
			{
				MessageBox.Show("Invalid Height Data, not floating point data", "Conversion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				Result = false;
			}

			// Width
			try
			{
				double Value = Convert.ToDouble(WidthText.Text);

				if (Value > 0.0)
					LocalItem.Width = Value;

				else
				{
					MessageBox.Show("Width must be greater than 0.0", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					Result = false;
				}


			}

			catch
			{
				MessageBox.Show("Invalid Width Data, not floating point data", "Conversion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				Result = false;

			}


			// Quantity Required
			try
			{
				int Value = Convert.ToInt32(QtyReqText.Text);

				if ((Value >= 0) && (Value >= LocalItem.MeasuredPartsCount))
					LocalItem.QtyRequired = Value;

				// Quantity required cannot be less that quantity already measured
				else if (Value < LocalItem.MeasuredPartsCount)
				{

					MessageBox.Show("Quantity Required CANNOT be less than Quantity Measured", "Quantity Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					Result = false;

				}

				// No negative quantities
				else
				{
					MessageBox.Show("Quantity must be greater than or equal to 0", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					Result = false;
				}


			}

			catch
			{
				MessageBox.Show("Invalid Quantity Required Data, not integer data", "Conversion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				Result = false;
			}


			return Result;
		}



		// OK button clicked
		private void OKBut_Click(object sender, EventArgs e)
		{

			if(TestDataEntry())
				this.DialogResult = DialogResult.OK;

		}

		private void QtyReqText_KeyPress(object sender, KeyPressEventArgs e)
		{

			if (e.KeyChar == 13)
			{
				// Only close if no errors
				if (TestDataEntry())
				{
					this.DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}

		private void HeightText_KeyPress(object sender, KeyPressEventArgs e)
		{

			if (e.KeyChar == 13)
			{
				// Only close if no errors
				if (TestDataEntry())
				{
					this.DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}

		private void WidthText_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
			{
				// Only close if no errors
				if (TestDataEntry())
				{
					this.DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}

	}
}
