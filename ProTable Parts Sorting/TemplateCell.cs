﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{
	public class TemplateCell : TemplateItem
	{

		TemplateColumn Column;
		TemplateRow Row;

		public int GetColumn
		{
			get { return Column.GetColumnNumber; }
		}

		public int GetRow
		{
			get { return Row.GetRowNumber; }
		}

		// String accessor for column.  Used during configuration
		public string GetColumnString
		{
			get { return Column.GetStringValue; }
		}

		public bool SetColumnString(string Data, out string ErrorMsg)
		{
			return Column.SetStringValue(Data, FieldName, out ErrorMsg);
		}

		// String accessor for row. Used during configuration
		public string GetRowString
		{
			get { return Row.GetStringValue; }
		}

		public bool SetRowString(string Data, out string ErrorMsg)
		{
			return Row.SetStringValue(Data, FieldName, out ErrorMsg);
		}


		public bool IsDefined(int CSVLineNumber)
		{

			if (CSVLineNumber == Row.GetRowNumber)
			{
				if (Column.GetColumnNumber != -1)
					return true;
				else
					return false;
			}

			else
				return false;
		}

		public bool IsDefined()
		{
			return (Column.GetColumnNumber != -1);
		}


		public TemplateCell()
		{

		}

		public TemplateCell(CellSettingsType CellReference, string FName, ParameterDataType FDataType, bool Required)
		{

			// Initialize data
			TextName = FName;
			DataType = FDataType;
			RequiredStatus = Required;

			// Create column instance
			Column = new TemplateColumn(CellReference.Column);

			// Create row instance
			Row = new TemplateRow(CellReference.Row);


		}



		public override DataConversionResult GetParameterValue(string DataString, ref ParamStruct ParamData, ref string ErrorMsg)
		{

			if ((Row.GetRowNumber != -1) && (Column.GetColumnNumber != -1))
			{
				return ConvertStringToValue(DataString, ref ParamData, Column.GetStringValue, ref ErrorMsg);

			}

			else
			{
				return DataConversionResult.Undefined;
			}
		}


	}
}
