﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class SingleOrderTemplateConfigDialog : Form
	{

		private SingleOrderHeaderConfiguration HeaderConfig;
		private SingleOrderLineItemTemplateConfiguration LineItemConfig;
		private PersistentParametersV1100 LocalParams;
		private int TIndex;

		public SingleOrderTemplateConfigDialog()
		{
			InitializeComponent();
		}

		public SingleOrderTemplateConfigDialog(PersistentParametersV1100 PParams, int TemplateIndex)
		{

			InitializeComponent();


			// Create a location to place the config panel
			Point HeaderLocation = new Point(225, 5);

			HeaderConfig = new SingleOrderHeaderConfiguration(HeaderLocation, PParams, TemplateIndex);

			Point LineItemLocation = new Point(3, 5);

			LineItemConfig = new SingleOrderLineItemTemplateConfiguration(LineItemLocation, PParams, TemplateIndex);


			// Save a reference to params
			LocalParams = PParams;
			TIndex = TemplateIndex;


			// Get the template data
			SingleOrderTemplateDataV1100 TemplateData;

			LocalParams.GetSOTemplateData(TIndex, out TemplateData);


			if (TemplateData.CSVMeasureUnits == MeasuringUnits.Inches)
				InchSelect.Checked = true;

			else
				MillimetersSelect.Checked = true;


			// Measuring Accuracy for width and height
			WidthPlusAccuracyText.Text = TemplateData.WidthPlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			HeightPlusAccuracyText.Text = TemplateData.HeightPlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			WidthMinusAccuracyText.Text = TemplateData.WidthMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			HeightMinusAccuracyText.Text = TemplateData.HeightMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			ThicknessPlusAccuracyText.Text = TemplateData.ThicknessPlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			ThicknessMinusAccuracyText.Text = TemplateData.ThicknessMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			AnglePlusAccuracyText.Text = TemplateData.AnglePlusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);
			AngleMinusAccuracyText.Text = TemplateData.AngleMinusTolerance.GetToleranceStr(LocalParams.SysUnits, LocalParams.MMPrecisionVal, LocalParams.InchPrecisionVal);


			this.Controls.Add(HeaderConfig.GetConfigurationPanel());
			this.Controls.Add(LineItemConfig.GetConfigurationPanel());
			this.ResumeLayout(false);


		}

		private void button1_Click(object sender, EventArgs e)
		{

			List<string> ErrorLog = new List<string>();

			bool LineItemResult = LineItemConfig.VerifyTemplateEntries(ref ErrorLog);
			bool HeaderResult = HeaderConfig.VerifyTemplateEntries(ref ErrorLog);
			bool AccuracyResult = true;

			// Get the saved template data for editing units/tolerances
			SingleOrderTemplateDataV1100 TemplateData;

			LocalParams.GetSOTemplateData(TIndex, out TemplateData);



			// The CSV measuring units
			if (InchSelect.Checked)
				TemplateData.CSVMeasureUnits = MeasuringUnits.Inches;

			else
				TemplateData.CSVMeasureUnits = MeasuringUnits.Millimeters;


			// Measuring Accuracy for height
			if (!TemplateData.HeightPlusTolerance.SetToleranceStr(HeightPlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring accuracy for width
			if (!TemplateData.WidthPlusTolerance.SetToleranceStr(WidthPlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring Accuracy for height
			if (!TemplateData.HeightMinusTolerance.SetToleranceStr(HeightMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring accuracy for width
			if (!TemplateData.WidthMinusTolerance.SetToleranceStr(WidthMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring Accuracy for thickness plus
			if (!TemplateData.ThicknessPlusTolerance.SetToleranceStr(ThicknessPlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring accuracy for thickness minus
			if (!TemplateData.ThicknessMinusTolerance.SetToleranceStr(ThicknessMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;


			// Measuring Accuracy for angle plus
			if (!TemplateData.AnglePlusTolerance.SetToleranceStr(AnglePlusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;

			// Measuring accuracy for angle minus
			if (!TemplateData.AngleMinusTolerance.SetToleranceStr(AngleMinusAccuracyText.Text, LocalParams.SysUnits))
				AccuracyResult = false;


			if (!HeaderResult || !LineItemResult)
			{
				string ErrorStr = "";

				for (int u = 0; u < ErrorLog.Count; u++)
					ErrorStr += string.Format("{0}\n", ErrorLog[u]);


				MessageBox.Show(ErrorStr, "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}

			// An error in one or more of the accuracy settings
			else if (!AccuracyResult)
				MessageBox.Show("Correct Accuracy Settings!", "Accuracy Error", MessageBoxButtons.OK, MessageBoxIcon.Error);


			// If no errors, everything ok. Close
			else
			{
				// Save data back to template
				LocalParams.SaveSOTemplateData(TIndex, TemplateData);

				// Set the result as OK
				this.DialogResult = DialogResult.OK;
			}
 
		}





	}
}
