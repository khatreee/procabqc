﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ProCABQC11
{
	[Serializable]
    public class VirtualAxis : ICloneable
    {

		private string[] AxisParameterNameStrings = new string[] 
		{

			"#Axis ID",
			"Datum Preset",
			"Scaling Factor",
			"Direction",
			"#END AXIS"
		};


		private enum AxisParameterNames
		{
			AxisID,
			DatumPreset,
			ScalingFactor,
			Direction,
			EndParameters
		}

		


        private double RawPosition;
        private double Offset;
        private double DatumPreset;
        private int EncoderDirection;
		private double ScalingFactor = 1.0;
        private static int AxisCount;
        private int AxisID;
		private string AxisFileName;
        
        private bool FirstData;

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		public string ScaleFactor
		{

			get { return string.Format("{0:F5}", ScalingFactor); }
			set
			{

				// Only do if string not null
				if (value.Length > 0)
				{

					try
					{
						// Convert string to FP
						double NewVal = Convert.ToDouble(value);

						// round to 5 decimal places
						ScalingFactor = Math.Abs(Math.Round(NewVal, 5));

						// Save the scale factor
//						this.SaveParameters();

					}

					catch (FormatException fEx)
					{
						MessageBox.Show(fEx.Message, "Floating Point Failure");

					}
				}
			}
		}







        public VirtualAxis()
        {

 
            // Assign an axis ID
            AxisID = AxisCount++;

			// Make a file name based on the axis ID
			AxisFileName = string.Format("Axis{0}.dat", AxisID);

			// Initialize the data
			Offset = 0.0;
			DatumPreset = 0.0;
			ScalingFactor = 1.0;
			EncoderDirection = 1;

            RawPosition = 0.0;
            FirstData = false;


        }


		// Member Functions

		// Get the Axis ID string
		public string GetExportAxisIDString()
		{

			return string.Format("{0} {1}", AxisParameterNameStrings[(int)AxisParameterNames.AxisID], AxisID);

		}


		public void ExportParameters(StreamWriter Stream)
		{

			// Write Axis ID
			Stream.WriteLine(GetExportAxisIDString());

			// Write Datum Preset
			Stream.WriteLine(string.Format("{0},{1}", AxisParameterNameStrings[(int)AxisParameterNames.DatumPreset], DatumPreset));

			// Write Scaling Factor
			Stream.WriteLine(string.Format("{0},{1}", AxisParameterNameStrings[(int)AxisParameterNames.ScalingFactor], ScalingFactor));

			// Write Encoder Direction
			Stream.WriteLine(string.Format("{0},{1}", AxisParameterNameStrings[(int)AxisParameterNames.Direction], EncoderDirection));

			// Write end section
			Stream.WriteLine(string.Format("{0}", AxisParameterNameStrings[(int)AxisParameterNames.EndParameters]));

		}


		public bool ImportParamsValues(StreamReader Stream)
		{

			// Create an array of strings to pass to the line parser
			string[] Columns;

			Char Delimit = ',';
			bool Loop = true;
			bool Result = true;


			while (Loop)
			{

				string InputLine = "";

				// Read a line from the stream
				InputLine = Stream.ReadLine();

				// If eof, set return status as false (error, eof reached) and break
				if (InputLine == null)
				{
					Loop = false;
					Result = false;
				}

				// Not EOF.  Process imported string
				else
				{
					// Split the line into columns delimited by comma
					Columns = InputLine.Split(Delimit);

					// Check if at end of parameters
					if (Columns[0].Equals(AxisParameterNameStrings[(int)AxisParameterNames.EndParameters]))
						Loop = false;

					// Save parameter based on parameter ID
					else
					{
						// Only try to save if two parameters
						if (Columns.Length == 2)
						{
							// Import the parameter based on the field name
							UpdateParameterFromImport(Columns[0], Columns[1]);
						}

					}

				}

			}


			return Result;


		}


		// Locates the enumerated value of the parameter based on the passed string
		private bool LookupParameterID(string ParameterID, out AxisParameterNames ID)
		{
			ID = AxisParameterNames.EndParameters;
			bool Result = false;

			for (AxisParameterNames u = AxisParameterNames.AxisID; u < AxisParameterNames.EndParameters; u++)
			{
				if (AxisParameterNameStrings[(int)u].Equals(ParameterID))
				{
					ID = u;
					Result = true;
					break;
				}
			}

			return Result;
			
		}



		private void UpdateParameterFromImport(string ParameterID, string Data)
		{

			AxisParameterNames Index;

			// Locate the associated enumerated parameter ID based on the passed string
			if (LookupParameterID(ParameterID, out Index))
			{
				// if valid ID found, save data to appropriate data member
				switch (Index)
				{

					case AxisParameterNames.DatumPreset :

						DatumPreset = Convert.ToDouble(Data);
						break;


					case AxisParameterNames.Direction :

						EncoderDirection = Convert.ToInt32(Data);
						break;


					case AxisParameterNames.ScalingFactor :

						ScalingFactor = Convert.ToDouble(Data);
						break;

	
				}
			}
		}

			

		// Save parameters to disk in local directory
		public void SaveParameters()
		{

			BinaryFormatter Formatter = new BinaryFormatter();

			using (Stream fstream = new FileStream(AxisFileName, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				Formatter.Serialize(fstream, this);
			}

		}

		// Attempt to read parameter data file
		// Reads data if file exists and returns true
		// Returns false if no data file exists
		public bool ReadParameters(ref VirtualAxis Reference)
		{

			// Check if data file exists.  If so, open it and return true.  If not, false
			if (File.Exists(AxisFileName))
			{

				BinaryFormatter Formatter = new BinaryFormatter();

				using (Stream fstream = File.OpenRead(AxisFileName))
				{
					Reference = (VirtualAxis)Formatter.Deserialize(fstream);
				}

				return true;

			}

			else
				return false;

		}

        public void SetRawPosition(double Value)
        {

            if (EncoderDirection != 0)
                RawPosition = -Value;

            else
                RawPosition = Value;

            FirstData = true;

        }


        public void ApplyDatumPreset()
        {

            Offset = (-RawPosition * ScalingFactor);
            Offset += DatumPreset;

			// Save the offset
			this.SaveParameters();
            
        }

		public double GetNativePosition()
		{

			return (RawPosition * ScalingFactor) + Offset;

		}

	
        public String GetPositionString(MeasuringUnits Units, int Precision)
        {

            if (FirstData)
            {

                String AxisStr;
				double Position;

                // Generate position in FP
                Position = GetNativePosition();

                // Decimal Inches
                if (Units == MeasuringUnits.Inches)
                {

                    if(Precision == 4)
                        AxisStr = String.Format("{0:F4}", Position / 25.4);

                    else if(Precision == 3)
                        AxisStr = String.Format("{0:F3}", Position / 25.4);

                    else if (Precision == 2)
                        AxisStr = String.Format("{0:F2}", Position / 25.4);

                    else
                        AxisStr = String.Format("{0:F1}", Position / 25.4);

                    return AxisStr;


                }

                // Millimeters
                else if (Units == MeasuringUnits.Millimeters)
                {
                    if(Precision >= 2)
                        AxisStr = String.Format("{0:F2}", Position);

                    else
                        AxisStr = String.Format("{0:F1}", Position);

                    return AxisStr;

                }

                else
                    return "Invalid Units";
            }

            else
                return "No Data";


            
        }

        public String GetDatumPresetStr(MeasuringUnits Units, int Precision)
        {
			String AxisStr;

			if (Units == MeasuringUnits.Inches)
            {

				if (Precision == 4)
					AxisStr = String.Format("{0:F4}", DatumPreset / 25.4);

				else if (Precision == 3)
					AxisStr = String.Format("{0:F3}", DatumPreset / 25.4);

				else if (Precision == 2)
					AxisStr = String.Format("{0:F2}", DatumPreset / 25.4);

				else
					AxisStr = String.Format("{0:F1}", DatumPreset / 25.4);
				
            }

            else
            {

				if (Precision >= 2)
					AxisStr = String.Format("{0:F2}", DatumPreset);

				else
					AxisStr = String.Format("{0:F1}", DatumPreset);
                
            }

			return AxisStr;

        }

        public void SetDatumPreset(String Str, MeasuringUnits Units)
        {
            if (Str != "")
            {
                try
                {
                    double Val = Convert.ToDouble(Str);

					if (Units == MeasuringUnits.Inches)
                        DatumPreset = Val * 25.4;

                    else
                        DatumPreset = Val;

					// Save the datum preset
//					this.SaveParameters();
                }

                catch (FormatException fEx)
                {
                    MessageBox.Show(fEx.Message, "Floating Point Failure");

                }
            }

        }



        public String GetEncoderDirStr()
        {

            return EncoderDirection.ToString();

        }


        public void SetEncoderDir(String Str)
        {

            if (Str != "")
            {
                try
                {
                    int Val = Convert.ToInt32(Str);

                    // Limit to 0 or 1
                    if (Val > 1)
                        Val = 1;

                    else if (Val < 0)
                        Val = 0;

                    // Save to direction
                    EncoderDirection = Val;

					// Save the direction
//					this.SaveParameters();
                }

                catch (FormatException fEx)
                {
                    MessageBox.Show(fEx.Message, "Data Conversion Failure");

                }
            }

           

        }

        // Indicates if position data has been received from axis
        public bool IsPositionValid()
        {
            return FirstData;

        }




            
            
            



        

    }
}
