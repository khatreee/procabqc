namespace ProCABQC11
{
	partial class PackListReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.ShipToCountry = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToCountry = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToCustomerName = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToAddress1 = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToAddress2 = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToCity = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToState = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToZip = new DevExpress.XtraReports.UI.XRLabel();
			this.ShipToAddress2 = new DevExpress.XtraReports.UI.XRLabel();
			this.ShipToCustomerName = new DevExpress.XtraReports.UI.XRLabel();
			this.ShipToAddress1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.ShipToCity = new DevExpress.XtraReports.UI.XRLabel();
			this.ShipToState = new DevExpress.XtraReports.UI.XRLabel();
			this.ShipToZip = new DevExpress.XtraReports.UI.XRLabel();
			this.BillToContact = new DevExpress.XtraReports.UI.XRLabel();
			this.ShipToContact = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.OrderID = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.CompanyLogo = new DevExpress.XtraReports.UI.XRPictureBox();
			this.CompanyZip = new DevExpress.XtraReports.UI.XRLabel();
			this.CompanyState = new DevExpress.XtraReports.UI.XRLabel();
			this.CompanyCity = new DevExpress.XtraReports.UI.XRLabel();
			this.CompanyAddress2 = new DevExpress.XtraReports.UI.XRLabel();
			this.CompanyAddress1 = new DevExpress.XtraReports.UI.XRLabel();
			this.CompanyName = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
			this.CompanyTelephone = new DevExpress.XtraReports.UI.XRLabel();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ShipToCountry,
            this.BillToCountry,
            this.xrLabel1,
            this.BillToCustomerName,
            this.BillToAddress1,
            this.BillToAddress2,
            this.BillToCity,
            this.BillToState,
            this.BillToZip,
            this.ShipToAddress2,
            this.ShipToCustomerName,
            this.ShipToAddress1,
            this.xrLabel5,
            this.ShipToCity,
            this.ShipToState,
            this.ShipToZip,
            this.BillToContact,
            this.ShipToContact,
            this.xrLine2,
            this.xrLabel2,
            this.OrderID,
            this.xrLine3});
			this.Detail.Height = 758;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// ShipToCountry
			// 
			this.ShipToCountry.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToCountry.Location = new System.Drawing.Point(375, 175);
			this.ShipToCountry.Name = "ShipToCountry";
			this.ShipToCountry.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToCountry.Size = new System.Drawing.Size(167, 25);
			this.ShipToCountry.StylePriority.UseFont = false;
			this.ShipToCountry.Text = "ShipToCountry";
			// 
			// BillToCountry
			// 
			this.BillToCountry.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToCountry.Location = new System.Drawing.Point(8, 175);
			this.BillToCountry.Name = "BillToCountry";
			this.BillToCountry.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToCountry.Size = new System.Drawing.Size(175, 25);
			this.BillToCountry.StylePriority.UseFont = false;
			this.BillToCountry.Text = "BillToCountry";
			// 
			// xrLabel1
			// 
			this.xrLabel1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.Location = new System.Drawing.Point(8, 17);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.Size = new System.Drawing.Size(83, 25);
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.Text = "Bill To:";
			// 
			// BillToCustomerName
			// 
			this.BillToCustomerName.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToCustomerName.Location = new System.Drawing.Point(8, 50);
			this.BillToCustomerName.Name = "BillToCustomerName";
			this.BillToCustomerName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToCustomerName.Size = new System.Drawing.Size(233, 25);
			this.BillToCustomerName.StylePriority.UseFont = false;
			this.BillToCustomerName.Text = "BillToCustomerName";
			// 
			// BillToAddress1
			// 
			this.BillToAddress1.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToAddress1.Location = new System.Drawing.Point(8, 100);
			this.BillToAddress1.Name = "BillToAddress1";
			this.BillToAddress1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToAddress1.Size = new System.Drawing.Size(233, 25);
			this.BillToAddress1.StylePriority.UseFont = false;
			this.BillToAddress1.Text = "BillToAddress1";
			// 
			// BillToAddress2
			// 
			this.BillToAddress2.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToAddress2.Location = new System.Drawing.Point(8, 125);
			this.BillToAddress2.Name = "BillToAddress2";
			this.BillToAddress2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToAddress2.Size = new System.Drawing.Size(233, 25);
			this.BillToAddress2.StylePriority.UseFont = false;
			this.BillToAddress2.Text = "BillToAddress2";
			// 
			// BillToCity
			// 
			this.BillToCity.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToCity.Location = new System.Drawing.Point(8, 150);
			this.BillToCity.Name = "BillToCity";
			this.BillToCity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToCity.Size = new System.Drawing.Size(150, 25);
			this.BillToCity.StylePriority.UseFont = false;
			this.BillToCity.Text = "BillToCity";
			// 
			// BillToState
			// 
			this.BillToState.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToState.Location = new System.Drawing.Point(167, 150);
			this.BillToState.Name = "BillToState";
			this.BillToState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToState.Size = new System.Drawing.Size(42, 25);
			this.BillToState.StylePriority.UseFont = false;
			this.BillToState.Text = "BillToState";
			// 
			// BillToZip
			// 
			this.BillToZip.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToZip.Location = new System.Drawing.Point(217, 150);
			this.BillToZip.Name = "BillToZip";
			this.BillToZip.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToZip.Size = new System.Drawing.Size(58, 25);
			this.BillToZip.StylePriority.UseFont = false;
			this.BillToZip.Text = "BillToZip";
			// 
			// ShipToAddress2
			// 
			this.ShipToAddress2.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToAddress2.Location = new System.Drawing.Point(375, 125);
			this.ShipToAddress2.Name = "ShipToAddress2";
			this.ShipToAddress2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToAddress2.Size = new System.Drawing.Size(233, 25);
			this.ShipToAddress2.StylePriority.UseFont = false;
			this.ShipToAddress2.Text = "ShipToAddress2";
			// 
			// ShipToCustomerName
			// 
			this.ShipToCustomerName.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToCustomerName.Location = new System.Drawing.Point(375, 50);
			this.ShipToCustomerName.Name = "ShipToCustomerName";
			this.ShipToCustomerName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToCustomerName.Size = new System.Drawing.Size(233, 25);
			this.ShipToCustomerName.StylePriority.UseFont = false;
			this.ShipToCustomerName.Text = "ShipToCustomerName";
			// 
			// ShipToAddress1
			// 
			this.ShipToAddress1.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToAddress1.Location = new System.Drawing.Point(375, 100);
			this.ShipToAddress1.Name = "ShipToAddress1";
			this.ShipToAddress1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToAddress1.Size = new System.Drawing.Size(233, 25);
			this.ShipToAddress1.StylePriority.UseFont = false;
			this.ShipToAddress1.Text = "ShipToAddress1";
			// 
			// xrLabel5
			// 
			this.xrLabel5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.Location = new System.Drawing.Point(375, 17);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.Size = new System.Drawing.Size(92, 25);
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.Text = "Ship To:";
			// 
			// ShipToCity
			// 
			this.ShipToCity.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToCity.Location = new System.Drawing.Point(375, 150);
			this.ShipToCity.Name = "ShipToCity";
			this.ShipToCity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToCity.Size = new System.Drawing.Size(150, 25);
			this.ShipToCity.StylePriority.UseFont = false;
			this.ShipToCity.Text = "ShipToCity";
			// 
			// ShipToState
			// 
			this.ShipToState.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToState.Location = new System.Drawing.Point(533, 150);
			this.ShipToState.Name = "ShipToState";
			this.ShipToState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToState.Size = new System.Drawing.Size(42, 25);
			this.ShipToState.StylePriority.UseFont = false;
			this.ShipToState.Text = "ShipToState";
			// 
			// ShipToZip
			// 
			this.ShipToZip.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToZip.Location = new System.Drawing.Point(583, 150);
			this.ShipToZip.Name = "ShipToZip";
			this.ShipToZip.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToZip.Size = new System.Drawing.Size(67, 25);
			this.ShipToZip.StylePriority.UseFont = false;
			this.ShipToZip.Text = "ShipToZip";
			// 
			// BillToContact
			// 
			this.BillToContact.Font = new System.Drawing.Font("Arial", 12F);
			this.BillToContact.Location = new System.Drawing.Point(8, 75);
			this.BillToContact.Name = "BillToContact";
			this.BillToContact.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.BillToContact.Size = new System.Drawing.Size(233, 25);
			this.BillToContact.StylePriority.UseFont = false;
			this.BillToContact.Text = "BillToContact";
			// 
			// ShipToContact
			// 
			this.ShipToContact.Font = new System.Drawing.Font("Arial", 12F);
			this.ShipToContact.Location = new System.Drawing.Point(375, 75);
			this.ShipToContact.Name = "ShipToContact";
			this.ShipToContact.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.ShipToContact.Size = new System.Drawing.Size(233, 25);
			this.ShipToContact.StylePriority.UseFont = false;
			this.ShipToContact.Text = "ShipToContact";
			// 
			// xrLine2
			// 
			this.xrLine2.Location = new System.Drawing.Point(0, 208);
			this.xrLine2.Name = "xrLine2";
			this.xrLine2.Size = new System.Drawing.Size(650, 17);
			// 
			// xrLabel2
			// 
			this.xrLabel2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.Location = new System.Drawing.Point(0, 233);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.Size = new System.Drawing.Size(142, 25);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.Text = "Order Number:";
			// 
			// OrderID
			// 
			this.OrderID.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.OrderID.Location = new System.Drawing.Point(150, 233);
			this.OrderID.Name = "OrderID";
			this.OrderID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.OrderID.Size = new System.Drawing.Size(233, 25);
			this.OrderID.StylePriority.UseFont = false;
			this.OrderID.Text = "OrderID";
			// 
			// xrLine3
			// 
			this.xrLine3.Location = new System.Drawing.Point(0, 267);
			this.xrLine3.Name = "xrLine3";
			this.xrLine3.Size = new System.Drawing.Size(650, 8);
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.CompanyLogo,
            this.CompanyZip,
            this.CompanyState,
            this.CompanyCity,
            this.CompanyAddress2,
            this.CompanyAddress1,
            this.CompanyName,
            this.xrLine1,
            this.CompanyTelephone});
			this.PageHeader.Height = 172;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// CompanyLogo
			// 
			this.CompanyLogo.Location = new System.Drawing.Point(333, 8);
			this.CompanyLogo.Name = "CompanyLogo";
			this.CompanyLogo.Size = new System.Drawing.Size(317, 142);
			this.CompanyLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// CompanyZip
			// 
			this.CompanyZip.Font = new System.Drawing.Font("Arial", 12F);
			this.CompanyZip.Location = new System.Drawing.Point(242, 92);
			this.CompanyZip.Name = "CompanyZip";
			this.CompanyZip.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.CompanyZip.Size = new System.Drawing.Size(83, 25);
			this.CompanyZip.StylePriority.UseFont = false;
			this.CompanyZip.Text = "CompanyZip";
			// 
			// CompanyState
			// 
			this.CompanyState.Font = new System.Drawing.Font("Arial", 12F);
			this.CompanyState.Location = new System.Drawing.Point(175, 92);
			this.CompanyState.Name = "CompanyState";
			this.CompanyState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.CompanyState.Size = new System.Drawing.Size(58, 25);
			this.CompanyState.StylePriority.UseFont = false;
			this.CompanyState.Text = "CompanyState";
			// 
			// CompanyCity
			// 
			this.CompanyCity.Font = new System.Drawing.Font("Arial", 12F);
			this.CompanyCity.Location = new System.Drawing.Point(8, 92);
			this.CompanyCity.Name = "CompanyCity";
			this.CompanyCity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.CompanyCity.Size = new System.Drawing.Size(150, 25);
			this.CompanyCity.StylePriority.UseFont = false;
			this.CompanyCity.Text = "CompanyCity";
			// 
			// CompanyAddress2
			// 
			this.CompanyAddress2.Font = new System.Drawing.Font("Arial", 12F);
			this.CompanyAddress2.Location = new System.Drawing.Point(8, 67);
			this.CompanyAddress2.Name = "CompanyAddress2";
			this.CompanyAddress2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.CompanyAddress2.Size = new System.Drawing.Size(317, 25);
			this.CompanyAddress2.StylePriority.UseFont = false;
			this.CompanyAddress2.Text = "CompanyAddress2";
			// 
			// CompanyAddress1
			// 
			this.CompanyAddress1.Font = new System.Drawing.Font("Arial", 12F);
			this.CompanyAddress1.Location = new System.Drawing.Point(8, 42);
			this.CompanyAddress1.Name = "CompanyAddress1";
			this.CompanyAddress1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.CompanyAddress1.Size = new System.Drawing.Size(317, 25);
			this.CompanyAddress1.StylePriority.UseFont = false;
			this.CompanyAddress1.Text = "CompanyAddress1";
			// 
			// CompanyName
			// 
			this.CompanyName.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CompanyName.Location = new System.Drawing.Point(8, 8);
			this.CompanyName.Name = "CompanyName";
			this.CompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.CompanyName.Size = new System.Drawing.Size(317, 25);
			this.CompanyName.StylePriority.UseFont = false;
			this.CompanyName.Text = "CompanyName";
			// 
			// xrLine1
			// 
			this.xrLine1.Location = new System.Drawing.Point(0, 150);
			this.xrLine1.Name = "xrLine1";
			this.xrLine1.Size = new System.Drawing.Size(650, 17);
			// 
			// CompanyTelephone
			// 
			this.CompanyTelephone.Font = new System.Drawing.Font("Arial", 12F);
			this.CompanyTelephone.Location = new System.Drawing.Point(8, 117);
			this.CompanyTelephone.Name = "CompanyTelephone";
			this.CompanyTelephone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.CompanyTelephone.Size = new System.Drawing.Size(158, 25);
			this.CompanyTelephone.StylePriority.UseFont = false;
			this.CompanyTelephone.Text = "CompanyTelephone";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 39;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// PackListReport
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter});
			this.Margins = new System.Drawing.Printing.Margins(100, 100, 60, 60);
			this.Name = "PackListReport";
			this.PageHeight = 1100;
			this.PageWidth = 850;
			this.Version = "9.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRLabel BillToCustomerName;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRLabel BillToCity;
		private DevExpress.XtraReports.UI.XRLabel BillToAddress2;
		private DevExpress.XtraReports.UI.XRLabel BillToAddress1;
		private DevExpress.XtraReports.UI.XRLabel BillToState;
		private DevExpress.XtraReports.UI.XRLabel BillToZip;
		private DevExpress.XtraReports.UI.XRLabel ShipToZip;
		private DevExpress.XtraReports.UI.XRLabel ShipToState;
		private DevExpress.XtraReports.UI.XRLabel ShipToCity;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel ShipToAddress1;
		private DevExpress.XtraReports.UI.XRLabel ShipToCustomerName;
		private DevExpress.XtraReports.UI.XRLabel ShipToAddress2;
		private DevExpress.XtraReports.UI.XRLabel ShipToContact;
		private DevExpress.XtraReports.UI.XRLabel BillToContact;
		private DevExpress.XtraReports.UI.XRLabel CompanyZip;
		private DevExpress.XtraReports.UI.XRLabel CompanyState;
		private DevExpress.XtraReports.UI.XRLabel CompanyCity;
		private DevExpress.XtraReports.UI.XRLabel CompanyAddress2;
		private DevExpress.XtraReports.UI.XRLabel CompanyAddress1;
		private DevExpress.XtraReports.UI.XRLabel CompanyName;
		private DevExpress.XtraReports.UI.XRLine xrLine2;
		private DevExpress.XtraReports.UI.XRLine xrLine1;
		private DevExpress.XtraReports.UI.XRLine xrLine3;
		private DevExpress.XtraReports.UI.XRLabel OrderID;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel ShipToCountry;
		private DevExpress.XtraReports.UI.XRLabel BillToCountry;
		private DevExpress.XtraReports.UI.XRLabel CompanyTelephone;
		private DevExpress.XtraReports.UI.XRPictureBox CompanyLogo;
	}
}
