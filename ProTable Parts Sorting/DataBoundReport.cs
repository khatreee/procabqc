using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace ProCABQC11
{
	public partial class DataBoundReport : DevExpress.XtraReports.UI.XtraReport
	{
		public DataBoundReport()
		{
			InitializeComponent();
		}

		public void GetDataSource(out DataSet Data)
		{

			Data = SystemData;

		}

		public void SetDataSource(DataSet Data)
		{
			SystemData = Data;
		}

		private void DataBoundReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{

		}



	}
}
