﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace ProCABQC11
{
	public class TemplateRow
	{

		protected int Row;

		public string GetStringValue
		{

			get
			{
				// if row undefined, return empty string
				if (Row < 0)
					return "";

				// If defined, return Row + 1.  Row is zero based.  CSV is 1 based
				else
					return (Row + 1).ToString();
			}

		}


		public bool SetStringValue(string Str, string Name, out string ErrorMsg)
		{

			// Check if null string.  If so, row undefined
			if (Str.Length == 0)
			{
				Row = -1;
				ErrorMsg = "";

				return true;
			}

			// Not null string.  try to convert to integer
			else
			{
				try
				{
					// Try to convert passed string to valid integer
					// If successful, test for validity, save and return true
					int Temp = Convert.ToInt32(Str) - 1;

					if (Temp < 0)
					{
						ErrorMsg = string.Format("Error:  {0} - Line numbers must be greater than 0", Name);
						return false;
					}

					else
					{
						Row = Temp;
						ErrorMsg = "";

						return true;
					}
				}

				catch
				{
					ErrorMsg = string.Format("Error:  {0} - Invalid input data.  Must be numeric only", Name);
					return false;
				}
			}
		}


		public int GetRowNumber
		{
			get { return Row; }
		}


		// The default constructor
		public TemplateRow()
		{

		}


		// The overridden constructor
		public TemplateRow(int RowReference)
		{

			Row = RowReference;

		}
	}
}
