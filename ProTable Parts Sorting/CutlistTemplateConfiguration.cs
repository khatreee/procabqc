﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	class CutlistTemplateConfiguration
	{
		// Position constants
		private const int TitleYPos = 5;
		private const int StartYPos = TitleYPos + 60;
		private const int ColumnTitleYPos = StartYPos - 25;
		private const int RowIncrementYPos = 28;

		private const int TextBoxXSize = 40;
		private const int TextBoxYSize = 20;

		private const int PanelXPadding = 10;
		private const int PanelYSize = 650;


		private const string LineItemTitleText = "Line Items";
		private const string BillToTitleText = "Bill To Customer";
		private const string ShipToTitleText = "Ship To Customer";
		private const string LineItemStartText = "Line Item Start Row";
		private const string OrderIDText = "Order ID";


		// Line item X position constants
		private const int LineItemTitleXPos = 50;
		private const int LineItemNameLabelXPos = 5;
		private const int LineItemDataTextBoxXPos = 160;
		private const int LineItemColumnTitleXPos = LineItemDataTextBoxXPos - 12;

		private const int LineItemPanelXSize = 220;


		// Bill To Customer X position constants
		private const int BillToTitleXPos = 50;
		private const int BillToNameLabelXPos = 35;
		private const int BillToDataTextBoxXPos = BillToNameLabelXPos + 155;
		private const int BillToColumnTitleXPos = BillToDataTextBoxXPos - 12;

		// Ship To Customer X position constants
		private const int ShipToTitleXPos = 315;
		private const int ShipToNameLabelXPos = 305;
		private const int ShipToDataTextBoxXPos = ShipToNameLabelXPos + 165;
		private const int ShipToColumnTitleXPos = ShipToDataTextBoxXPos - 12;

		private const int CustomerPanelXSize = 550;






		Panel LineItemPanel;
		Panel CustomerPanel;
		Panel TemplateConfigPanel;

		Label[] TemplateNames;
		TextBox[] TemplateColumnData;
		
		Label LineItemTitle;
		Label BillToTitle;
		Label ShipToTitle;

		Label LineItemColumnTitle;
		Label BillToColumnTitle;
		Label ShipToColumnTitle;

		Label LineItemStartLabel;
		TextBox LineItemStartData;

		CutlistTemplate LineItemTemplate;

		// A reference to the params
		PersistentParametersV1100 LocalParams;

		// local copy of template index
		int LocalTemplateIndex;


		public CutlistTemplateConfiguration()
		{

		}

		public CutlistTemplateConfiguration(Point PanelLocation, ref PersistentParametersV1100 PParams, int TemplateIndex)
		{

			// Create Panels
			LineItemPanel = new Panel();
			CustomerPanel = new Panel();
			TemplateConfigPanel = new Panel();


			LineItemPanel.SuspendLayout();
			CustomerPanel.SuspendLayout();
			TemplateConfigPanel.SuspendLayout();

			 
			// Create arrays of labels for template names
			TemplateNames = new Label[(int)CutlistLineItemColumnIDV1100.MaxItems];

			// Create array of text boxes for template data
			TemplateColumnData = new TextBox[(int)CutlistLineItemColumnIDV1100.MaxItems];

			// Create the template
			LineItemTemplate = new CutlistTemplate(ref PParams, TemplateIndex);

			// Store a object level reference to system params for use in VerifyTemplateEntries()
			LocalParams = PParams;

			// Save a copy
			LocalTemplateIndex = TemplateIndex;



			// Create the labels and textboxes for each template
			for (CutlistLineItemColumnIDV1100 u = CutlistLineItemColumnIDV1100.LineItemNumber; u < CutlistLineItemColumnIDV1100.MaxItems; u++)
			{
				TemplateNames[(int)u] = new Label();
				TemplateColumnData[(int)u] = new TextBox();
			}

			// A template item reference
			TemplateLineItemColumn Template;


			// Build screen for LineItem fields

			// Initialize Y position
			int YPos = StartYPos;
			
 			// Get each template item for the LineItems and load the data into the labels and text boxes
			for (CutlistLineItemColumnIDV1100 u = CutlistLineItemColumnIDV1100.LineItemNumber; u <= CutlistLineItemColumnIDV1100.UserDef3; u++)
			{
				// Get template item from template
				LineItemTemplate.GetLineItemColumnTemplate(u, out Template);

				// Update the text data
				TemplateNames[(int)u].Text = Template.FieldName;
				TemplateColumnData[(int)u].Text = Template.GetColumnString;

				// Set the parameters for the label
				TemplateNames[(int)u].Location = new Point(LineItemNameLabelXPos, YPos);
				TemplateNames[(int)u].AutoSize = true;

				// Set the parameters for the text box
				TemplateColumnData[(int)u].Location = new Point(LineItemDataTextBoxXPos, YPos);
				TemplateColumnData[(int)u].Size = new Size(TextBoxXSize, TextBoxYSize);

				// Increment to the next row in Y
				YPos += RowIncrementYPos;


				// Add the object to the panel
				LineItemPanel.Controls.Add(TemplateNames[(int)u]);
				LineItemPanel.Controls.Add(TemplateColumnData[(int)u]);

			}

			// Add extra space between line item start and other fields
			YPos += 15;

			// Create line item row start label
			LineItemStartLabel = new Label();
			LineItemStartLabel.Text = LineItemStartText;
			LineItemStartLabel.AutoSize = true;
			LineItemStartLabel.Location = new Point(LineItemNameLabelXPos, YPos);

			// Create text box for line item start data
			LineItemStartData = new TextBox();
			LineItemStartData.Text = LineItemTemplate.GetLineItemStartString;
			LineItemStartData.Location = new Point(LineItemDataTextBoxXPos, YPos);
			LineItemStartData.Size = new Size(TextBoxXSize, TextBoxYSize);

			// Create the title	and format it
			LineItemTitle = new Label();
			LineItemTitle.Location = new Point(LineItemTitleXPos, TitleYPos);
			LineItemTitle.Text = LineItemTitleText;
			LineItemTitle.AutoSize = true;
			LineItemTitle.Font = new Font("Microsoft Sans Serif", (float)16);

			// Create the column title
			LineItemColumnTitle = new Label();
			LineItemColumnTitle.Location = new Point(LineItemColumnTitleXPos, ColumnTitleYPos);
			LineItemColumnTitle.Text = "Column";
			LineItemColumnTitle.AutoSize = true;


			// Add the other components to the panel
			LineItemPanel.Controls.Add(LineItemTitle);
			LineItemPanel.Controls.Add(LineItemColumnTitle);
			LineItemPanel.Controls.Add(LineItemStartLabel);
			LineItemPanel.Controls.Add(LineItemStartData);


			// Build screen for BillTo fields

			// Initialize Y position
			YPos = StartYPos;

			// Get each template item for the LineItems and load the data into the labels and text boxes
			for (CutlistLineItemColumnIDV1100 u = CutlistLineItemColumnIDV1100.BillCustomerName; u <= CutlistLineItemColumnIDV1100.BillEmail; u++)
			{
				// Get template item from template
				LineItemTemplate.GetLineItemColumnTemplate(u, out Template);

				// Update the text data
				TemplateNames[(int)u].Text = Template.FieldName;
				TemplateColumnData[(int)u].Text = Template.GetColumnString;

				// Set the parameters for the label
				TemplateNames[(int)u].Location = new Point(BillToNameLabelXPos, YPos);
				TemplateNames[(int)u].AutoSize = true;

				// Set the parameters for the text box
				TemplateColumnData[(int)u].Location = new Point(BillToDataTextBoxXPos, YPos);
				TemplateColumnData[(int)u].Size = new Size(TextBoxXSize, TextBoxYSize);

				// Increment to the next row in Y
				YPos += RowIncrementYPos;


				// Add the object to the panel
				CustomerPanel.Controls.Add(TemplateNames[(int)u]);
				CustomerPanel.Controls.Add(TemplateColumnData[(int)u]);

			}

			// Add extra space between Order ID column and other fields
			YPos += 15;

			// Get Order ID Template Item
			LineItemTemplate.GetLineItemColumnTemplate(CutlistLineItemColumnIDV1100.OrderID, out Template);

			// Set the Order ID label 
			TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderID].Text = Template.FieldName;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderID].AutoSize = true;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderID].Location = new Point(BillToNameLabelXPos, YPos);

			// Create text box for Order ID data
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderID].Text = Template.GetColumnString;
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderID].Location = new Point(BillToDataTextBoxXPos, YPos);
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderID].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Increment to the next row in Y
			YPos += RowIncrementYPos;

			// Get Pack List Template Item
			LineItemTemplate.GetLineItemColumnTemplate(CutlistLineItemColumnIDV1100.PackListTemplate, out Template);

			// Set the Pack List Template label 
			TemplateNames[(int)CutlistLineItemColumnIDV1100.PackListTemplate].Text = Template.FieldName;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.PackListTemplate].AutoSize = true;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.PackListTemplate].Location = new Point(BillToNameLabelXPos, YPos);

			// Create text box for Packlist template data
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PackListTemplate].Text = Template.GetColumnString;
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PackListTemplate].Location = new Point(BillToDataTextBoxXPos, YPos);
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PackListTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Increment to the next row in Y
			YPos += RowIncrementYPos;

			// Get Part Label Template Item
			LineItemTemplate.GetLineItemColumnTemplate(CutlistLineItemColumnIDV1100.PartLabelTemplate, out Template);

			// Set the Part Label Template label 
			TemplateNames[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate].Text = Template.FieldName;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate].AutoSize = true;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate].Location = new Point(BillToNameLabelXPos, YPos);

			// Create text box for Part Label template data
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate].Text = Template.GetColumnString;
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate].Location = new Point(BillToDataTextBoxXPos, YPos);
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Increment to the next row in Y
			YPos += RowIncrementYPos;

			// Get Order user def 1 Item
			LineItemTemplate.GetLineItemColumnTemplate(CutlistLineItemColumnIDV1100.OrderUserDefined1, out Template);

			// Set the Order user def 1 label 
			TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1].Text = Template.FieldName;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1].AutoSize = true;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1].Location = new Point(BillToNameLabelXPos, YPos);

			// Create text box for Order user def 1 data
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1].Text = Template.GetColumnString;
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1].Location = new Point(BillToDataTextBoxXPos, YPos);
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Increment to the next row in Y
			YPos += RowIncrementYPos;

			// Get Order user def 1 Item
			LineItemTemplate.GetLineItemColumnTemplate(CutlistLineItemColumnIDV1100.TestThicknessDim, out Template);

			// Set the Order user def 1 label 
			TemplateNames[(int)CutlistLineItemColumnIDV1100.TestThicknessDim].Text = Template.FieldName;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.TestThicknessDim].AutoSize = true;
			TemplateNames[(int)CutlistLineItemColumnIDV1100.TestThicknessDim].Location = new Point(BillToNameLabelXPos, YPos);

			// Create text box for Order user def 1 data
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.TestThicknessDim].Text = Template.GetColumnString;
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.TestThicknessDim].Location = new Point(BillToDataTextBoxXPos, YPos);
			TemplateColumnData[(int)CutlistLineItemColumnIDV1100.TestThicknessDim].Size = new Size(TextBoxXSize, TextBoxYSize);

			// Increment to the next row in Y
			YPos += RowIncrementYPos;

			// Create the title	and format it
			BillToTitle = new Label();
			BillToTitle.Location = new Point(BillToTitleXPos, TitleYPos);
			BillToTitle.Text = BillToTitleText;
			BillToTitle.AutoSize = true;
			BillToTitle.Font = new Font("Microsoft Sans Serif", (float)16);

			// Create the column title
			BillToColumnTitle = new Label();
			BillToColumnTitle.Location = new Point(BillToColumnTitleXPos, ColumnTitleYPos);
			BillToColumnTitle.Text = "Column";
			BillToColumnTitle.AutoSize = true;


			// Add the other components to the panel
			CustomerPanel.Controls.Add(BillToTitle);
			CustomerPanel.Controls.Add(BillToColumnTitle);

			// The order ID label
			CustomerPanel.Controls.Add(TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderID]);

			// The order ID data
			CustomerPanel.Controls.Add(TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderID]);

			// The Packlist Template label
			CustomerPanel.Controls.Add(TemplateNames[(int)CutlistLineItemColumnIDV1100.PackListTemplate]);

			// The Packlist Template data
			CustomerPanel.Controls.Add(TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PackListTemplate]);

			// The Part Label Template label
			CustomerPanel.Controls.Add(TemplateNames[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate]);

			// The Part Label Template data
			CustomerPanel.Controls.Add(TemplateColumnData[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate]);

			// The Order User def 1 label
			CustomerPanel.Controls.Add(TemplateNames[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1]);

			// The Order user def 1 data
			CustomerPanel.Controls.Add(TemplateColumnData[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1]);

			// The Test Thickness (depth) label
			CustomerPanel.Controls.Add(TemplateNames[(int)CutlistLineItemColumnIDV1100.TestThicknessDim]);

			// The Test Thickness (depth) data
			CustomerPanel.Controls.Add(TemplateColumnData[(int)CutlistLineItemColumnIDV1100.TestThicknessDim]);




			// Build screen for ShipTo fields

			// Initialize Y position
			YPos = StartYPos;

			// Get each template item for the LineItems and load the data into the labels and text boxes
			for (CutlistLineItemColumnIDV1100 u = CutlistLineItemColumnIDV1100.ShipCustomerName; u <= CutlistLineItemColumnIDV1100.ShipEmail; u++)
			{
				// Get template item from template
				LineItemTemplate.GetLineItemColumnTemplate(u, out Template);

				// Update the text data
				TemplateNames[(int)u].Text = Template.FieldName;
				TemplateColumnData[(int)u].Text = Template.GetColumnString;

				// Set the parameters for the label
				TemplateNames[(int)u].Location = new Point(ShipToNameLabelXPos, YPos);
				TemplateNames[(int)u].AutoSize = true;

				// Set the parameters for the text box
				TemplateColumnData[(int)u].Location = new Point(ShipToDataTextBoxXPos, YPos);
				TemplateColumnData[(int)u].Size = new Size(TextBoxXSize, TextBoxYSize);

				// Increment to the next row in Y
				YPos += RowIncrementYPos;


				// Add the object to the panel
				CustomerPanel.Controls.Add(TemplateNames[(int)u]);
				CustomerPanel.Controls.Add(TemplateColumnData[(int)u]);

			}


			// Add extra space between line item start and other fields
			YPos += RowIncrementYPos;

			// Create the title	and format it
			ShipToTitle = new Label();
			ShipToTitle.Location = new Point(ShipToTitleXPos, TitleYPos);
			ShipToTitle.Text = ShipToTitleText;
			ShipToTitle.AutoSize = true;
			ShipToTitle.Font = new Font("Microsoft Sans Serif", (float)16);

			// Create the column title
			ShipToColumnTitle = new Label();
			ShipToColumnTitle.Location = new Point(ShipToColumnTitleXPos, ColumnTitleYPos);
			ShipToColumnTitle.Text = "Column";
			ShipToColumnTitle.AutoSize = true;


			// Add the other components to the panel
			CustomerPanel.Controls.Add(ShipToTitle);
			CustomerPanel.Controls.Add(ShipToColumnTitle);

					

			// Configure the panel location, size and other parameters
			LineItemPanel.Location = new Point(0, 0);
			LineItemPanel.Size = new Size(LineItemPanelXSize, PanelYSize);
			LineItemPanel.Font = new Font("Microsoft Sans Serif", (float)12);
			LineItemPanel.BorderStyle = BorderStyle.FixedSingle;
//			LineItemPanel.Margin = new Padding(5, 5, 5, 5);
			LineItemPanel.ResumeLayout(false);

			CustomerPanel.Location = new Point(LineItemPanelXSize + PanelXPadding, 0);
			CustomerPanel.Size = new Size(CustomerPanelXSize, PanelYSize);
			CustomerPanel.Font = new Font("Microsoft Sans Serif", (float)12);
			CustomerPanel.BorderStyle = BorderStyle.FixedSingle;
//			CustomerPanel.Margin = new Padding(5, 5, 5, 5);
			CustomerPanel.ResumeLayout(false);

			// Add the individual panels to the configuration panel
			TemplateConfigPanel.Location = PanelLocation;
			TemplateConfigPanel.Controls.Add(LineItemPanel);
			TemplateConfigPanel.Controls.Add(CustomerPanel);
			TemplateConfigPanel.Size = new Size(LineItemPanelXSize + 10 + CustomerPanelXSize + PanelLocation.X, PanelYSize);
			TemplateConfigPanel.ResumeLayout(true);







		}

		public Panel GetConfigurationPanel()
		{

			return TemplateConfigPanel;

		}


		public bool VerifyTemplateEntries(ref List<string> Errors)
		{

			bool NoErrors = true;
			string ErrorMsg = "";

			// Get a copy of the original template data
			CutlistTemplateDataV1100 TemplateData;
			LocalParams.GetMOTemplateData(LocalTemplateIndex, out TemplateData);

			// A template item reference
			TemplateLineItemColumn Template;

			// Let template items attempt to save data. 
			for (CutlistLineItemColumnIDV1100 u = CutlistLineItemColumnIDV1100.LineItemNumber; u < CutlistLineItemColumnIDV1100.MaxItems; u++)
			{
				// Get template item from template
				LineItemTemplate.GetLineItemColumnTemplate(u, out Template);
 
				// Attempt to set the column string value of the template item.  If false, an error occured
				bool Result = Template.SetColumnString(TemplateColumnData[(int)u].Text, out ErrorMsg);

				// An error
				if (!Result)
				{
					// Add error message to list
					Errors.Add(ErrorMsg);

					// Re-display original data
					TemplateColumnData[(int)u].Text = Template.GetColumnString;

					// Indicate that at least one error has occured
					NoErrors = false;
				}

				// Save the data
				else
				{
					TemplateData.SetColumnSettings(u, Template.GetColumn);
				}
 			}


			// Save template data to local parameters.  *** THIS MUST BE DONE BEFORE LINE ITEM START TESTED ***
			LocalParams.SaveMOTemplateData(LocalTemplateIndex, TemplateData);

			
			// Check if user entered data for line item start row is valid.  If not, add error message
			if (!LineItemTemplate.SetLineItemStartString(ref LocalParams, CSVFileType.CutlistFile, LocalTemplateIndex, LineItemStartData.Text, out ErrorMsg))
			{
				// Add error to list
				Errors.Add(ErrorMsg);

				// Reset original data
				LineItemStartData.Text = LineItemTemplate.GetLineItemStartString;

				// Indicate error
				NoErrors = false;
			}

	
			return NoErrors;
		}

	}
}
