﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace ProCABQC11
{
	public partial class UserMessageDlg : Form
	{
		public UserMessageDlg()
		{
			InitializeComponent();
		}

		public UserMessageDlg(string UserMessage)
		{

			InitializeComponent();

			MessageBox.SelectedText = "";

			MessageBox.Text = UserMessage;



		}


		private void button1_Click(object sender, EventArgs e)
		{

			this.DialogResult = DialogResult.OK;
		}

	}
}
