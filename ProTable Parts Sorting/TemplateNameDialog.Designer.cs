﻿namespace ProCABQC11
{
	partial class TemplateNameDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplateNameDialog));
			this.NameStr = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.OKBut = new System.Windows.Forms.Button();
			this.CancelBut = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// NameStr
			// 
			this.NameStr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NameStr.Location = new System.Drawing.Point(36, 41);
			this.NameStr.Name = "NameStr";
			this.NameStr.Size = new System.Drawing.Size(253, 26);
			this.NameStr.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(33, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(106, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Template Name";
			// 
			// OKBut
			// 
			this.OKBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OKBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OKBut.Location = new System.Drawing.Point(51, 93);
			this.OKBut.Name = "OKBut";
			this.OKBut.Size = new System.Drawing.Size(75, 35);
			this.OKBut.TabIndex = 2;
			this.OKBut.UseVisualStyleBackColor = true;
			this.OKBut.Click += new System.EventHandler(this.OKBut_Click);
			// 
			// CancelBut
			// 
			this.CancelBut.BackgroundImage = global::ProCABQC11.Properties.Resources.remove_48x48;
			this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBut.Location = new System.Drawing.Point(202, 93);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(75, 35);
			this.CancelBut.TabIndex = 3;
			this.CancelBut.UseVisualStyleBackColor = true;
			// 
			// TemplateNameDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(329, 152);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.OKBut);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.NameStr);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "TemplateNameDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Enter Template Name";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox NameStr;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button OKBut;
		private System.Windows.Forms.Button CancelBut;
	}
}