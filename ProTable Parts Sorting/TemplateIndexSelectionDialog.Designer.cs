﻿namespace ProCABQC11
{
	partial class TemplateIndexSelectionDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplateIndexSelectionDialog));
			this.NameList = new System.Windows.Forms.ListBox();
			this.OKBut = new System.Windows.Forms.Button();
			this.CancelBut = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// NameList
			// 
			this.NameList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NameList.FormattingEnabled = true;
			this.NameList.ItemHeight = 20;
			this.NameList.Location = new System.Drawing.Point(26, 22);
			this.NameList.Name = "NameList";
			this.NameList.Size = new System.Drawing.Size(298, 164);
			this.NameList.TabIndex = 0;
			this.NameList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NameList_MouseDoubleClick);
			// 
			// OKBut
			// 
			this.OKBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OKBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OKBut.Location = new System.Drawing.Point(65, 210);
			this.OKBut.Name = "OKBut";
			this.OKBut.Size = new System.Drawing.Size(75, 37);
			this.OKBut.TabIndex = 1;
			this.OKBut.UseVisualStyleBackColor = true;
			this.OKBut.Click += new System.EventHandler(this.OKBut_Click);
			// 
			// CancelBut
			// 
			this.CancelBut.BackgroundImage = global::ProCABQC11.Properties.Resources.remove_48x48;
			this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBut.Location = new System.Drawing.Point(211, 210);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(75, 37);
			this.CancelBut.TabIndex = 2;
			this.CancelBut.UseVisualStyleBackColor = true;
			// 
			// TemplateIndexSelectionDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(351, 273);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.OKBut);
			this.Controls.Add(this.NameList);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "TemplateIndexSelectionDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Select CSV Template";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox NameList;
		private System.Windows.Forms.Button OKBut;
		private System.Windows.Forms.Button CancelBut;
	}
}