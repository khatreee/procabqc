﻿namespace ProCABQC11
{
	partial class TemplateIndexEditDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplateIndexEditDialog));
			this.NameList = new System.Windows.Forms.ListBox();
			this.AddTemplateBut = new System.Windows.Forms.Button();
			this.RemoveTemplateBut = new System.Windows.Forms.Button();
			this.RenameTemplateBut = new System.Windows.Forms.Button();
			this.EditTemplateBut = new System.Windows.Forms.Button();
			this.CancelBut = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// NameList
			// 
			this.NameList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NameList.FormattingEnabled = true;
			this.NameList.ItemHeight = 16;
			this.NameList.Location = new System.Drawing.Point(26, 31);
			this.NameList.Name = "NameList";
			this.NameList.Size = new System.Drawing.Size(210, 180);
			this.NameList.TabIndex = 0;
			this.NameList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NameList_MouseDoubleClick);
			// 
			// AddTemplateBut
			// 
			this.AddTemplateBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AddTemplateBut.Location = new System.Drawing.Point(26, 242);
			this.AddTemplateBut.Name = "AddTemplateBut";
			this.AddTemplateBut.Size = new System.Drawing.Size(87, 41);
			this.AddTemplateBut.TabIndex = 1;
			this.AddTemplateBut.Text = "Add Template";
			this.AddTemplateBut.UseVisualStyleBackColor = true;
			this.AddTemplateBut.Click += new System.EventHandler(this.AddTemplateBut_Click);
			// 
			// RemoveTemplateBut
			// 
			this.RemoveTemplateBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RemoveTemplateBut.Location = new System.Drawing.Point(133, 242);
			this.RemoveTemplateBut.Name = "RemoveTemplateBut";
			this.RemoveTemplateBut.Size = new System.Drawing.Size(104, 41);
			this.RemoveTemplateBut.TabIndex = 2;
			this.RemoveTemplateBut.Text = "Remove Template";
			this.RemoveTemplateBut.UseVisualStyleBackColor = true;
			this.RemoveTemplateBut.Click += new System.EventHandler(this.RemoveTemplateBut_Click);
			// 
			// RenameTemplateBut
			// 
			this.RenameTemplateBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RenameTemplateBut.Location = new System.Drawing.Point(257, 242);
			this.RenameTemplateBut.Name = "RenameTemplateBut";
			this.RenameTemplateBut.Size = new System.Drawing.Size(104, 41);
			this.RenameTemplateBut.TabIndex = 3;
			this.RenameTemplateBut.Text = "Rename Template";
			this.RenameTemplateBut.UseVisualStyleBackColor = true;
			this.RenameTemplateBut.Click += new System.EventHandler(this.RenameTemplateBut_Click);
			// 
			// EditTemplateBut
			// 
			this.EditTemplateBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.EditTemplateBut.Location = new System.Drawing.Point(257, 31);
			this.EditTemplateBut.Name = "EditTemplateBut";
			this.EditTemplateBut.Size = new System.Drawing.Size(104, 40);
			this.EditTemplateBut.TabIndex = 4;
			this.EditTemplateBut.Text = "Edit Template";
			this.EditTemplateBut.UseVisualStyleBackColor = true;
			this.EditTemplateBut.Click += new System.EventHandler(this.EditTemplateBut_Click);
			// 
			// CancelBut
			// 
			this.CancelBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CancelBut.Location = new System.Drawing.Point(257, 90);
			this.CancelBut.Name = "CancelBut";
			this.CancelBut.Size = new System.Drawing.Size(104, 39);
			this.CancelBut.TabIndex = 5;
			this.CancelBut.Text = "Done";
			this.CancelBut.UseVisualStyleBackColor = true;
			// 
			// TemplateIndexEditDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(387, 295);
			this.Controls.Add(this.CancelBut);
			this.Controls.Add(this.EditTemplateBut);
			this.Controls.Add(this.RenameTemplateBut);
			this.Controls.Add(this.RemoveTemplateBut);
			this.Controls.Add(this.AddTemplateBut);
			this.Controls.Add(this.NameList);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "TemplateIndexEditDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Edit Template List";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox NameList;
		private System.Windows.Forms.Button AddTemplateBut;
		private System.Windows.Forms.Button RemoveTemplateBut;
		private System.Windows.Forms.Button RenameTemplateBut;
		private System.Windows.Forms.Button EditTemplateBut;
		private System.Windows.Forms.Button CancelBut;
	}
}