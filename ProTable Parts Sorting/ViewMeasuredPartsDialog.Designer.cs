﻿namespace ProCABQC11
{
	partial class ViewMeasuredPartsDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewMeasuredPartsDialog));
			this.PartListView = new System.Windows.Forms.ListView();
			this.PartInstance = new System.Windows.Forms.ColumnHeader();
			this.TimeStamp = new System.Windows.Forms.ColumnHeader();
			this.MeasuredHeight = new System.Windows.Forms.ColumnHeader();
			this.HeightInSpec = new System.Windows.Forms.ColumnHeader();
			this.MeasuredWidth = new System.Windows.Forms.ColumnHeader();
			this.WidthInSpec = new System.Windows.Forms.ColumnHeader();
			this.MeasurementType = new System.Windows.Forms.ColumnHeader();
			this.OkBut = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// PartListView
			// 
			this.PartListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PartInstance,
            this.TimeStamp,
            this.MeasuredHeight,
            this.HeightInSpec,
            this.MeasuredWidth,
            this.WidthInSpec,
            this.MeasurementType});
			this.PartListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PartListView.GridLines = true;
			this.PartListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.PartListView.Location = new System.Drawing.Point(-2, -1);
			this.PartListView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.PartListView.MultiSelect = false;
			this.PartListView.Name = "PartListView";
			this.PartListView.Size = new System.Drawing.Size(731, 371);
			this.PartListView.TabIndex = 0;
			this.PartListView.UseCompatibleStateImageBehavior = false;
			this.PartListView.View = System.Windows.Forms.View.Details;
			// 
			// PartInstance
			// 
			this.PartInstance.Text = "    Part";
			this.PartInstance.Width = 75;
			// 
			// TimeStamp
			// 
			this.TimeStamp.Text = "Time";
			this.TimeStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.TimeStamp.Width = 146;
			// 
			// MeasuredHeight
			// 
			this.MeasuredHeight.Text = "Measured Height";
			this.MeasuredHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.MeasuredHeight.Width = 120;
			// 
			// HeightInSpec
			// 
			this.HeightInSpec.Text = "In Spec";
			this.HeightInSpec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.HeightInSpec.Width = 64;
			// 
			// MeasuredWidth
			// 
			this.MeasuredWidth.Text = "Measured Width";
			this.MeasuredWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.MeasuredWidth.Width = 115;
			// 
			// WidthInSpec
			// 
			this.WidthInSpec.Text = "In Spec";
			this.WidthInSpec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.WidthInSpec.Width = 69;
			// 
			// MeasurementType
			// 
			this.MeasurementType.Text = "Measurement Type";
			this.MeasurementType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.MeasurementType.Width = 138;
			// 
			// OkBut
			// 
			this.OkBut.BackgroundImage = global::ProCABQC11.Properties.Resources.CheckMark_128x128;
			this.OkBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.OkBut.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OkBut.Location = new System.Drawing.Point(327, 389);
			this.OkBut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.OkBut.Name = "OkBut";
			this.OkBut.Size = new System.Drawing.Size(73, 38);
			this.OkBut.TabIndex = 1;
			this.OkBut.UseVisualStyleBackColor = true;
			// 
			// ViewMeasuredPartsDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(727, 442);
			this.Controls.Add(this.OkBut);
			this.Controls.Add(this.PartListView);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ViewMeasuredPartsDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Line Item Measured Parts Viewer";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView PartListView;
		private System.Windows.Forms.ColumnHeader PartInstance;
		private System.Windows.Forms.ColumnHeader TimeStamp;
		private System.Windows.Forms.ColumnHeader MeasuredHeight;
		private System.Windows.Forms.ColumnHeader HeightInSpec;
		private System.Windows.Forms.ColumnHeader MeasuredWidth;
		private System.Windows.Forms.ColumnHeader WidthInSpec;
		private System.Windows.Forms.Button OkBut;
		private System.Windows.Forms.ColumnHeader MeasurementType;
	}
}