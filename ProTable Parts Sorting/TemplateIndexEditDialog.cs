﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{
	public partial class TemplateIndexEditDialog : Form
	{

		private PersistentParametersV1100 LocalParams;


		public TemplateIndexEditDialog(PersistentParametersV1100 PParams)
		{
			InitializeComponent();

			// Load template list initially
			List<string> NameStr;

			// Load template list names depending on currently selected type
			if (PParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
			{
				PParams.GetMOTemplateNames(out NameStr);

				// Set title for dialog to reflect type of template
				this.Text = "Edit Template List For Multi Order Templates";

			}

			else
			{
				PParams.GetSOTemplateNames(out NameStr);

				// Set title for dialog to reflect type of template
				this.Text = "Edit Template List For Single Order Templates";
			}

			// Add names to list box
			for (int u = 0; u < NameStr.Count; u++)
			{
				NameList.Items.Add(NameStr[u]);
			}
			
			// Save the params reference
			LocalParams = PParams;


		}


		private void EditTemplate()
		{


			// Make sure that an item is selected
			if (NameList.SelectedIndex != -1)
			{
				if (LocalParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
				{
					CutlistTemplateConfigDialog TemplateDlg = new CutlistTemplateConfigDialog(LocalParams, NameList.SelectedIndex);
					TemplateDlg.ShowDialog();
				}

				else
				{
					SingleOrderTemplateConfigDialog TemplateDlg = new SingleOrderTemplateConfigDialog(LocalParams, NameList.SelectedIndex);
					TemplateDlg.ShowDialog();
				}
			}

			// No selection
			else
			{
				MessageBox.Show("A template MUST be selected to edit.", "No Template Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}


		private void EditTemplateBut_Click(object sender, EventArgs e)
		{


			EditTemplate();


		}

		private void AddTemplateBut_Click(object sender, EventArgs e)
		{


			TemplateNameDialog Dlg = new TemplateNameDialog();

			if (Dlg.ShowDialog() == DialogResult.OK)
			{

				List<string> TestNameStr;
				bool Duplicate = false;

				if (LocalParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
				{

					// Get current list of names
					LocalParams.GetMOTemplateNames(out TestNameStr);

					for (int u = 0; u < TestNameStr.Count; u++)
					{
						if (TestNameStr[u].Equals(Dlg.TemplateName))
						{
							Duplicate = true;
							break;
						}
					}

					if (!Duplicate)
					{

						// Create a new template and update name list
						LocalParams.CreateMOTemplateData(Dlg.TemplateName);

						RebuildTemplateNameList();

					}

					else
					{
						MessageBox.Show("Template name already exists.", "Duplicate Template Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}

				}

				else
				{

					// Get current list of names
					LocalParams.GetSOTemplateNames(out TestNameStr);

					for (int u = 0; u < TestNameStr.Count; u++)
					{
						if (TestNameStr[u].Equals(Dlg.TemplateName))
						{
							Duplicate = true;
							break;
						}
					}

					if (!Duplicate)
					{
						LocalParams.CreateSOTemplateData(Dlg.TemplateName);

						RebuildTemplateNameList();


					}

					else
					{
						MessageBox.Show("Template name already exists.", "Duplicate Template Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
			}
		}

		private void RemoveTemplateBut_Click(object sender, EventArgs e)
		{
			// A valid selection
			if (NameList.SelectedIndex != -1)
			{
				if (MessageBox.Show("WARNING:  Confirm Template Deletion?", "Template Deletion Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					if (LocalParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
					{
						if (LocalParams.DeleteMOTemplateData(NameList.SelectedIndex))
						{
							RebuildTemplateNameList();
						}

						else
							MessageBox.Show("CANNOT delete Default template", "Template Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}

					// Single order type
					else
					{
						if (LocalParams.DeleteSOTemplateData(NameList.SelectedIndex))
						{
							RebuildTemplateNameList();
						}

						else
							MessageBox.Show("CANNOT delete Default template", "Template Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}
				}
			}

			// No selection made
			else
				MessageBox.Show("A template MUST be selected to delete", "No Template Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
 

		}
		

		private void RebuildTemplateNameList()
		{

			List<string> NameStr;

			if (LocalParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
				LocalParams.GetMOTemplateNames(out NameStr);

			else
				LocalParams.GetSOTemplateNames(out NameStr);

			// Clear current list
			NameList.Items.Clear();

			// rebuild list
			for (int u = 0; u < NameStr.Count; u++)
				NameList.Items.Add(NameStr[u]);

		}

		private void RenameTemplateBut_Click(object sender, EventArgs e)
		{

			// A valid selection
			if (NameList.SelectedIndex != -1)
			{

				// Do not allow the default template to be renamed
				if (NameList.SelectedIndex != 0)
				{

					TemplateNameDialog Dlg = new TemplateNameDialog();

					if (Dlg.ShowDialog() == DialogResult.OK)
					{
						List<string> TestNameStr;
						bool Duplicate = false;

						if (LocalParams.CSVFileTypeInUse == CSVFileType.CutlistFile)
						{

							// Get current list of names
							LocalParams.GetMOTemplateNames(out TestNameStr);

							for (int u = 0; u < TestNameStr.Count; u++)
							{
								if (TestNameStr[u].Equals(Dlg.TemplateName))
								{
									Duplicate = true;
									break;
								}
							}

							if (!Duplicate)
							{
								LocalParams.RenameMOTemplateData(NameList.SelectedIndex, Dlg.TemplateName);

								RebuildTemplateNameList();

							}

							else
								MessageBox.Show("Template name already exists.", "Duplicate Template Name", MessageBoxButtons.OK, MessageBoxIcon.Error);

						}

						else
						{
							// Get current list of names
							LocalParams.GetSOTemplateNames(out TestNameStr);

							for (int u = 0; u < TestNameStr.Count; u++)
							{
								if (TestNameStr[u].Equals(Dlg.TemplateName))
								{
									Duplicate = true;
									break;
								}
							}

							if (!Duplicate)
							{
								LocalParams.RenameSOTemplateData(NameList.SelectedIndex, Dlg.TemplateName);

								RebuildTemplateNameList();

							}

							else
								MessageBox.Show("Template name already exists.", "Duplicate Template Name", MessageBoxButtons.OK, MessageBoxIcon.Error);


						}
					}
				}

				else
					MessageBox.Show("CANNOT rename default template.", "Template Rename Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}

			else
				MessageBox.Show("A template MUST be selected to rename", "No Template Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
 
		}

		private void NameList_MouseDoubleClick(object sender, MouseEventArgs e)
		{

			EditTemplate();

		}

		

	}
}
