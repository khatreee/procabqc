﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ProCAB QC")]
[assembly: AssemblyDescription("Part Sorting and Identification Software for ProTable-CAB 2D measuring system")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Accurate Technology, Inc.\n270 Rutledge Rd.  Unit E\nFletcher, NC  28732\nUSA\n\n+1 828 654 7920\nwww.proscale.com")]
[assembly: AssemblyProduct("ProCAB QC®")]
[assembly: AssemblyCopyright("Copyright ©  2009-2017  ATI")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b876cf52-c2be-4dbb-a041-1806f59e148e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.2.0")]
[assembly: AssemblyFileVersion("1.2.2.0")]
