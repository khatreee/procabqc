﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{

	[Serializable]
	public class CustomerInfo
	{

		private string InfoCustomerName = "";
		private string InfoAddress1 = "";
		private string InfoAddress2 = "";
		private string InfoCity = "";
		private string InfoState = "";
		private string InfoZip = "";
		private string InfoContactName = "";
		private string InfoTelephoneNumber = "";
		private string InfoCountry = "";
		private string InfoEmail = "";



		public string CustomerName
		{
			get { return InfoCustomerName; }
			set { InfoCustomerName = value; }
		}

		public string Address1
		{
 			get { return InfoAddress1; }
			set { InfoAddress1 = value; }
		}

		public string Address2
		{
			get { return InfoAddress2; }
			set { InfoAddress2 = value; }
		}

		public string City
		{
			get { return InfoCity; }
			set { InfoCity = value; }
		}

		public string State
		{
   			get { return InfoState; }
			set { InfoState = value; }
		}

		public string Zip
		{
			get { return InfoZip; }
			set { InfoZip = value; }
		}

		public string ContactName
		{
			get { return InfoContactName; }
			set { InfoContactName = value; }
		}

		public string TelephoneNumber
		{
			get { return InfoTelephoneNumber; }
			set { InfoTelephoneNumber = value; }
		}

		public string Country
		{
			get { return InfoCountry; }
			set { InfoCountry = value; }
		}

		public string Email
		{
			get { return InfoEmail; }
			set { InfoEmail = value; }
		}

		// The default constructor
		public CustomerInfo()
		{



		}







	}
}
