﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProCABQC11
{
	public class SingleOrderCSVReader : CSVReader
	{

		Order NewOrder;

		
		public SingleOrderCSVReader()
		{


		}

		protected override CSVReaderResultCodes ProcessCSVFile(ref PersistentParametersV1100 PParams, ref List<string> ErrorLog, ref SystemOrders OrdersList, int TemplateIndex)
		{

			// Create a SingleOrderLineItem template
			SingleOrderLineItemTemplate LineItemTemplate = new SingleOrderLineItemTemplate(ref PParams, TemplateIndex);

			// Create a SingleOrderHeader template
			SingleOrderHeaderTemplate HeaderTemplate = new SingleOrderHeaderTemplate(ref PParams, TemplateIndex);

			// Create an order
			NewOrder = new Order();

			// Determine if an OrderID field is defined in the header template
			// If so, wait to locate it in the header
			// If not, use the filename as the OrderID
			if (!HeaderTemplate.IsOrderIDDefined())
			{
				NewOrder.OrderNumber = BaseFileName;
			}

			// Start reading lines from CSV file
			string CSVLine = "";
			while ((CSVLine = CSVFileStreamIn.ReadLine()) != null)
			{
				if (CSVLineNumber >= LineItemTemplate.LineItemStart)
				{
					LineItem NewLineItem;
					TemplateParsingResultCodes ParseResult;
					List<string> ErrorList = new List<string>();

					// Parse a single LineItem line from the CSV file
					ParseResult = LineItemTemplate.ParseSingleOrderLineItem(CSVLineNumber, CSVLine, out NewLineItem, ref ErrorList);

					// If an error resulted, copy the errors from this parse to the error log
					if (ParseResult == TemplateParsingResultCodes.Error)
					{
						// Set flag to later indicate that parse errors occured
						ParseErrors = true;

						for (int u = 0; u < ErrorList.Count; u++)
						{
							ErrorLog.Add(ErrorList[u]);
						}
					}

					// The LineItem line was parsed without errors.  Add the line item to the order
					else if (ParseResult == TemplateParsingResultCodes.Success)
					{
						NewOrder.AddLineItem(ref NewLineItem);

					}
				}

				// In the order header area of the CSV file
				else
				{

					TemplateParsingResultCodes ParseResult;
					List<string> ErrorList = new List<string>();

					// Parse a single LineItem line from the CSV file
					ParseResult = HeaderTemplate.ParseSingleOrderHeaderLine(CSVLineNumber, CSVLine, ref NewOrder, ref ErrorList);

					// If an error resulted, copy the errors from this parse to the error log
					if (ParseResult == TemplateParsingResultCodes.Error)
					{
						// Set flag to later indicate that parse errors occured
						ParseErrors = true;

						for (int u = 0; u < ErrorList.Count; u++)
						{
							ErrorLog.Add(ErrorList[u]);
						}
					}
				}

				// Increment the line number being read
				CSVLineNumber++;
			}

			// All lines parsed.  Make sure that OrderID is non-null.
			// If so, save base file name as OrderID
			if (NewOrder.OrderNumber == "")
				NewOrder.OrderNumber = BaseFileName;

			// Get the template data
			SingleOrderTemplateDataV1100 TemplateData;
			PParams.GetSOTemplateData(TemplateIndex, out TemplateData);
								 
			// Save the CSV measuring units and tolerances to order
			NewOrder.CSVMeasurementUnits = TemplateData.CSVMeasureUnits;

			// Modify tolerances based on measuring units.
			// Tolerances from template are ALWAYS in millimeters
			if (NewOrder.CSVMeasurementUnits == MeasuringUnits.Millimeters)
			{
				NewOrder.HeightMinusAcc = TemplateData.HeightMinusTolerance.ToleranceVal;
				NewOrder.HeightPlusAcc = TemplateData.HeightPlusTolerance.ToleranceVal;
				NewOrder.WidthMinusAcc = TemplateData.WidthMinusTolerance.ToleranceVal;
				NewOrder.WidthPlusAcc = TemplateData.WidthPlusTolerance.ToleranceVal;
				NewOrder.ThicknessPlusAcc = TemplateData.ThicknessPlusTolerance.ToleranceVal;
				NewOrder.ThicknessMinusAcc = TemplateData.ThicknessMinusTolerance.ToleranceVal;

				// In degrees
				NewOrder.AnglePlusAcc = TemplateData.AnglePlusTolerance.ToleranceVal;
				NewOrder.AngleMinusAcc = TemplateData.AngleMinusTolerance.ToleranceVal;
			}

			// CSV file in inches.  Convert tolerance to inches
			else
			{
				NewOrder.HeightMinusAcc = TemplateData.HeightMinusTolerance.ToleranceVal / 25.4;
				NewOrder.HeightPlusAcc = TemplateData.HeightPlusTolerance.ToleranceVal / 25.4;
				NewOrder.WidthMinusAcc = TemplateData.WidthMinusTolerance.ToleranceVal / 25.4;
				NewOrder.WidthPlusAcc = TemplateData.WidthPlusTolerance.ToleranceVal / 25.4;
				NewOrder.ThicknessPlusAcc = TemplateData.ThicknessPlusTolerance.ToleranceVal / 25.4;
				NewOrder.ThicknessMinusAcc = TemplateData.ThicknessMinusTolerance.ToleranceVal / 25.4;

				// In degrees
				NewOrder.AnglePlusAcc = TemplateData.AnglePlusTolerance.ToleranceVal;
				NewOrder.AngleMinusAcc = TemplateData.AngleMinusTolerance.ToleranceVal;
			}
 

			// add new order to list of local system orders
			OrdersList.AddOrder(ref NewOrder);

			// Close the CSV file
			CloseCSVFile();

			// return result code based on parsing errors
			if (ParseErrors)
				return CSVReaderResultCodes.ParsingErrors;

			else
				return CSVReaderResultCodes.Success;

		}


		// Returns the order ID just created
		public string GetOrderNumber()
		{
			return NewOrder.OrderNumber;
		}
	}
}
