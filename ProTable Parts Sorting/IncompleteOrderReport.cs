using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ProCABQC11
{
	public partial class IncompleteOrderReport : DevExpress.XtraReports.UI.XtraReport
	{

		private const int VerticalLineSpacing = 17;
		private const int FirstPageStartYPos = 10;
		private const int EndOfPage = 645;
		private int PageBreakRef = EndOfPage;


		public IncompleteOrderReport()
		{
			InitializeComponent();
		}


		public void FormatIncompleteOrderReport(Order CurrentOrder, PersistentParametersV1100 PParams)
		{

			// Get current time
			DateTime Time = DateTime.Now;
			String DateStr;
			string MonthStr;
			string DayStr;
			string HourStr;
			string MinStr;
			string SecStr;

			
			if (Time.Month < 10)
				MonthStr = string.Format("0{0}", Time.Month);

			else
				MonthStr = Time.Month.ToString();


			if (Time.Day < 10)
				DayStr= string.Format("0{0}", Time.Day);

			else
				DayStr = Time.Day.ToString();



			if (PParams.UseNorthAmericanDateFormat)
			{
				// Build Date string in north american format   (month-day-year)
				DateStr = string.Format("{0}-{1}-{2}", MonthStr, DayStr, Time.Year.ToString());

			}

			else
			{
				// Build string in European format (day-month-year)
				DateStr = string.Format("{0}-{1}-{2}", DayStr, MonthStr, Time.Year.ToString());
			}


			if (Time.Hour < 10)
				HourStr = string.Format("0{0}", Time.Hour);

			else
				HourStr = Time.Hour.ToString();

			if (Time.Minute < 10)
				MinStr = string.Format("0{0}", Time.Minute);

			else
				MinStr = Time.Minute.ToString();


			if (Time.Second < 10)
				SecStr = string.Format("0{0}", Time.Second);

			else
				SecStr = Time.Second.ToString();


			// Build Time string without delimiters
			String TimeStr = string.Format("{0}:{1}:{2}", HourStr, MinStr, SecStr);

			// Set the time stamp string
			DateTimeStr.Text = string.Format("{0}  {1}", DateStr, TimeStr);


			// Set the order ID string
			OrderIDStr.Text = CurrentOrder.OrderNumber;


			// Print the line item header
//			int YPos = PrintLineItemHeader(new Point(0, FirstPageStartYPos));
			int YPos = FirstPageStartYPos;


			// For each line item in the order
			for (int u = 0; u < CurrentOrder.NumberOfLineItems(); u++)
			{
				LineItem LI;


				// Get the line item
				if (CurrentOrder.GetLineItem(out LI, u))
				{
					// Only print line items that are NOT complete
					if (!LI.IsQuantityComplete)
					{
						// Print the line item and its measurements
						YPos = FormatLineItem(LI, PParams, CurrentOrder, new Point(0, YPos));
					}

				}
			}

			 

		}

		private int PrintLineItemHeader(Point Pt)
		{


			XRLabel LineIDHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel PartIDHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomWidthHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomHeightHeader = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel QuantityHeader = new DevExpress.XtraReports.UI.XRLabel();

			// Check if page break is required
			int YPos = CheckForPageBreak(Pt.Y);


			// 
			// LineIDHeader
			// 
			LineIDHeader.Font = new System.Drawing.Font("Arial", 10F);
			LineIDHeader.Location = new System.Drawing.Point(0, YPos);
			LineIDHeader.Name = "LineIDHeader";
			LineIDHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			LineIDHeader.Size = new System.Drawing.Size(58, 25);
			LineIDHeader.StylePriority.UseFont = false;
			LineIDHeader.Text = "Line #";
			// 
			// PartIDHeader
			// 
			PartIDHeader.Font = new System.Drawing.Font("Arial", 10F);
			PartIDHeader.Location = new System.Drawing.Point(67, YPos);
			PartIDHeader.Name = "PartIDHeader";
			PartIDHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			PartIDHeader.Size = new System.Drawing.Size(67, 25);
			PartIDHeader.StylePriority.UseFont = false;
			PartIDHeader.Text = "Part ID";
			// 
			// NomWidthHeader
			// 
			NomWidthHeader.Font = new System.Drawing.Font("Arial", 10F);
			NomWidthHeader.Location = new System.Drawing.Point(325, YPos);
			NomWidthHeader.Name = "NomWidthHeader";
			NomWidthHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomWidthHeader.Size = new System.Drawing.Size(92, 25);
			NomWidthHeader.StylePriority.UseFont = false;
			NomWidthHeader.Text = "Nom. Width";
			// 
			// NomHeightHeader
			// 
			NomHeightHeader.Font = new System.Drawing.Font("Arial", 10F);
			NomHeightHeader.Location = new System.Drawing.Point(433, YPos);
			NomHeightHeader.Name = "NomHeightHeader";
			NomHeightHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomHeightHeader.Size = new System.Drawing.Size(108, 25);
			NomHeightHeader.StylePriority.UseFont = false;
			NomHeightHeader.Text = "Nom. Height";
			// 
			// QuantityHeader
			// 
			QuantityHeader.Font = new System.Drawing.Font("Arial", 10F);
			QuantityHeader.Location = new System.Drawing.Point(550, YPos);
			QuantityHeader.Name = "QuantityHeader";
			QuantityHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			QuantityHeader.Size = new System.Drawing.Size(100, 25);
			QuantityHeader.StylePriority.UseFont = false;
			QuantityHeader.Text = "Qty. Needed";

			// Add it to the detail
			this.Detail.Controls.Add(LineIDHeader);
			this.Detail.Controls.Add(PartIDHeader);
			this.Detail.Controls.Add(NomWidthHeader);
			this.Detail.Controls.Add(NomHeightHeader);
			this.Detail.Controls.Add(QuantityHeader);


			return YPos + VerticalLineSpacing;

		}

		private int FormatLineItem(LineItem LI, PersistentParametersV1100 PParams, Order CurrentOrder, Point Pt)
		{

			// Print Line item data here
			XRLabel LineItemNumber = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel PartID = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomWidth = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel NomHeight = new DevExpress.XtraReports.UI.XRLabel();
			XRLabel Quantity = new DevExpress.XtraReports.UI.XRLabel();

			// Check if page break is required
			int YPos = CheckForPageBreak(Pt.Y);


			// 
			// LineItemNumber
			// 
			LineItemNumber.Font = new System.Drawing.Font("Arial", 10F);
			LineItemNumber.Location = new System.Drawing.Point(15, YPos);
			LineItemNumber.Name = "LineItemNumber";
			LineItemNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			LineItemNumber.Size = new System.Drawing.Size(58, 25);
			LineItemNumber.StylePriority.UseFont = false;
			LineItemNumber.Text = LI.LineNumber.ToString();
			// 
			// PartID
			// 
			PartID.Font = new System.Drawing.Font("Arial", 10F);
			PartID.Location = new System.Drawing.Point(67, YPos);
			PartID.Name = "PartID";
			PartID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			PartID.Size = new System.Drawing.Size(242, 25);
			PartID.StylePriority.UseFont = false;
			PartID.Text = LI.PartIdStr;
			// 
			// NomWidth
			// 
			NomWidth.Font = new System.Drawing.Font("Arial", 10F);
			NomWidth.Location = new System.Drawing.Point(342, YPos);
			NomWidth.Name = "NomWidth";
			NomWidth.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomWidth.Size = new System.Drawing.Size(58, 25);
			NomWidth.StylePriority.UseFont = false;
			NomWidth.Text = PositionConversion.ConvertUnits(LI.Width, CurrentOrder.CSVMeasurementUnits, CurrentOrder.CSVMeasurementUnits, PParams.MMPrecisionVal, PParams.InchPrecisionVal);
			// 
			// NomHeight
			// 
			NomHeight.Font = new System.Drawing.Font("Arial", 10F);
			NomHeight.Location = new System.Drawing.Point(450, YPos);
			NomHeight.Name = "NomHeight";
			NomHeight.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			NomHeight.Size = new System.Drawing.Size(58, 25);
			NomHeight.StylePriority.UseFont = false;
			NomHeight.Text = PositionConversion.ConvertUnits(LI.Height, CurrentOrder.CSVMeasurementUnits, CurrentOrder.CSVMeasurementUnits, PParams.MMPrecisionVal, PParams.InchPrecisionVal); ;
			// 
			// Quantity
			// 
			Quantity.Font = new System.Drawing.Font("Arial", 10F);
			Quantity.Location = new System.Drawing.Point(585, YPos);
			Quantity.Name = "Quantity";
			Quantity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			Quantity.Size = new System.Drawing.Size(58, 25);
			Quantity.StylePriority.UseFont = false;
			Quantity.Text = (LI.QtyRequired - LI.QtyMeasured).ToString();

			// Add it to the detail
			this.Detail.Controls.Add(LineItemNumber);
			this.Detail.Controls.Add(PartID);
			this.Detail.Controls.Add(NomWidth);
			this.Detail.Controls.Add(NomHeight);
			this.Detail.Controls.Add(Quantity);


			// Add a blank line
			YPos += VerticalLineSpacing;

			return YPos;


		}

		private int CheckForPageBreak(int YPos)
		{

			if (YPos > PageBreakRef)
			{

				//				YPos = 0;

				// Do a page break
				XRPageBreak Break = new XRPageBreak();

				Break.Location = new Point(0, PageBreakRef);

				// Add it to the page
				this.Detail.Controls.Add(Break);

				PageBreakRef += EndOfPage;

			}

			return YPos;
		}

	}
}
