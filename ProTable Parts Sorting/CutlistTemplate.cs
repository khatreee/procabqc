﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace ProCABQC11
{

	public enum CutlistLineItemColumnIDV1100
	{
		LineItemNumber,
		PartID,
		QuantityRequired,
		NominalWidth,
		NominalHeight,
		NominalThickness,
		Material,
		Style,

		Hinging,
		Finish,
		Type,
		Location,
		Machining,
		Assembly,
		UserMessage,

		Comment,
		UserDef1,
		UserDef2,
		UserDef3,
		OrderID,
		PackListTemplate,
		PartLabelTemplate,
		OrderUserDefined1,
		TestThicknessDim,
		BillCustomerName,
		BillAddress1,
		BillAddress2,
		BillCity,
		BillState,
		BillZip,
		BillCountry,
		BillContact,
		BillTelephone,
		BillEmail,
		ShipCustomerName,
		ShipAddress1,
		ShipAddress2,
		ShipCity,
		ShipState,
		ShipZip,
		ShipCountry,
		ShipContact,
		ShipTelephone,
		ShipEmail,
		MaxItems
	}

	public class CutlistTemplate : MasterLineItemTemplate
	{

		private const string TemplateRegistryKeyStr = "Cutlist Line Item";

		private string[] ColumnNames = {		"Line Item Number",
												"Part ID*",
												"Quantity Required*",
												"Nominal Width*",
												"Nominal Height*",
												"Nominal Depth",
												"Material",
												"Style",

												"Hinging",
												"Finish",
												"Type",
												"Location",
												"Machining",
												"Assembly",
												"User Message",

												"Comment",
												"User Defined 1",
												"User Defined 2",
												"User Defined 3",
												"Order ID*",
												"Pack List Template",
												"Part Label Template",
												"Order User Def 1",
												"Test Depth Dim",
												"Bill Customer Name",
												"Bill Address 1",
												"Bill Address 2",
												"Bill City",
												"Bill State",
												"Bill Zip",
												"Bill Country",
												"Bill Contact",
												"Bill Telephone",
												"Bill Email",
												"Ship Customer Name",
												"Ship Address 1",
												"Ship Address 2",
												"Ship City",
												"Ship State",
												"Ship Zip",
												"Ship Country",
												"Ship Contact",
												"Ship Telephone",
												"Ship Email"

									   };

		private const string OutOfBoundsColumnErrorMsg = "Template defines a column that is outside the column range of the CSV file";


		public bool GetLineItemColumnTemplate(CutlistLineItemColumnIDV1100 Index, out TemplateLineItemColumn LineItemColumnTemplate)
		{

			if ((Index < CutlistLineItemColumnIDV1100.MaxItems) && (Index >= CutlistLineItemColumnIDV1100.LineItemNumber))
			{
				LineItemColumnTemplate = LineItemTemplateItems[(int)Index];
				return true;
			}

			else
			{
				LineItemColumnTemplate = (TemplateLineItemColumn)null;
				return false;
			}
		}


		public CutlistTemplate()
		{
		}


		// The overridden constructor
		public CutlistTemplate(ref PersistentParametersV1100 PParams, int TemplateIndex)
		{

			// Get the template data instance based on the index
			CutlistTemplateDataV1100 TemplateData;

			if (PParams.GetMOTemplateData(TemplateIndex, out TemplateData))
			{

				// Create the array of Template Line Item columns for a Single order template
				LineItemTemplateItems = new TemplateLineItemColumn[(int)CutlistLineItemColumnIDV1100.MaxItems];

				// Create each LineItemTemplateItem
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.LineItemNumber] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.LineItemNumber), ColumnNames[(int)CutlistLineItemColumnIDV1100.LineItemNumber], ParameterDataType.Integer, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.PartID] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.PartID), ColumnNames[(int)CutlistLineItemColumnIDV1100.PartID], ParameterDataType.String, true, true);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.QuantityRequired] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.QuantityRequired), ColumnNames[(int)CutlistLineItemColumnIDV1100.QuantityRequired], ParameterDataType.Integer, true, true);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.NominalWidth] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.NominalWidth), ColumnNames[(int)CutlistLineItemColumnIDV1100.NominalWidth], ParameterDataType.Float, true, true);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.NominalHeight] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.NominalHeight), ColumnNames[(int)CutlistLineItemColumnIDV1100.NominalHeight], ParameterDataType.Float, true, true);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.NominalThickness] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.NominalThickness), ColumnNames[(int)CutlistLineItemColumnIDV1100.NominalThickness], ParameterDataType.Float, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Material] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Material), ColumnNames[(int)CutlistLineItemColumnIDV1100.Material], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Style] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Style), ColumnNames[(int)CutlistLineItemColumnIDV1100.Style], ParameterDataType.String, true, false);

				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Hinging] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Hinging), ColumnNames[(int)CutlistLineItemColumnIDV1100.Hinging], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Finish] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Finish), ColumnNames[(int)CutlistLineItemColumnIDV1100.Finish], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Type] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Type), ColumnNames[(int)CutlistLineItemColumnIDV1100.Type], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Location] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Location), ColumnNames[(int)CutlistLineItemColumnIDV1100.Location], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Machining] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Machining), ColumnNames[(int)CutlistLineItemColumnIDV1100.Machining], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Assembly] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Assembly), ColumnNames[(int)CutlistLineItemColumnIDV1100.Assembly], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.UserMessage] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.UserMessage), ColumnNames[(int)CutlistLineItemColumnIDV1100.UserMessage], ParameterDataType.String, true, false);


				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.Comment] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.Comment), ColumnNames[(int)CutlistLineItemColumnIDV1100.Comment], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.UserDef1] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.UserDef1), ColumnNames[(int)CutlistLineItemColumnIDV1100.UserDef1], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.UserDef2] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.UserDef2), ColumnNames[(int)CutlistLineItemColumnIDV1100.UserDef2], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.UserDef3] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.UserDef3), ColumnNames[(int)CutlistLineItemColumnIDV1100.UserDef3], ParameterDataType.String, true, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.OrderID] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.OrderID), ColumnNames[(int)CutlistLineItemColumnIDV1100.OrderID], ParameterDataType.String, false, true);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.PackListTemplate] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.PackListTemplate), ColumnNames[(int)CutlistLineItemColumnIDV1100.PackListTemplate], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.PartLabelTemplate), ColumnNames[(int)CutlistLineItemColumnIDV1100.PartLabelTemplate], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.OrderUserDefined1), ColumnNames[(int)CutlistLineItemColumnIDV1100.OrderUserDefined1], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.TestThicknessDim] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.TestThicknessDim), ColumnNames[(int)CutlistLineItemColumnIDV1100.TestThicknessDim], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillCustomerName] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillCustomerName), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillCustomerName], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillAddress1] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillAddress1), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillAddress1], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillAddress2] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillAddress2), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillAddress2], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillCity] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillCity), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillCity], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillState] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillState), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillState], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillZip] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillZip), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillZip], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillCountry] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillCountry), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillCountry], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillContact] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillContact), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillContact], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillTelephone] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillTelephone), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillTelephone], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.BillEmail] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.BillEmail), ColumnNames[(int)CutlistLineItemColumnIDV1100.BillEmail], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipCustomerName] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipCustomerName), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipCustomerName], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipAddress1] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipAddress1), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipAddress1], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipAddress2] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipAddress2), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipAddress2], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipCity] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipCity), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipCity], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipState] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipState), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipState], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipZip] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipZip), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipZip], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipCountry] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipCountry), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipCountry], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipContact] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipContact), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipContact], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipTelephone] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipTelephone), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipTelephone], ParameterDataType.String, false, false);
				LineItemTemplateItems[(int)CutlistLineItemColumnIDV1100.ShipEmail] = new TemplateLineItemColumn(TemplateData.GetColumnSettings(CutlistLineItemColumnIDV1100.ShipEmail), ColumnNames[(int)CutlistLineItemColumnIDV1100.ShipEmail], ParameterDataType.String, false, false);



				// The cutlist line item start row value
				LineItemStartPos = TemplateData.LineItemStart;

			}

					
			
		}

		private void UpdateLineItem(ref LineItem LI, ref ParamStruct Data, CutlistLineItemColumnIDV1100 ID)
		{
			switch (ID)
			{
				case CutlistLineItemColumnIDV1100.LineItemNumber:

					LI.LineNumber = Data.IntVal;
					break;

				case CutlistLineItemColumnIDV1100.PartID:

					LI.PartIdStr = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.QuantityRequired:

					LI.QtyRequired = Data.IntVal;
					break;

				case CutlistLineItemColumnIDV1100.NominalWidth:

					LI.Width = Data.FPVal;
					break;

				case CutlistLineItemColumnIDV1100.NominalHeight:

					LI.Height = Data.FPVal;
					break;

				case CutlistLineItemColumnIDV1100.NominalThickness:

					LI.Thickness = Data.FPVal;
					break;

				case CutlistLineItemColumnIDV1100.Style:

					LI.StyleText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.Material:

					LI.MaterialText = Data.StringVal;
					break;



				case CutlistLineItemColumnIDV1100.Hinging:

					LI.HingingText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.Finish:

					LI.FinishText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.Type:

					LI.TypeText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.Location:

					LI.LocationText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.Machining:

					LI.MachiningText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.Assembly:

					LI.AssemblyText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.UserMessage:

					LI.UserMessageText = Data.StringVal;
					break;






				case CutlistLineItemColumnIDV1100.Comment:

					LI.CommentText = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.UserDef1:

					LI.UserDef1Text = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.UserDef2:

					LI.UserDef2Text = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.UserDef3:

					LI.UserDef3Text = Data.StringVal;
					break;

			}
		}


		private void UpdateOrderData(ref Order CurOrder, ref ParamStruct Data, CutlistLineItemColumnIDV1100 ID)
		{

			// Get the customer info data
			CustomerInfo BillTo;
			CustomerInfo ShipTo;

			CurOrder.GetCustomerInfoData(out BillTo, CustomersTypes.Billing);
			CurOrder.GetCustomerInfoData(out ShipTo, CustomersTypes.Shipping);

			switch (ID)
			{

				case CutlistLineItemColumnIDV1100.BillCustomerName:

					BillTo.CustomerName = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillAddress1:

					BillTo.Address1 = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillAddress2:

					BillTo.Address2 = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillCity:

					BillTo.City = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillState:

					BillTo.State = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillZip:

					BillTo.Zip = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillCountry:

					BillTo.Country = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillContact:

					BillTo.ContactName = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillTelephone:

					BillTo.TelephoneNumber = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.BillEmail:

					BillTo.Email = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipCustomerName:

					ShipTo.CustomerName = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipAddress1:

					ShipTo.Address1 = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipAddress2:

					ShipTo.Address2 = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipCity:

					ShipTo.City = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipState:

					ShipTo.State = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipZip:

					ShipTo.Zip = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipCountry:

					ShipTo.Country = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipContact:

					ShipTo.ContactName = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipTelephone:

					ShipTo.TelephoneNumber = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.ShipEmail:

					ShipTo.Email = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.OrderID:

					CurOrder.OrderNumber = Data.StringVal;
					break;

				case CutlistLineItemColumnIDV1100.PackListTemplate:

					CurOrder.PackListTemplateText = Data.StringVal;
					
					// Because the field is being set, it must be defined within the template
					CurOrder.PacklistTemplateFieldDefined = true;

					break;

				case CutlistLineItemColumnIDV1100.PartLabelTemplate:

					CurOrder.PartLabelTemplateText = Data.StringVal;

					// Because the field is being set, it must be defined within the template
					CurOrder.PartLabelTemplateFieldDefined = true;

					break;

				case CutlistLineItemColumnIDV1100.OrderUserDefined1:

					CurOrder.UserDefined1Text = Data.StringVal;
					break;


				case CutlistLineItemColumnIDV1100.TestThicknessDim:

					CurOrder.TestThicknessDimText = Data.StringVal;

					// Only set thickness defined to true IF string == "yes" or "YES"
					if((CurOrder.TestThicknessDimText == "yes") || (CurOrder.TestThicknessDimText == "YES"))
						CurOrder.TestThicknessDimFieldDefined = true;

					break;



				
			}
		}


		public TemplateParsingResultCodes ParseCutlistLineItem(int CSVLineNumber, string CSVLineString, ref SystemOrders OrderList, ref List<string> ErrorMsgLst, CutlistTemplateDataV1100 TemplateData)
		{

			// Create an array of strings to pass to the line parser
			string[] Columns;

			// Parse CSV line into columns of strings.  Returns true if one or more lines parsed.  False if all empty fields
			bool Result = ParseLineToColumns(CSVLineString, out Columns);

			// If true, one or more non-blank fields exist.
			if (Result)
			{
				// Instantiate the LineItem
				LineItem LI = new LineItem();

				// Create new order.  This may be discarded later if order already exists
				Order NewOrder = new Order();

 				// Cycle through all the template items
				for (int u = 0; u < LineItemTemplateItems.Length; u++)
				{
					// Check if a LineItemTemplate is defined.  If so, continue
					if (LineItemTemplateItems[u].IsDefined)
					{
						// Get the zero based column data stored in the template
						int ActiveColumn = LineItemTemplateItems[u].GetColumn;

						// Make sure the stored column is within range of the CSV file columns passed
						if (ActiveColumn < Columns.Length)
						{
							// Create a new data structure for the parameter parsing
							ParamStruct Data = new ParamStruct();
							string ErrorMsg = "";

							// Attempt to parse the cooresponding string into the correct data type based on the template
							DataConversionResult ConvertResult = LineItemTemplateItems[u].GetParameterValue(Columns[ActiveColumn], ref Data, ref ErrorMsg);

							// If an error occured, add the line number to the error message and add to the error list
							if (ConvertResult == DataConversionResult.Error)
							{
								string Msg = string.Format("Error - Line: {0}, {1}", CSVLineNumber + 1, ErrorMsg);
								ErrorMsgLst.Add(Msg);
							}

							// The conversion succeded.  Store the data to the line item or order depending on the line item status
							else
							{
								if (LineItemTemplateItems[u].FieldLineItem)
								{

									// Conversion successful.  Update appropriate LineItem field
									UpdateLineItem(ref LI, ref Data, (CutlistLineItemColumnIDV1100)u);
								}

								else
								{
									// Update the order header 
									UpdateOrderData(ref NewOrder, ref Data, (CutlistLineItemColumnIDV1100)u);

								}

							}
						}

						// The stored column in the template is outside the number of columns in the CSV file
						else
						{
							string Msg = string.Format("Error - Line: {0}, {1} at column {2}: {3}", CSVLineNumber + 1, LineItemTemplateItems[u].FieldName, LineItemTemplateItems[u].GetColumnString, OutOfBoundsColumnErrorMsg);
							ErrorMsgLst.Add(Msg);
						}
					}
				}

				// All template columns have been checked.  Determine what to do with the Line Item.

				// Determine the appropriate result code
				if (ErrorMsgLst.Count > 0)
				{
					// indicate error
					return TemplateParsingResultCodes.Error;
				}

				// Success!
				else
				{
					// All template items itterated.  Determine if new order.   true if order exists
					if (OrderList.DoesOrderExists(NewOrder.OrderNumber))
					{
						string ErrorMsg;

						// Try to add line item to existing order.  If false, line item was duplicated
						if (!OrderList.AddLineItemToOrder(NewOrder.OrderNumber, ref LI, out ErrorMsg))
						{

							string Msg = string.Format("Error - Line: {0}, {1}", CSVLineNumber + 1, ErrorMsg);
							ErrorMsgLst.Add(Msg);

							// indicate error
							return TemplateParsingResultCodes.Error;
						}

						// Line item added to existing order
						else
						{
							return TemplateParsingResultCodes.Success;
						}
					}

					// Order does not exist in list.  Add line item to order and add order to order list
					else
					{

						
						// Add line item to new order
						NewOrder.AddLineItem(ref LI);

						// Add CSV units and accuracy tolerances to order
						NewOrder.CSVMeasurementUnits = TemplateData.CSVMeasureUnits;

						// Modify tolerances based on measuring units.
						// Tolerances from template are ALWAYS in millimeters
						if (NewOrder.CSVMeasurementUnits == MeasuringUnits.Millimeters)
						{
							NewOrder.HeightMinusAcc = TemplateData.HeightMinusTolerance.ToleranceVal;
							NewOrder.HeightPlusAcc = TemplateData.HeightPlusTolerance.ToleranceVal;
							NewOrder.WidthMinusAcc = TemplateData.WidthMinusTolerance.ToleranceVal;
							NewOrder.WidthPlusAcc = TemplateData.WidthPlusTolerance.ToleranceVal;
							NewOrder.ThicknessPlusAcc = TemplateData.ThicknessPlusTolerance.ToleranceVal;
							NewOrder.ThicknessMinusAcc = TemplateData.ThicknessMinusTolerance.ToleranceVal;

							// In degrees
							NewOrder.AnglePlusAcc = TemplateData.AnglePlusTolerance.ToleranceVal;
							NewOrder.AngleMinusAcc = TemplateData.AngleMinusTolerance.ToleranceVal;
						}

						// CSV file in inches.  Convert tolerance to inches
						else
						{
							NewOrder.HeightMinusAcc = TemplateData.HeightMinusTolerance.ToleranceVal / 25.4;
							NewOrder.HeightPlusAcc = TemplateData.HeightPlusTolerance.ToleranceVal / 25.4;
							NewOrder.WidthMinusAcc = TemplateData.WidthMinusTolerance.ToleranceVal / 25.4;
							NewOrder.WidthPlusAcc = TemplateData.WidthPlusTolerance.ToleranceVal / 25.4;
							NewOrder.ThicknessPlusAcc = TemplateData.ThicknessPlusTolerance.ToleranceVal / 25.4;
							NewOrder.ThicknessMinusAcc = TemplateData.ThicknessMinusTolerance.ToleranceVal / 25.4;

							// In degrees
							NewOrder.AnglePlusAcc = TemplateData.AnglePlusTolerance.ToleranceVal;
							NewOrder.AngleMinusAcc = TemplateData.AngleMinusTolerance.ToleranceVal;
						}


						// Add new order to list
						OrderList.AddOrder(ref NewOrder);

						// return success
						return TemplateParsingResultCodes.Success;

					}
				}
			}

			// Blank line
			else
			{
				// Indicate blank line
				return TemplateParsingResultCodes.BlankLine;
			}
		}

        // FUNCTIONS FOR IMPORTING AND EXPORTING TEMPLATE DATA BELOW....

        // Gets the Line Item template column field name text
        public bool GetLineItemFieldText(out string FieldName, int Index)
        {

            bool Result = false;

            if (Index < (int)CutlistLineItemColumnIDV1100.MaxItems)
            {
                Result = true;
                FieldName = ColumnNames[Index];
            }

            else
            {
                Result = false;
                FieldName = "";
            }

            return Result;
        }


        // Searches field names for a match with passed name.  If match found, return true and index to field
		public bool LocateLineItemFieldNameIndex(string TargetName, out CutlistLineItemColumnIDV1100 Index)
        {

            bool Result = false;
            Index = CutlistLineItemColumnIDV1100.MaxItems;

            // Check each column name for comparison
			for (CutlistLineItemColumnIDV1100 u = CutlistLineItemColumnIDV1100.LineItemNumber; u < CutlistLineItemColumnIDV1100.MaxItems; u++)
            {
                // If text matches, save index and break
                if (ColumnNames[(int)u].Equals(TargetName))
                {
                    Result = true;
                    Index = u;
                    break;
                }
            }

            return Result;
        }

 
	}
}
