﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ProCABQC11
{

	public class ItemListHeader
	{

		private int DisplayIndex;
		private int Width;
		private String Title;
		private string Tag;


		public ItemListHeader()
		{

		}


		public ItemListHeader(int Index, int Wid, string Str, string TagStr)
		{

			DisplayIndex = Index;
			Width = Wid;
			Title = Str;
			Tag = TagStr;
			
		}

		public void UpdateData(int Index, int Wid, string Str, string TagStr)
		{

		  	DisplayIndex = Index;
			Width = Wid;
			Title = Str;
			Tag = TagStr;
			
		}



		public int DisplayIdx
		{
			get { return DisplayIndex; }
			set { DisplayIndex = value; }
		}

		public int ColWidth
		{
			get { return Width; }
			set { Width = value; }
		}

		public string ColTitle
		{
			get { return Title; }
			set { Title = value; }
		}

		public string ColTag
		{
			get { return Tag; }
			set { Tag = value; }
		}



	}


	public class ItemListLayout
	{

		// Default column names
		private enum LineItemListColumns
		{
			LineIDCol,
			PartIDCol,
			HeightCol,
			WidthCol,
			ThichnessCol,
			QtyReqCol,
			QtyMeasCol,
			TypeCol,
			MaterialCol,
			FinishCol,
			HingingCol,
			MachiningCol,
			AssemblyCol,
			CommentCol,
			UserDef1Col,
			UserDef2Col,
			UserDef3Col,
			LocationCol,
			StyleCol


		}


		private int MAX_COLUMNS = 19;
		private ItemListHeader[] Headers;
		private StreamReader LayoutStream;
		private StreamWriter LayoutStreamWriter;
		private string LayoutFileName = "Item List Layout.txt";
		private int LineCount;

				 

		

		public ItemListLayout()
		{

			Headers = new ItemListHeader[MAX_COLUMNS];

			Headers[(int)LineItemListColumns.LineIDCol] = new ItemListHeader(0, 70, "Line No.", "LineItemHeader");
			Headers[(int)LineItemListColumns.PartIDCol] = new ItemListHeader(1, 110, "Part Ident", "PartIDHeader");
			Headers[(int)LineItemListColumns.WidthCol] = new ItemListHeader(2, 70, "Width", "widthHeader");
			Headers[(int)LineItemListColumns.HeightCol] = new ItemListHeader(3, 70, "Height", "heightHeader");
			Headers[(int)LineItemListColumns.ThichnessCol] = new ItemListHeader(4, 70, "Depth", "depthHeader");
			Headers[(int)LineItemListColumns.QtyReqCol] = new ItemListHeader(5, 110, "Qty. Required", "QtyReqHeader");
			Headers[(int)LineItemListColumns.QtyMeasCol] = new ItemListHeader(6, 110, "Qty. Measured", "QtyMeasuredHeader");
			Headers[(int)LineItemListColumns.TypeCol] = new ItemListHeader(7, 110, "Type", "TypeHeader");
			Headers[(int)LineItemListColumns.MaterialCol] = new ItemListHeader(8, 110, "Material", "MaterialHeader");
			Headers[(int)LineItemListColumns.FinishCol] = new ItemListHeader(9, 110, "Finish", "FinishHeader");
			Headers[(int)LineItemListColumns.HingingCol] = new ItemListHeader(10, 110, "Hinging", "HingingHeader");
			Headers[(int)LineItemListColumns.MachiningCol] = new ItemListHeader(11, 110, "Machining", "MachiningHeader");
			Headers[(int)LineItemListColumns.AssemblyCol] = new ItemListHeader(12, 110, "Assembly", "AssemblyHeader");
			Headers[(int)LineItemListColumns.CommentCol] = new ItemListHeader(13, 110, "Comments", "CommentsHeader");
			Headers[(int)LineItemListColumns.UserDef1Col] = new ItemListHeader(14, 110, "User Def 1", "User1Header");
			Headers[(int)LineItemListColumns.UserDef2Col] = new ItemListHeader(15, 110, "User Def 2", "User2Header");
			Headers[(int)LineItemListColumns.UserDef3Col] = new ItemListHeader(16, 110, "User Def 3", "User3Header");
			Headers[(int)LineItemListColumns.LocationCol] = new ItemListHeader(17, 110, "Location", "LocationHeader");
			Headers[(int)LineItemListColumns.StyleCol] = new ItemListHeader(18, 110, "Style", "StyleHeader");




		}

		public int GetColumnDisplayIndex(int Idx)
		{
			if (Idx >= 0 && Idx < Headers.Length)
				return Headers[Idx].DisplayIdx;

			else
				return 0;

		}

		public bool SetColumnDisplayIndex(int Idx, int Disp)
		{

			if (Idx >= 0 && Idx < Headers.Length)
			{
				Headers[Idx].DisplayIdx = Disp;

				return true;
			}

			else
				return false;


		}


		public int GetColumnWidth(int Idx)
		{
			if (Idx >= 0 && Idx < Headers.Length)
				return Headers[Idx].ColWidth;

			else
				return 0;

		}

		public bool SetColumnWidth(int Idx, int Width)
		{

			if (Idx >= 0 && Idx < Headers.Length)
			{
				Headers[Idx].ColWidth = Width;

				return true;
			}

			else
				return false;


		}


		public String GetColumnTitle(int Idx)
		{

			if (Idx >= 0 && Idx < Headers.Length)
				return Headers[Idx].ColTitle;

			else
				return "";

		}


		public bool SetColumnTitle(int Idx, String Title)
		{

			if (Idx >= 0 && Idx < Headers.Length)
			{
				Headers[Idx].ColTitle = Title;

				return true;
			}

			else
				return false;


		}


		public String GetColumnTag(int Idx)
		{

			if (Idx >= 0 && Idx < Headers.Length)
				return Headers[Idx].ColTag;

			else
				return "";

		}


		public bool SetColumnTag(int Idx, String Tag)
		{

			if (Idx >= 0 && Idx < Headers.Length)
			{
				Headers[Idx].ColTag = Tag;

				return true;
			}

			else
				return false;


		}


		public int NumberOfListColumns()
		{
			return Headers.Length;

		}

		// Tries to open the Item List Layout configuration file.  If failure, uses defaults
		public bool OpenItemListLayout()
		{

			// Create an array of strings to pass to the line parser
			string[] Fields;
			Char Delimit = ',';

			// See if the layout file exists.  If not, return false
			if (!File.Exists(LayoutFileName))
			{

				return false;

			}


			else
			{


				// File exists, open it.  
				try
				{

					LineCount = 0;

					// Assign the stream reader to the file
					LayoutStream = File.OpenText(LayoutFileName);

					// Start reading lines from layout file
					string LayoutLine = "";
					while ((LayoutLine = LayoutStream.ReadLine()) != null)

					{
						// If a comment, skip that line
						if (LayoutLine[0] == '#')
							continue;

						else
						{
							// Split each line into individual strings that are delimited by commas
							Fields = LayoutLine.Split(Delimit);

							// Be sure that there is the correct number of entries (Column Index, Size, Text, Tag (relates to column function))
							if (Fields.Length == 4)
							{
								Headers[LineCount].UpdateData(Convert.ToInt32(Fields[0]), Convert.ToInt32(Fields[1]), Fields[2], Fields[3]);

							}

							LineCount++;

							// Break out if more lines than columns
							if (LineCount >= Headers.Length)
								break;
						}
					

					}

					LayoutStream.Close();

					return true;

				}

				catch (Exception e)
				{

					MessageBox.Show("Error reading layout configuration: " + e.Message);

					return false;

				}

			}


		}


		// Tries to save the Item List Layout configuration file.  
		public bool SaveItemListLayout()
		{

			StreamWriter LayoutWriteStream;
			int Index, ColWidth;
			string Title, Tag;


			// Try to save file  
			try
			{

				// Try to open the stream for the configuration file
				using (LayoutWriteStream = new StreamWriter(LayoutFileName))
				{

					// Start writing the data

					for (int u = 0; u < Headers.Length; u++)
					{

						Index = Headers[u].DisplayIdx;
						ColWidth = Headers[u].ColWidth;
						Title = Headers[u].ColTitle;
						Tag = Headers[u].ColTag;

						LayoutWriteStream.WriteLine(string.Format("{0},{1},{2},{3}", Index, ColWidth, Title, Tag));
							
					}

				}


				LayoutWriteStream.Close();

				return true;




			}

			catch (Exception e)
			{

				MessageBox.Show("Error writing layout configuration: " + e.Message);

				return false;

			}

			
		}


	}
}
